﻿using DiLe.Mes.Model.Common.APP;
using DiLe.Mes.Service.APP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Application {
    public class APPClient : IApplicationClient {
        private readonly EquipmentOperationService _equipmentOperation;

        private readonly EquipmentStatusService _equipmentStatus;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="equipmentOperationService"></param>
        public APPClient(EquipmentOperationService equipmentOperationService, EquipmentStatusService equipmentStatusService) {
            _equipmentOperation = equipmentOperationService;
            _equipmentStatus = equipmentStatusService;
        }
        #region 设备运行
        /// <summary>
        /// 
        /// </summary>
        public void SaveEquipmentOperation(EquipmentOperationEntity operationEntity) {
            if (operationEntity.Id > 0) {
                _equipmentOperation.Update(operationEntity);
            } else {
                _equipmentOperation.Insert(operationEntity);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public EquipmentOperationEntity GetEquipmentOperation(Expression<Func<EquipmentOperationEntity, bool>> whereExpression) {
            var data = _equipmentOperation.GetEntityBy(whereExpression);
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<EquipmentOperationEntity>> GetEquipmentOperationListAsync(Expression<Func<EquipmentOperationEntity, bool>> whereExpression) {
            var data = await _equipmentOperation.GetListAsync(whereExpression);
            return data;
        }
        #endregion

        #region 设备状态
        /// <summary>
        /// 
        /// </summary>
        public async Task SaveEquipmentStatus(EquipmentStatusInfoEntity operationEntity) {
            if (operationEntity.Id > 0) {
                await _equipmentStatus.UpdateAsync(operationEntity);
            } else {
                await _equipmentStatus.InsertAsync(operationEntity);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public async Task<EquipmentStatusInfoEntity> GetEquipmentStatus(Expression<Func<EquipmentStatusInfoEntity, bool>> whereExpression) {
            var data = await _equipmentStatus.GetEntityByAsync(whereExpression);
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<EquipmentStatusInfoEntity>> GetEquipmentStatusListAsync(Expression<Func<EquipmentStatusInfoEntity, bool>> whereExpression) {
            var data = await _equipmentStatus.GetListAsync(whereExpression);
            return data;
        }
        #endregion
    }
}
