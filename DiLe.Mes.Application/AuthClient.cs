﻿using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Service.Common.Organization;
using MapleLeaf.Core.Cache.Redis;
using MapleLeaf.Core.Const;
using MapleLeaf.Core.Enum.Auth;
using MapleLeaf.Core.FriendlyException;
using MapleLeaf.Core.Handler;

namespace DiLe.Mes.Application {
    /// <summary>
    /// 
    /// </summary>
    public class AuthClient : IApplicationClient {
        /// <summary>
        /// 
        /// </summary>
        private readonly SysUserService _sysUserService;
        /// <summary>
        /// 
        /// </summary>
        private readonly SysRoleService _sysRoleService;
        /// <summary>
        /// 
        /// </summary>
        public AuthClient(SysUserService sysUserService, SysRoleService sysRoleService) {
            _sysUserService = sysUserService;
            _sysRoleService = sysRoleService;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="input">登录参数</param>
        /// <returns>Token信息</returns>
        public async Task<LoginOutPut> Login(LoginInput input) {
            var redisClent = new RedisClient(15);
            var rediskey = $"{input.Account}_{input.Device}";
            var redisToken = redisClent.Get(rediskey);
            if (!redisToken.IsNullOrEmpty()) {
                //throw ApiFriendlyException.OhBus(ResponseCodeConst.RepeatLogin);
                return new LoginOutPut { Account = input.Account, Token = redisToken };
            }
            UserInfo user;
            List<RoleEntity> roles;
            if (input.Device.Equals(AuthDeviceTypeEnum.CLOUD) && input.Account == SuperAccountConst.CloudAdmin) {
                if (input.Password != "admin") {
                    throw ApiFriendlyException.OhBus(ResponseCodeConst.AccountORPasswordError);
                }
                user = new UserInfo { Name = "云平台管理员", Account = input.Account };
                roles = new List<RoleEntity>() { new RoleEntity { Name = "云平台管理员" } };
            } else if (input.Device.Equals(AuthDeviceTypeEnum.LOACL) && input.Account == SuperAccountConst.BizAdmin) {
                if (input.Password != "bizadmin") {
                    throw ApiFriendlyException.OhBus(ResponseCodeConst.AccountORPasswordError);
                }
                user = new UserInfo { Name = "工厂管理员", Account = input.Account };
                roles = new List<RoleEntity>() { new RoleEntity { Name = "工厂管理员" } };
            } else {
                user = await _sysUserService.CheckUserInfoByAccount(input.Account!, input.Password!);
                if (user == null) {
                    throw ApiFriendlyException.OhBus(ResponseCodeConst.AccountORPasswordError);
                }
                roles = await _sysRoleService.GetRoleListByUserId(user.Id);
            }
            user.RoleCodeList = roles?.ConvertAll(p => p.Name) ?? new List<string>();
            user.Device = input.Device;
            string token = JwtHandler.CreateToken(user);
            redisClent.Set(rediskey, token, TimeSpan.FromHours(4));
            return new LoginOutPut { Account = user.Account, Token = token };
        }
        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public void LogOut(string account) {
            var redisClent = new RedisClient(15);
            var redisToken = redisClent.Get(account);
            if (!redisToken.IsNullOrEmpty()) {
                redisClent.Remove(account);
            }
        }
    }
}
