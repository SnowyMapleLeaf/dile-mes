﻿using DiLe.Mes.Model.Common.APP;
using DiLe.Mes.Model.Local.Abnormal.Entity;
using DiLe.Mes.Service.APP;
using DiLe.Mes.Service.Local.Abnormal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Application.Local {
    public class AbnormalClient : IApplicationClient {
        private readonly AlarmTempInfoService _alarmTempInfo;
        private readonly AlarmInfoService _alarmInfo;

        public AbnormalClient(AlarmTempInfoService alarmInfoService, AlarmInfoService alarmInfo) {
            _alarmTempInfo = alarmInfoService;
            _alarmInfo = alarmInfo;
        }

        #region 临时报警信息
        /// <summary>
        /// 
        /// </summary>
        public async Task<AlarmTempInfoEntity?> InsertAlarmTempInfo(AlarmTempInfoEntity alarmInfo) {
            AlarmTempInfoEntity? res = null;
            res = await _alarmTempInfo.InsertReturnEntityAsync(alarmInfo);
            if (res != null) {
                var info = res.Adapt<AlarmTempInfoEntity, AlarmInfoEntity>();
                info.StartAlarmTime = DateTime.Now;
                info.IsPushed = false;
                info.IsAlarmRelease = false;
                info.AlarmDate = DateTime.Now.ToString("yyyy-MM-dd");
                await _alarmInfo.InsertAsync(info);
            }
            return res;
        }
        /// <summary>
        /// 
        /// </summary>
        public async Task<AlarmTempInfoEntity> GetAlarmTempInfoAsync(Expression<Func<AlarmTempInfoEntity, bool>> whereExpression) {
            var data = await _alarmTempInfo.GetEntityByAsync(whereExpression);
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        public async Task<bool> DeleteAlarmTempInfo(Expression<Func<AlarmTempInfoEntity, bool>> whereExpression) {
            var data = await _alarmTempInfo.DeleteAsync(whereExpression);
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        public async Task<bool> DeleteAlarmTempInfo(string deviceNo, string alarmData) {
            var data = await _alarmTempInfo.DeleteAsync(p => p.DeviceNo == deviceNo && p.AlarmData == alarmData);
            if (data) {
                var info = await _alarmInfo.GetEntityByAsync(p => p.DeviceNo == deviceNo && p.AlarmData == alarmData && p.EndAlarmTime == null);
                if (info != null) {
                    info.EndAlarmTime = DateTime.Now;
                    info.IsAlarmRelease = true;
                    await _alarmInfo.UpdateAsync(info);
                }
            }
            return data;
        }
        #endregion

        #region 报警信息
        /// <summary>
        /// 
        /// </summary>
        public async Task<List<AlarmInfoEntity>> GetAlarmInfoListAsync(Expression<Func<AlarmInfoEntity, bool>> whereExpression) {
            var datalist = await _alarmInfo.GetListAsync(whereExpression);
            return datalist;
        }
        /// <summary>
        /// 
        /// </summary>
        public async Task<int> UpdateAlarmInfoAsync(List<AlarmInfoEntity> alarmInfos) {
            var res = await _alarmInfo.UpdateAsync(alarmInfos);
            return res;
        }
        #endregion
    }
}
