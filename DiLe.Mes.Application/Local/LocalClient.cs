﻿using DiLe.Mes.Model.Local.Entity;
using DiLe.Mes.Service.Local;

namespace DiLe.Mes.Application.Local {
    /// <summary>
    /// 
    /// </summary>
    public class LocalClient : IApplicationClient {
        public PointInfoService _pointInfo;
        public OtherPointInfoService _1311pointInfo;
        public AOIPointInfoService _aoipointInfo;
        public EMPointInfoService _empointInfo;
        public FMPointInfoService _fmpointInfo;
        public TrimPointInfoService _trimpointInfo;
        /// <summary>
        /// 
        /// </summary>
        public LocalClient(PointInfoService pointInfoService,
                           AOIPointInfoService aOIPointInfoService,
                           EMPointInfoService eMPointInfoService,
                           FMPointInfoService fMPointInfoService,
                           TrimPointInfoService trimPointInfoService,
                           OtherPointInfoService otherPointInfoService) {
            _pointInfo = pointInfoService;
            _aoipointInfo = aOIPointInfoService;
            _empointInfo = eMPointInfoService;
            _fmpointInfo = fMPointInfoService;
            _trimpointInfo = trimPointInfoService;
            _1311pointInfo = otherPointInfoService;
        }
        #region PLC点位数据传输-成型机
        /// <summary>
        /// 获取PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<List<PointInfoEntity>> GetPointInfoList(Expression<Func<PointInfoEntity, bool>> whereExp) {
            var list = await _pointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public PointInfoEntity? GetCurrentTimePointInfo(string sign, DateTime? dateTime = null) {
            var where = Expressionable.Create<PointInfoEntity>();
            where.And(x => x.DeviceNo == sign);
            if (dateTime == null) {
                where.And(p => p.CreateTime >= DateTime.Now.AddHours(-1).Date);
            } else {
                where.And(p => p.CreateTime <= dateTime);
            }
            var list = _pointInfo.GetList(where.ToExpression());
            var data = list.OrderByDescending(p => p.CreateTime).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> InsertPointInfo(List<PointInfoEntity> entitys) {
            if (entitys.None()) {
                return false;
            }
            int res = await _pointInfo.InsertAsync(entitys);

            return res > 0;
        }
        /// <summary>
        /// PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> SavePointInfo(PointInfoEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _pointInfo.InsertAsync(entity);
            } else {
                res = await _pointInfo.UpdateAsync(entity);
            }
            return res;
        }
        #endregion
        #region PLC点位数据传输-1311
        /// <summary>
        /// 获取PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<List<OtherPointInfoEntity>> Get1311PointInfoList(Expression<Func<OtherPointInfoEntity, bool>> whereExp) {
            var list = await _1311pointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> Insert1311PointInfo(List<OtherPointInfoEntity> entitys) {
            if (entitys.None()) {
                return false;
            }
            int res = await _1311pointInfo.InsertAsync(entitys);

            return res > 0;
        }
        /// <summary>
        /// PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> Save1311PointInfo(OtherPointInfoEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _1311pointInfo.InsertAsync(entity);
            } else {
                res = await _1311pointInfo.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 获取PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public OtherPointInfoEntity? GetCurrent1311PointInfo(string sign, DateTime? dateTime = null) {
            var where = Expressionable.Create<OtherPointInfoEntity>();
            where.And(x => x.DeviceNo == sign);
            if (dateTime == null) {
                where.And(p => p.CreateTime >= DateTime.Now.AddHours(-1).Date);
            } else {
                where.And(p => p.CreateTime <= dateTime);
            }
            var list = _1311pointInfo.GetList(where.ToExpression());
            var data = list.OrderByDescending(p => p.CreateTime).FirstOrDefault();
            return data;
        }
        #endregion
        #region PLC点位数据传输-AOI
        /// <summary>
        /// 获取PLC点位数据传输-AOI
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<List<AOIPointInfoEntity>> GetAOIPointInfoList(Expression<Func<AOIPointInfoEntity, bool>> whereExp) {
            var list = await _aoipointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public AOIPointInfoEntity? GetCurrentTimeAOIPointInfo(string sign, DateTime? dateTime = null) {
            var where = Expressionable.Create<AOIPointInfoEntity>();
            where.And(x => x.DeviceNo == sign);
            if (dateTime == null) {
                where.And(p => p.CreateTime >= DateTime.Now.AddHours(-1).Date);
            } else {
                where.And(p => p.CreateTime <= dateTime);
            }
            var list = _aoipointInfo.GetList(where.ToExpression());
            var data = list.OrderByDescending(p => p.CreateTime).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// PLC点位数据传输
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> InsertAOIPointInfo(List<AOIPointInfoEntity> entitys) {
            if (entitys.None()) {
                return false;
            }
            int res = await _aoipointInfo.InsertAsync(entitys);

            return res > 0;
        }
        /// <summary>
        /// PLC点位数据传输
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> SaveAOIPointInfo(AOIPointInfoEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _aoipointInfo.InsertAsync(entity);
            } else {
                res = await _aoipointInfo.UpdateAsync(entity);
            }
            return res;
        }
        #endregion

        #region PLC点位数据传输-电能表
        /// <summary>
        /// 获取PLC点位数据传输-电能表
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<List<EMPointInfoEntity>> GetEMPointInfoList(Expression<Func<EMPointInfoEntity, bool>> whereExp) {
            var list = await _empointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// PLC点位数据传输-电能表
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> InsertEMPointInfo(List<EMPointInfoEntity> entitys) {
            if (entitys.None()) {
                return false;
            }
            int res = await _empointInfo.InsertAsync(entitys);

            return res > 0;
        }
        /// <summary>
        /// PLC点位数据传输-电能表
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> SaveEMPointInfo(EMPointInfoEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _empointInfo.InsertAsync(entity);
            } else {
                res = await _empointInfo.UpdateAsync(entity);
            }
            return res;
        }
        #endregion

        #region PLC点位数据传输-流量计
        /// <summary>
        /// 获取PLC点位数据传输-流量计
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<List<FMPointInfoEntity>> GetFMPointInfoList(Expression<Func<FMPointInfoEntity, bool>> whereExp) {
            var list = await _fmpointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// PLC点位数据传输-流量计
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> InsertFMPointInfo(List<FMPointInfoEntity> entitys) {
            if (entitys.None()) {
                return false;
            }
            int res = await _fmpointInfo.InsertAsync(entitys);

            return res > 0;
        }
        /// <summary>
        /// PLC点位数据传输-流量计
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> SavePMPointInfo(FMPointInfoEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _fmpointInfo.InsertAsync(entity);
            } else {
                res = await _fmpointInfo.UpdateAsync(entity);
            }
            return res;
        }
        #endregion

        #region PLC点位数据传输-切边机
        /// <summary>
        /// 获取PLC点位数据传输-切边机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<List<TrimPointInfoEntity>> GetTrimPointInfoList(Expression<Func<TrimPointInfoEntity, bool>> whereExp) {
            var list = await _trimpointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TrimPointInfoEntity? GetCurrentTrimPointInfoList(string sign, DateTime? dateTime = null) {
            var where = Expressionable.Create<TrimPointInfoEntity>();
            where.And(x => x.DeviceNo == sign);
            if (dateTime == null) {
                where.And(p => p.CreateTime >= DateTime.Now.AddHours(-1).Date);
            } else {
                where.And(p => p.CreateTime <= dateTime);
            }
            var list = _trimpointInfo.GetList(where.ToExpression());
            var data = list.OrderByDescending(p => p.CreateTime).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// PLC点位数据传输-切边机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> InsertTrimPointInfo(List<TrimPointInfoEntity> entitys) {
            if (entitys.None()) {
                return false;
            }
            int res = await _trimpointInfo.InsertAsync(entitys);

            return res > 0;
        }
        /// <summary>
        /// PLC点位数据传输-切边机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> SaveTrimPointInfo(TrimPointInfoEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _trimpointInfo.InsertAsync(entity);
            } else {
                res = await _trimpointInfo.UpdateAsync(entity);
            }
            return res;
        }
        #endregion

    }
}
