using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Model.Common.Mould.Relation.Maintenance;
using DiLe.Mes.Service.Common.Mould.Maintenance;

namespace DiLe.Mes.Application.Common.Mould
{
    public class MouldMaintenanceClient : IApplicationClient
    {
        private readonly MouldMaintenancePlanService _mouldMaintenancePlan;
        private readonly MouldMaintenanceProjectService _mouldMaintenanceProject;
        private readonly MouldMaintenanceRecordService _mouldMaintenanceRecord;
        private readonly MouldMaintenancePlan2ProjectService _mouldPlan2Project;
        private readonly MouldMaintenanceRecord2SparePartService _mouldRecord2SparePart;
        private readonly MouldMaintenanceRecord2ProjectService _mouldRecord2Project;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldMaintenanceClient(MouldMaintenancePlanService mouldMaintenancePlan,
                                      MouldMaintenanceProjectService mouldMaintenanceProject,
                                      MouldMaintenanceRecordService mouldMaintenanceRecord,
                                      MouldMaintenancePlan2ProjectService mouldMaintenancePlan2Project,
                                      MouldMaintenanceRecord2SparePartService mouldMaintenanceRecord2SparePart,
                                      MouldMaintenanceRecord2ProjectService mouldMaintenanceRecord2Project)
        {
            _mouldMaintenancePlan = mouldMaintenancePlan;
            _mouldMaintenanceProject = mouldMaintenanceProject;
            _mouldMaintenanceRecord = mouldMaintenanceRecord;
            _mouldPlan2Project = mouldMaintenancePlan2Project;
            _mouldRecord2SparePart = mouldMaintenanceRecord2SparePart;
            _mouldRecord2Project = mouldMaintenanceRecord2Project;
        }
        #region 保养计划
        /// <summary>
        /// 获取保养计划列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldMaintenancePlanEntity>> GetMouldMaintenancePlanPageListAsync(Expression<Func<MouldMaintenancePlanEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldMaintenancePlanEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldMaintenancePlan.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取保养计划列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldMaintenancePlanEntity>> GetMouldMaintenancePlanListAsync(Expression<Func<MouldMaintenancePlanEntity, bool>>? whereExp = null)
        {
            List<MouldMaintenancePlanEntity> res;
            if (whereExp == null)
            {
                res = await _mouldMaintenancePlan.GetAllListAsync();
            }
            else
            {
                res = await _mouldMaintenancePlan.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取保养计划信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldMaintenancePlanEntity> GetMouldMaintenancePlanInfoAsync(long id)
        {
            var res = await _mouldMaintenancePlan.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存保养计划信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldMaintenancePlanAsync(MouldMaintenancePlanEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("BYJH");
                var res = await _mouldMaintenancePlan.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldMaintenancePlan.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除保养计划信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldMaintenancePlanAsync(List<long> ids)
        {
            var res = await _mouldMaintenancePlan.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 保养项目
        /// <summary>
        /// 获取保养项目列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldMaintenanceProjectEntity>> GetMouldMaintenanceProjectPageListAsync(Expression<Func<MouldMaintenanceProjectEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldMaintenanceProjectEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldMaintenanceProject.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取保养项目列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldMaintenanceProjectEntity>> GetMouldMaintenanceProjectListAsync(Expression<Func<MouldMaintenanceProjectEntity, bool>> whereExp)
        {
            var res = await _mouldMaintenanceProject.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取保养项目信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldMaintenanceProjectEntity> GetMouldMaintenanceProjectInfoAsync(long id)
        {
            var res = await _mouldMaintenanceProject.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存保养项目信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldMaintenanceProjectAsync(MouldMaintenanceProjectEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("BYXM");
                var res = await _mouldMaintenanceProject.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldMaintenanceProject.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除保养项目信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldMaintenanceProjectAsync(List<long> ids)
        {
            var res = await _mouldMaintenanceProject.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 保养记录
        /// <summary>
        /// 获取保养记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldMaintenanceRecordEntity>> GetMouldMaintenanceRecordPageListAsync(Expression<Func<MouldMaintenanceRecordEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldMaintenanceRecordEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldMaintenanceRecord.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取保养记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldMaintenanceRecordEntity>> GetMouldMaintenanceRecordListAsync(Expression<Func<MouldMaintenanceRecordEntity, bool>>? whereExp = null)
        {
            List<MouldMaintenanceRecordEntity> res;
            if (whereExp == null)
            {
                res = await _mouldMaintenanceRecord.GetAllListAsync();
            }
            else
            {
                res = await _mouldMaintenanceRecord.GetListAsync(whereExp);
            }

            return res;
        }
        /// <summary>
        /// 获取保养记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldMaintenanceRecordEntity> GetMouldMaintenanceRecordInfoAsync(long id)
        {
            var res = await _mouldMaintenanceRecord.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存保养记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldMaintenanceRecordAsync(MouldMaintenanceRecordEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("BYJL");
                var res = await _mouldMaintenanceRecord.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldMaintenanceRecord.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除保养记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldMaintenanceRecordAsync(List<long> ids)
        {
            var res = await _mouldMaintenanceRecord.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion

        #region 保养计划关联的保养项目
        /// <summary>
        /// 保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldMaintenancePlan2ProjectEntity>> GetMouldMaintenancePlan2ProjectListByPlanIdAsync(long planId)
        {
            var list = await _mouldPlan2Project.GetListAsync(p => p.MaintenancePlanId == planId);
            return list;
        }
        /// <summary>
        /// 添加保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMouldMaintenancePlan2ProjectAsync(long planId, List<MouldMaintenancePlan2ProjectEntity> datas)
        {
            datas.ForEach(x => x.MaintenancePlanId = planId);
            var res = await _mouldPlan2Project.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMouldMaintenancePlan2ProjectAsync(List<MouldMaintenancePlan2ProjectEntity> datas)
        {
            var res = await _mouldPlan2Project.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMouldMaintenancePlan2ProjectAsync(List<long> ids)
        {
            var res = await _mouldPlan2Project.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion

        #region 保养记录关联保养项目
        ///<summary>
        /// 保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldMaintenanceRecord2ProjectEntity>> GetMouldMaintenanceRecord2ProjectListByPlanIdAsync(long recordId)
        {
            var list = await _mouldRecord2Project.GetListAsync(p => p.MaintenanceRecordId == recordId);
            return list;
        }
        /// <summary>
        /// 添加保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMouldMaintenanceRecord2ProjectAsync(long recordId, List<MouldMaintenanceRecord2ProjectEntity> datas)
        {
            datas.ForEach(x => x.MaintenanceRecordId = recordId);
            var res = await _mouldRecord2Project.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMouldMaintenanceRecord2ProjectAsync(List<MouldMaintenanceRecord2ProjectEntity> datas)
        {
            var res = await _mouldRecord2Project.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMouldMaintenanceRecord2ProjectAsync(List<long> ids)
        {
            var res = await _mouldRecord2Project.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 保养记录关联备件
        /// <summary>
        /// 保养记录关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldMaintenanceRecord2SparePartEntity>> GetMouldMaintenanceRecord2SparePartListByPlanIdAsync(long recordId)
        {
            var list = await _mouldRecord2SparePart.GetListAsync(p => p.MaintenanceRecordId == recordId);
            return list;
        }
        /// <summary>
        /// 添加保养记录关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMouldMaintenanceRecord2SparePartAsync(long recordId, List<MouldMaintenanceRecord2SparePartEntity> datas)
        {
            datas.ForEach(x => x.MaintenanceRecordId = recordId);
            var res = await _mouldRecord2SparePart.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新保养记录关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMouldMaintenanceRecord2SparePartAsync(List<MouldMaintenanceRecord2SparePartEntity> datas)
        {
            var res = await _mouldRecord2SparePart.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除保养记录关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMouldMaintenanceRecord2SparePartAsync(List<long> ids)
        {
            var res = await _mouldRecord2SparePart.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion

    }
}
