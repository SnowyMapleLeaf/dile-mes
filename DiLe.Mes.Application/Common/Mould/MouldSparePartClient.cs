using DiLe.Mes.Model.Common.Mould.Entity.SparePart;
using DiLe.Mes.Model.Common.Mould.Relation.SparePart;
using DiLe.Mes.Service.Common.Mould.SparePart;

namespace DiLe.Mes.Application.Common.Mould
{
    public class MouldSparePartClient : IApplicationClient
    {
        private readonly MouldSparePartLedgerService _mouldSparePartLedger;
        private readonly MouldSparePartStockInService _mouldSparePartStockIn;
        private readonly MouldSparePartStockOutService _mouldSparePartStockOut;
        private readonly MouldSparePartTypeService _mouldSparePartType;
        private readonly MouldSparePart2MouldInfoService _mouldSparePart2MouldInfo;
        private readonly MouldStockOut2SparePartService _mouldStockOut2SparePart;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldSparePartClient(MouldSparePartLedgerService mouldSparePartLedger,
                                    MouldSparePartStockInService mouldSparePartStockIn,
                                    MouldSparePartStockOutService mouldSparePartStockOut,
                                    MouldSparePartTypeService mouldSparePartType,
                                    MouldSparePart2MouldInfoService mouldSparePart2MouldInfo,
                                    MouldStockOut2SparePartService mouldStockOut2SparePart)
        {
            _mouldSparePartLedger = mouldSparePartLedger;
            _mouldSparePartStockIn = mouldSparePartStockIn;
            _mouldSparePartStockOut = mouldSparePartStockOut;
            _mouldSparePartType = mouldSparePartType;
            _mouldSparePart2MouldInfo = mouldSparePart2MouldInfo;
            _mouldStockOut2SparePart = mouldStockOut2SparePart;
        }
        #region 备件台账
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldSparePartLedgerEntity>> GetMouldSparePartLedgerPageListAsync(Expression<Func<MouldSparePartLedgerEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldSparePartLedgerEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldSparePartLedger.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldSparePartLedgerEntity>> GetMouldSparePartLedgerListAsync(Expression<Func<MouldSparePartLedgerEntity, bool>> whereExp)
        {
            var res = await _mouldSparePartLedger.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取备件台账信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldSparePartLedgerEntity> GetMouldSparePartLedgerInfoAsync(long id)
        {
            var res = await _mouldSparePartLedger.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存备件台账信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldSparePartLedgerAsync(MouldSparePartLedgerEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("BJTZ");
                var res = await _mouldSparePartLedger.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldSparePartLedger.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除备件台账信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldSparePartLedgerAsync(List<long> ids)
        {
            var res = await _mouldSparePartLedger.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 备件入库
        /// <summary>
        /// 获取备件入库列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldSparePartStockInEntity>> GetMouldSparePartStockInPageListAsync(Expression<Func<MouldSparePartStockInEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldSparePartStockInEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldSparePartStockIn.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取备件入库列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldSparePartStockInEntity>> GetMouldSparePartStockInListAsync(Expression<Func<MouldSparePartStockInEntity, bool>> whereExp)
        {
            var res = await _mouldSparePartStockIn.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取备件入库信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldSparePartStockInEntity> GetMouldSparePartStockInInfoAsync(long id)
        {
            var res = await _mouldSparePartStockIn.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存备件入库信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldSparePartStockInAsync(MouldSparePartStockInEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("BJRK");
                var res = await _mouldSparePartStockIn.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldSparePartStockIn.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除备件入库信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldSparePartStockInAsync(List<long> ids)
        {
            var res = await _mouldSparePartStockIn.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 备件出库
        /// <summary>
        /// 获取备件出库列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldSparePartStockOutEntity>> GetMouldSparePartStockOutPageListAsync(Expression<Func<MouldSparePartStockOutEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldSparePartStockOutEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldSparePartStockOut.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取备件出库列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldSparePartStockOutEntity>> GetMouldSparePartStockOutListAsync(Expression<Func<MouldSparePartStockOutEntity, bool>> whereExp)
        {
            var res = await _mouldSparePartStockOut.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取备件出库信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldSparePartStockOutEntity> GetMouldSparePartStockOutInfoAsync(long id)
        {
            var res = await _mouldSparePartStockOut.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存备件出库信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldSparePartStockOutEntity> SaveMouldSparePartStockOutAsync(MouldSparePartStockOutEntity entity)
        {
            MouldSparePartStockOutEntity data;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("BJCK");
                data = await _mouldSparePartStockOut.InsertReturnEntityAsync(entity);
            }
            else
            {
                await _mouldSparePartStockOut.UpdateAsync(entity);
                data = await GetMouldSparePartStockOutInfoAsync(entity.Id);
            }
            return data;
        }
        /// <summary>
        /// 删除备件出库信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldSparePartStockOutAsync(List<long> ids)
        {
            var res = await _mouldSparePartStockOut.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 备件类型
        /// <summary>
        /// 获取备件类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldSparePartTypeEntity>> GetMouldSparePartTypePageListAsync(Expression<Func<MouldSparePartTypeEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldSparePartTypeEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldSparePartType.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取备件类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldSparePartTypeEntity>> GetMouldSparePartTypeListAsync(Expression<Func<MouldSparePartTypeEntity, bool>> whereExp)
        {
            var res = await _mouldSparePartType.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取备件类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldSparePartTypeEntity> GetMouldSparePartTypeInfoAsync(long id)
        {
            var res = await _mouldSparePartType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存备件类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldSparePartTypeAsync(MouldSparePartTypeEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("BJLX");
                var res = await _mouldSparePartType.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldSparePartType.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除备件类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldSparePartTypeAsync(List<long> ids)
        {
            var res = await _mouldSparePartType.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新备件类型状态
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateMouldSparePartTypeStatusAsync(List<long> ids, bool status)
        {
            var res = await _mouldSparePartType.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion

        #region 备件关联模具
        /// <summary>
        /// 备件关联模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldSparePart2MouldInfoEntity>> GetMouldSparePart2MouldInfoListByLedgerIdAsync(long ledgerId)
        {
            var list = await _mouldSparePart2MouldInfo.GetListAsync(p => p.SparePartId == ledgerId);
            return list;
        }
        /// <summary>
        /// 添加备件关联模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMouldSparePart2MouldInfoAsync(long ledgerId, List<MouldSparePart2MouldInfoEntity> datas)
        {
            datas.ForEach(x => x.SparePartId = ledgerId);
            var res = await _mouldSparePart2MouldInfo.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新备件关联模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMouldSparePart2MouldInfoAsync(List<MouldSparePart2MouldInfoEntity> datas)
        {
            var res = await _mouldSparePart2MouldInfo.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除备件关联模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMouldSparePart2MouldInfoAsync(List<long> ids)
        {
            var res = await _mouldSparePart2MouldInfo.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 出库关联备件
        /// <summary>
        /// 获取出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldStockOut2SparePartEntity>> GetMouldStockOut2SparePartListByStockOutIdAsync(long stockOutId)
        {
            var list = await _mouldStockOut2SparePart.GetListAsync(p => p.StockOutId == stockOutId);
            return list;
        }
        /// <summary>
        /// 获取出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldStockOut2SparePartEntity>> GetMouldStockOut2SparePartListAsync(Expression<Func<MouldStockOut2SparePartEntity, bool>> whereExp)
        {
            var list = await _mouldStockOut2SparePart.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 添加出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMouldStockOut2SparePartAsync(long stockOutId, List<MouldStockOut2SparePartEntity> datas)
        {
            datas.ForEach(x => x.StockOutId = stockOutId);
            var res = await _mouldStockOut2SparePart.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMouldStockOut2SparePartAsync(List<MouldStockOut2SparePartEntity> datas)
        {
            var res = await _mouldStockOut2SparePart.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMouldStockOut2SparePartAsync(List<long> ids)
        {
            var res = await _mouldStockOut2SparePart.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
