using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Service.Common.Mould.Info;

namespace DiLe.Mes.Application.Common.Mould
{
    public class MouldInfoClient : IApplicationClient
    {
        private readonly MouldInfoService _mouldInfo;
        private readonly MouldStatusService _mouldStatus;
        private readonly MouldTypeService _mouldType;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldInfoClient(MouldInfoService mouldInfo, MouldStatusService mouldStatus, MouldTypeService mouldType)
        {
            _mouldInfo = mouldInfo;
            _mouldStatus = mouldStatus;
            _mouldType = mouldType;
        }
        #region 模具档案
        /// <summary>
        /// 获取模具档案列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldInfoEntity>> GetMouldInfoPageListAsync(Expression<Func<MouldInfoEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldInfoEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldInfo.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取模具档案列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldInfoEntity>> GetMouldInfoListAsync(Expression<Func<MouldInfoEntity, bool>>? whereExp = null)
        {
            List<MouldInfoEntity> res;
            if (whereExp == null)
            {
                res = await _mouldInfo.GetAllListAsync();
            }
            else
            {
                res = await _mouldInfo.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取模具档案信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldInfoEntity> GetMouldInfoAsync(long id)
        {
            var res = await _mouldInfo.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 获取模具档案信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldInfoEntity> GetMouldInfoAsync(Expression<Func<MouldInfoEntity, bool>> whereExp)
        {
            var res = await _mouldInfo.GetEntityByAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 保存模具档案信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldInfoAsync(MouldInfoEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("MJDA");
                var res = await _mouldInfo.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldInfo.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 保存模具档案信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> InsertMouldInfoAsync(List<MouldInfoEntity> infoEntities)
        {
            foreach (var item in infoEntities)
            {
                if (!item.Code.IsNullOrEmpty())
                {
                    continue;
                }
                item.Code = CodeHandler.GetBusinessCode("MJDA");
            }
            var res = await _mouldInfo.InsertAsync(infoEntities);
            return res;
        }
        /// <summary>
        /// 保存模具档案信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> UpdateMouldInfoAsync(List<MouldInfoEntity> infoEntities)
        {
            var res = await _mouldInfo.UpdateAsync(infoEntities);
            return res;
        }
        /// <summary>
        /// 删除模具档案信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldInfoAsync(List<long> ids)
        {
            var res = await _mouldInfo.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 模具状态
        /// <summary>
        /// 获取模具状态列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldStatusEntity>> GetMouldStatusPageListAsync(Expression<Func<MouldStatusEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldStatusEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldStatus.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取模具状态列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldStatusEntity>> GetMouldStatusListAsync(Expression<Func<MouldStatusEntity, bool>>? whereExp = null)
        {
            List<MouldStatusEntity> res;
            if (whereExp == null)
            {
                res = await _mouldStatus.GetAllListAsync();
            }
            else
            {
                res = await _mouldStatus.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取模具状态信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldStatusEntity> GetMouldStatusInfoAsync(long id)
        {
            var res = await _mouldStatus.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存模具状态信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldStatusAsync(MouldStatusEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("MJZT");
                var res = await _mouldStatus.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldStatus.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除模具状态信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldStatusAsync(List<long> ids)
        {
            var res = await _mouldStatus.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新模具状态
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateMouldStatusStatusAsync(List<long> ids, bool status)
        {
            var res = await _mouldStatus.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
        #region 模具类型
        /// <summary>
        /// 获取模具类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldTypeEntity>> GetMouldTypePageListAsync(Expression<Func<MouldTypeEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldTypeEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldType.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取模具类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldTypeEntity>> GetMouldTypeListAsync(Expression<Func<MouldTypeEntity, bool>>? whereExp = null)
        {
            List<MouldTypeEntity> res;
            if (whereExp == null)
            {
                res = await _mouldType.GetAllListAsync();
            }
            else
            {
                res = await _mouldType.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取模具类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldTypeEntity> GetMouldTypeInfoAsync(long id)
        {
            var res = await _mouldType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 获取模具类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldTypeEntity> GetMouldTypeInfoAsync(Expression<Func<MouldTypeEntity, bool>> whereExp)
        {
            var res = await _mouldType.GetEntityByAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 保存模具类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldTypeAsync(MouldTypeEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("MJLX");
                var res = await _mouldType.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldType.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除模具类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldTypeAsync(List<long> ids)
        {
            var res = await _mouldType.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新模具类型状态
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateMouldTypeStatusAsync(List<long> ids, bool status)
        {
            var res = await _mouldType.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
    }
}
