﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Model.Common.Mould.Relation.Maintenance;

namespace DiLe.Mes.Application.Common.Mould.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class MouldMaintenanceRecordDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldMaintenanceRecordEntity Model { get; set; }
        /// <summary>
        /// 计划
        /// </summary>
        public MouldMaintenancePlanEntity PlanModel { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public MouldInfoEntity MouldModel { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldMaintenanceRecordDetailDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldMaintenanceRecordEntity Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<MouldMaintenanceRecord2ProjectEntity> ProjectRelations { get; set; } = new List<MouldMaintenanceRecord2ProjectEntity>();
        /// <summary>
        /// 
        /// </summary>
        public List<MouldMaintenanceProjectEntity> ProjectDataList { get; set; } = new List<MouldMaintenanceProjectEntity>();

        /// <summary>
        /// 
        /// </summary>
        public List<MouldMaintenanceRecord2SparePartEntity> SparePartRelations { get; set; } = new List<MouldMaintenanceRecord2SparePartEntity>();
        /// <summary>
        ///
        /// </summary>
        public List<MouldSparePartLedgerModel> SparePartDataList { get; set; } = new List<MouldSparePartLedgerModel>();

        /// <summary>
        /// 计划
        /// </summary>
        public MouldMaintenancePlanEntity PlanModel { get; set; }
        /// <summary>
        /// 模具
        /// </summary>
        public MouldInfoEntity MouldModel { get; set; }
    }
}
