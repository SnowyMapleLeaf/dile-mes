﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Mould.Entity.Repair;

namespace DiLe.Mes.Application.Common.Mould.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class MouldRepairRecordDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldRepairRecordEntity Model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MouldInfoEntity EquipmentModel { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="R"></typeparam>
    /// <typeparam name="RT"></typeparam>
    public class MouldRepairRecordDetailDto<T, R, RT> : CommonDto<T, R, RT> {

        /// <summary>
        /// 
        /// </summary>
        public MouldInfoEntity MouldModel { get; set; }
    }
}
