﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Mould.Entity.SparePart;
using DiLe.Mes.Model.Common.Mould.Relation.SparePart;

namespace DiLe.Mes.Application.Common.Mould.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class MouldSparePartLedgerDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldSparePartLedgerModel Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<MouldSparePart2MouldInfoEntity> Relations { get; set; } = new List<MouldSparePart2MouldInfoEntity>();
        /// <summary>
        /// 
        /// </summary>
        public List<MouldInfoEntity> DataList { get; set; } = new List<MouldInfoEntity>();
    }

    /// <summary>
    /// 
    /// </summary>
    public class MouldSparePartLedgerModel : MouldSparePartLedgerEntity {
        /// <summary>
        /// 总库存
        /// </summary>
        public int InventoryTotalLimit { get; set; }
    }

}
