﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Application.Common.Mould.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class MouldMaintenancePlanDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldMaintenancePlanEntity Model { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public MouldInfoEntity MouldModel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ExecuteRuleEntity ExecuteRuleModel { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="R"></typeparam>
    /// <typeparam name="RT"></typeparam>
    public class MouldMaintenancePlanDto<T, R, RT> : CommonDto<T, R, RT> {

        /// <summary>
        /// 
        /// </summary>
        public MouldInfoEntity MouldInfoModel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ExecuteRuleEntity ExecuteRuleModel { get; set; }
    }
}
