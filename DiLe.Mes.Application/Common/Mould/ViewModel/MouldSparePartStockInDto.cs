﻿using DiLe.Mes.Model.Common.Mould.Entity.SparePart;

namespace DiLe.Mes.Application.Common.Mould.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class MouldSparePartStockInDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldSparePartStockInEntity Model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MouldSparePartLedgerEntity MouldSparePartLedgerModel { get; set; }
    }

}
