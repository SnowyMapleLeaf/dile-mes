﻿using DiLe.Mes.Model.Common.Mould.Entity.SparePart;
using DiLe.Mes.Model.Common.Mould.Relation.SparePart;

namespace DiLe.Mes.Application.Common.Mould.ViewModel {

    /// <summary>
    /// 
    /// </summary>
    public class MouldSparePartStockOutDetailDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldSparePartStockOutEntity Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<MouldStockOut2SparePartEntity> Relations { get; set; } = new List<MouldStockOut2SparePartEntity>();
        /// <summary>
        /// 
        /// </summary>
        public List<MouldSparePartLedgerModel> DataList { get; set; } = new List<MouldSparePartLedgerModel>();
    }
}
