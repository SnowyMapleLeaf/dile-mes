using DiLe.Mes.Model.Common.Mould.Entity.Repair;
using DiLe.Mes.Model.Common.Mould.Relation.Repair;
using DiLe.Mes.Service.Common.Mould.Repair;
using DiLe.Mes.Service.Dto;

namespace DiLe.Mes.Application.Common.Mould
{
    public class MouldRepairClient : IApplicationClient
    {
        private readonly MouldRepairRecordService _mouldRepairRecord;
        private readonly MouldRepairTypeService _mouldRepairType;
        private readonly MouldRepairRecord2SparePartService _repairRecord2SparePart;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldRepairClient(MouldRepairRecordService mouldRepairRecord, MouldRepairTypeService mouldRepairType, MouldRepairRecord2SparePartService repairRecord2SparePart)
        {
            _mouldRepairRecord = mouldRepairRecord;
            _mouldRepairType = mouldRepairType;
            _repairRecord2SparePart = repairRecord2SparePart;
        }
        #region 维修记录
        /// <summary>
        /// 获取维修记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldRepairRecordEntity>> GetMouldRepairRecordPageListAsync(Expression<Func<MouldRepairRecordEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldRepairRecordEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldRepairRecord.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取维修记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldRepairRecordEntity>> GetMouldRepairRecordListAsync(Expression<Func<MouldRepairRecordEntity, bool>>? whereExp = null)
        {
            List<MouldRepairRecordEntity> res;
            if (whereExp == null)
            {
                res = await _mouldRepairRecord.GetAllListAsync();
            }
            else
            {
                res = await _mouldRepairRecord.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取维修记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldRepairRecordEntity> GetMouldRepairRecordInfoAsync(long id)
        {
            var res = await _mouldRepairRecord.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<RepairRecordModel>> GetMouldRepairRecordModelListAsync()
        {
            var res = await _mouldRepairRecord.GetMouldRepairRecordModelListAsync();
            return res;
        }
        /// <summary>
        /// 保存维修记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldRepairRecordAsync(MouldRepairRecordEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("WXJL");
                var res = await _mouldRepairRecord.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldRepairRecord.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除维修记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldRepairRecordAsync(List<long> ids)
        {
            var res = await _mouldRepairRecord.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 故障类型
        /// <summary>
        /// 获取故障类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldRepairTypeEntity>> GetMouldRepairTypePageListAsync(Expression<Func<MouldRepairTypeEntity, bool>> whereExp, PaginationModel pagination)
        {
            var page = pagination.Adapt<PaginationModel<MouldRepairTypeEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldRepairType.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取故障类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldRepairTypeEntity>> GetMouldRepairTypeListAsync(Expression<Func<MouldRepairTypeEntity, bool>> whereExp)
        {
            var res = await _mouldRepairType.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取故障类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldRepairTypeEntity> GetMouldRepairTypeInfoAsync(long id)
        {
            var res = await _mouldRepairType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存故障类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldRepairTypeAsync(MouldRepairTypeEntity entity)
        {
            long id;
            if (entity.Id == 0)
            {
                entity.Code = CodeHandler.GetBusinessCode("GZLX");
                var res = await _mouldRepairType.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            }
            else
            {
                var res = await _mouldRepairType.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除故障类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldRepairTypeAsync(List<long> ids)
        {
            var res = await _mouldRepairType.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新故障类型状态
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateMouldRepairTypeStatusAsync(List<long> ids, bool status)
        {
            var res = await _mouldRepairType.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
        #region 设备维修关联备件
        /// <summary>
        /// 获取设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldRepairRecord2SparePartEntity>> GetMouldRepairRecord2SparePartListByRecordIdAsync(long recordId)
        {
            var list = await _repairRecord2SparePart.GetListAsync(p => p.RepairRecordId == recordId);
            return list;
        }
        /// <summary>
        /// 添加设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMouldRepairRecord2SparePartAsync(long recordId, List<MouldRepairRecord2SparePartEntity> datas)
        {
            datas.ForEach(x => x.RepairRecordId = recordId);
            var res = await _repairRecord2SparePart.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMouldRepairRecord2SparePartAsync(List<MouldRepairRecord2SparePartEntity> datas)
        {
            var res = await _repairRecord2SparePart.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMouldRepairRecord2SparePartAsync(List<long> ids)
        {
            var res = await _repairRecord2SparePart.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
