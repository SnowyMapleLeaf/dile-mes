﻿using DiLe.Mes.Model.Common;
using DiLe.Mes.Model.Common.Process.Entity;
using DiLe.Mes.Model.Common.System.Entity;
using DiLe.Mes.Service.Common.Process;
using DiLe.Mes.Service.Common.System;
using MapleLeaf.Core.Cron;
using Mapster;

namespace DiLe.Mes.Application.Common {
    /// <summary>
    /// 系统设置
    /// </summary>
    public class SystemClient : IApplicationClient {

        private readonly WarehouseService _warehouseService;
        private readonly TeamService _teamService;
        private readonly MeasureUnitService _measureUnitService;
        private readonly ExecuteRuleService _executeRuleService;
        private readonly PositionService _position;
        private readonly ProcessService _process;
        private readonly CustomerService _customer;
        private readonly CustomerTypeService _customerType;
        private readonly RegionService _region;
        private readonly SupplierService _supplier;
        private readonly MaintenanceLevelService _maintenanceLevel;
        private readonly SparePartInventoryService _sparePartInventory;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="warehouseService"></param>
        /// <param name="teamService"></param>
        public SystemClient(WarehouseService warehouseService,
                            TeamService teamService,
                            MeasureUnitService measureUnitService,
                            ExecuteRuleService ruleService,
                            ProcessService processService,
                            CustomerService customerService,
                            CustomerTypeService customerTypeService,
                            RegionService regionService,
                            SupplierService supplierService,
                            PositionService position,
                            MaintenanceLevelService maintenanceLevel,
                            SparePartInventoryService sparePartInventory) {
            _warehouseService = warehouseService;
            _teamService = teamService;
            _measureUnitService = measureUnitService;
            _executeRuleService = ruleService;
            _process = processService;
            _customer = customerService;
            _customerType = customerTypeService;
            _region = regionService;
            _supplier = supplierService;
            _position = position;
            _maintenanceLevel = maintenanceLevel;
            _sparePartInventory = sparePartInventory;
        }
        #region 计量单位
        /// <summary>
        /// 获取计量单位列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MeasureUnitEntity>> GetMeasureUnitListAsync(Expression<Func<MeasureUnitEntity, bool>> whereExp) {
            var res = await _measureUnitService.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取计量单位列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MeasureUnitEntity>> GetMeasureUnitPageListAsync(Expression<Func<MeasureUnitEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<MeasureUnitEntity>>();
            RefAsync<int> total = 0;
            List<MeasureUnitEntity> res = await _measureUnitService.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取计量单位
        /// </summary>
        /// <returns></returns>
        public async Task<MeasureUnitEntity> GetMeasureUnitInfoAsync(long id) {
            var res = await _measureUnitService.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存计量单位
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveMeasureUnitAsync(MeasureUnitEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("JLDW");
                res = await _measureUnitService.InsertAsync(entity);
            } else {
                res = await _measureUnitService.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除计量单位
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMeasureUnitAsync(List<long> ids) {
            var res = await _measureUnitService.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 仓库
        /// <summary>
        /// 获取仓库列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<WarehouseEntity>> GetWarehouseListAsync(Expression<Func<WarehouseEntity, bool>> whereExpression) {
            List<WarehouseEntity> res = await _warehouseService.GetListAsync(whereExpression);
            return res;
        }
        /// <summary>
        /// 获取仓库列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<WarehouseEntity>> GetWarehousePageListAsync(Expression<Func<WarehouseEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<WarehouseEntity>>();
            RefAsync<int> total = 0;
            var res = await _warehouseService.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取仓库
        /// </summary>
        /// <returns></returns>
        public async Task<WarehouseEntity> GetWarehouseInfoAsync(long? id) {
            var res = await _warehouseService.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存仓库
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveWarehouseAsync(WarehouseEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("CK");
                res = await _warehouseService.InsertAsync(entity);
            } else {
                res = await _warehouseService.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除仓库
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteWarehouseAsync(List<long> ids) {
            var res = await _warehouseService.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新仓库状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateWarehouseStatusAsync(List<long> ids, bool status) {
            var res = await _warehouseService.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 班组
        /// <summary>
        /// 获取班组列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<TeamEntity>> GetTeamListAsync(Expression<Func<TeamEntity, bool>> whereExp) {
            var res = await _teamService.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取班组列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<TeamEntity>> GetTeamPageListAsync(Expression<Func<TeamEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<TeamEntity>>();
            RefAsync<int> total = 0;
            var res = await _teamService.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取班组
        /// </summary>
        /// <returns></returns>
        public async Task<TeamEntity> GetTeamInfoAsync(long id) {
            var res = await _teamService.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存班组
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveTeamAsync(TeamEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BZ");
                res = await _teamService.InsertAsync(entity);
            } else {
                res = await _teamService.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除班组
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteTeamAsync(List<long> ids) {
            var res = await _teamService.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新班组状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateTeamStatusAsync(List<long> ids, bool status) {
            var res = await _teamService.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 规则
        /// <summary>
        /// 获取规则列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ExecuteRuleEntity>> GetExecuteRuleListAsync(Expression<Func<ExecuteRuleEntity, bool>> whereExp) {
            var res = await _executeRuleService.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取规则列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<ExecuteRuleEntity>> GetExecuteRuleListAsync(Expression<Func<ExecuteRuleEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<ExecuteRuleEntity>>();
            RefAsync<int> total = 0;
            var res = await _executeRuleService.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取规则
        /// </summary>
        /// <returns></returns>
        public async Task<ExecuteRuleEntity> GetExecuteRuleInfoAsync(long id) {
            var res = await _executeRuleService.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存规则
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveExecuteRuleAsync(ExecuteRuleEntity entity) {
            bool res;
            entity.RuleDescription = CronHandler.GetDescription(entity.RuleExpression);
            if (entity.Id == 0) {
                res = await _executeRuleService.InsertAsync(entity);
            } else {
                res = await _executeRuleService.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除规则
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteExecuteRuleAsync(List<long> ids) {
            var res = await _executeRuleService.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新规则状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateExecuteRuleStatusAsync(List<long> ids, bool status) {
            var res = await _executeRuleService.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 工序
        /// <summary>
        /// 获取工序列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<ProcessEntity>> GetProcessPageListAsync(Expression<Func<ProcessEntity, bool>> whereExpressionint, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<ProcessEntity>>();
            RefAsync<int> total = 0;
            var res = await _process.GetPageListAsync(whereExpressionint, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取工序列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProcessEntity>> GetProcessListAsync(Expression<Func<ProcessEntity, bool>> whereExpressionint) {
            var res = await _process.GetListAsync(whereExpressionint);
            return res;
        }
        /// <summary>
        /// 获取工序
        /// </summary>
        /// <returns></returns>
        public async Task<ProcessEntity> GetProcessInfoAsync(long? id) {
            var res = await _process.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存工序
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveProcessAsync(ProcessEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("SCGX");
                res = await _process.InsertAsync(entity);
            } else {
                res = await _process.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除工序
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteProcessAsync(List<long> ids) {
            var res = await _process.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新工序状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateProcessStatusAsync(List<long> ids, bool status) {
            var res = await _process.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 客户
        /// <summary>
        /// 获取客户列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<CustomerEntity>> GetCustomerPageListAsync(Expression<Func<CustomerEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<CustomerEntity>>();
            RefAsync<int> total = 0;
            var res = await _customer.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取客户列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<CustomerEntity>> GetCustomerListAsync(Expression<Func<CustomerEntity, bool>> whereExp) {
            var res = await _customer.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取客户
        /// </summary>
        /// <returns></returns>
        public async Task<CustomerEntity> GetCustomerInfoAsync(long? id) {
            var res = await _customer.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存客户
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveCustomerAsync(CustomerEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("KHBH");
                res = await _customer.InsertAsync(entity);
            } else {
                res = await _customer.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除客户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCustomerAsync(List<long> ids) {
            var res = await _customer.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 客户类型
        /// <summary>
        /// 获取客户类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<CustomerTypeEntity>> GetCustomerTypePageListAsync(Expression<Func<CustomerTypeEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<CustomerTypeEntity>>();
            RefAsync<int> total = 0;
            var res = await _customerType.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取客户类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<CustomerTypeEntity>> GetCustomerTypeListAsync(Expression<Func<CustomerTypeEntity, bool>> whereExp) {
            var res = await _customerType.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取客户类型
        /// </summary>
        /// <returns></returns>
        public async Task<CustomerTypeEntity> GetCustomerTypeInfoAsync(long id) {
            var res = await _customerType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存客户类型
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveCustomerTypeAsync(CustomerTypeEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("KHBH");
                res = await _customerType.InsertAsync(entity);
            } else {
                res = await _customerType.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除客户类型
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCustomerTypeAsync(List<long> ids) {
            var res = await _customerType.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 区域
        /// <summary>
        /// 获取区域列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<RegionEntity>> GetRegionListAsync(Expression<Func<RegionEntity, bool>> whereExp) {
            var res = await _region.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取区域列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<RegionEntity>> GetRegionPageListAsync(Expression<Func<RegionEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<RegionEntity>>();
            RefAsync<int> total = 0;
            var res = await _region.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取区域
        /// </summary>
        /// <returns></returns>
        public async Task<RegionEntity> GetRegionInfoAsync(long? id) {
            var res = await _region.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存区域
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveRegionAsync(RegionEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("QY");
                res = await _region.InsertAsync(entity);
            } else {
                res = await _region.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除区域
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRegionAsync(List<long> ids) {
            var res = await _region.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 供应商
        /// <summary>
        /// 获取供应商列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<SupplierEntity>> GetSupplierPageListAsync(Expression<Func<SupplierEntity, bool>> whereExpression, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<SupplierEntity>>();
            RefAsync<int> total = 0;
            var res = await _supplier.GetPageListAsync(whereExpression, pagination.PageIndex, pagination.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取供应商列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<SupplierEntity>> GetSupplierListAsync(Expression<Func<SupplierEntity, bool>> whereExpression) {
            var res = await _supplier.GetListAsync(whereExpression);
            return res;
        }
        /// <summary>
        /// 获取供应商
        /// </summary>
        /// <returns></returns>
        public async Task<SupplierEntity> GetSupplierInfoAsync(long? id) {
            var res = await _supplier.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存供应商
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveSupplierAsync(SupplierEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("GYS");
                res = await _supplier.InsertAsync(entity);
            } else {
                res = await _supplier.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除供应商
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSupplierAsync(List<long> ids) {
            var res = await _supplier.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新供应商状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateSupplierStatusAsync(List<long> ids, bool status) {
            var res = await _supplier.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 存放位置
        /// <summary>
        /// 获取存放位置
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<PositionEntity>> GetPositionListAsync(Expression<Func<PositionEntity, bool>> whereExp) {
            var res = await _position.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取存放位置
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<PositionEntity>> GetPositionPageListAsync(Expression<Func<PositionEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<PositionEntity>>();
            RefAsync<int> total = 0;
            List<PositionEntity> res = await _position.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取存放位置
        /// </summary>
        /// <returns></returns>
        public async Task<PositionEntity> GetPositionAsync(long id) {
            var res = await _position.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存存放位置
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SavePositionAsync(PositionEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _position.InsertAsync(entity);
            } else {
                res = await _position.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除存放位置
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeletePositionAsync(List<long> ids) {
            var res = await _position.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新存放位置
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdatePositionStatusAsync(List<long> ids, bool status) {
            var res = await _position.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
        #region 保养级别
        /// <summary>
        /// 获取保养级别
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<MaintenanceLevelEntity>> GetMaintenanceLevelListAsync(Expression<Func<MaintenanceLevelEntity, bool>> whereExp) {
            var res = await _maintenanceLevel.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取保养级别
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<MaintenanceLevelEntity>> GetMaintenanceLevelPageListAsync(Expression<Func<MaintenanceLevelEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<MaintenanceLevelEntity>>();
            RefAsync<int> total = 0;
            List<MaintenanceLevelEntity> res = await _maintenanceLevel.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取保养级别
        /// </summary>
        /// <returns></returns>
        public async Task<MaintenanceLevelEntity> GetMaintenanceLevelAsync(long id) {
            var res = await _maintenanceLevel.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存保养级别
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveMaintenanceLevelAsync(MaintenanceLevelEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BYJB");
                res = await _maintenanceLevel.InsertAsync(entity);
            } else {
                res = await _maintenanceLevel.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除保养级别
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMaintenanceLevelAsync(List<long> ids) {
            var res = await _maintenanceLevel.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新保养级别状态
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateMaintenanceLevelStatusAsync(List<long> ids, bool status) {
            var res = await _maintenanceLevel.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
        #region 备件库存
        /// <summary>
        /// 获取备件库存
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<SparePartInventoryEntity>> GetSparePartInventoryListAsync(Expression<Func<SparePartInventoryEntity, bool>> whereExp) {
            var res = await _sparePartInventory.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取备件库存
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<SparePartInventoryEntity>> GetSparePartInventoryPageListAsync(Expression<Func<SparePartInventoryEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<SparePartInventoryEntity>>();
            RefAsync<int> total = 0;
            var res = await _sparePartInventory.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取备件库存
        /// </summary>
        /// <returns></returns>
        public async Task<SparePartInventoryEntity> GetSparePartInventoryAsync(long id) {
            var res = await _sparePartInventory.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 获取备件库存
        /// </summary>
        /// <returns></returns>
        public async Task<SparePartInventoryEntity> GetSparePartInventoryAsync(Expression<Func<SparePartInventoryEntity, bool>> whereExp) {
            var res = await _sparePartInventory.GetEntityByAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 保存备件库存
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveSparePartInventoryAsync(SparePartInventoryEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _sparePartInventory.InsertAsync(entity);
            } else {
                res = await _sparePartInventory.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 保存备件库存
        /// </summary>
        /// <returns></returns>
        public async Task<bool> InsertSparePartInventoryAsync(SparePartInventoryEntity entity) {
            var res = await _sparePartInventory.InsertAsync(entity);
            return res;
        }
        /// <summary>
        /// 保存备件库存
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateSparePartInventoryAsync(List<SparePartInventoryEntity> list) {
            var res = await _sparePartInventory.UpdateAsync(list);
            return res > 0;
        }
        /// <summary>
        /// 删除备件库存
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSparePartInventoryAsync(List<long> ids) {
            var res = await _sparePartInventory.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
