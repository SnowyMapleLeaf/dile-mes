using DiLe.Mes.Model.Common.WorkOrder.Entity;
using DiLe.Mes.Model.Common.WorkOrder.Relation;
using DiLe.Mes.Service.Common.WorkOrder;
using DiLe.Mes.Service.Dto;

namespace DiLe.Mes.Application.Common {
    public class WorkOrderClient : IApplicationClient {
        private readonly StartWorkRecordService _startWorkRecord;
        private readonly ReportWorkRecordService _reportWorkRecord;
        private readonly StartWorkRecord2MouldInfoService _startWorkRecord2MouldInfo;
        private readonly MouldChangeService _mouldChange;
        private readonly OrderService _order;
        /// <summary>
        /// 构造函数
        /// </summary>
        public WorkOrderClient(StartWorkRecordService startWorkRecord,
                               ReportWorkRecordService reportWorkRecord,
                               StartWorkRecord2MouldInfoService startWorkRecord2MouldInfo,
                               MouldChangeService mouldChange,
                               OrderService order) {
            _startWorkRecord = startWorkRecord;
            _reportWorkRecord = reportWorkRecord;
            _mouldChange = mouldChange;
            _startWorkRecord2MouldInfo = startWorkRecord2MouldInfo;
            _order = order;
        }
        #region 开工记录
        /// <summary>
        /// 获取开工记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<StartWorkRecordEntity>> GetStartWorkRecordPageListAsync(Expression<Func<StartWorkRecordEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<StartWorkRecordEntity>>();
            RefAsync<int> total = 0;
            var res = await _startWorkRecord.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取开工记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<StartWorkRecordEntity>> GetStartWorkRecordListAsync(Expression<Func<StartWorkRecordEntity, bool>>? whereExp = null) {
            List<StartWorkRecordEntity> res;
            if (whereExp == null) {
                res = await _startWorkRecord.GetAllListAsync();
            } else {
                res = await _startWorkRecord.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取开工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<StartWorkRecordEntity> GetStartWorkRecordInfoAsync(Expression<Func<StartWorkRecordEntity, bool>> whereExp) {
            var res = await _startWorkRecord.GetEntityByAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取开工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<StartWorkRecordEntity> GetStartWorkRecordInfoAsync(long id) {
            var res = await _startWorkRecord.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存开工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveStartWorkRecordAsync(StartWorkRecordEntity entity) {
            long id;
            if (entity.Id == 0) {
                var res = await _startWorkRecord.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _startWorkRecord.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 保存开工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateStartWorkRecordAsync(List<StartWorkRecordEntity> entitys) {

            var res = await _startWorkRecord.UpdateAsync(entitys);

            return res;
        }
        /// <summary>
        /// 删除开工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteStartWorkRecordAsync(List<long> ids) {
            var res = await _startWorkRecord.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 报工记录
        /// <summary>
        /// 获取报工记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<ReportWorkRecordEntity>> GetReportWorkRecordPageListAsync(Expression<Func<ReportWorkRecordEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<ReportWorkRecordEntity>>();
            RefAsync<int> total = 0;
            var res = await _reportWorkRecord.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取报工记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReportWorkRecordEntity>> GetReportWorkRecordListAsync(Expression<Func<ReportWorkRecordEntity, bool>>? whereExp = null) {
            List<ReportWorkRecordEntity>? res;
            if (whereExp == null) {
                res = await _reportWorkRecord.GetAllListAsync();
            } else {
                res = await _reportWorkRecord.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取报工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<ReportWorkRecordEntity> GetReportWorkRecordInfoAsync(Expression<Func<ReportWorkRecordEntity, bool>> whereExp) {
            var res = await _reportWorkRecord.GetEntityByAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取报工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<ReportWorkRecordEntity> GetReportWorkRecordInfoAsync(long id) {
            var res = await _reportWorkRecord.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存报工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveReportWorkRecordAsync(ReportWorkRecordEntity entity) {
            long id;
            if (entity.Id == 0) {
                var res = await _reportWorkRecord.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _reportWorkRecord.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 保存报工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateReportWorkRecordAsync(List<ReportWorkRecordEntity> entitys) {
            var res = await _reportWorkRecord.UpdateAsync(entitys);
            return res;
        }
        /// <summary>
        /// 删除报工记录信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteReportWorkRecordAsync(List<long> ids) {
            var res = await _reportWorkRecord.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 开工记录和模具
        /// <summary>
        /// 开工记录和模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MouldInfoModel>> GetMouldInfoModelList(List<long> recordids) {
            var list = await _startWorkRecord2MouldInfo.GetMouldInfoModelList(recordids);
            return list;
        }

        /// <summary>
        /// 开工记录和模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<StartWorkRecord2MouldInfoEntity>> GetStartWorkRecord2MouldListAsync(Expression<Func<StartWorkRecord2MouldInfoEntity, bool>>? whereExp = null) {
            List<StartWorkRecord2MouldInfoEntity> list;
            if (whereExp == null) {
                list = await _startWorkRecord2MouldInfo.GetAllListAsync();
            } else {
                list = await _startWorkRecord2MouldInfo.GetListAsync(whereExp);
            }
            return list;
        }
        /// <summary>
        /// 开工记录和模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<StartWorkRecord2MouldInfoEntity>> GetStartWorkRecord2MouldListByRecordIdAsync(long recordId) {
            var list = await _startWorkRecord2MouldInfo.GetListAsync(p => p.StartWorkRecordId == recordId);
            return list;
        }
        /// <summary>
        /// 添加开工记录和模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertStartWorkRecord2MouldInfoAsync(long recordId, List<StartWorkRecord2MouldInfoEntity> datas) {
            datas.ForEach(x => x.StartWorkRecordId = recordId);
            var res = await _startWorkRecord2MouldInfo.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新开工记录和模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateStartWorkRecord2MouldInfoAsync(List<StartWorkRecord2MouldInfoEntity> datas) {
            var res = await _startWorkRecord2MouldInfo.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除开工记录和模具
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteStartWorkRecord2MouldInfoAsync(List<long> ids) {
            var res = await _startWorkRecord2MouldInfo.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 模具变更
        /// <summary>
        /// 获取模具变更列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldChangeEntity>> GetMouldChangePageListAsync(Expression<Func<MouldChangeEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<MouldChangeEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldChange.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取模具变更列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldChangeEntity>> GetMouldChangeListAsync(Expression<Func<MouldChangeEntity, bool>> whereExp) {
            var res = await _mouldChange.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取模具变更信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldChangeEntity> GetMouldChangeInfoAsync(long id) {
            var res = await _mouldChange.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存模具变更信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldChangeAsync(MouldChangeEntity entity) {
            long id;
            if (entity.Id == 0) {
                var res = await _mouldChange.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _mouldChange.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 保存模具变更信息
        /// </summary>
        /// <returns></returns>
        public async Task<int> InertMouldChangeListAsync(List<MouldChangeEntity> list) {
            var res = await _mouldChange.InsertAsync(list);
            return res;
        }
        /// <summary>
        /// 删除模具变更信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldChangeAsync(List<long> ids) {
            var res = await _mouldChange.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 订单
        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<OrderEntity>> GetOrderListAsync(Expression<Func<OrderEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<OrderEntity>>();
            RefAsync<int> total = 0;
            var res = await _order.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<OrderEntity>> GetOrderListAsync(Expression<Func<OrderEntity, bool>>? whereExp = null) {
            List<OrderEntity> res;
            if (whereExp == null) {
                res = await _order.GetAllListAsync();
            } else {
                res = await _order.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <returns></returns>
        public async Task<OrderEntity> GetOrderInfoAsync(long id) {
            var res = await _order.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存订单信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveOrderAsync(OrderEntity entity) {
            long id;
            if (entity.Id == 0) {
                var res = await _order.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _order.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 保存订单信息
        /// </summary>
        /// <returns></returns>
        public async Task<int> InertOrderListAsync(List<OrderEntity> list) {
            var res = await _order.InsertAsync(list);
            return res;
        }
        /// <summary>
        /// 删除订单信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteOrderAsync(List<long> ids) {
            var res = await _order.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
