using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Service.Common.Abnormal;

namespace DiLe.Mes.Application.Common {
    public class AbnormalClient : IApplicationClient {

        private readonly MouldAbnormalService _mouldAbnormal;
        private readonly ProductionOutputAbnormalService _productionOutputAbnormal;
        private readonly ProductionQualityAbnormalService _productionQualityAbnormal;
        /// <summary>
        /// 构造函数
        /// </summary>
        public AbnormalClient(MouldAbnormalService mouldAbnormal,
                              ProductionOutputAbnormalService productionOutputAbnormal,
                              ProductionQualityAbnormalService productionQualityAbnormal) {

            _mouldAbnormal = mouldAbnormal;
            _productionOutputAbnormal = productionOutputAbnormal;
            _productionQualityAbnormal = productionQualityAbnormal;
        }

        #region 模具异常
        /// <summary>
        /// 获取模具异常列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MouldAbnormalEntity>> GetMouldAbnormalPageListAsync(Expression<Func<MouldAbnormalEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<MouldAbnormalEntity>>();
            RefAsync<int> total = 0;
            var res = await _mouldAbnormal.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取模具异常列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldAbnormalEntity>> GetMouldAbnormalListAsync(Expression<Func<MouldAbnormalEntity, bool>> whereExp) {
            var res = await _mouldAbnormal.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取模具异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<MouldAbnormalEntity> GetMouldAbnormalInfoAsync(long id) {
            var res = await _mouldAbnormal.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存模具异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMouldAbnormalAsync(MouldAbnormalEntity entity) {
            long id;
            if (entity.Id == 0) {
                var res = await _mouldAbnormal.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _mouldAbnormal.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除模具异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteMouldAbnormalAsync(List<long> ids) {
            var res = await _mouldAbnormal.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 产出量异常
        /// <summary>
        /// 获取产出量异常列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<ProductionOutputAbnormalEntity>> GetProductionOutputAbnormalPageListAsync(Expression<Func<ProductionOutputAbnormalEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<ProductionOutputAbnormalEntity>>();
            RefAsync<int> total = 0;
            var res = await _productionOutputAbnormal.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取产出量异常列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductionOutputAbnormalEntity>> GetProductionOutputAbnormalListAsync(Expression<Func<ProductionOutputAbnormalEntity, bool>> whereExp) {
            var res = await _productionOutputAbnormal.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取产出量异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<ProductionOutputAbnormalEntity> GetProductionOutputAbnormalInfoAsync(long id) {
            var res = await _productionOutputAbnormal.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存产出量异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveProductionOutputAbnormalAsync(ProductionOutputAbnormalEntity entity) {
            long id;
            if (entity.Id == 0) {
                var res = await _productionOutputAbnormal.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _productionOutputAbnormal.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除产出量异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteProductionOutputAbnormalAsync(List<long> ids) {
            var res = await _productionOutputAbnormal.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 产品品质异常
        /// <summary>
        /// 获取产品品质异常列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<ProductionQualityAbnormalEntity>> GetProductionQualityAbnormalPageListAsync(Expression<Func<ProductionQualityAbnormalEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<ProductionQualityAbnormalEntity>>();
            RefAsync<int> total = 0;
            var res = await _productionQualityAbnormal.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取产品品质异常列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductionQualityAbnormalEntity>> GetProductionQualityAbnormalListAsync(Expression<Func<ProductionQualityAbnormalEntity, bool>> whereExp) {
            var res = await _productionQualityAbnormal.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取产品品质异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<ProductionQualityAbnormalEntity> GetProductionQualityAbnormalInfoAsync(long id) {
            var res = await _productionQualityAbnormal.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存产品品质异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveProductionQualityAbnormalAsync(ProductionQualityAbnormalEntity entity) {
            long id;
            if (entity.Id == 0) {
                var res = await _productionQualityAbnormal.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _productionQualityAbnormal.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除产品品质异常信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteProductionQualityAbnormalAsync(List<long> ids) {
            var res = await _productionQualityAbnormal.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
