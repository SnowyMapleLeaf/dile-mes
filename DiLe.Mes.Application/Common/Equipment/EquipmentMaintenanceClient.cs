﻿using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;
using DiLe.Mes.Model.Common.Equipment.Relation.Maintenance;
using DiLe.Mes.Service.Common.Equipment.Maintenance;

namespace DiLe.Mes.Application.Common.Equipment {
    public class EquipmentMaintenanceClient : IApplicationClient {

        private readonly MaintenanceProjectService _maintenanceProject;
        private readonly MaintenancePlanService _maintenancePlan;
        private readonly MaintenanceRecordService _maintenanceRecord;
        private readonly MaintenancePlan2ProjectService _plan2Project;
        private readonly MaintenanceRecord2ProjectService _record2Project;
        /// <summary>
        /// 
        /// </summary>
        public EquipmentMaintenanceClient(MaintenanceProjectService maintenanceProject,
                                          MaintenancePlanService maintenancePlan,
                                          MaintenanceRecordService maintenanceRecord,
                                          MaintenancePlan2ProjectService plan2ProjectService,
                                          MaintenanceRecord2ProjectService record2Project) {
            _maintenanceProject = maintenanceProject;
            _maintenancePlan = maintenancePlan;
            _maintenanceRecord = maintenanceRecord;
            _plan2Project = plan2ProjectService;
            _record2Project = record2Project;
        }
        #region 保养计划
        /// <summary>
        /// 获取保养计划列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MaintenancePlanEntity>> GetMaintenancePlanPageListAsync(Expression<Func<MaintenancePlanEntity, bool>> whereExpression, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<MaintenancePlanEntity>>();
            RefAsync<int> total = 0;
            var list = await _maintenancePlan.GetPageListAsync(whereExpression, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = list;
            return page;
        }
        /// <summary>
        /// 获取保养计划列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MaintenancePlanEntity>> GetMaintenancePlanListAsync(Expression<Func<MaintenancePlanEntity, bool>>? whereExpression = null) {
            List<MaintenancePlanEntity> list;
            if (whereExpression == null) {
                list = await _maintenancePlan.GetAllListAsync();
            } else {
                list = await _maintenancePlan.GetListAsync(whereExpression);
            }
            return list;
        }
        /// <summary>
        /// 获取保养计划
        /// </summary>
        /// <returns></returns>
        public async Task<MaintenancePlanEntity> GetMaintenancePlanAsync(long id) {
            var res = await _maintenancePlan.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存保养计划
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMaintenancePlanAsync(MaintenancePlanEntity entity) {
            long id;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BYJH");
                var res = await _maintenancePlan.InsertReturnEntityAsync(entity);
                id = res == null ? 0 : res.Id;
            } else {
                var res = await _maintenancePlan.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除保养计划
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMaintenancePlanAsync(List<long> ids) {
            var res = await _maintenancePlan.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 保养记录
        /// <summary>
        /// 获取保养记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MaintenanceRecordEntity>> GetMaintenanceRecordPageListAsync(Expression<Func<MaintenanceRecordEntity, bool>> whereExpression, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<MaintenanceRecordEntity>>();
            RefAsync<int> total = 0;
            var list = await _maintenanceRecord.GetPageListAsync(whereExpression, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = list;
            return page;
        }
        /// <summary>
        /// 获取保养记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MaintenanceRecordEntity>> GetMaintenanceRecordListAsync(Expression<Func<MaintenanceRecordEntity, bool>>? whereExpression = null) {
            List<MaintenanceRecordEntity> list;
            if (whereExpression == null) {
                list = await _maintenanceRecord.GetAllListAsync();
            } else {
                list = await _maintenanceRecord.GetListAsync(whereExpression);
            }
            return list;
        }
        /// <summary>
        /// 获取保养记录
        /// </summary>
        /// <returns></returns>
        public async Task<MaintenanceRecordEntity> GetMaintenanceRecordAsync(long id) {
            var res = await _maintenanceRecord.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存保养记录
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveMaintenanceRecordAsync(MaintenanceRecordEntity entity) {
            long id;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BYRWH");
                var res = await _maintenanceRecord.InsertReturnEntityAsync(entity);
                id = res.Id;
            } else {
                var res = await _maintenanceRecord.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除保养记录
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMaintenanceRecordAsync(List<long> ids) {
            var res = await _maintenanceRecord.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 保养项目
        /// <summary>
        /// 获取保养项目列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<MaintenanceProjectEntity>> GetMaintenanceProjectPageListAsync(Expression<Func<MaintenanceProjectEntity, bool>> whereExpression, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<MaintenanceProjectEntity>>();
            RefAsync<int> total = 0;
            var list = await _maintenanceProject.GetPageListAsync(whereExpression, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = list;
            return page;
        }
        /// <summary>
        /// 获取保养项目列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<MaintenanceProjectEntity>> GetMaintenanceProjectListAsync(Expression<Func<MaintenanceProjectEntity, bool>> whereExpression) {
            var list = await _maintenanceProject.GetListAsync(whereExpression);
            return list;
        }
        /// <summary>
        /// 获取保养项目
        /// </summary>
        /// <returns></returns>
        public async Task<MaintenanceProjectEntity> GetMaintenanceProjectAsync(long id) {
            var res = await _maintenanceProject.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存保养项目
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveMaintenanceProjectAsync(MaintenanceProjectEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BYXM");
                res = await _maintenanceProject.InsertAsync(entity);
            } else {
                res = await _maintenanceProject.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除保养项目
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMaintenanceProjectAsync(List<long> ids) {
            var res = await _maintenanceProject.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新保养项目
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateMaintenanceProjectStatusAsync(List<long> ids, bool status) {
            var res = await _maintenanceProject.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion

        #region 保养计划关联的保养项目
        /// <summary>
        /// 保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MaintenancePlan2ProjectEntity>> GetMaintenancePlan2ProjectListByPlanIdAsync(long planId) {
            var list = await _plan2Project.GetListAsync(p => p.MaintenancePlanId == planId);
            return list;
        }
        /// <summary>
        /// 添加保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMaintenancePlan2ProjectAsync(long planId, List<MaintenancePlan2ProjectEntity> datas) {
            datas.ForEach(x => x.MaintenancePlanId = planId);
            var res = await _plan2Project.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMaintenancePlan2ProjectAsync(List<MaintenancePlan2ProjectEntity> datas) {
            var res = await _plan2Project.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除保养计划关联的保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMaintenancePlan2ProjectAsync(List<long> ids) {
            var res = await _plan2Project.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion

        #region 保养记录关联保养项目
        ///<summary>
        /// 保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<MaintenanceRecord2ProjectEntity>> GetMaintenanceRecord2ProjectListByPlanIdAsync(long recordId) {
            var list = await _record2Project.GetListAsync(p => p.MaintenanceRecordId == recordId);
            return list;
        }
        /// <summary>
        /// 添加保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertMaintenanceRecord2ProjectAsync(long recordId, List<MaintenanceRecord2ProjectEntity> datas) {
            datas.ForEach(x => x.MaintenanceRecordId = recordId);
            var res = await _record2Project.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateMaintenanceRecord2ProjectAsync(List<MaintenanceRecord2ProjectEntity> datas) {
            var res = await _record2Project.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除保养记录关联保养项目
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteMaintenanceRecord2ProjectAsync(List<long> ids) {
            var res = await _record2Project.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
