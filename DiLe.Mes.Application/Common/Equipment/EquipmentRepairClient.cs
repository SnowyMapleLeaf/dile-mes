﻿using DiLe.Mes.Model.Common.Equipment.Entity.Repair;
using DiLe.Mes.Model.Common.Equipment.Relation.Repair;
using DiLe.Mes.Service.Common.Equipment.Repair;
using DiLe.Mes.Service.Dto;
using Mapster;

namespace DiLe.Mes.Application.Common.Equipment {
    public class EquipmentRepairClient : IApplicationClient {
        private RepairTypeService _repairType;
        private RepairRecordService _repairRecord;
        private RepairRecord2SparePartService _repairRecord2SparePart;
        /// <summary>
        /// 设备维修
        /// </summary>
        public EquipmentRepairClient(RepairTypeService repairType, RepairRecordService repairRecord, RepairRecord2SparePartService repairRecordSparePart) {
            _repairType = repairType;
            _repairRecord = repairRecord;
            _repairRecord2SparePart = repairRecordSparePart;
        }
        #region 故障类型
        /// <summary>
        /// 获取故障类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<RepairTypeEntity>> GetRepairTypeListAsync(Expression<Func<RepairTypeEntity, bool>> whereExpression) {
            var list = await _repairType.GetListAsync(whereExpression);
            return list;
        }
        /// <summary>
        /// 获取故障类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<RepairTypeEntity>> GetRepairTypePageListAsync(Expression<Func<RepairTypeEntity, bool>> whereExpression, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<RepairTypeEntity>>();
            RefAsync<int> total = 0;
            var list = await _repairType.GetPageListAsync(whereExpression, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = list;
            return page;
        }
        /// <summary>
        /// 获取故障类型
        /// </summary>
        /// <returns></returns>
        public async Task<RepairTypeEntity> GetRepairTypeAsync(long? id) {
            var res = await _repairType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存故障类型
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveRepairTypeAsync(RepairTypeEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("SBDA");
                res = await _repairType.InsertAsync(entity);
            } else {
                res = await _repairType.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除故障类型
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRepairTypeAsync(List<long> ids) {
            var res = await _repairType.DeleteByIdAsync(ids);
            return res > 0;
        }

        /// <summary>
        /// 更新维修类型状态
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> UpdateRepairTypeStatusAsync(List<long> ids, bool status) {
            var res = await _repairType.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
        #region 维修记录
        /// <summary>
        /// 获取维修记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<RepairRecordEntity>> GetRepairRecordPageListAsync(Expression<Func<RepairRecordEntity, bool>> whereExpression, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<RepairRecordEntity>>();
            RefAsync<int> total = 0;
            var list = await _repairRecord.GetPageListAsync(whereExpression, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = list;
            return page;
        }
        /// <summary>
        /// 获取维修记录列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<RepairRecordEntity>> GetRepairRecordListAsync(Expression<Func<RepairRecordEntity, bool>>? whereExpression = null) {
            List<RepairRecordEntity> list;
            if (whereExpression == null) {
                list = await _repairRecord.GetAllListAsync();
            } else {
                list = await _repairRecord.GetListAsync(whereExpression);
            }
            return list;
        }
        /// <summary>
        /// 获取维修记录
        /// </summary>
        /// <returns></returns>
        public async Task<List<RepairRecordEntity>> GetRepairRecordListAsync(List<long> ids) {
            var res = await _repairRecord.GetListAsync(p => ids.Contains(p.Id));
            return res;
        }
        /// <summary>
        /// 获取维修记录
        /// </summary>
        /// <returns></returns>
        public async Task<RepairRecordEntity> GetRepairRecordAsync(long id) {
            var res = await _repairRecord.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<RepairRecordModel>> GetRepairRecordModelListAsync() {
            var res = await _repairRecord.GetRepairRecordModelListAsync();
            return res;
        }

        /// <summary>
        /// 保存维修记录
        /// </summary>
        /// <returns></returns>
        public async Task<RepairRecordEntity> SaveRepairRecordAsync(RepairRecordEntity entity) {
            RepairRecordEntity repair;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("WXGD");
                var res = await _repairRecord.InsertReturnEntityAsync(entity);
                repair = res;
            } else {
                var res = await _repairRecord.UpdateAsync(entity);
                repair = await _repairRecord.GetByIdAsync(entity.Id);
            }
            return repair;
        }
        /// <summary>
        /// 删除维修记录
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRepairRecordAsync(List<long> ids) {
            var res = await _repairRecord.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion


        #region 设备维修关联备件
        /// <summary>
        /// 获取设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<RepairRecord2SparePartEntity>> GetRepairRecord2SparePartListByRecordIdAsync(long recordId) {
            var list = await _repairRecord2SparePart.GetListAsync(p => p.RepairRecordId == recordId);
            return list;
        }
        /// <summary>
        /// 添加设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertRepairRecord2SparePartAsync(long recordId, List<RepairRecord2SparePartEntity> datas) {
            datas.ForEach(x => x.RepairRecordId = recordId);
            var res = await _repairRecord2SparePart.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateRepairRecord2SparePartAsync(List<RepairRecord2SparePartEntity> datas) {
            var res = await _repairRecord2SparePart.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除设备维修关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteRepairRecord2SparePartAsync(List<long> ids) {
            var res = await _repairRecord2SparePart.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
