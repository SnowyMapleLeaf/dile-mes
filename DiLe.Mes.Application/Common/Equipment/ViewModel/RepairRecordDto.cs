﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Equipment.Entity.Repair;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class RepairRecordDto {
        /// <summary>
        /// 
        /// </summary>
        public RepairRecordEntity Model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EquipmentInfoEntity EquipmentModel { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="R"></typeparam>
    /// <typeparam name="RT"></typeparam>
    public class RepairRecordDetailDto<T, R, RT> : CommonDto<T, R, RT> {

        /// <summary>
        /// 
        /// </summary>
        public EquipmentInfoEntity EquipmentModel { get; set; }
    }
}
