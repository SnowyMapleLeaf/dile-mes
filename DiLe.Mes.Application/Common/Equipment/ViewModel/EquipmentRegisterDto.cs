﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentRegisterDto {
        /// <summary>
        /// 
        /// </summary>
        public EquipmentRegisterEntity Model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EquipmentInfoEntity Equipment { get; set; }
    }
}
