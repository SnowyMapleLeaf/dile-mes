﻿using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class MaintenancePlanDto {
        /// <summary>
        /// 
        /// </summary>
        public MaintenancePlanEntity Model { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public EquipmentInfoEntity EquipmentModel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ExecuteRuleEntity ExecuteRuleModel { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="R"></typeparam>
    /// <typeparam name="RT"></typeparam>
    public class MaintenancePlanDetailDto<T, R, RT> : CommonDto<T, R, RT> {

        /// <summary>
        /// 
        /// </summary>
        public EquipmentInfoEntity EquipmentModel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ExecuteRuleEntity ExecuteRuleModel { get; set; }
    }
}
