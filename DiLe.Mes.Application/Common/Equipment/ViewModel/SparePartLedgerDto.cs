﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class SparePartLedgerDto {
        /// <summary>
        /// 
        /// </summary>
        public SparePartLedgerEntity Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<SparePartEquipmentEntity> Relations { get; set; } = new List<SparePartEquipmentEntity>();
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentInfoEntity> DataList { get; set; } = new List<EquipmentInfoEntity>();
    }
}
