﻿using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {

    /// <summary>
    /// 
    /// </summary>
    public class SparePartStockOutDto {
        /// <summary>
        /// 
        /// </summary>
        public SparePartStockOutEntity Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<StockOutSparePartEntity> Relations { get; set; } = new List<StockOutSparePartEntity>();
        /// <summary>
        /// 
        /// </summary>
        public List<SparePartLedgerDetailDto> DataList { get; set; } = new List<SparePartLedgerDetailDto>();
    }
}
