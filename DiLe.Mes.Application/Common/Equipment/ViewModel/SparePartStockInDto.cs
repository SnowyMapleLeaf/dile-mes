﻿using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class SparePartStockInDto {
        /// <summary>
        /// 
        /// </summary>
        public SparePartStockInEntity Model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SparePartLedgerDetailDto SparePartLedgerModel { get; set; }
    }
}
