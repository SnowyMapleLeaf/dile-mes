﻿using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class SparePartLedgerDetailDto : SparePartLedgerEntity {
        /// <summary>
        /// 总库存
        /// </summary>
        public int InventoryTotalLimit { get; set; }
    }
}
