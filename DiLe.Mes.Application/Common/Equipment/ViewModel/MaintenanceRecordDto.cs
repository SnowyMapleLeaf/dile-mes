﻿using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Equipment.Relation.Maintenance;

namespace DiLe.Mes.Application.Common.Equipment.ViewModel {
    /// <summary>
    /// 
    /// </summary>
    public class MaintenanceRecordDto {
        /// <summary>
        /// 
        /// </summary>
        public MaintenanceRecordEntity Model { get; set; }
        /// <summary>
        /// 计划
        /// </summary>
        public MaintenancePlanEntity PlanModel { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public EquipmentInfoEntity EquipmentModel { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MaintenanceRecordDetailDto {
        /// <summary>
        /// 
        /// </summary>
        public MaintenanceRecordEntity Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<MaintenanceRecord2ProjectEntity> ProjectRelations { get; set; } = new List<MaintenanceRecord2ProjectEntity>();
        /// <summary>
        /// 
        /// </summary>
        public List<MaintenanceProjectEntity> ProjectDataList { get; set; } = new List<MaintenanceProjectEntity>();

        /// <summary>
        /// 计划
        /// </summary>
        public MaintenancePlanEntity PlanModel { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public EquipmentInfoEntity EquipmentModel { get; set; }
        /// <summary>
        /// 备件出库
        /// </summary>
        public SparePartStockOutDto SparePartStockOutModel { get; set; }
    }
}
