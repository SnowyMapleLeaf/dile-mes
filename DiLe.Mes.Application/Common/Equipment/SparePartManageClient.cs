﻿using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;
using DiLe.Mes.Service.Common.Equipment.SparePart;
using Mapster;

namespace DiLe.Mes.Application.Common.Equipment {
    /// <summary>
    /// 备件管理
    /// </summary>
    public class SparePartManageClient : IApplicationClient {

        private readonly SparePartLedgerService _sparePartLedger;
        private readonly SparePartTypeService _sparePartType;
        private readonly SparePartEquipmentService _sparePartEquipment;
        private readonly SparePartStockInService _sparePartStockIn;
        private readonly SparePartStockOutService _sparePartStockOut;
        private readonly StockOutSparePartService _stockOutSparePart;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="sparePartLedger"></param>
        public SparePartManageClient(SparePartLedgerService sparePartLedger,
                                     SparePartTypeService sparePartType,
                                     SparePartEquipmentService sparePartEquipment,
                                     SparePartStockInService sparePartStockIn,
                                     SparePartStockOutService sparePartStockOut,
                                     StockOutSparePartService stockOutSparePart) {
            _sparePartLedger = sparePartLedger;
            _sparePartType = sparePartType;
            _sparePartEquipment = sparePartEquipment;
            _sparePartStockIn = sparePartStockIn;
            _sparePartStockOut = sparePartStockOut;
            _stockOutSparePart = stockOutSparePart;
        }
        #region 备件台账
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<SparePartLedgerEntity>> GetSparePartLedgerListAsync(Expression<Func<SparePartLedgerEntity, bool>> whereExp) {
            var res = await _sparePartLedger.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<SparePartLedgerEntity>> GetSparePartLedgerPageListAsync(Expression<Func<SparePartLedgerEntity, bool>> whereExp, PaginationModel pagination) {
            var model = pagination.Adapt<PaginationModel<SparePartLedgerEntity>>();
            RefAsync<int> total = 0;
            var res = await _sparePartLedger.GetPageListAsync(whereExp, model.PageIndex, model.PageSize, total);
            model.Record = res;
            model.Total = total;
            return model;
        }
        /// <summary>
        /// 获取备件台账
        /// </summary>
        /// <returns></returns>
        public async Task<SparePartLedgerEntity> GetSparePartLedgerAsync(long id) {
            var res = await _sparePartLedger.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存备件台账
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveSparePartLedgerAsync(SparePartLedgerEntity entity) {
            long resId;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BJBH");
                var data = await _sparePartLedger.InsertReturnEntityAsync(entity);
                resId = data == null ? 0 : data.Id;
            } else {
                var res = await _sparePartLedger.UpdateAsync(entity);
                resId = !res ? 0 : entity.Id;
            }
            return resId;
        }
        /// <summary>
        /// 删除备件台账
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSparePartLedgerAsync(List<long> ids) {
            var res = await _sparePartLedger.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 备件类型
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<SparePartTypeEntity>> GetSparePartTypeListAsync(Expression<Func<SparePartTypeEntity, bool>> whereExp) {
            var res = await _sparePartType.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<SparePartTypeEntity>> GetSparePartTypePageListAsync(Expression<Func<SparePartTypeEntity, bool>> whereExp, PaginationModel pagination) {
            var model = pagination.Adapt<PaginationModel<SparePartTypeEntity>>();
            RefAsync<int> total = 0;
            var res = await _sparePartType.GetPageListAsync(whereExp, model.PageIndex, model.PageSize, total);
            model.Record = res;
            model.Total = total;
            return model;
        }
        /// <summary>
        /// 获取备件类型
        /// </summary>
        /// <returns></returns>
        public async Task<SparePartTypeEntity> GetSparePartTypeAsync(long? id) {
            var res = await _sparePartType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存备件类型
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveSparePartTypeAsync(SparePartTypeEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("SBLX");
                res = await _sparePartType.InsertAsync(entity);
            } else {
                res = await _sparePartType.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除备件类型
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSparePartTypeAsync(List<long> ids) {
            var res = await _sparePartType.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新备件类型
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateSparePartTypeStatusAsync(List<long> ids, bool status) {
            var res = await _sparePartType.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
        #region 出库
        /// <summary>
        /// 获取出库列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<SparePartStockOutEntity>> GetSparePartStockOutListAsync(Expression<Func<SparePartStockOutEntity, bool>> whereExp) {
            var res = await _sparePartStockOut.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取出库列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<SparePartStockOutEntity>> GetSparePartStockOutPageListAsync(Expression<Func<SparePartStockOutEntity, bool>> whereExp, PaginationModel pagination) {
            var model = pagination.Adapt<PaginationModel<SparePartStockOutEntity>>();
            RefAsync<int> total = 0;
            var res = await _sparePartStockOut.GetPageListAsync(whereExp, model.PageIndex, model.PageSize, total);
            model.Record = res;
            model.Total = total;
            return model;
        }
        /// <summary>
        /// 获取出库
        /// </summary>
        /// <returns></returns>
        public async Task<SparePartStockOutEntity> GetSparePartStockOutAsync(long id) {
            var res = await _sparePartStockOut.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存出库
        /// </summary>
        /// <returns></returns>
        public async Task<SparePartStockOutEntity> SaveSparePartStockOutAsync(SparePartStockOutEntity entity) {
            SparePartStockOutEntity data;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BJBH");
                data = await _sparePartStockOut.InsertReturnEntityAsync(entity);
            } else {
                await _sparePartStockOut.UpdateAsync(entity);
                data = await GetSparePartStockOutAsync(entity.Id);
            }
            return data;
        }
        /// <summary>
        /// 删除出库
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSparePartStockOutAsync(List<long> ids) {
            var res = await _sparePartStockOut.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 入库
        /// <summary>
        /// 获取出库列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<SparePartStockInEntity>> GetSparePartStockInListAsync(Expression<Func<SparePartStockInEntity, bool>> whereExp) {
            var res = await _sparePartStockIn.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取入库列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<SparePartStockInEntity>> GetSparePartStockInPageListAsync(Expression<Func<SparePartStockInEntity, bool>> whereExp, PaginationModel pagination) {
            var model = pagination.Adapt<PaginationModel<SparePartStockInEntity>>();
            RefAsync<int> total = 0;
            var res = await _sparePartStockIn.GetPageListAsync(whereExp, model.PageIndex, model.PageSize, total);
            model.Record = res;
            model.Total = total;
            return model;
        }
        /// <summary>
        /// 获取入库
        /// </summary>
        /// <returns></returns>
        public async Task<SparePartStockInEntity> GetSparePartStockInAsync(long id) {
            var res = await _sparePartStockIn.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存入库
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveSparePartStockInAsync(SparePartStockInEntity entity) {
            long resId;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("BJBH");
                var data = await _sparePartStockIn.InsertReturnEntityAsync(entity);
                resId = data == null ? 0 : data.Id;
            } else {
                var res = await _sparePartStockIn.UpdateAsync(entity);
                resId = res ? entity.Id : 0;
            }
            return resId;
        }
        /// <summary>
        /// 保存入库
        /// </summary>
        /// <returns></returns>
        public async Task<long> InsertSparePartStockInAsync(SparePartStockInEntity entity) {
            entity.Code = CodeHandler.GetBusinessCode("BJBH");
            var data = await _sparePartStockIn.InsertReturnEntityAsync(entity);
            long resId = data == null ? 0 : data.Id;
            return resId;
        }
        /// <summary>
        /// 删除入库
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSparePartStockInAsync(List<long> ids) {
            var res = await _sparePartStockIn.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion


        #region 备件关联设备
        /// <summary>
        /// 备件关联设备
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<SparePartEquipmentEntity>> GetSparePartEquipmentListByLedgerIdAsync(long ledgerId) {
            var list = await _sparePartEquipment.GetListAsync(p => p.SparePartId == ledgerId);
            return list;
        }
        /// <summary>
        /// 添加备件关联设备
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertSparePartEquipmentAsync(long ledgerId, List<SparePartEquipmentEntity> datas) {
            datas.ForEach(x => x.SparePartId = ledgerId);
            var res = await _sparePartEquipment.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新备件关联设备
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateSparePartEquipmentAsync(List<SparePartEquipmentEntity> datas) {
            var res = await _sparePartEquipment.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除备件关联设备
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteSparePartEquipmentAsync(List<long> ids) {
            var res = await _sparePartEquipment.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 出库关联备件
        /// <summary>
        /// 获取出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<StockOutSparePartEntity>> GetStockOutSparePartList(Expression<Func<StockOutSparePartEntity, bool>> whereExp) {
            var list = await _stockOutSparePart.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<List<StockOutSparePartEntity>> GetStockOutSparePartListByStockOutIdAsync(long stockOutId) {
            var list = await _stockOutSparePart.GetListAsync(p => p.StockOutId == stockOutId);
            return list;
        }
        /// <summary>
        /// 添加出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> InsertStockOutSparePartAsync(long stockOutId, List<StockOutSparePartEntity> datas) {
            datas.ForEach(x => x.StockOutId = stockOutId);
            var res = await _stockOutSparePart.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateStockOutSparePartAsync(List<StockOutSparePartEntity> datas) {
            var res = await _stockOutSparePart.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除出库关联备件
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteStockOutSparePartAsync(List<long> ids) {
            var res = await _stockOutSparePart.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
