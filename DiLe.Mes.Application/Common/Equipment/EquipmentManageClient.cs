﻿using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Service.Common.Abnormal;
using DiLe.Mes.Service.Common.Equipment.Info;
using DiLe.Mes.Service.Dto;

namespace DiLe.Mes.Application.Common.Equipment {
    /// <summary>
    /// 设备管理
    /// </summary>
    public class EquipmentManageClient : IApplicationClient {

        private readonly EquipmentInfoService _equipmentInfo;
        private readonly EquipmentTypeService _equipmentType;
        private readonly EquipmentStatusService _equipmentStatus;
        private readonly AbnormalAlarmRuleService _abnormalAlarmRule;
        private readonly EquipmentRegisterService _equipmentregister;

        public EquipmentManageClient(EquipmentInfoService equipmentInfo,
                                     EquipmentTypeService equipmentType,
                                     EquipmentStatusService statusService,
                                     EquipmentRegisterService equipmentRegister,
                                     AbnormalAlarmRuleService abnormalAlarmRule) {
            _equipmentInfo = equipmentInfo;
            _equipmentType = equipmentType;
            _equipmentStatus = statusService;
            _equipmentregister = equipmentRegister;
            _abnormalAlarmRule = abnormalAlarmRule;
        }
        #region 设备档案
        /// <summary>
        /// 获取设备档案列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<EquipmentInfoEntity>> GetEquipmentInfoListAsync(Expression<Func<EquipmentInfoEntity, bool>>? whereExp = null) {
            List<EquipmentInfoEntity> res;
            if (whereExp == null) {
                res = await _equipmentInfo.GetAllListAsync();
            } else {
                res = await _equipmentInfo.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取设备档案列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<EquipmentInfoEntity>> GetEquipmentInfoPageListAsync(Expression<Func<EquipmentInfoEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<EquipmentInfoEntity>>();
            RefAsync<int> total = 0;
            List<EquipmentInfoEntity> res = await _equipmentInfo.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取设备档案
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoEntity>> GetEquipmentInfoListAsync(List<long> ids) {
            var res = await _equipmentInfo.GetListAsync(p => ids.Contains(p.Id));
            return res;
        }
        /// <summary>
        /// 获取设备档案
        /// </summary>
        /// <returns></returns>
        public async Task<EquipmentInfoEntity> GetEquipmentInfoAsync(long id) {
            var res = await _equipmentInfo.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 获取设备档案
        /// </summary>
        /// <returns></returns>
        public async Task<EquipmentInfoEntity> GetEquipmentInfoAsync(Expression<Func<EquipmentInfoEntity, bool>> whereExp) {
            var res = await _equipmentInfo.GetEntityByAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 保存设备档案
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveEquipmentInfoAsync(EquipmentInfoEntity entity) {
            bool res;
            if (entity.Id == 0) {
                if (entity.Code.IsNullOrEmpty()) {
                    entity.Code = CodeHandler.GetBusinessCode("SBDA");
                }
                res = await _equipmentInfo.InsertAsync(entity);
            } else {
                res = await _equipmentInfo.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除设备档案
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteEquipmentInfoAsync(List<long> ids) {
            var res = await _equipmentInfo.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 设备类型
        /// <summary>
        /// 获取设备类型列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<EquipmentTypeEntity>> GetEquipmentTypeListAsync(Expression<Func<EquipmentTypeEntity, bool>>? whereExp = null) {
            List<EquipmentTypeEntity> res;
            if (whereExp == null) {
                res = await _equipmentType.GetAllListAsync();
            } else {
                res = await _equipmentType.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取设备类型列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public List<EquipmentTypeEntity> GetEquipmentTypeList(Expression<Func<EquipmentTypeEntity, bool>>? whereExp = null) {
            List<EquipmentTypeEntity> res;
            if (whereExp == null) {
                res = _equipmentType.GetAllList();
            } else {
                res = _equipmentType.GetList(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取设备状态列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<EquipmentTypeEntity>> GetEquipmentTypePageListAsync(Expression<Func<EquipmentTypeEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<EquipmentTypeEntity>>();
            RefAsync<int> total = 0;
            List<EquipmentTypeEntity> res = await _equipmentType.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取设备类型列表
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<EquipmentTypeEntity>> GetEquipmentTypeListAsync(List<long> ids) {
            List<EquipmentTypeEntity> res = await _equipmentType.GetListAsync(p => ids.Contains(p.Id));
            return res;
        }
        /// <summary>
        /// 获取设备类型
        /// </summary>
        /// <returns></returns>
        public async Task<EquipmentTypeEntity> GetEquipmentTypeAsync(long? id) {
            var res = await _equipmentType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存设备类型
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveEquipmentTypeAsync(EquipmentTypeEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("SBLX");
                res = await _equipmentType.InsertAsync(entity);
            } else {
                res = await _equipmentType.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除设备类型
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteEquipmentTypeAsync(List<long> ids) {
            var res = await _equipmentType.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新设备类型
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateEquipmentTypeStatusAsync(List<long> ids, bool status) {
            var res = await _equipmentType.UpdateStatusAsync(ids, status);
            return res > 0;
        }
        #endregion
        #region 设备状态
        /// <summary>
        /// 获取设备状态列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<EquipmentStatusEntity>> GetEquipmentStatusListAsync(Expression<Func<EquipmentStatusEntity, bool>> whereExp) {
            var res = await _equipmentStatus.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取设备状态列表
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<PaginationModel<EquipmentStatusEntity>> GetEquipmentStatusPageListAsync(Expression<Func<EquipmentStatusEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<EquipmentStatusEntity>>();
            RefAsync<int> total = 0;
            List<EquipmentStatusEntity> res = await _equipmentStatus.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取设备状态
        /// </summary>
        /// <returns></returns>
        public async Task<EquipmentStatusEntity> GetEquipmentStatusAsync(long id) {
            var res = await _equipmentStatus.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存设备状态
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveEquipmentStatusAsync(EquipmentStatusEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("SBZT");
                res = await _equipmentStatus.InsertAsync(entity);
            } else {
                res = await _equipmentStatus.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除设备状态
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteEquipmentStatusAsync(List<long> ids) {
            var res = await _equipmentStatus.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 设备注册
        /// <summary>
        /// 获取设备注册列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<EquipmentRegisterEntity>> GetEquipmentRegisterPageListAsync(Expression<Func<EquipmentRegisterEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<EquipmentRegisterEntity>>();
            RefAsync<int> total = 0;
            var res = await _equipmentregister.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取设备注册列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentRegisterEntity>> GetEquipmentRegisterListAsync(Expression<Func<EquipmentRegisterEntity, bool>>? whereExp = null) {
            List<EquipmentRegisterEntity>? res;
            if (whereExp == null) {
                res = await _equipmentregister.GetAllListAsync();
            } else {
                res = await _equipmentregister.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取设备注册信息
        /// </summary>
        /// <returns></returns>
        public async Task<EquipmentRegisterEntity> GetEquipmentRegisterInfoAsync(long id) {
            var res = await _equipmentregister.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存设备注册信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveEquipmentRegisterAsync(EquipmentRegisterEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _equipmentregister.InsertAsync(entity);
            } else {
                res = await _equipmentregister.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除设备注册信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteEquipmentRegisterAsync(List<long> ids) {
            var res = await _equipmentregister.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新设备注册信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateEquipmentRegisterStatusAsync(List<long> ids, bool status) {
            var dataList = await _equipmentregister.GetListAsync(p => ids.Contains(p.Id));
            if (dataList.None()) {
                return false;
            }
            dataList.ForEach(x => x.Status = status);
            var res = await _equipmentregister.UpdateAsync(dataList);
            return res > 0;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListAsync(long orgid) {
            var res = await _equipmentregister.GetEquipmentInfoModelListAsync(orgid);
            return res;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public List<EquipmentInfoModel> GetEquipmentInfoModelList(long orgid) {
            var res = _equipmentregister.GetEquipmentInfoModelList(orgid);
            return res;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListAsync() {
            var res = await _equipmentregister.GetEquipmentInfoModelListAsync();
            return res;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public List<EquipmentInfoModel> GetEquipmentInfoModelList() {
            var res = _equipmentregister.GetEquipmentInfoModelList();
            return res;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListBySign(List<string> signlist) {
            var res = await _equipmentregister.GetEquipmentInfoModelListBySign(signlist);
            return res;
        }
        /// <summary>
        /// 根据设备编号获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListByCode(List<string> codes) {
            var res = await _equipmentregister.GetEquipmentInfoModelListByCode(codes);
            return res;
        }
        /// <summary>
        /// 根据设备编号获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<EquipmentInfoModel> GetEquipmentInfoModelByCode(string code) {
            var res = await _equipmentregister.GetEquipmentInfoModelListByCode([code]);
            return res?.FirstOrDefault() ?? new EquipmentInfoModel();
        }
        #endregion
        #region 报警规则信息
        /// <summary>
        /// 获取报警规则信息
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public async Task<List<AbnormalAlarmRuleEntity>> GetAbnormalAlarmRuleListAsync(Expression<Func<AbnormalAlarmRuleEntity, bool>>? whereExp = null) {
            List<AbnormalAlarmRuleEntity> res;
            if (whereExp == null) {
                res = await _abnormalAlarmRule.GetAllListAsync();
            } else {
                res = await _abnormalAlarmRule.GetListAsync(whereExp);
            }
            res = res?.OrderBy(t => t.AlarmName,new NumericComparer()).ToList() ?? [];
            return res;
        }
        /// <summary>
        /// 获取报警规则信息
        /// </summary>
        /// <returns></returns>
        public async Task<AbnormalAlarmRuleEntity> GetAbnormalAlarmRuleAsync(long? id) {
            var res = await _abnormalAlarmRule.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存报警规则信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveAbnormalAlarmRuleAsync(AbnormalAlarmRuleEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _abnormalAlarmRule.InsertAsync(entity);
            } else {
                res = await _abnormalAlarmRule.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 保存报警规则信息
        /// </summary>
        /// <returns></returns>
        public async Task<int> InsertAbnormalAlarmRuleAsync(List<AbnormalAlarmRuleEntity> entitys) {
            var res = await _abnormalAlarmRule.InsertAsync(entitys);
            return res;
        }
        /// <summary>
        /// 保存报警规则信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UpdateAbnormalAlarmRuleAsync(List<AbnormalAlarmRuleEntity> entitys) {

            var res = await _abnormalAlarmRule.UpdateAsync(entitys);

            return res > 0;
        }
        /// <summary>
        /// 删除报警规则信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAbnormalAlarmRuleAsync(List<long> ids) {
            var res = await _abnormalAlarmRule.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
