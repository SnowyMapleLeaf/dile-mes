﻿using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.Organization.Relation;
using DiLe.Mes.Service.Common.Organization;
using Mapster;

namespace DiLe.Mes.Application.Common {
    /// <summary>
    /// 组织管理
    /// </summary>
    public class OrganizationClient : IApplicationClient {
        /// <summary>
        /// 
        /// </summary>
        private readonly SysUserService _userService;
        private readonly JobPostService _jobPostService;
        private readonly OrganizationService _organizationService;
        private readonly SysRoleService _sysRoleService;
        private readonly PermissionService _permission;

        private readonly UserRoleService _userRoleService;

        public OrganizationClient(SysUserService userService,
                            JobPostService jobPostService,
                            OrganizationService organizationService,
                            SysRoleService sysRoleService,
                            PermissionService permissionService,
                            UserRoleService userRoleService) {
            _userService = userService;
            _jobPostService = jobPostService;
            _organizationService = organizationService;
            _sysRoleService = sysRoleService;
            _permission = permissionService;
            _userRoleService = userRoleService;
        }

        #region 人员
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task ResetPasswordAsync(List<long> ids) {
            await _userService.ResetPasswordAsync(ids);
        }
        /// <summary>
        /// 获取人员列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserEntity>> GetUserListAsync(Expression<Func<UserEntity, bool>>? whereExp = null) {
            List<UserEntity>? list;
            if (whereExp == null) {
                list = await _userService.GetAllListAsync();
            } else {
                list = await _userService.GetListAsync(whereExp);
            }
            return list;
        }
        /// <summary>
        /// 获取人员列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserEntity>> GetUserListByOrgIdAsync(long orgId) {
            var res = await _userService.GetListAsync(p => p.DepartmentId == orgId);
            return res;
        }
        /// <summary>
        /// 获取人员列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserEntity>> GetUserListByOrgIdAsync(List<long> orgIds) {
            var res = await _userService.GetListAsync(p => orgIds.Contains(p.DepartmentId));
            return res;
        }
        /// <summary>
        /// 获取人员列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserEntity>> GetUserListByIdAsync(List<long> ids) {
            var res = await _userService.GetListAsync(p => ids.Contains(p.Id));
            return res;
        }
        /// <summary>
        /// 获取人员列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<UserEntity>> GetUserPageListAsync(Expression<Func<UserEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<UserEntity>>();
            RefAsync<int> total = 0;
            List<UserEntity> res = await _userService.GetPageListAsync(whereExp, pagination.PageIndex, pagination.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取人员信息
        /// </summary>
        /// <returns></returns>
        public async Task<UserEntity> GetUserByIdAsync(long? id) {
            var res = new UserEntity();
            if (id != null) {
                res = await _userService.GetByIdAsync(id);
            }
            return res;
        }
        /// <summary>
        /// 获取人员
        /// </summary>
        /// <returns></returns>
        public async Task<UserEntity> GetUserByAccountAsync(string acount) {
            var res = await _userService.GetEntityByAsync(p => p.Account == acount);
            return res;
        }

        /// <summary>
        /// 保存人员
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveUserAsync(UserEntity entity) {
            bool res;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("RY");
                res = await _userService.InsertAsync(entity);
            } else {
                res = await _userService.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除人员
        /// </summary>
        /// <returns></returns>
        public async Task<int> DeleteUserAsync(List<long> ids) {
            var res = await _userService.DeleteByIdAsync(ids);
            return res;
        }
        /// <summary>
        /// 更新人员状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateUserStatusAsync(List<long> ids, bool status) {
            var res = await _userService.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 岗位
        /// <summary>
        /// 获取岗位列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<JobPostEntity>> GetJobPostPageListAsync(Expression<Func<JobPostEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<JobPostEntity>>();
            RefAsync<int> total = 0;
            List<JobPostEntity> res = await _jobPostService.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取岗位列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<JobPostEntity>> GetJobPostListAsync(Expression<Func<JobPostEntity, bool>> whereExp) {
            List<JobPostEntity> res = await _jobPostService.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取岗位列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<JobPostEntity>> GetJobPostListAsync(List<long> ids) {
            List<JobPostEntity> res = await _jobPostService.GetListAsync(p => ids.Contains(p.Id));
            return res;
        }
        /// <summary>
        /// 获取岗位
        /// </summary>
        /// <returns></returns>
        public async Task<JobPostEntity> GetJobPostAsync(long id) {
            var res = await _jobPostService.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存岗位
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveJobPostAsync(JobPostEntity jobPostEntity) {
            bool res;
            if (jobPostEntity.Id == 0) {
                jobPostEntity.Code = CodeHandler.GetBusinessCode("GW");
                res = await _jobPostService.InsertAsync(jobPostEntity);
            } else {
                res = await _jobPostService.UpdateAsync(jobPostEntity);
            }
            return res;
        }
        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteJobPostAsync(List<long> ids) {
            var res = await _jobPostService.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新岗位状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateJobPostStatusAsync(List<long> ids, bool status) {
            var res = await _jobPostService.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 组织
        /// <summary>
        /// 获取组织列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<OrganizationEntity>> GetOrganizationListAsync(Expression<Func<OrganizationEntity, bool>>? whereExp = null) {
            List<OrganizationEntity> res;
            if (whereExp == null) {
                res = await _organizationService.GetAllListAsync();
            } else {
                res = await _organizationService.GetListAsync(whereExp);
            }
            res = [.. res.OrderBy(p => p.OrderNumber)];
            return res;
        }
        /// <summary>
        /// 获取组织列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<OrganizationEntity>> GetOrganizationPageListAsync(Expression<Func<OrganizationEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<OrganizationEntity>>();
            RefAsync<int> total = 0;
            List<OrganizationEntity> res = await _organizationService.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取组织
        /// </summary>
        /// <returns></returns>
        public async Task<List<OrganizationEntity>> GetOrganizationListAsync(List<long> ids) {
            var res = await _organizationService.GetListAsync(p => ids.Contains(p.Id));
            return res;
        }
        /// <summary>
        /// 获取组织
        /// </summary>
        /// <returns></returns>
        public async Task<OrganizationEntity> GetOrganizationAsync(long? id) {
            OrganizationEntity res = new();
            if (id > 0) {
                res = await _organizationService.GetByIdAsync(id);
            }
            return res;
        }

        /// <summary>
        /// 保存组织
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveOrganizationAsync(OrganizationEntity entity) {
            bool res;
            if (entity.Id == 0) {
                res = await _organizationService.InsertAsync(entity);
            } else {
                res = await _organizationService.UpdateAsync(entity);
            }
            return res;
        }
        /// <summary>
        /// 删除组织
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteOrganizationAsync(List<long> ids) {
            var res = await _organizationService.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新组织状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateOrganizationStatusAsync(List<long> ids, bool status) {
            var res = await _organizationService.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion
        #region 角色
        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<RoleEntity>> GetRoleListAsync(Expression<Func<RoleEntity, bool>> whereExp) {
            var res = await _sysRoleService.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<RoleEntity>> GetRolePageListAsync(Expression<Func<RoleEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<RoleEntity>>();
            RefAsync<int> total = 0;
            List<RoleEntity> res = await _sysRoleService.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取角色
        /// </summary>
        /// <returns></returns>
        public async Task<RoleEntity> GetRoleInfoAsync(long id) {
            var res = await _sysRoleService.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存角色
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveRoleAsync(RoleEntity entity) {
            long res;
            if (entity.Id == 0) {
                var data = await _sysRoleService.InsertReturnEntityAsync(entity);
                res = data.Id;
            } else {
                await _sysRoleService.UpdateAsync(entity);
                res = entity.Id;
            }
            return res;
        }
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRoleAsync(List<long> ids) {
            var res = await _sysRoleService.DeleteByIdAsync(ids);
            return res > 0;
        }
        /// <summary>
        /// 更新角色状态
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateRoleStatusAsync(List<long> ids, bool status) {
            var res = await _sysRoleService.UpdateStatusAsync(ids, status);
            return res;
        }
        #endregion

        #region 权限
        /// <summary>
        /// 获取权限
        /// </summary>
        /// <returns></returns>
        public async Task<PermissionEntity> GetPermissionByRoleIdAsync(long roleId) {
            var res = await _permission.GetEntityByAsync(p => p.RoleId == roleId);
            return res;
        }
        /// <summary>
        /// 保存权限
        /// </summary>
        /// <returns></returns>
        public async Task<long> SavePermissionAsync(PermissionEntity entity) {
            long res;
            if (entity.Id == 0) {
                var data = await _permission.InsertReturnEntityAsync(entity);
                res = data.Id;
            } else {
                await _permission.UpdateAsync(entity);
                res = entity.Id;
            }
            return res;
        }
        /// <summary>
        /// 删除权限
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeletePermissionAsync(List<long> ids) {
            var res = await _permission.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 用户-角色
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="datas"></param>
        public async Task<bool> SaveUserRoleAsync(long roleId, List<UserRoleEntity> datas) {
            var list = await _userRoleService.GetListAsync(p => p.RoleId == roleId);
            if (list.Any()) {
                var ids = list.ConvertAll(x => x.Id);
                await _userRoleService.DeleteByIdAsync(ids);
            }
            datas.ForEach(p => p.RoleId = roleId);
            var res = await _userRoleService.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        public async Task<List<UserRoleEntity>> GetUserRoleListAsync(long roleId) {
            var list = await _userRoleService.GetListAsync(p => p.RoleId == roleId);
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        public async Task<UserRoleEntity> GetUserRoleByUserIdAsync(long userId) {
            var list = await _userRoleService.GetEntityByAsync(p => p.UserId == userId);
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        public async Task<List<UserRoleEntity>> GetUserRoleListAsync(List<long> roleIds) {
            var list = await _userRoleService.GetListAsync(p => roleIds.Contains(p.RoleId));
            return list;
        }
        #endregion
    }
}
