using DiLe.Mes.Model.Common.Process.Entity;
using DiLe.Mes.Model.Common.Product.Entity;
using DiLe.Mes.Model.Common.Product.Relation;
using DiLe.Mes.Service.Common.Process;
using DiLe.Mes.Service.Common.Product;

namespace DiLe.Mes.Application.Common {
    public class ProductClient : IApplicationClient {
        private readonly ProductInfoService _productInfo;
        private readonly ProductTypeService _productType;
        private readonly ProductInfo2ProcessAttributeService _productInfo2ProcessAttribute;
        private readonly ProductInfo2AttributeService _productInfo2Attribute;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProductClient(ProductInfoService productInfo,
                             ProductTypeService productType,
                             ProductInfo2ProcessAttributeService productInfo2ProcessAttributeService,
                             ProductInfo2AttributeService productInfo2AttributeService) {
            _productInfo = productInfo;
            _productType = productType;
            _productInfo2ProcessAttribute = productInfo2ProcessAttributeService;
            _productInfo2Attribute = productInfo2AttributeService;
        }
        #region 产品信息
        /// <summary>
        /// 获取产品信息列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<ProductInfoEntity>> GetProductInfoPageListAsync(Expression<Func<ProductInfoEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<ProductInfoEntity>>();
            RefAsync<int> total = 0;
            var res = await _productInfo.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取产品信息列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductInfoEntity>> GetProductInfoListAsync(Expression<Func<ProductInfoEntity, bool>> whereExp) {
            var res = await _productInfo.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取产品信息信息
        /// </summary>
        /// <returns></returns>
        public async Task<ProductInfoEntity> GetProductInfoInfoAsync(long id) {
            var res = await _productInfo.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存产品信息信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveProductInfoAsync(ProductInfoEntity entity) {
            long id;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("CP");
                var res = await _productInfo.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _productInfo.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除产品信息信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteProductInfoAsync(List<long> ids) {
            var res = await _productInfo.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 产品类型
        /// <summary>
        /// 获取产品类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<ProductTypeEntity>> GetProductTypePageListAsync(Expression<Func<ProductTypeEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<ProductTypeEntity>>();
            RefAsync<int> total = 0;
            var res = await _productType.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = res;
            return page;
        }
        /// <summary>
        /// 获取产品类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductTypeEntity>> GetProductTypeListAsync(Expression<Func<ProductTypeEntity, bool>> whereExp) {
            var res = await _productType.GetListAsync(whereExp);
            return res;
        }
        /// <summary>
        /// 获取产品类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<ProductTypeEntity> GetProductTypeInfoAsync(long id) {
            var res = await _productType.GetByIdAsync(id);
            return res;
        }
        /// <summary>
        /// 保存产品类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<long> SaveProductTypeAsync(ProductTypeEntity entity) {
            long id;
            if (entity.Id == 0) {
                entity.Code = CodeHandler.GetBusinessCode("CPLX");
                var res = await _productType.InsertReturnEntityAsync(entity);
                id = res != null ? res.Id : 0;
            } else {
                var res = await _productType.UpdateAsync(entity);
                id = res ? entity.Id : 0;
            }
            return id;
        }
        /// <summary>
        /// 删除产品类型信息
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteProductTypeAsync(List<long> ids) {
            var res = await _productType.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion

        #region 产品信息关联制程属性
        /// <summary>
        /// 产品信息关联制程属性
        /// </summary>
        public async Task<List<ProductInfo2ProcessAttributeEntity>> GetProductInfo2ProcessAttributeListByInfoIdAsync(long infoId) {
            var list = await _productInfo2ProcessAttribute.GetListAsync(p => p.InfoId == infoId);
            return list;
        }
        /// <summary>
        /// 添加产品信息关联制程属性
        /// </summary>
        public async Task<bool> InsertProductInfo2ProcessAttributeAsync(long infoId, List<ProductInfo2ProcessAttributeEntity> datas) {
            datas.ForEach(x => x.InfoId = infoId);
            var res = await _productInfo2ProcessAttribute.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新产品信息关联制程属性
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateProductInfo2ProcessAttributeAsync(List<ProductInfo2ProcessAttributeEntity> datas) {
            var res = await _productInfo2ProcessAttribute.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除产品信息关联制程属性
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteProductInfo2ProcessAttributeAsync(List<long> ids) {
            var res = await _productInfo2Attribute.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
        #region 产品信息关联属性
        /// <summary>
        /// 产品信息关联属性
        /// </summary>
        public async Task<List<ProductInfo2AttributeEntity>> GetProductInfo2AttributeListByInfoIdAsync(long infoId) {
            var list = await _productInfo2Attribute.GetListAsync(p => p.InfoId == infoId);
            return list;
        }
        /// <summary>
        /// 添加产品信息关联属性
        /// </summary>
        public async Task<bool> InsertProductInfo2AttributeAsync(long infoId, List<ProductInfo2AttributeEntity> datas) {
            datas.ForEach(x => x.InfoId = infoId);
            var res = await _productInfo2Attribute.InsertAsync(datas);
            return res > 0;
        }
        /// <summary>
        /// 更新产品信息关联属性
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> UpdateProductInfo2AttributeAsync(List<ProductInfo2AttributeEntity> datas) {
            var res = await _productInfo2Attribute.UpdateAsync(datas);
            return res > 0;
        }

        /// <summary>
        /// 删除产品信息关联属性
        /// </summary>
        /// <param name="ledgerId"></param>
        /// <param name="datas"></param>
        public async Task<bool> DeleteProductInfo2AttributeAsync(List<long> ids) {
            var res = await _productInfo2Attribute.DeleteByIdAsync(ids);
            return res > 0;
        }
        #endregion
    }
}
