﻿using DiLe.Mes.Model.Cloud.Dashboard.Equipment;
using DiLe.Mes.Service.Cloud.Dashboard.Equipment;

namespace DiLe.Mes.Application.Cloud.Dashboard {
    /// <summary>
    /// 设备Dashboard
    /// </summary>
    public class EquipmentDashboardClient : IApplicationClient {
        public CloudOtherPointInfoService _1311pointInfo;
        public CloudPointInfoService _pointInfo;
        public CloudAOIPointInfoService _aoipointInfo;
        public CloudEMPointInfoService _empointInfo;
        public CloudFMPointInfoService _pmpointInfo;
        public CloudTrimPointInfoService _trimlpointInfo;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="equipmentDashboard"></param>
        public EquipmentDashboardClient(CloudPointInfoService pointInfoService,
                           CloudAOIPointInfoService aOIPointInfoService,
                           CloudEMPointInfoService eMPointInfoService,
                           CloudFMPointInfoService pMPointInfoService,
                           CloudTrimPointInfoService trimlPointInfoService,
                           CloudOtherPointInfoService otherPointInfoService) {
            _pointInfo = pointInfoService;
            _aoipointInfo = aOIPointInfoService;
            _empointInfo = eMPointInfoService;
            _pmpointInfo = pMPointInfoService;
            _trimlpointInfo = trimlPointInfoService;
            _1311pointInfo = otherPointInfoService;
        }

        /// <summary>
        /// 获取成型机列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<CloudPointInfoEntity>> GetPointInfoListAsync(Expression<Func<CloudPointInfoEntity, bool>> whereExp) {
            var list = await _pointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取PLC点位数据传输-成型机
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<List<CloudOtherPointInfoEntity>> Get1311PointInfoListAsync(Expression<Func<CloudOtherPointInfoEntity, bool>> whereExp) {
            var list = await _1311pointInfo.GetListAsync(whereExp);
            return list;
        }

        /// <summary>
        /// 获取切片机列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<CloudTrimPointInfoEntity>> GetTrimlPointInfoListAsync(Expression<Func<CloudTrimPointInfoEntity, bool>> whereExp) {
            var list = await _trimlpointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取AOI列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<CloudAOIPointInfoEntity>> GetAOIPointInfoListAsync(Expression<Func<CloudAOIPointInfoEntity, bool>> whereExp) {
            var list = await _aoipointInfo.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取电能表列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<CloudEMPointInfoEntity>> GetEMPointInfoListAsync(Expression<Func<CloudEMPointInfoEntity, bool>>? whereExp = null) {
            List<CloudEMPointInfoEntity> list;
            if (whereExp == null) {
                list = await _empointInfo.GetAllListAsync();
            } else {
                list = await _empointInfo.GetListAsync(whereExp);
            }
            return list;
        }
        /// <summary>
        /// 获取流量计列表
        /// </summary>
        /// <returns></returns>
        public async Task<List<CloudFMPointInfoEntity>> GetFMPointInfoListAsync(Expression<Func<CloudFMPointInfoEntity, bool>>? whereExp = null) {
            List<CloudFMPointInfoEntity> list;
            if (whereExp == null) {
                list = await _pmpointInfo.GetAllListAsync();
            } else {
                list = await _pmpointInfo.GetListAsync(whereExp);
            }
            return list;
        }


    }

}
