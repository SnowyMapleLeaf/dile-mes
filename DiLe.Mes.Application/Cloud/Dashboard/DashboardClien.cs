﻿using DiLe.Mes.Model.Cloud.Dashboard.Entity;
using DiLe.Mes.Service.Cloud.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Application.Cloud.Dashboard {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dashboard"></param>
    public class DashboardClient(DashboardService dashboard) : IApplicationClient {
        private readonly DashboardService _dashboard = dashboard;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> DeleteDashboardAsync(Expression<Func<DashboardEntity, bool>> whereExpression) {
            var res = await _dashboard.DeleteByAsync(whereExpression);
            return res > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<bool> InsertDashboardAsync(List<DashboardEntity> dashboards) {
            var res = await _dashboard.InsertAsync(dashboards);
            return res > 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<List<DashboardEntity>> GetDashboardListAsync(Expression<Func<DashboardEntity, bool>>? whereExpression = null) {
            List<DashboardEntity>? res;
            if (whereExpression == null) {
                res = await _dashboard.GetListAsync();
            } else {
                res = await _dashboard.GetListAsync(whereExpression);
            }
            return res;
        }
    }
}
