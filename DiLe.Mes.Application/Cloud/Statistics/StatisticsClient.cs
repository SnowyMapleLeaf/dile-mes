﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Cloud.Statistics;
using DiLe.Mes.Model.Cloud.Statistics.Model;
using DiLe.Mes.Service.Cloud.Statistics;

namespace DiLe.Mes.Application.Cloud.Statistics {
    public class StatisticsClient : IApplicationClient {

        private AOIYieldRateService _aOIYieldRate;
        private EquipmentStatisticsService _equipmentStatistics;
        private EquipmentOutputStatisticsService _equipmentOutputStatistics;
        private readonly AlarmInfoStatisticsService _alarmInfoStatistics;

        private readonly EMPointStatisticsService _eMPointStatistics;
        private readonly FMPointStatisticsService _fMPointStatistics;
        private readonly AOIPointStatisticsService _aOIPointStatistics;
        private readonly TrimPointStatisticsService _trimPointStatistics;
        private readonly OtherPointStatisticsService _otherPointStatistics;
        private readonly FormingPointStatisticsService _formingPointStatistics;


        public StatisticsClient(AOIYieldRateService aOIYieldRate,
                                EquipmentStatisticsService equipmentStatistics,
                                EquipmentOutputStatisticsService equipmentOutputStatistics,
                                AlarmInfoStatisticsService alarmInfoStatistics,
                                EMPointStatisticsService eMPointStatistics,
                                FMPointStatisticsService fMPointStatistics,
                                AOIPointStatisticsService aOIPointStatistics,
                                TrimPointStatisticsService trimPointStatistics,
                                OtherPointStatisticsService otherPointStatistics,
                                FormingPointStatisticsService formingPointStatistics) {
            _aOIYieldRate = aOIYieldRate;
            _equipmentStatistics = equipmentStatistics;
            _equipmentOutputStatistics = equipmentOutputStatistics;
            _alarmInfoStatistics = alarmInfoStatistics;
            _eMPointStatistics = eMPointStatistics;
            _fMPointStatistics = fMPointStatistics;

            _aOIPointStatistics = aOIPointStatistics;
            _trimPointStatistics = trimPointStatistics;
            _otherPointStatistics = otherPointStatistics;
            _formingPointStatistics = formingPointStatistics;

        }

        #region 品质数据统计
        /// <summary>
        /// 获取保养计划列表
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<AOIYieldRateStatisticsEntity>> GetAOIYieldRateStatisticsPageListAsync(Expression<Func<AOIYieldRateStatisticsEntity, bool>> whereExpression, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<AOIYieldRateStatisticsEntity>>();
            RefAsync<int> total = 0;
            var list = await _aOIYieldRate.GetPageListAsync(whereExpression, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = list;
            return page;
        }
        /// <summary>
        /// 获取品质数据统计
        /// </summary>
        /// <returns></returns>
        public async Task<List<AOIYieldRateStatisticsEntity>> GetAOIYieldRateStatisticsListAsync(Expression<Func<AOIYieldRateStatisticsEntity, bool>>? whereExp = null) {
            List<AOIYieldRateStatisticsEntity> res;
            if (whereExp == null) {
                res = await _aOIYieldRate.GetAllListAsync();
            } else {
                res = await _aOIYieldRate.GetListAsync(whereExp);
            }
            return res;
        }

        /// <summary>
        /// 保存品质数据统计
        /// </summary>
        /// <returns></returns>
        public async Task<int> InsertAOIYieldRateStatistics(List<AOIYieldRateStatisticsEntity> aOIYields) {
            var res = await _aOIYieldRate.InsertAsync(aOIYields);
            return res;
        }
        /// <summary>
        /// 保存品质数据统计
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateAOIYieldRateStatistics(List<AOIYieldRateStatisticsEntity> aOIYields) {
            var res = await _aOIYieldRate.UpdateAsync(aOIYields);
            return res;
        }
        #endregion

        #region 成本数据统计
        /// <summary> 
        /// 获取成本数据统计
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentStatisticsEntity>> GetEquipmentUtilizationRateStatisticsList(Expression<Func<EquipmentStatisticsEntity, bool>>? whereExp = null) {
            List<EquipmentStatisticsEntity> res;
            if (whereExp == null) {
                res = await _equipmentStatistics.GetAllListAsync();
            } else {
                res = await _equipmentStatistics.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取成本数据统计
        /// </summary>
        /// <returns></returns>
        public bool DeletEquipmentStatisticsList(Expression<Func<EquipmentStatisticsEntity, bool>> whereExp) {
            var res = _equipmentStatistics.DeleteBy(whereExp);
            return res > 0;
        }
        /// <summary>
        /// 保存成本数据统计
        /// </summary>
        /// <returns></returns>
        public int InsertEquipmentStatistics(List<EquipmentStatisticsEntity> entities) {
            var res = _equipmentStatistics.Insert(entities);
            return res;
        }
        #endregion
        #region 交期数据统计
        /// <summary>
        /// 获取交期数据统计
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentOutputStatisticsEntity>> GetEquipmentOutputStatisticsList(Expression<Func<EquipmentOutputStatisticsEntity, bool>>? whereExp = null) {
            List<EquipmentOutputStatisticsEntity> res;
            if (whereExp == null) {
                res = await _equipmentOutputStatistics.GetAllListAsync();
            } else {
                res = await _equipmentOutputStatistics.GetListAsync(whereExp);
            }
            return res;
        }
        /// <summary>
        /// 获取交期数据统计
        /// </summary>
        /// <returns></returns>
        public bool DeletEquipmentOutputStatisticsList(Expression<Func<EquipmentOutputStatisticsEntity, bool>> whereExp) {
            var res = _equipmentOutputStatistics.DeleteBy(whereExp);
            return res > 0;
        }
        /// <summary>
        /// 保存交期数据统计
        /// </summary>
        /// <returns></returns>
        public int InsertEquipmentOutputStatistics(List<EquipmentOutputStatisticsEntity> entities) {
            var res = _equipmentOutputStatistics.Insert(entities);
            return res;
        }
        #endregion
        #region 报警信息
        /// <summary>
        /// 获取报警信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<AlarmInfoStatisticsEntity>> GetAlarmInfoStatisticsListAsync(Expression<Func<AlarmInfoStatisticsEntity, bool>> whereExp) {
            var list = await _alarmInfoStatistics.GetListAsync(whereExp);
            return list;
        }
        /// <summary>
        /// 获取报警信息
        /// </summary>
        /// <returns></returns>
        public async Task<PaginationModel<AlarmInfoStatisticsEntity>> GetAlarmInfoStatisticsPageListAsync(Expression<Func<AlarmInfoStatisticsEntity, bool>> whereExp, PaginationModel pagination) {
            var page = pagination.Adapt<PaginationModel<AlarmInfoStatisticsEntity>>();
            RefAsync<int> total = 0;
            var list = await _alarmInfoStatistics.GetPageListAsync(whereExp, page.PageIndex, page.PageSize, total);
            page.Total = total;
            page.Record = list;
            return page;
        }
        /// <summary>
        /// 获取报警信息-数量
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetAlarmInfoStatisticsCountAsync(Expression<Func<AlarmInfoStatisticsEntity, bool>> whereExp) {
            var res = await _alarmInfoStatistics.GetCountByAsync(whereExp);
            return res;
        }

        /// <summary>
        /// 获取报警信息报表
        /// </summary>
        public async Task<List<AlarmReportModel>> GetAlarmInfoReportAsync(List<string> dayArray, long orgId) {
            var res = await _alarmInfoStatistics.GetAlarmInfoReportAsync(dayArray, orgId);
            return res;
        }
        /// <summary>
        /// 获取报警信息列表
        /// </summary>
        public List<AlarmInfoStatisticsEntity> GetAlarmInfoReportList(List<string> signs) {
            var res = _alarmInfoStatistics.GetAlarmInfoReportList(signs);
            return res;
        }
        #endregion

        #region 统计信息
        /// <summary>
        /// 获取流量表
        /// </summary>
        /// <returns></returns>
        public async Task<List<FMPointStatisticsEntity>> GetFMPointStatisticsListAsync(Expression<Func<FMPointStatisticsEntity, bool>> whereExpression) {
            var list = await _fMPointStatistics.GetListAsync(whereExpression);
            return list;
        }
        /// <summary>
        /// 插入流量表
        /// </summary>
        /// <returns></returns>
        public async Task<int> InsertFMPointStatisticsAsync(List<FMPointStatisticsEntity> list) {
            var res = await _fMPointStatistics.InsertAsync(list);
            return res;
        }
        /// <summary>
        /// 插入流量表
        /// </summary>
        /// <returns></returns>
        public int InsertFMPointStatistics(List<FMPointStatisticsEntity> list) {
            var res = _fMPointStatistics.Insert(list);
            return res;
        }
        /// <summary>
        /// 获取电能表
        /// </summary>
        /// <returns></returns>
        public async Task<List<EMPointStatisticsEntity>> GetEMPointStatisticsListAsync(Expression<Func<EMPointStatisticsEntity, bool>> whereExpression) {
            var list = await _eMPointStatistics.GetListAsync(whereExpression);
            return list;
        }
        /// <summary>
        /// 插入电能表
        /// </summary>
        /// <returns></returns>
        public async Task<int> InsertEMPointStatisticsAsync(List<EMPointStatisticsEntity> list) {
            var res = await _eMPointStatistics.InsertAsync(list);
            return res;
        }
        /// <summary>
        /// 插入电能表
        /// </summary>
        /// <returns></returns>
        public int InsertEMPointStatistics(List<EMPointStatisticsEntity> list) {
            var res = _eMPointStatistics.Insert(list);
            return res;
        }
        
        
      
       
     
        
        #endregion
    }
}
