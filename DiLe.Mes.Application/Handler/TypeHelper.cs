﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Mould.Entity.Info;

namespace DiLe.Mes.Application.Handler {
    /// <summary>
    /// 
    /// </summary>
    public class TypeHelper {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, List<long>> GetEquipmentTypeDic(List<EquipmentTypeEntity> typelist) {
            var dic = new Dictionary<string, List<long>>();
            var temps = typelist.Where(p => p.ParentId == 0).ToList();
            foreach (var item in temps) {
                var items = GetChildEquipmentTypeList(item.Id, typelist);
                if (items.None()) {
                    continue;
                }
                items.Add(item.Id);
                dic[item.Name] = items;
            }
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<long> GetChildEquipmentTypeList(long parentId, List<EquipmentTypeEntity> typelist) {
            var list = new List<long>();
            var temps = typelist.Where(p => p.ParentId == parentId).ToList();
            foreach (var item in temps) {
                list.Add(item.Id);
                var items = GetChildEquipmentTypeList(item.Id, typelist);
                if (!items.None()) {
                    list.AddRange(items);
                }
            }
            return list;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, List<long>> GetMouldTypeDic(List<MouldTypeEntity> typelist) {
            var dic = new Dictionary<string, List<long>>();
            var temps = typelist.Where(p => p.ParentId == 0).ToList();
            foreach (var item in temps) {
                var items = GetChildMouldTypeList(item.Id, typelist);
                if (items.None()) {
                    continue;
                }
                items.Add(item.Id);
                dic[item.Name] = items;
            }
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<long> GetChildMouldTypeList(long parentId, List<MouldTypeEntity> typelist) {
            var list = new List<long>();
            var temps = typelist.Where(p => p.ParentId == parentId).ToList();
            foreach (var item in temps) {
                list.Add(item.Id);
                var items = GetChildMouldTypeList(item.Id, typelist);
                if (!items.None()) {
                    list.AddRange(items);
                }
            }
            return list;
        }

    }
}
