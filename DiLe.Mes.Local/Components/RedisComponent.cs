﻿using MapleLeaf.Core.Cache.Redis;

namespace DiLe.Mes.Local.Components {
    /// <summary>
    /// 
    /// </summary>
    public static class RedisComponent {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void AddRedisComponent(this WebApplicationBuilder builder) {
            builder.Services.AddSingleton<RedisClient>();
        }
    }
}
