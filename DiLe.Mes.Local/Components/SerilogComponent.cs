﻿using MapleLeaf.Core;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;

namespace DiLe.Mes.Local.Components {
    /// <summary>
    /// 
    /// </summary>
    public static class SerilogComponent {
        /// <summary>
        /// 
        /// </summary>
        public static void AddSerilogComponent(this WebApplicationBuilder builder) {
            string conn = AppHelper.GetConfig("ConnectionStrings", "ConnectionString");

            Log.Logger = new LoggerConfiguration()
                       .MinimumLevel.Override("Microsoft", LogEventLevel.Debug) // 排除Microsoft的日志
                       .Enrich.FromLogContext() // 注册日志上下文
                                                //.WriteTo.Console(new CompactJsonFormatter()) // 输出到控制台
                                                //.WriteTo.File
                       .CreateLogger();

            //添加日志
            builder.Host.UseSerilog(Log.Logger, dispose: true);
        }
    }
}
