﻿using DiLe.Mes.Model.Common.Organization.Entity;
using MapleLeaf.Core.AppSetting;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;
using System.Reflection;

namespace DiLe.Mes.Local.Components {
    /// <summary>
    /// 
    /// </summary>
    public static class InitTableComponent {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void AddInitTableComponent(this WebApplicationBuilder builder) {
            AppSettingModel appinfo = AppHelper.AppSettingInfo;

            if (!appinfo.SeedData.ToBoolean()) {
                return;
            }
            Assembly assembly = typeof(UserEntity).Assembly;
            Type[] allTypes = assembly.GetTypes();


            Task.Run(() => {
                Type[] localtypes = allTypes.Where(it => it.FullName!.StartsWith("DiLe.Mes.Model.Local")).ToArray();
                SqlSugarHelper.LocalDb.CodeFirst.SetStringDefaultLength(2000).InitTables(localtypes);
            });
            Task.Run(() => {
                Type[] commiontypes = allTypes.Where(it => it.FullName!.StartsWith("DiLe.Mes.Model.Common")).ToArray();
                SqlSugarHelper.MasterDb.CodeFirst.SetStringDefaultLength(2000).InitTables(commiontypes);
            });
        }
    }
}

