﻿using DiLe.Mes.Model.Common.WorkOrder.Entity;
using DiLe.Mes.Model.Common.WorkOrder.Relation;

namespace DiLe.Mes.Local.Controllers.APP.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class StartWorkRecordDto {
        /// <summary>
        /// 
        /// </summary>
        public StartWorkRecordEntity Model { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public List<StartWorkRecord2MouldInfoEntity> Relations { set; get; }
    }
}
