using DiLe.Mes.Local.Components;
using DiLe.Mes.Local.Middleware;
using Newtonsoft.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

//添加项目静态信息
AppHelper.Initialize(builder.Configuration, builder.Environment);


//builder.AddSerilogComponent();
//依赖注入
builder.AddIocComponent();
//数据库
builder.AddSqlSugarSetup();

//JWT
builder.AddAuthComponent();
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
builder.Services.AddCors(options => {
    options.AddPolicy(MyAllowSpecificOrigins, builder => {
        builder.AllowAnyMethod().SetIsOriginAllowed(_ => true).AllowAnyHeader().AllowCredentials();
    });
});


// 添加 EasyNetQ 依赖注入并配置连接字符串
builder.Services.AddEasyNetQ();
//系统启动完成后执行
builder.AddBackgroundComponent();
//获取IP
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

// Add services to the container.
builder.Services.AddControllers(options => {
    options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true;
    options.AllowEmptyInputInBodyModelBinding = true;
}).AddNewtonsoftJson(opt => {
    opt.SerializerSettings.NullValueHandling = NullValueHandling.Include;
    //大驼峰
    //opt.SerializerSettings.ContractResolver = new DefaultContractResolver();
    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
});
//注册消息推送
builder.Services.AddSignalR();

//添加Swagger
builder.AddSwaggerComponent();

//注册成服务
builder.Host.UseWindowsService();

var app = builder.Build();

AppHelper.SetServiceProvider(app.Services);
//初始化表
builder.AddInitTableComponent();

app.UseMiddleware<RequestLoggingMiddleware>();

app.UseCors(MyAllowSpecificOrigins);
//启用跨域问题
//if (app.Environment.IsDevelopment()) {
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
// 封装Swagger展示
app.UseSwaggerMilddleware();

//异常处理
app.UseMiddleware<ExceptionMiddleware>();


app.UseHttpsRedirection();
// Configure the HTTP request pipeline.
//认证中间件
app.UseAuthentication();
//授权中间件
app.UseAuthorization();

app.MapControllers();

app.Run();


