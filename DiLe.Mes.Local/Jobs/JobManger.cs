﻿namespace DiLe.Mes.Local.Jobs {
    /// <summary>
    /// 
    /// </summary>
    public class JobManger {

        private static readonly QuartzHelper _quartz = new();

        private static readonly string CRON_STR = AppHelper.GetConfig("AppSetting", "DataPushCron");

        private static readonly string DATAPUSHSTATISTICS_CRON_STR = AppHelper.GetConfig("AppSetting", "DataPushStatisticsCron");
        private static readonly string DEALWITHEQUIPMENTSTATISTICS_CRON_STR = AppHelper.GetConfig("AppSetting", "DealwithEquipmentStatisticsCron");
        private static readonly string DELIVERY_CRON_STR = AppHelper.GetConfig("AppSetting", "DeliveryCron");
        /// <summary>
        /// 推送数据
        /// </summary>
        public static void StartDataPushJob() {
            _quartz.StartJob<DataPushJob>("DataPushJob", "DataPushGroup", CRON_STR);
        }

        /// <summary>
        /// 推送
        /// </summary>
        public static void StartDataPushStatisticsJob() {
            _quartz.StartJob<DataPushStatisticsJob>("DataPushStatisticsJob", "DataPushStatisticsGroup", DATAPUSHSTATISTICS_CRON_STR);
        }
        /// <summary>
        /// 处理数据
        /// </summary>
        public static void DealwithEquipmentOperationJob() {
            _quartz.StartJob<DayEquipmentOperationJob>("DayEquipmentOperationJob", "DayEquipmentOperationGroup", "0 0 8 * * ?");
            _quartz.StartJob<NightEquipmentOperationJob>("NightEquipmentOperationJob", "NightEquipmentOperationGroup", "0 0 20 * * ?");
        }

        /// <summary>
        /// 推送报警信息
        /// </summary>
        public static void DataPushAlarmInfo() {
            _quartz.StartJob<DataPushAlarmJob>("DataPushAlarmJob", "DataPushAlarmGroup", DATAPUSHSTATISTICS_CRON_STR);
        }
        /// <summary>
        /// 定时推送
        /// </summary>
        public static void StartDeliveryJob() {
            _quartz.StartJob<DeliveryYesterDayJob>("YesterDayJob", "YesterDayGroup", DELIVERY_CRON_STR);
            _quartz.StartJob<PointStatisticsJob>("PointStatisticsJob", "PointStatisticsGroup", "0 0 8 * * ?");
        }
    }
}
