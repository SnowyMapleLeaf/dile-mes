﻿using DiLe.Mes.Model.Common.APP;
using Quartz;

namespace DiLe.Mes.Local.Jobs {
    /// <summary>
    /// 
    /// </summary>
    [DisallowConcurrentExecution]
    public class DayEquipmentOperationJob : IJob {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Execute(IJobExecutionContext context) {
            await SetDayEquipmentOperation();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task SetDayEquipmentOperation() {

            DateTime currentDate = DateTime.Now.Date.AddHours(8);

            var where = Expressionable.Create<EquipmentOperationEntity>();
            where.And(p => p.CreateTime < currentDate);
            where.And(p => p.IsFinish == false || p.EndTime == null);

            var list = SqlSugarHelper.MasterDb.Queryable<EquipmentOperationEntity>().Where(where.ToExpression()).ToList();
            if (list.None()) {
                return;
            }
            var dataMap = list.GroupBy(x => x.DeviceNo).Select(x => (x.Key, x.OrderBy(y => y.CreateTime).Last())).ToList();

            DateTime operationDate = DateTime.Now.Date.AddDays(-1);
            DateTime startDate = DateTime.Now.Date.AddHours(7);


            var inserts = new List<EquipmentOperationEntity>();
            var updates = new List<EquipmentOperationEntity>();
            foreach (var item in dataMap) {
                var up = item.Item2;
                bool check = await CheckEquipmentStatisticsAsync(item.Key, operationDate, startDate);
                if (!check) {
                    continue;
                }
                var inp = up.Adapt<EquipmentOperationEntity>();
                inp.StartTime = DateTime.Now.Date.AddHours(8).AddSeconds(1);
                inp.Id = 0;
                inp.IsFinish = false;
                inp.CreateTime = null;
                inp.UpdateTime = null;
                inp.EndTime = null;
                inp.OperationDateTime = DateTime.Now.Date;
                inserts.Add(inp);
                up.IsFinish = true;
                up.EndTime = DateTime.Now.Date.AddHours(8).AddSeconds(-1);
                updates.Add(up);

            }
            if (!inserts.None()) {
                await SqlSugarHelper.MasterDb.Insertable(inserts).ExecuteCommandAsync();
            }
            if (!updates.None()) {
                await SqlSugarHelper.MasterDb.Updateable(updates).ExecuteCommandAsync();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceno"></param>
        /// <param name="operationDate"></param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public async Task<bool> CheckEquipmentStatisticsAsync(string deviceno, DateTime operationDate, DateTime startDate) {


            var where = Expressionable.Create<EquipmentOperationEntity>();
            where.And(p => p.DeviceNo == deviceno);
            where.And(p => p.CreateTime <= startDate);
            where.And(p => p.OperationDateTime == operationDate);

            var count = await SqlSugarHelper.MasterDb.Queryable<EquipmentOperationEntity>().Where(where.ToExpression()).CountAsync();
            return count > 0;
        }
    }
}
