﻿using MapleLeaf.Core.Const;
using MapleLeaf.Core.Handler;
using MapleLeaf.Core.Swagger;

namespace DiLe.Mes.Local.Middleware {
    /// <summary>
    /// 
    /// </summary>
    public static class SwaggerMiddleware {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        public static void UseSwaggerMilddleware(this IApplicationBuilder app) {
            ArgumentNullException.ThrowIfNull(app);

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                //根据版本名称倒序 遍历展示
                SwaggerExtensions.GetLocalSwaggerApiInfos().OrderBy(e => e.Order).ToList().ForEach(version => {
                    c.SwaggerEndpoint($"/swagger/{version.UrlPrefix}/swagger.json", $"{version.Name}");
                });

            });
        }
    }
}
