﻿namespace DiLe.Mes.Service.Dto {
    /// <summary>
    /// 设备信息
    /// </summary>
    public class MouldInfoModel {
        public MouldInfoModel() { }
        /// <summary>
        /// 模具主键
        /// </summary>
        public long MouldId { get; set; }
        /// <summary>
        /// 模具名称
        /// </summary>
        public string MouldName { get; set; }
        /// <summary>
        /// 模具编号
        /// </summary>
        public string MouldCode { get; set; }
        /// <summary>
        /// 模具类型
        /// </summary>
        public long MouldTypeId { get; set; }
        /// <summary>
        /// 模具类型名称
        /// </summary>
        public string MouldTypeName { get; set; }
        /// <summary>
        /// 开工记录
        /// </summary>
        public long StartWorkRecordId { get; set; }
        /// <summary>
        /// 模具穴数
        /// </summary>
        public int CavityNum { get; set; } = 0;

    }
}
