﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Service.Dto {
    public class RepairRecordModel {
        /// <summary>
        /// 故障类型
        /// </summary>
        public long? FaultType { get; set; }
        /// <summary>
        /// 故障类型
        /// </summary>
        public string? FaultTypeName { get; set; }
        /// <summary>
        /// 工单状态
        /// </summary>
        public string? WorkOrderStatus { get; set; }

    }
}
