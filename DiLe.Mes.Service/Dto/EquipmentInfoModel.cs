﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Service.Dto {
    /// <summary>
    /// 设备信息
    /// </summary>
    public class EquipmentInfoModel {
        public EquipmentInfoModel() { }
        /// <summary>
        /// 设备主键
        /// </summary>
        public long EquipmentId { get; set; }
        /// <summary>
        /// 位置信息
        /// </summary>
        public long? PositionId { get; set; }
        /// <summary>
        /// 工厂主键
        /// </summary>
        public long? FactoryId { get; set; }
        
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 设备编号
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification { get; set; }
        /// <summary>
        /// 设备类型
        /// </summary>
        public long EquipmentTypeId { get; set; }
        /// <summary>
        /// 设备类型名称
        /// </summary>
        public string EquipmentTypeName { get; set; }
        /// <summary>
        /// 设备标识
        /// </summary>
        public string EquipmentSign { get; set; }

        /// <summary>
        /// 日班标准工时
        /// </summary>
        public decimal DayStandardHour { get; set; }
        /// <summary>
        /// 夜班标准工时
        /// </summary>
        public decimal NightStandardHour { get; set; }

    }
}
