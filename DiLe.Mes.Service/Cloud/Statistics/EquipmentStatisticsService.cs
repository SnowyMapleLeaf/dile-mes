﻿using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentStatisticsService(ISqlSugarClient db) : BaseRepository<EquipmentStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {
    }
}
