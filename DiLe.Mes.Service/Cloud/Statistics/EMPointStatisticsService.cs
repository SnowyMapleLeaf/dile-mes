﻿using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 
    /// </summary>
    public class EMPointStatisticsService(ISqlSugarClient db) : BaseRepository<EMPointStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
