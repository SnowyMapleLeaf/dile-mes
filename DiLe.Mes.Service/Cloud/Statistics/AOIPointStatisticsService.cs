﻿using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// AOI
    /// </summary>
    /// <param name="db"></param>
    public class AOIPointStatisticsService(ISqlSugarClient db) : BaseRepository<AOIPointStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
