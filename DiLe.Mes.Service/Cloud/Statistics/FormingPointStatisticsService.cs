﻿using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 成型机
    /// </summary>
    /// <param name="db"></param>
    public class FormingPointStatisticsService(ISqlSugarClient db) : BaseRepository<FormingPointStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
