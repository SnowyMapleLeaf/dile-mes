﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 
    /// </summary>
    public class FMPointStatisticsService(ISqlSugarClient db) : BaseRepository<FMPointStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
