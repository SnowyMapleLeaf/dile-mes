﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentOutputStatisticsService(ISqlSugarClient db) : BaseRepository<EquipmentOutputStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
