﻿using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 1311-成型机
    /// </summary>
    /// <param name="db"></param>
    public class OtherPointStatisticsService(ISqlSugarClient db) : BaseRepository<OtherPointStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
