﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Cloud.Statistics.Model;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="db"></param>
    public class AlarmInfoStatisticsService(ISqlSugarClient db) : BaseRepository<AlarmInfoStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {


        /// <summary>
        /// 获取报警信息报表
        /// </summary>
        public async Task<List<AlarmReportModel>> GetAlarmInfoReportAsync(List<string> dayArray, long orgId) {
            var sql = @"SELECT count(*) count,abnormaldate ,equipmentcode 
                        FROM  statistics_alarminfo 
                        WHERE  factoryid = @orgid
                         AND   abnormaldate  in (@day)
                        GROUP BY abnormaldate, equipmentcode";
            var list = await Context.Ado.SqlQueryAsync<AlarmReportModel>(sql, new { day = dayArray, orgid = orgId });

            return list;

        }
        /// <summary>c
        /// 获取报警信息报表
        /// </summary>
        public List<AlarmInfoStatisticsEntity> GetAlarmInfoReportList(List<string> codes) {
            try {
                List<string> sqlarr = [];
                foreach (var code in codes) {
                    sqlarr.Add($@" (select * from cloud.statistics_alarminfo  where equipmentcode = '{code}'  order by createtime desc LIMIT 5 offset 0)");
                }
                var sql = string.Join(" UNION ALL ", sqlarr);
                var list = db.Ado.SqlQuery<AlarmInfoStatisticsEntity>(sql).ToList();
                return list;
            } catch (Exception ex) {
                var a = ex.Message;
                return new List<AlarmInfoStatisticsEntity>();
            }
        }

    }
}
