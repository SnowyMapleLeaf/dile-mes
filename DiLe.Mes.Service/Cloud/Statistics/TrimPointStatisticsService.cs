﻿using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 切箍边
    /// </summary>
    /// <param name="db"></param>
    public class TrimPointStatisticsService(ISqlSugarClient db) : BaseRepository<TrimPointStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
