﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;

namespace DiLe.Mes.Service.Cloud.Statistics {
    /// <summary>
    /// 
    /// </summary>
    public class AOIYieldRateService(ISqlSugarClient db) : BaseRepository<AOIYieldRateStatisticsEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
