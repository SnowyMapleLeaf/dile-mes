﻿using DiLe.Mes.Model.Cloud.Dashboard.Equipment;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Cloud.Dashboard.Equipment {
    /// <summary>
    /// 
    /// </summary>
    public class CloudTrimPointInfoService(ISqlSugarClient db) : BaseRepository<CloudTrimPointInfoEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
