﻿using DiLe.Mes.Model.Cloud.Dashboard.Equipment;

namespace DiLe.Mes.Service.Cloud.Dashboard.Equipment {
    /// <summary>
    /// 
    /// </summary>
    public class CloudEMPointInfoService(ISqlSugarClient db) : BaseRepository<CloudEMPointInfoEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
