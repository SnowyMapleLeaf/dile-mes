﻿using DiLe.Mes.Model.Cloud.Dashboard.Equipment;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Cloud.Dashboard.Equipment {
    /// <summary>
    /// 
    /// </summary>
    public class CloudAOIPointInfoService(ISqlSugarClient db) : BaseRepository<CloudAOIPointInfoEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {

    }
}
