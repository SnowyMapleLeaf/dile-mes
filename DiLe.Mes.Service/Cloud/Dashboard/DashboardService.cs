﻿using DiLe.Mes.Model.Cloud.Dashboard.Entity;

namespace DiLe.Mes.Service.Cloud.Dashboard {
    public class DashboardService(ISqlSugarClient db) : BaseRepository<DashboardEntity>(db, DataBaseConfigIdConst.Cloud), IScoped {
    }
}
