﻿using DiLe.Mes.Model.Common.APP;

namespace DiLe.Mes.Service.APP {
    /// <summary>
    /// 设备状态
    /// </summary>
    public class EquipmentStatusService(ISqlSugarClient db) : BaseRepository<EquipmentStatusInfoEntity>(db), IScoped {

    }
}
