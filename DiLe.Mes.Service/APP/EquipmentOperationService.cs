﻿using DiLe.Mes.Model.Common.APP;

namespace DiLe.Mes.Service.APP {
    /// <summary>
    /// 设备运行
    /// </summary>
    public class EquipmentOperationService(ISqlSugarClient db) : BaseRepository<EquipmentOperationEntity>(db), IScoped {

    }
}
