﻿using DiLe.Mes.Model.Local.Abnormal.Entity;

namespace DiLe.Mes.Service.Local.Abnormal {
    /// <summary>
    /// 报警信息
    /// </summary>
    public class AlarmInfoService(ISqlSugarClient db) : BaseRepository<AlarmInfoEntity>(db, DataBaseConfigIdConst.Local), IScoped {

    }
}
