﻿using DiLe.Mes.Model.Local.Abnormal.Entity;

namespace DiLe.Mes.Service.Local.Abnormal {
    /// <summary>
    /// 报警临时信息
    /// </summary>
    public class AlarmTempInfoService(ISqlSugarClient db) : BaseRepository<AlarmTempInfoEntity>(db, DataBaseConfigIdConst.Local), IScoped {

    }
}
