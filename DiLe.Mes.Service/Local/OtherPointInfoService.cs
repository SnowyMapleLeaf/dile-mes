﻿using DiLe.Mes.Model.Local.Entity;

namespace DiLe.Mes.Service.Local {
    /// <summary>
    /// 
    /// </summary>
    public class OtherPointInfoService(ISqlSugarClient db) : BaseRepository<OtherPointInfoEntity>(db, DataBaseConfigIdConst.Local), IScoped {

    }
}
