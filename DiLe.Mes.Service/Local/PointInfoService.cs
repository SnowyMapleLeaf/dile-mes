﻿using DiLe.Mes.Model.Local.Entity;

namespace DiLe.Mes.Service.Local {
    /// <summary>
    /// 
    /// </summary>
    public class PointInfoService(ISqlSugarClient db) : BaseRepository<PointInfoEntity>(db, DataBaseConfigIdConst.Local), IScoped {
    }
}
