﻿using DiLe.Mes.Model.Local.Entity;

namespace DiLe.Mes.Service.Local {
    /// <summary>
    /// 
    /// </summary>
    public class EMPointInfoService(ISqlSugarClient db) : BaseRepository<EMPointInfoEntity>(db, DataBaseConfigIdConst.Local), IScoped {

    }
}
