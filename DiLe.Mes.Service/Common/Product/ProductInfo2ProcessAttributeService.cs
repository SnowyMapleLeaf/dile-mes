﻿using DiLe.Mes.Model.Common.Product.Relation;

namespace DiLe.Mes.Service.Common.Product {
    /// <summary>
    /// 产品信息和制程属性
    /// </summary>
    public class ProductInfo2ProcessAttributeService(ISqlSugarClient db) : BaseRepository<ProductInfo2ProcessAttributeEntity>(db), IScoped {

    }
}
