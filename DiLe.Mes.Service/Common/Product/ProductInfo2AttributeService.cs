﻿using DiLe.Mes.Model.Common.Product.Relation;

namespace DiLe.Mes.Service.Common.Product {
    /// <summary>
    /// 产品信息和产品属性
    /// </summary>
    public class ProductInfo2AttributeService(ISqlSugarClient db) : BaseRepository<ProductInfo2AttributeEntity>(db), IScoped {

    }
}
