﻿using DiLe.Mes.Model.Common.Product.Entity;

namespace DiLe.Mes.Service.Common.Product {
    /// <summary>
    /// 产品信息
    /// </summary>
    public class ProductInfoService(ISqlSugarClient db) : BaseRepository<ProductInfoEntity>(db), IScoped {

    }
}
