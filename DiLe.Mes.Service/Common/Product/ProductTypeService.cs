﻿using DiLe.Mes.Model.Common.Product.Entity;

namespace DiLe.Mes.Service.Common.Product {
    /// <summary>
    /// 产品类型
    /// </summary>
    public class ProductTypeService(ISqlSugarClient db) : BaseRepository<ProductTypeEntity>(db), IScoped {

    }
}
