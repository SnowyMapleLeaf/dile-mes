﻿using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;

namespace DiLe.Mes.Service.Common.Equipment.Maintenance {
    /// <summary>
    /// 保养记录
    /// </summary>
    public class MaintenanceRecordService(ISqlSugarClient db) : BaseRepository<MaintenanceRecordEntity>(db), IScoped {
    }
}
