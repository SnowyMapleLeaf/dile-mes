﻿using DiLe.Mes.Model.Common.Equipment.Relation.Maintenance;

namespace DiLe.Mes.Service.Common.Equipment.Maintenance {
    /// <summary>
    /// 保养记录和项目
    /// </summary>
    public class MaintenanceRecord2ProjectService(ISqlSugarClient db) : BaseRepository<MaintenanceRecord2ProjectEntity>(db), IScoped {
    }
}
