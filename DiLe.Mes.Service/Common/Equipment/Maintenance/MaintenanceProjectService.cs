﻿using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;

namespace DiLe.Mes.Service.Common.Equipment.Maintenance {
    /// <summary>
    /// 保养项目
    /// </summary>
    public class MaintenanceProjectService(ISqlSugarClient db) : DataBaseRepository<MaintenanceProjectEntity>(db), IScoped {
    }
}
