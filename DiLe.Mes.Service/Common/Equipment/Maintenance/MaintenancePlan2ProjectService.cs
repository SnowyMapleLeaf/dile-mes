﻿using DiLe.Mes.Model.Common.Equipment.Relation.Maintenance;

namespace DiLe.Mes.Service.Common.Equipment.Maintenance {
    /// <summary>
    /// 保养项目
    /// </summary>
    public class MaintenancePlan2ProjectService(ISqlSugarClient db) : BaseRepository<MaintenancePlan2ProjectEntity>(db), IScoped {
    }
}
