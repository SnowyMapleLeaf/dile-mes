﻿using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;

namespace DiLe.Mes.Service.Common.Equipment.Maintenance {
    /// <summary>
    /// 保养计划
    /// </summary>
    public class MaintenancePlanService(ISqlSugarClient db) : BaseRepository<MaintenancePlanEntity>(db), IScoped {
    }
}
