﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;

namespace DiLe.Mes.Service.Common.Equipment.Info {
    /// <summary>
    /// 设备档案
    /// </summary>
    public class EquipmentInfoService(ISqlSugarClient db) : BaseRepository<EquipmentInfoEntity>(db), IScoped {

    }
}
