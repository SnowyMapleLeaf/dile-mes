﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.Info {
    /// <summary>
    /// 设备状态
    /// </summary>
    public class EquipmentStatusService(ISqlSugarClient db) : BaseRepository<EquipmentStatusEntity>(db), IScoped {
    }
}
