﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.Info {
    /// <summary>
    /// 设备类型
    /// </summary>
    public class EquipmentTypeService(ISqlSugarClient db) : DataBaseRepository<EquipmentTypeEntity>(db), IScoped {
    }
    

}
