﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Service.Dto;

namespace DiLe.Mes.Service.Common.Equipment.Info {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentRegisterService(ISqlSugarClient db) : BaseRepository<EquipmentRegisterEntity>(db), IScoped {

        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListAsync(long orgid) {
            var list = await Context.Queryable<EquipmentRegisterEntity>()
                                 .LeftJoin<EquipmentInfoEntity>((r, i) => r.EquipmentInfoId == i.Id)
                                 .LeftJoin<EquipmentTypeEntity>((r, i, t) => i.TypeId == t.Id)
                                 .Where((r, i, t) => i.FactoryId == orgid)
                                 .Select((r, i, t) => new EquipmentInfoModel {
                                     EquipmentId = i.Id,
                                     PositionId = i.PositionId,
                                     FactoryId = i.FactoryId,
                                     EquipmentCode = i.Code,
                                     EquipmentTypeId = i.TypeId,
                                     EquipmentTypeName = t.Name,
                                     Specification = i.Specification,
                                     EquipmentName = i.Name,
                                     EquipmentSign = r.EquipmentSign,
                                     DayStandardHour = i.DayStandardHour ?? 0,
                                     NightStandardHour = i.NightStandardHour ?? 0
                                 })
                                 .ToListAsync();
            return list;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public List<EquipmentInfoModel> GetEquipmentInfoModelList(long orgid) {
            var list = Context.Queryable<EquipmentRegisterEntity>()
                                 .LeftJoin<EquipmentInfoEntity>((r, i) => r.EquipmentInfoId == i.Id)
                                 .LeftJoin<EquipmentTypeEntity>((r, i, t) => i.TypeId == t.Id)
                                 .Where((r, i, t) => i.FactoryId == orgid)
                                 .Select((r, i, t) => new EquipmentInfoModel {
                                     EquipmentId = i.Id,
                                     PositionId = i.PositionId,
                                     FactoryId = i.FactoryId,
                                     EquipmentCode = i.Code,
                                     EquipmentTypeId = i.TypeId,
                                     EquipmentTypeName = t.Name,
                                     Specification = i.Specification,
                                     EquipmentName = i.Name,
                                     EquipmentSign = r.EquipmentSign,
                                     DayStandardHour = i.DayStandardHour ?? 0,
                                     NightStandardHour = i.NightStandardHour ?? 0
                                 })
                                 .ToList();
            return list;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public List<EquipmentInfoModel> GetEquipmentInfoModelList() {
            var list = Context.Queryable<EquipmentRegisterEntity>()
                                 .LeftJoin<EquipmentInfoEntity>((r, i) => r.EquipmentInfoId == i.Id)
                                 .LeftJoin<EquipmentTypeEntity>((r, i, t) => i.TypeId == t.Id)
                                 .Select((r, i, t) => new EquipmentInfoModel {
                                     EquipmentId = i.Id,
                                     PositionId = i.PositionId,
                                     FactoryId = i.FactoryId,
                                     EquipmentCode = i.Code,
                                     EquipmentTypeId = i.TypeId,
                                     EquipmentTypeName = t.Name,
                                     Specification = i.Specification,
                                     EquipmentName = i.Name,
                                     EquipmentSign = r.EquipmentSign,
                                     DayStandardHour = i.DayStandardHour ?? 0,
                                     NightStandardHour = i.NightStandardHour ?? 0
                                 })
                                 .ToList();
            return list;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListAsync() {
            var list = await Context.Queryable<EquipmentRegisterEntity>()
                                 .LeftJoin<EquipmentInfoEntity>((r, i) => r.EquipmentInfoId == i.Id)
                                 .LeftJoin<EquipmentTypeEntity>((r, i, t) => i.TypeId == t.Id)
                                 .Select((r, i, t) => new EquipmentInfoModel {
                                     EquipmentId = i.Id,
                                     PositionId = i.PositionId,
                                     FactoryId = i.FactoryId,
                                     EquipmentCode = i.Code,
                                     EquipmentTypeId = i.TypeId,
                                     EquipmentTypeName = t.Name,
                                     Specification = i.Specification,
                                     EquipmentName = i.Name,
                                     EquipmentSign = r.EquipmentSign,
                                     DayStandardHour = i.DayStandardHour ?? 0,
                                     NightStandardHour = i.NightStandardHour ?? 0
                                 })
                                 .ToListAsync();
            return list;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListBySign(List<string> signlist) {
            var list = await Context.Queryable<EquipmentRegisterEntity>()
                                 .LeftJoin<EquipmentInfoEntity>((r, i) => r.EquipmentInfoId == i.Id)
                                 .LeftJoin<EquipmentTypeEntity>((r, i, t) => i.TypeId == t.Id)
                                 .Where((r, i, t) => signlist.Contains(r.EquipmentSign))
                                 .Select((r, i, t) => new EquipmentInfoModel {
                                     EquipmentId = i.Id,
                                     PositionId = i.PositionId,
                                     FactoryId = i.FactoryId,
                                     EquipmentCode = i.Code,
                                     EquipmentTypeId = i.TypeId,
                                     EquipmentTypeName = t.Name,
                                     Specification = i.Specification,
                                     EquipmentName = i.Name,
                                     EquipmentSign = r.EquipmentSign,
                                     DayStandardHour = i.DayStandardHour ?? 0,
                                     NightStandardHour = i.NightStandardHour ?? 0
                                 })
                                 .ToListAsync();
            return list;
        }
        /// <summary>
        /// 根据设备标记获取设备信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<EquipmentInfoModel>> GetEquipmentInfoModelListByCode(List<string> codes) {
            var list = await Context.Queryable<EquipmentRegisterEntity>()
                                 .LeftJoin<EquipmentInfoEntity>((r, i) => r.EquipmentInfoId == i.Id)
                                 .LeftJoin<EquipmentTypeEntity>((r, i, t) => i.TypeId == t.Id)
                                 .Where((r, i, t) => codes.Contains(i.Code))
                                 .Select((r, i, t) => new EquipmentInfoModel {
                                     EquipmentId = i.Id,
                                     PositionId = i.PositionId,
                                     FactoryId = i.FactoryId,
                                     EquipmentCode = i.Code,
                                     EquipmentTypeId = i.TypeId,
                                     EquipmentTypeName = t.Name,
                                     Specification = i.Specification,
                                     EquipmentName = i.Name,
                                     EquipmentSign = r.EquipmentSign,
                                     DayStandardHour = i.DayStandardHour ?? 0,
                                     NightStandardHour = i.NightStandardHour ?? 0
                                 })
                                 .ToListAsync();
            return list;
        }
    }
}
