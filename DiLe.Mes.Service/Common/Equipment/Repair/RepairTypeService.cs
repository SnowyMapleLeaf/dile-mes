﻿using DiLe.Mes.Model.Common.Equipment.Entity.Repair;

namespace DiLe.Mes.Service.Common.Equipment.Repair {
    /// <summary>
    /// 维修类型
    /// </summary>
    public class RepairTypeService(ISqlSugarClient db) : DataBaseRepository<RepairTypeEntity>(db), IScoped {
    }
}
