﻿using DiLe.Mes.Model.Common.Equipment.Entity.Repair;
using DiLe.Mes.Service.Dto;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.Repair {
    /// <summary>
    /// 维修记录
    /// </summary>
    public class RepairRecordService(ISqlSugarClient db) : DataBaseRepository<RepairRecordEntity>(db), IScoped {


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<RepairRecordModel>> GetRepairRecordModelListAsync() {

            var list = await Context.Queryable<RepairRecordEntity>()
                               .LeftJoin<RepairTypeEntity>((r, i) => r.FaultType == i.Id)
                               .Select((r, i) => new RepairRecordModel {
                                   FaultType = i.Id,
                                   FaultTypeName = i.Name,
                                   WorkOrderStatus = r.WorkOrderStatus
                               })
                               .ToListAsync();
            return list;
        }

    }
}
