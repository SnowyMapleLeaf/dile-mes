﻿using DiLe.Mes.Model.Common.Equipment.Relation.Repair;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.Repair
{
    /// <summary>
    /// 维修记录关联备件
    /// </summary>
    public class RepairRecord2SparePartService(ISqlSugarClient db) : BaseRepository<RepairRecord2SparePartEntity>(db), IScoped {
    }
}
