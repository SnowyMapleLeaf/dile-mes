﻿using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.SparePart
{
    /// <summary>
    /// 出库关联备件
    /// </summary>
    public class StockOutSparePartService(ISqlSugarClient db) : BaseRepository<StockOutSparePartEntity>(db), IScoped {
    }
}
