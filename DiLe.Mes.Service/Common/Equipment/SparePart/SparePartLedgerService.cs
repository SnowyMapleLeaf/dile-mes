﻿using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Service.Common.Equipment.SparePart
{
    /// <summary>
    /// 备件台账
    /// </summary>
    public class SparePartLedgerService(ISqlSugarClient db) : BaseRepository<SparePartLedgerEntity>(db), IScoped {
    }
}
