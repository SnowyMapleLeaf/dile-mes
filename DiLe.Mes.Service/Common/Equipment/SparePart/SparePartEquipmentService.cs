﻿using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.SparePart
{
    /// <summary>
    /// 备件关联设备
    /// </summary>
    public class SparePartEquipmentService(ISqlSugarClient db) : BaseRepository<SparePartEquipmentEntity>(db), IScoped {
    }
}
