﻿using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.SparePart
{
    /// <summary>
    /// 备件出库
    /// </summary>
    public class SparePartStockOutService(ISqlSugarClient db) : BaseRepository<SparePartStockOutEntity>(db), IScoped {
    }
}
