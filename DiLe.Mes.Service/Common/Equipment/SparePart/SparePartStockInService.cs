﻿using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Equipment.SparePart
{
    /// <summary>
    /// 备件入库
    /// </summary>
    public class SparePartStockInService(ISqlSugarClient db) : BaseRepository<SparePartStockInEntity>(db), IScoped {
    }
}
