﻿using DiLe.Mes.Model.Common.Process.Entity;

namespace DiLe.Mes.Service.Common.Process {
    /// <summary>
    /// 工序
    /// </summary>
    public class ProcessService(ISqlSugarClient db) : DataBaseRepository<ProcessEntity>(db), IScoped {

    }
}
