﻿using DiLe.Mes.Model.Common.Mould.Relation.Maintenance;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Maintenance
{
    /// <summary>
    /// 保养记录和项目
    /// </summary>
    public class MouldMaintenanceRecord2ProjectService(ISqlSugarClient db) : BaseRepository<MouldMaintenanceRecord2ProjectEntity>(db), IScoped {
    }
}
