﻿using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Maintenance
{
    /// <summary>
    /// 保养项目
    /// </summary>
    public class MouldMaintenanceProjectService(ISqlSugarClient db) : BaseRepository<MouldMaintenanceProjectEntity>(db), IScoped {
    }
}
