﻿using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Maintenance {
    /// <summary>
    /// 保养记录
    /// </summary>
    public class MouldMaintenanceRecordService(ISqlSugarClient db) : BaseRepository<MouldMaintenanceRecordEntity>(db), IScoped {
    }
}
