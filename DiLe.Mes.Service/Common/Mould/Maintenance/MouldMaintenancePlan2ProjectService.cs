﻿using DiLe.Mes.Model.Common.Mould.Relation.Maintenance;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Maintenance
{
    /// <summary>
    /// 保养项目
    /// </summary>
    public class MouldMaintenancePlan2ProjectService(ISqlSugarClient db) : BaseRepository<MouldMaintenancePlan2ProjectEntity>(db), IScoped {
    }
}
