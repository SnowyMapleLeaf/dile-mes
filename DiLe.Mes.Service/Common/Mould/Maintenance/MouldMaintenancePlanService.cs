﻿using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Maintenance
{
    /// <summary>
    /// 保养计划
    /// </summary>
    public class MouldMaintenancePlanService(ISqlSugarClient db) : BaseRepository<MouldMaintenancePlanEntity>(db), IScoped {
    }
}
