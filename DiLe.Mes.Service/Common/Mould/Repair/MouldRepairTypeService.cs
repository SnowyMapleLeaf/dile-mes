﻿using DiLe.Mes.Model.Common.Mould.Entity.Repair;

namespace DiLe.Mes.Service.Common.Mould.Repair {
    /// <summary>
    /// 维修类型
    /// </summary>
    public class MouldRepairTypeService(ISqlSugarClient db) : DataBaseRepository<MouldRepairTypeEntity>(db), IScoped {
    }
}
