﻿using DiLe.Mes.Model.Common.Mould.Relation.Repair;

namespace DiLe.Mes.Service.Common.Mould.Repair {
    /// <summary>
    /// 维修记录关联备件
    /// </summary>
    public class MouldRepairRecord2SparePartService(ISqlSugarClient db) : BaseRepository<MouldRepairRecord2SparePartEntity>(db), IScoped {
    }
}
