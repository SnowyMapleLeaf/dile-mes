﻿using DiLe.Mes.Model.Common.Mould.Entity.Repair;
using DiLe.Mes.Service.Dto;

namespace DiLe.Mes.Service.Common.Mould.Repair {
    /// <summary>
    /// 维修记录
    /// </summary>
    public class MouldRepairRecordService(ISqlSugarClient db) : BaseRepository<MouldRepairRecordEntity>(db), IScoped {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<RepairRecordModel>> GetMouldRepairRecordModelListAsync() {

            var list = await Context.Queryable<MouldRepairRecordEntity>()
                               .LeftJoin<MouldRepairTypeEntity>((r, i) => r.FaultType == i.Id)
                               .Select((r, i) => new RepairRecordModel {
                                   FaultType = i.Id,
                                   FaultTypeName = i.Name,
                                   WorkOrderStatus = r.WorkOrderStatus
                               })
                               .ToListAsync();
            return list;
        }
    }
}
