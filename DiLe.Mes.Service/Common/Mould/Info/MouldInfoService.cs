﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Info
{
    /// <summary>
    /// 模具档案
    /// </summary>
    public class MouldInfoService(ISqlSugarClient db) : BaseRepository<MouldInfoEntity>(db), IScoped {
    }
}
