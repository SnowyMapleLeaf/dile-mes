﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Info
{
    /// <summary>
    /// 设备类型
    /// </summary>
    public class MouldTypeService(ISqlSugarClient db) : DataBaseRepository<MouldTypeEntity>(db), IScoped {
    }
}
