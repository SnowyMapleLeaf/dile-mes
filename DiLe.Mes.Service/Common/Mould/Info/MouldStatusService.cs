﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Mould.Info
{
    /// <summary>
    /// 模具状态
    /// </summary>
    public class MouldStatusService(ISqlSugarClient db) : DataBaseRepository<MouldStatusEntity>(db), IScoped {
    }
}
