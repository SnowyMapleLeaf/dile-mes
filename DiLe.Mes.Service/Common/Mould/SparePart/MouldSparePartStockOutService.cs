﻿using DiLe.Mes.Model.Common.Mould.Entity.SparePart;

namespace DiLe.Mes.Service.Common.Mould.SparePart {
    /// <summary>
    /// 备件出库
    /// </summary>
    public class MouldSparePartStockOutService(ISqlSugarClient db) : BaseRepository<MouldSparePartStockOutEntity>(db), IScoped {
    }
}
