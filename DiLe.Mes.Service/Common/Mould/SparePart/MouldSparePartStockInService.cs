﻿using DiLe.Mes.Model.Common.Mould.Entity.SparePart;

namespace DiLe.Mes.Service.Common.Mould.SparePart {
    /// <summary>
    /// 备件入库
    /// </summary>
    public class MouldSparePartStockInService(ISqlSugarClient db) : BaseRepository<MouldSparePartStockInEntity>(db), IScoped {
    }
}
