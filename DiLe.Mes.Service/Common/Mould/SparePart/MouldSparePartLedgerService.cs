﻿using DiLe.Mes.Model.Common.Mould.Entity.SparePart;

namespace DiLe.Mes.Service.Common.Mould.SparePart {
    /// <summary>
    /// 备件台账
    /// </summary>
    public class MouldSparePartLedgerService(ISqlSugarClient db) : BaseRepository<MouldSparePartLedgerEntity>(db), IScoped {
    }
}
