﻿using DiLe.Mes.Model.Common.Mould.Relation.SparePart;

namespace DiLe.Mes.Service.Common.Mould.SparePart {
    /// <summary>
    /// 出库关联备件
    /// </summary>
    public class MouldStockOut2SparePartService(ISqlSugarClient db) : BaseRepository<MouldStockOut2SparePartEntity>(db), IScoped {
    }
}
