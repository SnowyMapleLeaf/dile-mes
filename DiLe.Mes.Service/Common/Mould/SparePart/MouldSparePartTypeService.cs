﻿using DiLe.Mes.Model.Common.Mould.Entity.SparePart;

namespace DiLe.Mes.Service.Common.Mould.SparePart {
    /// <summary>
    /// 备件类型
    /// </summary>
    public class MouldSparePartTypeService(ISqlSugarClient db) : DataBaseRepository<MouldSparePartTypeEntity>(db), IScoped {
    }
}
