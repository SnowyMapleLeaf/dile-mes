﻿using DiLe.Mes.Model.Common.Mould.Relation.SparePart;

namespace DiLe.Mes.Service.Common.Mould.SparePart {
    /// <summary>
    /// 备件关联模具
    /// </summary>
    public class MouldSparePart2MouldInfoService(ISqlSugarClient db) : BaseRepository<MouldSparePart2MouldInfoEntity>(db), IScoped {
    }
}
