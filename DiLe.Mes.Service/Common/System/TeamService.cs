﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 班组管理
    /// </summary>
    public class TeamService(ISqlSugarClient db) : DataBaseRepository<TeamEntity>(db), IScoped {

    }
}
