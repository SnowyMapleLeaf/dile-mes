﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 供应商
    /// </summary>
    public class SupplierService(ISqlSugarClient db) : DataBaseRepository<SupplierEntity>(db), IScoped {

    }
}
