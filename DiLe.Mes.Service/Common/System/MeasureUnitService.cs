﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 计量单位
    /// </summary>
    public class MeasureUnitService(ISqlSugarClient db) : BaseRepository<MeasureUnitEntity>(db), IScoped {

    }
}
