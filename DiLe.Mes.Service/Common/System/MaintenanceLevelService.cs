﻿using DiLe.Mes.Model.Common.System.Entity;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 保养级别
    /// </summary>
    public class MaintenanceLevelService(ISqlSugarClient db) : DataBaseRepository<MaintenanceLevelEntity>(db), IScoped {

    }
}
