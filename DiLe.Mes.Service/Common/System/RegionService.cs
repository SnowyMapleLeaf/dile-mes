﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 区域
    /// </summary>
    public class RegionService(ISqlSugarClient db) : BaseRepository<RegionEntity>(db), IScoped {

    }
}
