﻿using DiLe.Mes.Model.Common.System.Entity;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 存放位置
    /// </summary>
    public class PositionService(ISqlSugarClient db) : DataBaseRepository<PositionEntity>(db), IScoped {
    }
}
