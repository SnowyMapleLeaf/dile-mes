﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 仓库管理
    /// </summary>
    public class WarehouseService(ISqlSugarClient db) : DataBaseRepository<WarehouseEntity>(db), IScoped {


    }
}
