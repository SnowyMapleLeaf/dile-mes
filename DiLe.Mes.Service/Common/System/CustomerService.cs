﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 客户
    /// </summary>
    public class CustomerService(ISqlSugarClient db) : BaseRepository<CustomerEntity>(db), IScoped {

    }
}
