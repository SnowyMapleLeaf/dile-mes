﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 执行规则
    /// </summary>
    public class ExecuteRuleService(ISqlSugarClient db) : DataBaseRepository<ExecuteRuleEntity>(db), IScoped {

    }
}
