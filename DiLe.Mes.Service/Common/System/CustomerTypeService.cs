﻿using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 客户类型
    /// </summary>
    public class CustomerTypeService(ISqlSugarClient db) : BaseRepository<CustomerTypeEntity>(db), IScoped {

    }
}
