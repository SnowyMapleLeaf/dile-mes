﻿using DiLe.Mes.Model.Common;

namespace DiLe.Mes.Service.Common.System {
    /// <summary>
    /// 备件库存
    /// </summary>
    public class SparePartInventoryService(ISqlSugarClient db) : BaseRepository<SparePartInventoryEntity>(db), IScoped {

    }
}
