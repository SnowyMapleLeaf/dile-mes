﻿using DiLe.Mes.Model.Common.Organization.Entity;

namespace DiLe.Mes.Service.Common.Organization {
    /// <summary>
    /// 权限管理
    /// </summary>
    public class PermissionService(ISqlSugarClient db) : BaseRepository<PermissionEntity>(db), IScoped {

    }
}
