﻿using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.Organization.Relation;

namespace DiLe.Mes.Service.Common.Organization {
    /// <summary>
    /// 
    /// </summary>
    public class SysRoleService(ISqlSugarClient db) : DataBaseRepository<RoleEntity>(db), IScoped {
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<List<RoleEntity>> GetRoleListByUserId(long userId) {
            var roles = await Context.Queryable<RoleEntity>()
                .InnerJoin<UserRoleEntity>((i, r) => i.Id == r.RoleId)
                .Where((i, r) => r.UserId == userId).ToListAsync();
            return roles;
        }
    }
}
