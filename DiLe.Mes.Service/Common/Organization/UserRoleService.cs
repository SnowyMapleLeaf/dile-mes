﻿using DiLe.Mes.Model.Common.Organization.Relation;

namespace DiLe.Mes.Service.Common.Organization {
    /// <summary>
    /// 班组管理
    /// </summary>
    public class UserRoleService(ISqlSugarClient db) : BaseRepository<UserRoleEntity>(db), IScoped {

    }
}
