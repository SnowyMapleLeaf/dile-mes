﻿using DiLe.Mes.Model.Common.Organization.Entity;
using MapleLeaf.Core.Dto;

namespace DiLe.Mes.Service.Common.Organization {
    /// <summary>
    /// 
    /// <inheritdoc cref="ISysUserService"/>
    /// </summary>
    public class SysUserService(ISqlSugarClient db) : DataBaseRepository<UserEntity>(db), IScoped {
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<UserInfo> CheckUserInfoByAccount(string account, string password) {
            var user = await Context.Queryable<UserEntity>()
                                                    .Where(p => p.Account == account && p.Password == password)
                                                    .Select(p => new UserInfo {
                                                        Id = p.Id,
                                                        Name = p.Name,
                                                        Account = p.Account,
                                                        OrgId = p.DepartmentId
                                                    })
                                                    .FirstAsync();
            return user;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task ResetPasswordAsync(List<long> ids) {
            await Context.Updateable<UserEntity>()
                                    .SetColumns(x => x.Password == "888888")
                                    .Where(it => ids.Contains(it.Id))
                                    .ExecuteCommandAsync();

        }
    }

}
