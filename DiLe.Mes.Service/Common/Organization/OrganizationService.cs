﻿using DiLe.Mes.Model.Common.Organization.Entity;

namespace DiLe.Mes.Service.Common.Organization {
    /// <summary>
    /// 
    /// </summary>
    public class OrganizationService(ISqlSugarClient db) : DataBaseRepository<OrganizationEntity>(db), IScoped {

    }
}
