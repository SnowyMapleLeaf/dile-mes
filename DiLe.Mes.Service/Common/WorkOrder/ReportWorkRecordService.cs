﻿using DiLe.Mes.Model.Common.WorkOrder.Entity;

namespace DiLe.Mes.Service.Common.WorkOrder {
    /// <summary>
    /// 
    /// </summary>
    public class ReportWorkRecordService(ISqlSugarClient db) : BaseRepository<ReportWorkRecordEntity>(db), IScoped {
    }
}
