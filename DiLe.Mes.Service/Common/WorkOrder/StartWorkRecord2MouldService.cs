﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.WorkOrder.Relation;
using DiLe.Mes.Service.Dto;

namespace DiLe.Mes.Service.Common.WorkOrder {
    /// <summary>
    /// 
    /// </summary>
    public class StartWorkRecord2MouldInfoService(ISqlSugarClient db) : BaseRepository<StartWorkRecord2MouldInfoEntity>(db), IScoped {

        /// <summary>
        /// 根据获取模具信息
        /// </summary>
        /// <returns></returns>
        public async Task<List<MouldInfoModel>> GetMouldInfoModelList(List<long> ids) {
            var list = await Context.Queryable<StartWorkRecord2MouldInfoEntity>()
                                 .LeftJoin<MouldInfoEntity>((r, i) => r.MouldId == i.Id)
                                 .LeftJoin<MouldTypeEntity>((r, i, t) => i.TypeId == t.Id)
                                 .Where(r => ids.Contains(r.StartWorkRecordId))
                                 .Select((r, i, t) => new MouldInfoModel {
                                     MouldId = i.Id,
                                     MouldCode = i.Code,
                                     MouldTypeId = i.TypeId,
                                     MouldTypeName = t.Name,
                                     MouldName = i.Name,
                                     StartWorkRecordId = r.StartWorkRecordId,
                                     CavityNum = i.CavityNum,
                                 })
                                 .ToListAsync();
            return list;
        }



    }
}
