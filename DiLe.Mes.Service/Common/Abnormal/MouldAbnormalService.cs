﻿using DiLe.Mes.Model.Common.Abnormal.Entity;

namespace DiLe.Mes.Service.Common.Abnormal {
    /// <summary>
    /// 模具异常
    /// </summary>
    public class MouldAbnormalService(ISqlSugarClient db) : BaseRepository<MouldAbnormalEntity>(db), IScoped {

    }
}
