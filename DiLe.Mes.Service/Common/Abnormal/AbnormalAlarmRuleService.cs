﻿using DiLe.Mes.Model.Common.Abnormal.Entity;

namespace DiLe.Mes.Service.Common.Abnormal {
    /// <summary>
    /// 异常报警规则
    /// </summary>
    public class AbnormalAlarmRuleService(ISqlSugarClient db) : BaseRepository<AbnormalAlarmRuleEntity>(db), IScoped {
    }
}
