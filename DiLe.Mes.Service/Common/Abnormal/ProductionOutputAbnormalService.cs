﻿using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Service.Interface;
using MapleLeaf.DataBase.Repository;

namespace DiLe.Mes.Service.Common.Abnormal {

    /// <summary>
    /// 产出量异常
    /// </summary>
    public class ProductionOutputAbnormalService(ISqlSugarClient db) : BaseRepository<ProductionOutputAbnormalEntity>(db), IScoped {
    
    }
}
