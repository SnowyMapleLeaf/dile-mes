﻿using DiLe.Mes.Model.Common.Abnormal.Entity;

namespace DiLe.Mes.Service.Common.Abnormal {
    /// <summary>
    /// 产品品质异常
    /// </summary>
    public class ProductionQualityAbnormalService(ISqlSugarClient db) : BaseRepository<ProductionQualityAbnormalEntity>(db), IScoped {

    }
}
