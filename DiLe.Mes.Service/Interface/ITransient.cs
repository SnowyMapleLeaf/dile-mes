﻿namespace DiLe.Mes.Service.Interface {
    /// <summary>
    /// 暂时生存期服务是每次从服务容器进行请求时创建的。
    /// 这种生存期适合轻量级、 无状态的服务。
    /// 在处理请求的应用中，在请求结束时会释放暂时服务。
    /// 通常我们使用 ITransient 接口依赖表示该生命周期。
    /// </summary>
    public interface ITransient {

    }
}