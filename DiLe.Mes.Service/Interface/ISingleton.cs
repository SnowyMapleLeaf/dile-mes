﻿namespace DiLe.Mes.Service.Interface {
    /// <summary>
    /// 在首次请求它们时进行创建，之后每个后续请求都使用相同的实例。
    /// 通常我们使用 ISingleton 接口依赖表示该生命周期。
    /// </summary>
    public interface ISingleton {

    }
}