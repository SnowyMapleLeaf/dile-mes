﻿namespace DiLe.Mes.Service.Interface {
    /// <summary>
    /// 作用域生存期服务针对每个客户端请求（连接）创建一次。
    /// 在处理请求的应用中，
    /// 在请求结束时会释放有作用域的服务。
    /// 通常我们使用 IScoped 接口依赖表示该生命周期。
    /// </summary>
    public interface IScoped {

    }
}