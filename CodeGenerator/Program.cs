﻿using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.WorkOrder.Entity;
using MapleLeaf.Core.EventBus;

namespace CodeGenerator {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine("----------代码生成器----------");
            var dic = new Dictionary<string, string>();

            dic.Add(typeof(MouldAbnormalEntity).Name, "模具异常");
            dic.Add(typeof(ProductionOutputAbnormalEntity).Name, "产出量异常");
            dic.Add(typeof(ProductionQualityAbnormalEntity).Name, "产品品质异常");

            foreach (var item in dic) {
                var file = ControllerCodeFactory.Create("Common", "Abnormal", item.Key, item.Value);
                Console.WriteLine($"file:{file}");
            }
            var file3 = ClientCodeFactory.Create("Common", "Abnormal", "Abnormal", dic);
            Console.WriteLine(file3);
            var startDate = DateTime.Now.Date.AddDays(-1);


            Console.ReadKey();
        }
    }
}