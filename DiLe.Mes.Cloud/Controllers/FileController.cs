﻿using DiLe.Mes.Application.Cloud.Statistics;
using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Cloud.Statistics;
using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.APP;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.Process.Entity;
using MapleLeaf.Core.Cryptogram;
using MapleLeaf.Core.Filter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.StaticFiles;
using MiniExcelLibs;
using MiniExcelLibs.OpenXml;
using Newtonsoft.Json.Linq;

namespace DiLe.Mes.Cloud.Controllers {
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class FileController : ApiBaseController {



        private readonly IWebHostEnvironment _webHostEnvironment;
        /// <summary>
        /// 
        /// </summary>
        public FileController(IWebHostEnvironment webHostEnvironment) {
            _webHostEnvironment = webHostEnvironment;
        }
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task<ApiResult<string>> UploadFileAsync(IFormFile file) {
            string fileOid = CommonHandle.GenerateUniqueId();
            string rootPath = Path.Combine(_webHostEnvironment.ContentRootPath, $"wwwroot\\TemplateFiles\\{fileOid}");
            if (!Directory.Exists(rootPath)) {
                Directory.CreateDirectory(rootPath);
            }
            if (file.Length > 0) {
                var filePath = Path.Combine(rootPath, file.FileName);
                using Stream fileStream = new FileStream(filePath, FileMode.Create);
                await file.CopyToAsync(fileStream);
            }
            return Success<string>(fileOid);
        }
        /// <summary>
        /// 获取文件
        /// </summary>
        /// <param name="fileOid"></param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public async Task<FileStreamResult?> GetFileAsync(string fileOid) {
            string rootPath = Path.Combine(_webHostEnvironment.ContentRootPath, $"wwwroot\\TemplateFiles\\{fileOid}");

            DirectoryInfo folder = new DirectoryInfo(rootPath);
            var file = folder.GetFiles()?.FirstOrDefault();
            if (file == null) {
                return null;
            }

            var stream = System.IO.File.OpenRead(file.FullName);
            string contentType = await GetFileContentTypeAsync(file.FullName);
            // 设置响应头信息
            HttpContext.Response.Headers.AccessControlExposeHeaders = "Content-Disposition";
            return File(stream, contentType, file.FullName);
        }
        /// <summary>
        /// 获取文件ContentType
        /// </summary>
        /// <param name="fileName">文件名称</param>
        /// <returns></returns>
        private async static Task<string> GetFileContentTypeAsync(string fileName) {
            return await Task.Run(() => {
                string suffix = Path.GetExtension(fileName);
                var provider = new FileExtensionContentTypeProvider();
                var contentType = provider.Mappings[suffix];
                return contentType;
            });
        }
    }


}
