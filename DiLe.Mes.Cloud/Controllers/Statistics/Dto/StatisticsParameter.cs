﻿namespace DiLe.Mes.Cloud.Controllers.Statistics.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class StatisticsParameter {
        /// <summary>
        /// 
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// FORMING:成型机 TRIM:切箍边机  AOI:AOI/CCD  STACKING：堆叠机
        /// </summary>
        public string? Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long? OrgId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public static Dictionary<string, object> StatisticsTypeDic = new() { { "FORMING", "成型机" }, { "TRIM", "切箍边机" }, { "AOI", "AOI" }, { "STACKING", "堆叠机" } };
    }
    /// <summary>
    /// 
    /// </summary>
    public class PointStatisticsPageParameter: PointStatisticsParameter {
        /// <summary>
        /// 
        /// </summary>
        public PaginationModel Pagination { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PointStatisticsParameter {
        /// <summary>
        /// 
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndDate { get; set; }
    }
}
