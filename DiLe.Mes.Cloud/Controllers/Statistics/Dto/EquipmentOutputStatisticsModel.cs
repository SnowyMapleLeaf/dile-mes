﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;

namespace DiLe.Mes.Cloud.Controllers.Statistics.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentOutputStatisticsModel : EquipmentOutputStatisticsEntity {
        /// <summary>
        /// 产出数量
        /// </summary>
        public int OutputQuantity { get; set; } = 0;
        /// <summary>
        /// 模次
        /// </summary>
        public int MouldTimes { get; set; } = 0;
    }
}
