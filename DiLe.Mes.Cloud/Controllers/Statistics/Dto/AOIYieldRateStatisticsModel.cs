﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Cloud.Statistics;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Service.Dto;

namespace DiLe.Mes.Cloud.Controllers.Statistics.Dto {
    /// <summary>
    /// 良率
    /// </summary>
    public class AOIYieldRateStatisticsModel : AOIYieldRateStatisticsEntity {
        /// <summary>
        /// NG数量
        /// </summary>
        public int? NGNumber { get; set; }
        /// <summary>
        /// 检验数量
        /// </summary>
        public int InspectionQuantity { get; set; }
        /// <summary>
        /// 良率
        /// </summary>
        public decimal AOIYieldRate { get; set; }
        /// <summary>
        /// 良品数量
        /// </summary>
        public int YieldQuantity { get; set; }
        /// <summary>
        /// 不良率
        /// </summary>
        public decimal AOIBadRate { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StatisticsDto<T> {
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<T> DataArr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Sum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal UsageRate { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UtilizationRateParamter {
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentInfoModel> Infos { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentStatisticsEntity> Statisticslist { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<long>> Dic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentTypeEntity> TypeList { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class StatisticsParamter<T> {
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentInfoModel> Infos { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<T> Statisticslist { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<long>> Dic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentTypeEntity> TypeList { get; set; }
    }
}
