﻿using DiLe.Mes.Model.Cloud.Statistics;

namespace DiLe.Mes.Cloud.Controllers.Statistics.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentUtilizationRateStatisticsModel : EquipmentStatisticsEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        [SugarColumn(ColumnDescription = "设备名称")]
        public string EquipmentName { set; get; }
        /// <summary>
        /// 稼动率
        /// </summary>
        public decimal UtilizationRate { get; set; } = 0;

        /// <summary>
        /// 标准工时
        /// </summary>
        public decimal StandardWorkingHour { get; set; }
        /// <summary>
        /// 利用率
        /// </summary>
        public decimal UsageRate { get; set; } = 0;

    }
}
