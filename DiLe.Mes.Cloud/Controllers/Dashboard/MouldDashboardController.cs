﻿using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Cloud.Controllers.Dashboard.Dto;
using DiLe.Mes.Model.Common.Mould.Entity.Info;
using Microsoft.AspNetCore.Authorization;

namespace DiLe.Mes.Cloud.Controllers.Dashboard
{
    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Dashboard)]
    public class MouldDashboardController : ApiBaseController {

        private readonly MouldInfoClient _mouldInfoClient;

        /// <summary>
        /// 
        /// </summary>
        public MouldDashboardController(MouldInfoClient mouldInfoClient) {
            _mouldInfoClient = mouldInfoClient;
        }
        /// <summary>
        /// 获取模具Dashboard列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult<MouldDashboardDto>> GetMouldDashboard() {
            var dto = new MouldDashboardDto();
            //模具
            var eqList = await _mouldInfoClient.GetMouldInfoListAsync();
            //模具类型
            var typeList = await _mouldInfoClient.GetMouldTypeListAsync();
            var dic = TypeHelper.GetMouldTypeDic(typeList);
            dto.MouldNumList = GetMouldNumModels(dic, eqList);
            dto.MouldRepairList = GetMouldRepairModels(dic, eqList);
            dto.MouldPlanList = GetMouldPlanModels(dic, eqList);
            dto.MouldRecordList = GetMouldRecordModels(dic, eqList);
            return Success(dto);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static List<MouldNumModel> GetMouldNumModels(Dictionary<string, List<long>> dic, List<MouldInfoEntity> datalist) {
            var list = new List<MouldNumModel>();
            foreach (var item in dic) {
                var infos = datalist.Where(x => item.Value.Contains(x.TypeId)).ToList();
                foreach (var info in infos) {
                    var m = info.Adapt<MouldNumModel>();
                    m.Type = item.Key;
                    list.Add(m);
                }
            }
            return list;
        }
        /// <summary>
        /// 维修记录
        /// </summary>
        /// <returns></returns>
        private static List<MouldRepairModel> GetMouldRepairModels(Dictionary<string, List<long>> dic, List<MouldInfoEntity> datalist) {
            var list = new List<MouldRepairModel>();
            foreach (var item in dic) {
                var infos = datalist.Where(x => item.Value.Contains(x.TypeId)).ToList();
                foreach (var info in infos) {
                    var m = info.Adapt<MouldRepairModel>();
                    m.Type = item.Key;
                    list.Add(m);
                }
            }
            return list;
        }
        /// <summary>
        /// 保养计划
        /// </summary>
        /// <returns></returns>
        private static List<MPlanStatistic> GetMouldPlanModels(Dictionary<string, List<long>> dic, List<MouldInfoEntity> datalist) {
            var list = new List<MPlanStatistic>();
            foreach (var item in dic) {
                var infos = datalist.Where(x => item.Value.Contains(x.TypeId)).ToList();
                foreach (var info in infos) {
                    var m = info.Adapt<MPlanStatistic>();
                    m.Type = item.Key;
                    list.Add(m);
                }
            }
            return list;
        }

        /// <summary>
        /// 保养记录
        /// </summary>
        /// <returns></returns>
        private static List<MRecordStatistic> GetMouldRecordModels(Dictionary<string, List<long>> dic, List<MouldInfoEntity> datalist) {
            var list = new List<MRecordStatistic>();
            foreach (var item in dic) {
                var infos = datalist.Where(x => item.Value.Contains(x.TypeId)).ToList();
                foreach (var info in infos) {
                    var m = info.Adapt<MRecordStatistic>();
                    m.Type = item.Key;
                    list.Add(m);
                }
            }
            return list;
        }


    }
}
