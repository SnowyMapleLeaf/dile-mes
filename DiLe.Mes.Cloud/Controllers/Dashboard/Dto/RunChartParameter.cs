﻿using DiLe.Mes.Cloud.Controllers.Statistics.Dto;

namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class RunChartParameter : StatisticsParameter {

        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class AOIYieldRateRunChartDto {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<AOIYieldRateStatisticsModel> DataList { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class OutputRunChartDto {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentOutputStatisticsModel> DataList { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UtilizationRateRunChartDto {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentUtilizationRateStatisticsModel> DataList { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UsageRateRunChartDto {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentUtilizationRateStatisticsModel> DataList { get; set; }
    }

}
