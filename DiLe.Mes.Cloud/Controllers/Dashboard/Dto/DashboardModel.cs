﻿using DiLe.Mes.Model.Cloud.Dashboard.Equipment;

namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class DashboardModel {
        /// <summary>
        /// 
        /// </summary>
        public string ChartType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> XAxis { set; get; } = new List<string>();
        /// <summary>
        /// 
        /// </summary>
        public List<DataItem> DataArr { set; get; } = new List<DataItem>();
        /// <summary>
        /// 
        /// </summary>
        public List<MinMaxItem> MinMaxArr { set; get; } = new List<MinMaxItem>();
    }
    /// <summary>
    /// 
    /// </summary>
    public class MinMaxItem {
        /// <summary>
        /// 
        /// </summary>
        public int Min { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Max { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class DataItem {
        /// <summary>
        /// 
        /// </summary>
        public int MinMaxIndex { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Suffix { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public List<double?> Data { set; get; } = new List<double?>();
    }
    /// <summary>
    /// 
    /// </summary>
    public class CloudPointInfoModel : CloudPointInfoEntity {
        #region 设备信息
        /// <summary>
        /// 设备编号
        /// </summary>
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { set; get; }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification { set; get; }
        /// <summary>
        /// 设备类型
        /// </summary>
        public string EquipmentType { set; get; }
        /// <summary>
        /// 所在工厂
        /// </summary>
        public string FactoryName { set; get; }
        /// <summary>
        /// 存放位置
        /// </summary>
        public string EquipmentPosition { set; get; }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        public int OrderNum { set; get; }
        /// <summary>
        /// 左侧热压上模温度1
        /// </summary>
        public string LeftUpperTemp1 { set; get; } = "0";
        /// <summary>
        /// 左侧热压上模温度2
        /// </summary>
        public string LeftUpperTemp2 { set; get; } = "0";
        /// <summary>
        /// 左侧热压上模温度3
        /// </summary>
        public string LeftUpperTemp3 { set; get; } = "0";
        /// <summary>
        /// 左侧热压上模温度4
        /// </summary>
        public string LeftUpperTemp4 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度1
        /// </summary>
        public string LeftDownTemp1 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度2
        /// </summary>
        public string LeftDownTemp2 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度3
        /// </summary>
        public string LeftDownTemp3 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度4
        /// </summary>
        public string LeftDownTemp4 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度1
        /// </summary>
        public string RightUpperTemp1 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度2
        /// </summary>
        public string RightUpperTemp2 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度3
        /// </summary>
        public string RightUpperTemp3 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度4
        /// </summary>
        public string RightUpperTemp4 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度1
        /// </summary>
        public string RightDownTemp1 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度2
        /// </summary>
        public string RightDownTemp2 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度3
        /// </summary>
        public string RightDownTemp3 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度4
        /// </summary>
        public string RightDownTemp4 { set; get; } = "0";
    }
    /// <summary>
    /// 
    /// </summary>
    public class CloudOtherPointInfoModel : CloudOtherPointInfoEntity {
        #region 设备信息
        /// <summary>
        /// 设备编号
        /// </summary>
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { set; get; }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification { set; get; }
        /// <summary>
        /// 设备类型
        /// </summary>
        public string EquipmentType { set; get; }
        /// <summary>
        /// 所在工厂
        /// </summary>
        public string FactoryName { set; get; }
        /// <summary>
        /// 存放位置
        /// </summary>
        public string EquipmentPosition { set; get; }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public int OrderNum { set; get; }
        /// <summary>
        /// 左侧热压上模温度1
        /// </summary>
        public string LeftUpperTemp1 { set; get; } = "0";
        /// <summary>
        /// 左侧热压上模温度2
        /// </summary>
        public string LeftUpperTemp2 { set; get; } = "0";
        /// <summary>
        /// 左侧热压上模温度3
        /// </summary>
        public string LeftUpperTemp3 { set; get; } = "0";
        /// <summary>
        /// 左侧热压上模温度4
        /// </summary>
        public string LeftUpperTemp4 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度1
        /// </summary>
        public string LeftDownTemp1 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度2
        /// </summary>
        public string LeftDownTemp2 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度3
        /// </summary>
        public string LeftDownTemp3 { set; get; } = "0";
        /// <summary>
        /// 左侧热压下模温度4
        /// </summary>
        public string LeftDownTemp4 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度1
        /// </summary>
        public string RightUpperTemp1 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度2
        /// </summary>
        public string RightUpperTemp2 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度3
        /// </summary>
        public string RightUpperTemp3 { set; get; } = "0";
        /// <summary>
        /// 右侧热压上模温度4
        /// </summary>
        public string RightUpperTemp4 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度1
        /// </summary>
        public string RightDownTemp1 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度2
        /// </summary>
        public string RightDownTemp2 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度3
        /// </summary>
        public string RightDownTemp3 { set; get; } = "0";
        /// <summary>
        /// 右侧热压下模温度4
        /// </summary>
        public string RightDownTemp4 { set; get; } = "0";
    }


    /// <summary>
    /// 
    /// </summary>
    public class CloudTrimPointInfoModel : CloudTrimPointInfoEntity {
        #region 设备信息
        /// <summary>
        /// 设备编号
        /// </summary>
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { set; get; }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification { set; get; }
        /// <summary>
        /// 设备类型
        /// </summary>
        public string EquipmentType { set; get; }
        /// <summary>
        /// 所在工厂
        /// </summary>
        public string FactoryName { set; get; }
        /// <summary>
        /// 存放位置
        /// </summary>
        public string EquipmentPosition { set; get; }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public int OrderNum { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CloudAOIPointInfoModel : CloudAOIPointInfoEntity {
        #region 设备信息
        /// <summary>
        /// 设备编号
        /// </summary>
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { set; get; }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification { set; get; }
        /// <summary>
        /// 设备类型
        /// </summary>
        public string EquipmentType { set; get; }
        /// <summary>
        /// 所在工厂
        /// </summary>
        public string FactoryName { set; get; }
        /// <summary>
        /// 存放位置
        /// </summary>
        public string EquipmentPosition { set; get; }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public int OrderNum { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CloudEMPointInfoModel : CloudEMPointInfoEntity {
        /// <summary>
        /// 
        /// </summary>
        public int OrderNum { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CloudFMPointInfoModel : CloudFMPointInfoEntity {
        /// <summary>
        /// 
        /// </summary>
        public int OrderNum { set; get; }
    }

}
