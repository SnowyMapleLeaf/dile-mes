﻿namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class MouldDashboardDto {
        /// <summary>
        /// 
        /// </summary>
        public List<MouldNumModel> MouldNumList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MouldRepairModel> MouldRepairList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MPlanStatistic> MouldPlanList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MRecordStatistic> MouldRecordList { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>

    public class MouldNumModel {
        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 模具寿命
        /// </summary>
        public int MouldLife { get; set; } = 0;
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 累计模次
        /// </summary>
        public int TotalNum { get; set; }
        /// <summary>
        /// 剩余模次
        /// </summary>
        public int SurplusNum { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldRepairModel {
        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 维修单号
        /// </summary>
        public string RepairCode { get; set; }

        /// <summary>
        /// 维修负责人
        /// </summary>
        public string RepairManager { get; set; }
        /// <summary>
        /// 维修开始时间
        /// </summary>
        public DateTime RepairSatrtDate { get; set; }
        /// <summary>
        /// 维修结束时间
        /// </summary>
        public DateTime RepairEndDate { get; set; }
        /// <summary>
        /// 维修内容
        /// </summary>
        public string RepairInfo { get; set; }
    }
}
