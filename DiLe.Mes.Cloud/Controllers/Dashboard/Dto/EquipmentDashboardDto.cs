﻿using DiLe.Mes.Model.Cloud.Dashboard.Equipment;

namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentDashboardDto {
        /// <summary>
        /// 成型机
        /// </summary>
        public List<CloudPointInfoModel> PointInfoList { set; get; }
        /// <summary>
        /// 成型机-1311
        /// </summary>
        public List<CloudOtherPointInfoModel> OtherPointInfoList { set; get; }
        /// <summary>
        /// 切边机
        /// </summary>
        public List<CloudTrimPointInfoModel> TrimlPointInfoList { set; get; }
        /// <summary>
        /// AOI、CCD
        /// </summary>
        public List<CloudAOIPointInfoModel> AOIPointInfoList { set; get; }
        /// <summary>
        /// 电能表
        /// </summary>
        public List<CloudEMPointInfoModel> EMPointInfoList { set; get; }
        /// <summary>
        /// 流量计
        /// </summary>
        public List<CloudFMPointInfoModel> PMPointInfoList { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentDashboardModel {
        /// <summary>
        /// 
        /// </summary>
        public List<EAStatistic> AlarmData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentRepairModel> EquipmentRepairList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EPlanStatistic> EquipmentPlanList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ERecordStatistic> EquipmentRecordList { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EquipmentRepairModel {
        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string EquipmentCode { get; set; }

        /// <summary>
        /// 维修单号
        /// </summary>
        public string RepairCode { get; set; }

        /// <summary>
        /// 维修负责人
        /// </summary>
        public string RepairManager { get; set; }
        /// <summary>
        /// 维修开始时间
        /// </summary>
        public DateTime? RepairSatrtDate { get; set; }
        /// <summary>
        /// 维修结束时间
        /// </summary>
        public DateTime? RepairEndDate { get; set; }
        /// <summary>
        /// 维修内容
        /// </summary>
        public string RepairInfo { get; set; }
    }




}
