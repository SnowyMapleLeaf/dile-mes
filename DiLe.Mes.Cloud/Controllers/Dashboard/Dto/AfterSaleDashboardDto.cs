﻿namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto {
    /// <summary>
    /// 售服Dashboard
    /// </summary>
    public class AfterSaleDashboardDto {
        /// <summary>
        /// 
        /// </summary>
        public Statistic Statistic { get; set; }
        /// <summary>
        /// 设备报警
        /// </summary>
        public List<EquipAbnormalItem> EquipAbnormal { get; set; }
        /// <summary>
        /// 设备故障报警分布
        /// </summary>
        public List<EquipFaultSpreadItem> EquipFaultSpread { get; set; }
        /// <summary>
        /// 设备报警
        /// </summary>
        public EquipWarning EquipWarning { get; set; }
        /// <summary>
        /// 设备保养计划
        /// </summary>
        public EquipUpkeepStatus EquipUpkeepStatus { get; set; }
        /// <summary>
        /// 设备保养任务
        /// </summary>
        public EquipUkeepTask EquipUkeepTask { get; set; }
        /// <summary>
        /// 设备任务状态
        /// </summary>
        public List<UpkeepTaskStatusItem> EquipUpkeepTaskStatus { get; set; }
        /// <summary>
        /// 模具状态
        /// </summary>
        public List<MoldStateArrItem> MoldStateArr { get; set; }
        /// <summary>
        /// 模具故障
        /// </summary>
        public List<MoldFaultArrItem> MoldFaultArr { get; set; }
        /// <summary>
        /// 模具报警
        /// </summary>
        public List<MoldWarningArrItem> MoldWarningArr { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MouldUpkeepStatus MouldUpkeepStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MouldUkeepTask MouldUkeepTask { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<UpkeepTaskStatusItem> MouldUpkeepTaskStatus { get; set; }
    }
    /// <summary>
    /// 统计
    /// </summary>
    public class Statistic {
        /// <summary>
        /// 设备总数
        /// </summary>
        public string EquipTotal { get; set; }
        /// <summary>
        /// 设备维修
        /// </summary>
        public string EquipRepair { get; set; }
        /// <summary>
        /// 设备保养
        /// </summary>
        public string EquipUpkeep { get; set; }
        /// <summary>
        /// 模具总数
        /// </summary>
        public string MoldTotal { get; set; }
        /// <summary>
        /// 模具维修
        /// </summary>
        public string MoldRepair { get; set; }
        /// <summary>
        /// 模具保养
        /// </summary>
        public string MoldUpkeep { get; set; }
    }
    /// <summary>
    /// 设备报警
    /// </summary>
    public class EquipAbnormalItem {
        /// <summary>
        /// A厂房设备一故障,请及时处理
        /// </summary>
        public string Abnormal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WarningTime { get; set; }
        /// <summary>
        /// 设备报警
        /// </summary>
        public string WarnType { get; set; }
    }
    /// <summary>
    /// 设备故障报警分布
    /// </summary>
    public class EquipFaultSpreadItem {
        /// <summary>
        /// 故障类型
        /// </summary>
        public string FaultType { get; set; }
        /// <summary>
        /// 故障数量
        /// </summary>
        public int FaultNum { get; set; }
        /// <summary>
        /// 已处理数量
        /// </summary>
        public int HandleNum { get; set; }
    }
    /// <summary>
    /// 设备报警
    /// </summary>
    public class EquipWarning {
        /// <summary>
        /// 
        /// </summary>
        public List<string> xAxis { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DataArrItem> dataArr { get; set; }
    }
    /// <summary>
    /// 模具状态
    /// </summary>
    public class MoldStateArrItem {
        /// <summary>
        /// 模具状态
        /// </summary>
        public string MoldState { get; set; }
        /// <summary>
        /// 模具数量
        /// </summary>
        public int MoldNum { get; set; }
        /// <summary>
        /// 占比
        /// </summary>
        public string Proportion { get; set; }
    }
    /// <summary>
    /// 模具故障
    /// </summary>
    public class MoldFaultArrItem {
        /// <summary>
        /// 故障类型
        /// </summary>
        public string FaultType { get; set; }
        /// <summary>
        /// 故障数量
        /// </summary>
        public int FaultNum { get; set; }
        /// <summary>
        /// 已处理数量
        /// </summary>
        public int HandleNum { get; set; }
    }
    /// <summary>
    /// 模具报警
    /// </summary>
    public class MoldWarningArrItem {
        /// <summary>
        /// 报警信息
        /// </summary>
        public string Abnormal { get; set; }
        /// <summary>
        /// 模具报警类型
        /// </summary>
        public string WarnType { get; set; }
        /// <summary>
        /// 报警时间
        /// </summary>
        public string WarningTime { get; set; }
    }

    /// <summary>
    /// 设备保养计划
    /// </summary>
    public class EquipUpkeepStatus {
        /// <summary>
        /// 
        /// </summary>
        public string ChartType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DataArrItem> DataArr { get; set; }
        /// <summary>
        /// 40台
        /// </summary>
        public string Title { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class DataArrItem {
        /// <summary>
        /// 
        /// </summary>
        public List<double> Data { get; set; } = new List<double>();
        /// <summary>
        /// 
        /// </summary>
        public string Suffix { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class Chart {
        /// <summary>
        /// 
        /// </summary>
        public string chartType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DataArrItem> DataArr { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class EquipUkeepTask {
        /// <summary>
        /// 
        /// </summary>
        public int TaskTotal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Chart Chart { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldUpkeepStatus {
        /// <summary>
        /// 
        /// </summary>
        public string ChartType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DataArrItem> DataArr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldUkeepTask {
        /// <summary>
        /// 
        /// </summary>
        public int TaskTotal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Chart Chart { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UpkeepTaskStatusItem {
        /// <summary>
        /// 
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }
    }


    #region 设备
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentOnlineStatisticsModel {
        /// <summary>
        /// 
        /// </summary>
        public List<StatisticItem> Statistic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EOStatistic> Data { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class StatisticItem {
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TotalNum { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int OnlineNum { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class EOStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OnTime { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class EquipmentRepairStatisticsModel {
        /// <summary>
        /// 
        /// </summary>
        public List<StatisticItem> Statistic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EPStatistic> RepairData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EAStatistic> AlarmData { get; set; }
    }
    /// <summary>
    /// 维修
    /// </summary>
    public class EPStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 维修开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 维修完成时间
        /// </summary>
        public string EndDate { get; set; }
        /// <summary>
        /// 故障类型
        /// </summary>
        public string FaultTypeName { get; set; }
        /// <summary>
        /// 维修级别
        /// </summary>
        public string Level { get; set; }


    }
    /// <summary>
    /// 报警信息
    /// </summary>
    public class EAStatistic {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 报警时间
        /// </summary>
        public DateTime? AlarmDate { get; set; }
        /// <summary>
        /// 设备存放位置
        /// </summary>
        public string PositionName { get; set; }
        /// <summary>
        /// 报警内容
        /// </summary>
        public string AbnormalInfo { get; set; }
        /// <summary>
        /// 报警级别
        /// </summary>
        public int AbnormalLevel { get; set; }


    }
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentMaintenanceStatisticsModel {
        /// <summary>
        /// 
        /// </summary>
        public List<StatisticItem> Statistic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<EPlanStatistic> PlanData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ERecordStatistic> RecordData { get; set; }
    }
    /// <summary>
    /// 保养计划
    /// </summary>
    public class EPlanStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 计划开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 计划结束时间
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 计划类型
        /// </summary>
        public string PlanTypeName { get; set; }
        /// <summary>
        /// 计划编号
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 计划内容
        /// </summary>
        public string PlanInfo { get; set; }
        /// <summary>
        /// 计划负责人
        /// </summary>
        public string MaintenanceManger { get; set; }

    }
    /// <summary>
    /// 保养记录
    /// </summary>
    public class ERecordStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 保养开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 保养结束时间
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 保养类型
        /// </summary>
        public string MaintenanceType { get; set; }
        /// <summary>
        /// 保养编号
        /// </summary>
        public string MaintenanceCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MaintenanceInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MaintenanceManger { get; set; }

    }
    #endregion
    #region 模具
    /// <summary>
    /// 
    /// </summary>
    public class MouldOnlineStatisticsModel {
        /// <summary>
        /// 
        /// </summary>
        public List<StatisticItem> Statistic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MOStatistic> Data { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MOStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string MouldCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MouldName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OnTime { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class MouldRepairStatisticsModel {
        /// <summary>
        /// 
        /// </summary>
        public List<StatisticItem> Statistic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MPStatistic> RepairData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MRStatistic> ReportRepairData { get; set; }
    }
    /// <summary>
    /// 维修
    /// </summary>
    public class MPStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string MouldCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MouldName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 维修开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 维修完成时间
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 故障类型
        /// </summary>
        public string FaultTypeName { get; set; }
        /// <summary>
        /// 维修级别
        /// </summary>
        public string Level { get; set; }


    }
    /// <summary>
    /// 保修
    /// </summary>
    public class MRStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string MouldCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MouldName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 报修时间
        /// </summary>
        public DateTime? ReporRepairDate { get; set; }
        /// <summary>
        /// 模具存放位置
        /// </summary>
        public string PositionName { get; set; }
        /// <summary>
        /// 报修内容
        /// </summary>
        public string ReporRepairInfo { get; set; }
        /// <summary>
        /// 报修级别
        /// </summary>
        public int ReporRepairLevel { get; set; }


    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldMaintenanceStatisticsModel {
        /// <summary>
        /// 
        /// </summary>
        public List<StatisticItem> Statistic { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MPlanStatistic> PlanData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<MRecordStatistic> RecordData { get; set; }
    }
    /// <summary>
    /// 保养计划
    /// </summary>
    public class MPlanStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string MouldCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MouldName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 计划开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 计划结束时间
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 计划类型
        /// </summary>
        public string PlanTypeName { get; set; }
        /// <summary>
        /// 计划编号
        /// </summary>
        public string PlanCode { get; set; }
        /// <summary>
        /// 计划内容
        /// </summary>
        public string PlanInfo { get; set; }
        /// <summary>
        /// 计划负责人
        /// </summary>
        public string PlanManger { get; set; }

    }
    /// <summary>
    /// 保养记录
    /// </summary>
    public class MRecordStatistic {
        /// <summary>
        /// 
        /// </summary>
        public string MouldCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MouldName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 保养开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 保养结束时间
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 计划类型
        /// </summary>
        public string PlanTypeName { get; set; }
        /// <summary>
        /// 保养编号
        /// </summary>
        public string MaintenanceCode { get; set; }

        /// <summary>
        /// 保养负责人
        /// </summary>
        public string MaintenanceManger { get; set; }
        /// <summary>
        /// 保养内容
        /// </summary>
        public string MaintenanceInfo { get; set; }


    }
    #endregion
}
