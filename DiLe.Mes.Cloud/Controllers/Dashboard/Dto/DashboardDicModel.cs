﻿namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto {
    /// <summary>
    /// 仪表板字典
    /// </summary>
    public class DashboardDicModel {
        /// <summary>
        /// key
        /// </summary>
        public string Key { set; get; }
        /// <summary>
        /// key
        /// </summary>
        public string Month { set; get; }

    }
}
