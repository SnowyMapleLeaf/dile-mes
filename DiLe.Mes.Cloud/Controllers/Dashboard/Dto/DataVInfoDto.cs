﻿using DiLe.Mes.Cloud.Controllers.Abnormal.Dto;

namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class DataVInfoDto {
        /// <summary>
        /// 
        /// </summary>
        public List<AbnormalInfo> AbnormalInfos { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EquipWarning WarningRunChart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EquipmentNumberChart EquipmentNumberChart { get; set; }
        /// <summary>
        /// 维修完成图
        /// </summary>
        public Chart RepairCompleteChart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SparePartUseChart SparePartUseChart { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentNumberChart {
        /// <summary>
        /// 
        /// </summary>
        public string ChartType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DataArrItem> DataArr { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class SparePartUseChart {
        /// <summary>
        /// 
        /// </summary>
        public List<string> xAxis { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DataArrItem> dataArr { get; set; }
    }
}
