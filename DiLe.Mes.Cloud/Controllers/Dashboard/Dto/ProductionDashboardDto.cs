﻿using DiLe.Mes.Cloud.Controllers.Statistics.Dto;

namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductionDashboardDto {
        /// <summary>
        /// AOI/CCD良率
        /// </summary>
        public AOIYieldRateDto AOIYieldRateDto { get; set; }
        /// <summary>
        /// AOI/CCD检验数
        /// </summary>
        public AOIInspectionDto AOIInspectionDto { get; set; }
        /// <summary>
        /// AOI/CCD不良率
        /// </summary>
        public AOIBadRateDto AOIBadRateDto { get; set; }

        /// <summary>
        /// 成型制程产出
        /// </summary>
        public EquipmentOutputDto OutputDto { get; set; }
        /// <summary>
        /// 切箍边制程产出
        /// </summary>
        public EquipmentOutputDto TrimOutputDto { get; set; } = new EquipmentOutputDto();
        /// <summary>
        /// 堆叠机制程产出
        /// </summary>
        public EquipmentOutputDto StackingOutputDto { get; set; } = new EquipmentOutputDto();

        /// <summary>
        /// 成型制程设备稼动率
        /// </summary>
        public UtilizationRateDto UtilizationRateDto { get; set; } = new UtilizationRateDto();
        /// <summary>
        /// 切箍边制程设备稼动率
        /// </summary>
        public UtilizationRateDto TrimUtilizationRateDto { get; set; } = new UtilizationRateDto();
        /// <summary>
        /// AOI制程设备稼动率
        /// </summary>
        public UtilizationRateDto AOIUtilizationRateDto { get; set; } = new UtilizationRateDto();
        /// <summary>
        /// 总耗能
        /// </summary>
        public EnergyConsumptionDto EnergyConsumptionDto { get; set; } = new EnergyConsumptionDto();

        /// <summary>
        /// 累计订单数量
        /// </summary>
        public CumulativeOrderQuantityDto CumulativeOrderQuantityDto { get; set; }
        /// <summary>
        /// 累计入库数量
        /// </summary>
        public CumulativeStockInQuantityDto CumulativeStockInQuantityDto { get; set; }
        /// <summary>
        /// 成型机使用率
        /// </summary>
        public UsageRateDto UsageRateDto { get; set; }
    }
    /// <summary>
    /// 各AOI/CCD良率
    /// </summary>
    public class AOIYieldRateDto {
        /// <summary>
        /// AOI/CCD制程总平均良率
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<AOIYieldRateStatisticsModel> Items { get; set; } = new List<AOIYieldRateStatisticsModel>();
    }
    /// <summary>
    /// 各AOI/CCD不良数
    /// </summary>
    public class AOIBadRateDto {
        /// <summary>
        /// AOI/CCD制程总不良数
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<AOIYieldRateStatisticsModel> Items { get; set; } = new List<AOIYieldRateStatisticsModel>();
    }
    /// <summary>
    /// 各AOI/CCD检验数
    /// </summary>
    public class AOIInspectionDto {
        /// <summary>
        /// AOI/CCD制程总检验数
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<AOIYieldRateStatisticsModel> Items { get; set; } = new List<AOIYieldRateStatisticsModel>();
    }

    /// <summary>
    /// 产出数
    /// </summary>
    public class EquipmentOutputDto {
        /// <summary>
        /// 
        /// </summary>
        public int Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentOutputStatisticsModel> Items { get; set; } = new List<EquipmentOutputStatisticsModel>();
    }


    /// <summary>
    /// 稼动率
    /// </summary>
    public class UtilizationRateDto {
        /// <summary>
        /// 
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentUtilizationRateStatisticsModel> Items { get; set; } = new List<EquipmentUtilizationRateStatisticsModel>();
    }


    /// <summary>
    /// 耗能
    /// </summary>
    public class EnergyConsumptionDto {
        /// <summary>
        /// 总耗能
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<EnergyConsumptionItem> Items { get; set; } = new List<EnergyConsumptionItem> { new EnergyConsumptionItem() };
    }
    /// <summary>
    /// 耗能
    /// </summary>
    public class EnergyConsumptionItem {
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { get; set; } = "";
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentCode { get; set; } = "";
        /// <summary>
        /// 耗能
        /// </summary>
        public decimal EnergyConsumption { get; set; } = 0;
    }


    /// <summary>
    /// 累积订单数量
    /// </summary>
    public class CumulativeOrderQuantityDto {
        /// <summary>
        /// 累积订单总数
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<CumulativeItem> Items { get; set; }
    }
    /// <summary>
    /// 累积入库数量
    /// </summary>
    public class CumulativeStockInQuantityDto {
        /// <summary>
        /// 累积入库总数
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<CumulativeItem> Items { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CumulativeItem {
        /// <summary>
        /// 设备编号
        /// </summary>
        [JsonIgnore]
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 订单数量
        /// </summary>
        public int OrderQuantity { get; set; } = 0;
        /// <summary>
        /// 入库数量
        /// </summary>
        public int StockInQuantity { get; set; } = 0;
    }
    /// <summary>
    /// 使用率
    /// </summary>
    public class UsageRateDto {
        /// <summary>
        /// 使用率
        /// </summary>
        public decimal Total { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public List<EquipmentUtilizationRateStatisticsModel> Items { get; set; }
    }
}
