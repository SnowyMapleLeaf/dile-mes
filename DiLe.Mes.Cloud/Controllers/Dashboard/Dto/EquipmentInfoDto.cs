﻿using DiLe.Mes.Model.Common.Equipment.Entity.Manage;

namespace DiLe.Mes.Cloud.Controllers.Dashboard.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class EquipmentInfoDto {
        /// <summary>
        /// 设备
        /// </summary>
        public EquipmentInfoEntity Equipment { set; get; }
        /// <summary>
        /// 设备类型
        /// </summary>
        public EquipmentTypeEntity? EquipmentType { set; get; }
        /// <summary>
        /// 设备注册
        /// </summary>
        public EquipmentRegisterEntity? EquipmentRegister { set; get; }
    }
}
