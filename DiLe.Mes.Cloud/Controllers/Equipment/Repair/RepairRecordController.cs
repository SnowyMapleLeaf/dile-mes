﻿using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Common.Equipment.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Equipment.Entity.Repair;
using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Model.Common.Equipment.Relation.Repair;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Equipment.Repair {
    /// <summary>
    /// 维修记录
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class RepairRecordController : ApiBaseController {

        private readonly EquipmentRepairClient _equipmentRepair;
        private readonly SparePartManageClient _sparePartManageClient;
        private readonly EquipmentExjosnHandler _handler;
        private readonly EquipmentManageClient _equipmentClient;


        /// <summary>
        /// 构造函数
        /// </summary>
        public RepairRecordController(EquipmentRepairClient repairClient,
                                      EquipmentExjosnHandler handler,
                                      SparePartManageClient sparePartManageClient,
                                      EquipmentManageClient equipmentClient) {
            _equipmentRepair = repairClient;
            _handler = handler;
            _sparePartManageClient = sparePartManageClient;
            _equipmentClient = equipmentClient;
        }
        /// <summary>
        /// 获取维修记录列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetRepairRecordList(RepairRecordQueryParameter parameter) {
            var whereExpression = Expressionable.Create<RepairRecordEntity>();
            whereExpression.And(p => p.FactoryId == AppHelper.QueryOrgId);
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.EquipmentId > 0) {
                whereExpression.And(x => x.EquipmentId == parameter.EquipmentId);
            }
            if (parameter.Pagination == null) {
                var res = await _equipmentRepair.GetRepairRecordListAsync(whereExpression.ToExpression());
                var list = await _handler.GetRepairRecordDto(res);
                return Success(list);
            } else {
                var res = await _equipmentRepair.GetRepairRecordPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetRepairRecordDto(res.Record);
                var data = res.Adapt<PaginationModel<RepairRecordDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取维修记录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetRepairRecord(long id) {
            var dto = new RepairRecordDetailDto<RepairRecordEntity, RepairRecord2SparePartEntity, SparePartLedgerDetailDto>();
            dto.Model = await _equipmentRepair.GetRepairRecordAsync(id);
            await _handler.FillRepairRecordExjosn(new List<RepairRecordEntity> { dto.Model });
            if (dto.Model != null) {
                var temps = await _equipmentRepair.GetRepairRecord2SparePartListByRecordIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.SparePartId).Distinct().ToList();
                    var dataList = await _sparePartManageClient.GetSparePartLedgerListAsync(p => ids.Contains(p.Id));
                    dto.DataList = await _handler.FillSparePartLedgerExjosn(dataList);
                }
                dto.EquipmentModel = await _equipmentClient.GetEquipmentInfoAsync(dto.Model.EquipmentId);
                if (dto.EquipmentModel != null) {
                    await _handler.FillEquipmentInfoExjosn(new List<EquipmentInfoEntity> { dto.EquipmentModel });
                }
            }
            return Success(dto);
        }
        /// <summary>
        /// 保存维修记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult<RepairRecordEntity>> SaveRepairRecord(SaveParameter<RepairRecordEntity, RepairRecord2SparePartEntity> parameter) {
            var res = await _equipmentRepair.SaveRepairRecordAsync(parameter.Model);
            if (res.Id > 0) {
                if (!parameter.AddRelations.None()) {
                    await _equipmentRepair.InsertRepairRecord2SparePartAsync(res.Id, parameter.AddRelations);
                }
                if (!parameter.EidtRelations.None()) {
                    await _equipmentRepair.UpdateRepairRecord2SparePartAsync(parameter.EidtRelations);
                }
                if (!parameter.DeleteRelations.None()) {
                    await _equipmentRepair.DeleteRepairRecord2SparePartAsync(parameter.DeleteRelations);
                }
            }
            return Success(res);
        }
        /// <summary>
        /// 删除维修记录
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteRepairRecord(List<long> ids) {
            var res = await _equipmentRepair.DeleteRepairRecordAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
