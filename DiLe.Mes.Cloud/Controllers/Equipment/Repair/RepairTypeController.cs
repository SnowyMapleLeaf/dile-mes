﻿using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Model.Common.Equipment.Entity.Repair;

namespace DiLe.Mes.Cloud.Controllers.Equipment.Repair
{
    /// <summary>
    /// 维修类型
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class RepairTypeController : ApiBaseController {
        private readonly EquipmentRepairClient _equipmentRepair;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="repairClient"></param>
        public RepairTypeController(EquipmentRepairClient repairClient) {
            _equipmentRepair = repairClient;
        }
        /// <summary>
        /// 获取维修类型列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetRepairTypeList(QueryParameter query) {
            var whereExpression = Expressionable.Create<RepairTypeEntity>();
            if (!query.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(query.Keyword!));
                whereExpression.Or(p => p.Code.Contains(query.Keyword!));
            }
            if (query.Pagination == null) {
                List<RepairTypeEntity> res = await _equipmentRepair.GetRepairTypeListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _equipmentRepair.GetRepairTypePageListAsync(whereExpression.ToExpression(), query.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取维修类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetRepairType(long id) {
            var res = await _equipmentRepair.GetRepairTypeAsync(id);
            if (res?.ParentId > 0) {
                var parent = await _equipmentRepair.GetRepairTypeAsync(res.ParentId);
                res.ExtJson.Add("ParentName", parent?.Name);
            }
            return Success(res);
        }
        /// <summary>
        /// 保存维修类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveRepairType(RepairTypeEntity entity) {
            var res = await _equipmentRepair.SaveRepairTypeAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除维修类型
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteRepairType(List<long> ids) {
            var res = await _equipmentRepair.DeleteRepairTypeAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新维修类型状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateRepairTypeStatus(CommomParameter parameter) {
            var res = await _equipmentRepair.UpdateRepairTypeStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }
    }
}
