﻿using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Common.Equipment.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;

namespace DiLe.Mes.Cloud.Controllers.Equipment.SparePart {
    /// <summary>
    /// 备件台账
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class SparePartLedgerController : ApiBaseController {

        private readonly SparePartManageClient _manageClient;
        private readonly EquipmentManageClient _equipmentClient;

        private readonly EquipmentExjosnHandler _handler;


        /// <summary>
        /// 构造函数
        /// </summary>
        public SparePartLedgerController(SparePartManageClient sparePartManage,
                                         EquipmentManageClient equipmentManageClient,
                                         EquipmentExjosnHandler handler) {
            _manageClient = sparePartManage;
            _equipmentClient = equipmentManageClient;
            _handler = handler;
        }
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetSparePartLedgerPageList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<SparePartLedgerEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _manageClient.GetSparePartLedgerListAsync(whereExpression.ToExpression());
                var list = await _handler.FillSparePartLedgerExjosn(res);
                return Success(list);
            } else {
                var res = await _manageClient.GetSparePartLedgerPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.FillSparePartLedgerExjosn(res.Record);
                var data = res.Adapt<PaginationModel<SparePartLedgerDetailDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取备件台账
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetSparePartLedger(long id) {
            var dto = new SparePartLedgerDto();
            var res = await _manageClient.GetSparePartLedgerAsync(id);
            dto.Model = res;
            if (res.Id > 0) {
                var temps = await _manageClient.GetSparePartEquipmentListByLedgerIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.EquipmentId).Distinct().ToList();
                    dto.DataList = await _equipmentClient.GetEquipmentInfoListAsync(ids);
                    await _handler.FillEquipmentInfoExjosn(dto.DataList);
                }
            }
            await _handler.FillSparePartLedgerExjosn(new List<SparePartLedgerEntity> { dto.Model });
            return Success(dto);
        }
        /// <summary>
        /// 保存备件台账
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveSparePartLedger(SaveParameter<SparePartLedgerEntity, SparePartEquipmentEntity> parameter) {
            var resId = await _manageClient.SaveSparePartLedgerAsync(parameter.Model);
            if (resId > 0) {
                if (!parameter.AddRelations.None()) {
                    await _manageClient.InsertSparePartEquipmentAsync(resId, parameter.AddRelations);
                }
                if (!parameter.EidtRelations.None()) {
                    await _manageClient.UpdateSparePartEquipmentAsync(parameter.EidtRelations);
                }
                if (!parameter.DeleteRelations.None()) {
                    await _manageClient.DeleteSparePartEquipmentAsync(parameter.DeleteRelations);
                }
            }
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除备件台账
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteSparePartLedger(List<long> ids) {
            var res = await _manageClient.DeleteSparePartLedgerAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
