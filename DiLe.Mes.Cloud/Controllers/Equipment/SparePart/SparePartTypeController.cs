﻿using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using Microsoft.AspNetCore.Mvc;

namespace DiLe.Mes.Cloud.Controllers.Equipment.SparePart
{
    /// <summary>
    /// 备件类型
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class SparePartTypeController : ApiBaseController {

        private readonly SparePartManageClient _manageClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="sparePartManage"></param>
        public SparePartTypeController(SparePartManageClient sparePartManage) {
            _manageClient = sparePartManage;
        }
        /// <summary>
        /// 获取备件类型列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetSparePartTypeList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<SparePartTypeEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _manageClient.GetSparePartTypeListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _manageClient.GetSparePartTypePageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取备件类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetSparePartType(long id) {
            var res = await _manageClient.GetSparePartTypeAsync(id);
            if (res?.ParentId > 0) {
                var parent = await _manageClient.GetSparePartTypeAsync(res.ParentId);
                res.ExtJson.Add("ParentName", parent?.Name);
            }
            return Success(res);
        }
        /// <summary>
        /// 保存备件类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveSparePartType(SparePartTypeEntity entity) {
            var res = await _manageClient.SaveSparePartTypeAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除备件类型
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteSparePartType(List<long> ids) {
            var res = await _manageClient.DeleteSparePartTypeAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新备件类型状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateSparePartTypeStatus(CommomParameter parameter) {
            var res = await _manageClient.UpdateSparePartTypeStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }

    }
}
