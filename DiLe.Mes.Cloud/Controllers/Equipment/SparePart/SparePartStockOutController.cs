﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Common.Equipment.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common;
using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;

namespace DiLe.Mes.Cloud.Controllers.Equipment.SparePart {
    /// <summary>
    /// 出库管理
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class SparePartStockOutController : ApiBaseController {

        private readonly SparePartManageClient _manageClient;
        private readonly EquipmentManageClient _equipmentClient;
        private readonly EquipmentExjosnHandler _handler;
        private readonly SystemClient _systemClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        public SparePartStockOutController(SparePartManageClient sparePartManage,
                                           EquipmentManageClient equipmentManageClient,
                                           EquipmentExjosnHandler handler,
                                           SystemClient systemClient) {
            _manageClient = sparePartManage;
            _equipmentClient = equipmentManageClient;
            _handler = handler;
            _systemClient = systemClient;
        }
        /// <summary>
        /// 获取出库管理列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetSparePartStockOutPageList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<SparePartStockOutEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _manageClient.GetSparePartStockOutListAsync(whereExpression.ToExpression());
                await _handler.FillSparePartStockOutExjosn(res);
                return Success(res);
            } else {
                var res = await _manageClient.GetSparePartStockOutPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillSparePartStockOutExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取出库管理
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetSparePartStockOut(long id) {
            var dto = new SparePartStockOutDto();
            var res = await _manageClient.GetSparePartStockOutAsync(id);
            dto.Model = res;
            if (res.Id > 0) {
                var temps = await _manageClient.GetStockOutSparePartListByStockOutIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.SparePartId).Distinct().ToList();
                    var dataList = await _manageClient.GetSparePartLedgerListAsync(p => ids.Contains(p.Id));
                    dto.DataList = await _handler.FillSparePartLedgerExjosn(dataList);
                }
            }
            await _handler.FillSparePartStockOutExjosn([dto.Model]);
            return Success(dto);
        }
        /// <summary>
        /// 保存出库管理
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveSparePartStockOut(SaveParameter<SparePartStockOutEntity, StockOutSparePartEntity> parameter) {
            var res = await _manageClient.SaveSparePartStockOutAsync(parameter.Model);
            if (res.Id > 0) {
                if (!parameter.AddRelations.None()) {
                    await _manageClient.InsertStockOutSparePartAsync(res.Id, parameter.AddRelations);
                    var maps = parameter.AddRelations.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in maps) {
                        await MinusSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                }
                if (!parameter.EidtRelations.None()) {
                    await _manageClient.UpdateStockOutSparePartAsync(parameter.EidtRelations);
                    var ids = parameter.EidtRelations.ConvertAll(x => x.Id);
                    var oldRels = await _manageClient.GetStockOutSparePartList(p => ids.Contains(p.Id));
                    var oldmaps = oldRels.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in oldmaps) {
                        await MinusSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                    var maps = parameter.EidtRelations.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in maps) {
                        await AddSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                }
                if (!parameter.DeleteRelations.None()) {
                    await _manageClient.DeleteStockOutSparePartAsync(parameter.DeleteRelations);
                    var maps = parameter.AddRelations.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in maps) {
                        await AddSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                }
            }
            return res.Id > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除出库管理
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteSparePartStockOut(List<long> ids) {
            var rels = await _manageClient.GetStockOutSparePartList(p => ids.Contains(p.StockOutId));
            var dataList = await _manageClient.GetSparePartStockOutListAsync(p => ids.Contains(p.Id));
            foreach (var item in dataList) {
                var maps = rels.Where(p => p.StockOutId == item.Id).GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                foreach (var map in maps) {
                    await AddSparePartInventory(item.WarehouseId, map.Key, map.Item2);
                }
            }
            var res = await _manageClient.DeleteSparePartStockOutAsync(ids);
            return res ? Success() : Fail();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="sparePartId"></param>
        /// <param name="quantity"></param>
        private async Task AddSparePartInventory(long warehouseId, long sparePartId, int quantity) {
            var inventory = await _systemClient.GetSparePartInventoryAsync(p => p.SparePartId == sparePartId && p.WarehouseId == warehouseId);
            if (inventory == null) {
                inventory = new SparePartInventoryEntity() {
                    InventoryQuantity = quantity,
                    SparePartId = sparePartId,
                    WarehouseId = warehouseId
                };
                await _systemClient.InsertSparePartInventoryAsync(inventory);
            } else {
                inventory.InventoryQuantity += quantity;
                await _systemClient.UpdateSparePartInventoryAsync([inventory]);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="sparePartId"></param>
        /// <param name="quantity"></param>
        private async Task MinusSparePartInventory(long warehouseId, long sparePartId, int quantity) {
            var inventory = await _systemClient.GetSparePartInventoryAsync(p => p.SparePartId == sparePartId && p.WarehouseId == warehouseId);
            if (inventory != null) {
                inventory.InventoryQuantity -= quantity;
                await _systemClient.UpdateSparePartInventoryAsync([inventory]);
            }
        }
    }
}
