﻿using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;
using DiLe.Mes.Model.Common.Equipment.Entity.SparePart;
using DiLe.Mes.Model.Common.Equipment.Relation.Maintenance;
using DiLe.Mes.Model.Common.Equipment.Relation.SparePart;

namespace DiLe.Mes.Cloud.Controllers.Equipment {

    /// <summary>
    /// 
    /// </summary>
    public class EquipmentInfoQueryParameter : QueryParameter {
        /// <summary>
        /// 位置
        /// </summary>
        public long? PositionId { get; set; } = 0;
        /// <summary>
        /// 状态
        /// </summary>
        public long? StatusId { get; set; } = 0;
        /// <summary>
        /// 类型
        /// </summary>
        public long? TypeId { get; set; } = 0;

    }
    /// <summary>
    /// 
    /// </summary>
    public class RepairRecordQueryParameter : QueryParameter {
        /// <summary>
        /// 
        /// </summary>
        public long EquipmentId { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MaintenanceRecordQueryParameter : QueryParameter {
        /// <summary>
        /// 
        /// </summary>
        public long EquipmentId { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public long MaintenancePlanId { get; set; } = 0;
    }

    /// <summary>
    /// 设备保养
    /// </summary>
    public class MaintenanceRecordSaveParameter {
        /// <summary>
        /// 保养记录
        /// </summary>

        public SaveParameter<MaintenanceRecordEntity, MaintenanceRecord2ProjectEntity> MaintenanceRecordParameter { set; get; }
        /// <summary>
        /// 出库
        /// </summary>
        public SaveParameter<SparePartStockOutEntity, StockOutSparePartEntity> StockOutParameter { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PointPositionInfo {
        /// <summary>
        /// 类型主键
        /// </summary>
        public long TypeId { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string FileId { get; set; }
        /// <summary>
        /// 位置信息
        /// </summary>
        public List<AbnormalAlarmRuleEntity> PointArr { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AbnormalUploadParameter { 
        /// <summary>
        /// 
        /// </summary>
        public List<IFormFile> Files { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long TypeId { get; set; }
    }
}

