﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using Microsoft.AspNetCore.Authorization;

namespace DiLe.Mes.Cloud.Controllers.Equipment.Info {
    /// <summary>
    /// 设备档案
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class EquipmentInfoController : ApiBaseController {
        private readonly EquipmentManageClient _manageClient;
        private readonly SystemClient _systemClient;
        private readonly EquipmentExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public EquipmentInfoController(EquipmentManageClient equipmentManage,
                                       SystemClient systemClient,
                                       EquipmentExjosnHandler handler) {
            _manageClient = equipmentManage;
            _systemClient = systemClient;
            _handler = handler; 
        }
        /// <summary>
        /// 获取设备档案列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetEquipmentInfoList(EquipmentInfoQueryParameter parameter) {
            var whereExpression = Expressionable.Create<EquipmentInfoEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword!));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword!));
                whereExpression.Or(p => p.Specification.Contains(parameter.Keyword!));
            }
            if (parameter.TypeId > 0) {
                whereExpression.And(p => p.TypeId == parameter.TypeId);
            }
            if (parameter.StatusId > 0) {
                whereExpression.And(p => p.CurrentStatus == parameter.StatusId);
            }
            if (parameter.PositionId > 0) {
                whereExpression.And(p => p.PositionId == parameter.PositionId);
            }
            whereExpression.AndIF(AppHelper.QueryOrgId > 0, p => p.FactoryId == AppHelper.QueryOrgId);
            if (parameter.Pagination == null) {
                var res = await _manageClient.GetEquipmentInfoListAsync(whereExpression.ToExpression());
                await _handler.FillEquipmentInfoExjosn(res);
                return Success(res);
            } else {
                var res = await _manageClient.GetEquipmentInfoPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillEquipmentInfoExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取设备档案
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetEquipmentInfo(long id) {
            var res = await _manageClient.GetEquipmentInfoAsync(id);
            await _handler.FillEquipmentInfoExjosn(new List<EquipmentInfoEntity> { res });

            //客户
            var customer = await _systemClient.GetCustomerInfoAsync(res.CustomerId);
            res.ExtJson.Add("CustomerName", customer?.Name);
            //制程
            var process = await _systemClient.GetProcessInfoAsync(res.ProcessId);
            res.ExtJson.Add("ProcessName", process?.Name);
            return Success(res);
        }
        /// <summary>
        /// 保存设备档案
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveEquipmentInfo(EquipmentInfoEntity entity) {
            var res = await _manageClient.SaveEquipmentInfoAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除设备档案
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteEquipmentInfo(List<long> ids) {
            var res = await _manageClient.DeleteEquipmentInfoAsync(ids);
            return res ? Success() : Fail();
        }

      
    }
}
