﻿using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;

namespace DiLe.Mes.Cloud.Controllers.Equipment.Info
{
    /// <summary>
    /// 设备状态
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class EquipmentStatusController : ApiBaseController {
        private readonly EquipmentManageClient _manageClient;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="equipmentManage"></param>
        public EquipmentStatusController(EquipmentManageClient equipmentManage) {
            _manageClient = equipmentManage;
        }
        /// <summary>
        /// 获取设备状态列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetEquipmentStatusPageList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<EquipmentStatusEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.And(p => p.Name.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _manageClient.GetEquipmentStatusListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _manageClient.GetEquipmentStatusPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取设备状态
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetEquipmentStatusAsync(long id) {
            var res = await _manageClient.GetEquipmentStatusAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存设备状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveEquipmentStatus(EquipmentStatusEntity entity) {
            var res = await _manageClient.SaveEquipmentStatusAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除设备状态
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteEquipmentStatus(List<long> ids) {
            var res = await _manageClient.DeleteEquipmentStatusAsync(ids);
            return res ? Success() : Fail();
        }

    }
}
