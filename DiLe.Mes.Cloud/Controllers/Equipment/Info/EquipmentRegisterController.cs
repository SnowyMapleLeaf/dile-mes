﻿using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Common.Equipment.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;

namespace DiLe.Mes.Cloud.Controllers.Equipment.Info {
    /// <summary>
    /// 设备注册
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class EquipmentRegisterController : ApiBaseController {
        private readonly EquipmentManageClient _manageClient;

        private readonly EquipmentExjosnHandler _handler;

        /// <summary>
        /// 构造函数
        /// </summary>
        public EquipmentRegisterController(EquipmentManageClient equipmentManage, EquipmentExjosnHandler handler) {
            _manageClient = equipmentManage;
            _handler = handler;
        }
        /// <summary>
        /// 获取注册信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetEquipmentRegisterList(QueryParameter parameter) {
            var orgid = AppHelper.QueryOrgId;
            var whereExpression = Expressionable.Create<EquipmentRegisterEntity>();
            
            if (parameter.Pagination == null) {
                var res = await _manageClient.GetEquipmentRegisterListAsync(whereExpression.ToExpression());
                var list = await _handler.GetEquipmentRegisterExjosn(res);
                return Success(list);
            } else {
                var res = await _manageClient.GetEquipmentRegisterPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetEquipmentRegisterExjosn(res.Record);
                var data = res.Adapt<PaginationModel<EquipmentRegisterDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取注册信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetEquipmentRegister(long id) {
            var res = await _manageClient.GetEquipmentRegisterInfoAsync(id);
            var data = await _handler.GetEquipmentRegisterExjosn(new List<EquipmentRegisterEntity> { res });
            return Success(data?.FirstOrDefault());
        }
        /// <summary>
        /// 保存注册信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveEquipmentRegister(EquipmentRegisterEntity entity) {
            var res = await _manageClient.SaveEquipmentRegisterAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除注册信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteEquipmentRegister(List<long> ids) {
            var res = await _manageClient.DeleteEquipmentRegisterAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新注册信息状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateEquipmentRegisterStatus(CommomParameter parameter) {
            var res = await _manageClient.UpdateEquipmentRegisterStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }


        /// <summary>
        /// 上传注册信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult UpdateEquipmentRegister(IFormFile file) {
            var fileName = file.FileName;
            var dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tempfile");
            if (!Directory.Exists(dir)) {
                Directory.CreateDirectory(dir);
            }
            var filefullPath = Path.Combine(dir, fileName);
            using (FileStream fs = new(filefullPath, FileMode.Create)) {
                file.CopyTo(fs);
                fs.Flush();
            }



            return Success();
        }
    }
}
