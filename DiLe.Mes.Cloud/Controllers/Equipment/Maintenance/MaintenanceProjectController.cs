﻿using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;

namespace DiLe.Mes.Cloud.Controllers.Equipment.Maintenance {
    /// <summary>
    /// 保养项目
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class MaintenanceProjectController : ApiBaseController {
        private readonly EquipmentMaintenanceClient _maintenanceClient;
        private readonly EquipmentExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MaintenanceProjectController(EquipmentMaintenanceClient maintenanceClient, EquipmentExjosnHandler handler) {
            _maintenanceClient = maintenanceClient;
            _handler = handler;
        }
        /// <summary>
        /// 获取保养项目列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMaintenanceProjectList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MaintenanceProjectEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _maintenanceClient.GetMaintenanceProjectListAsync(whereExpression.ToExpression());
                await _handler.FillMaintenanceProjectExjosn(res);
                return Success(res);
            } else {
                var res = await _maintenanceClient.GetMaintenanceProjectPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillMaintenanceProjectExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取保养项目
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMaintenanceProject(long id) {
            var res = await _maintenanceClient.GetMaintenanceProjectAsync(id);
            await _handler.FillMaintenanceProjectExjosn(new List<MaintenanceProjectEntity> { res });
            return Success(res);
        }
        /// <summary>
        /// 获取保养项目
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult<List<MaintenanceProjectEntity>>> GetMaintenanceProjectListByPlanId(long planId) {
            var list = new List<MaintenanceProjectEntity>();
            var temps = await _maintenanceClient.GetMaintenancePlan2ProjectListByPlanIdAsync(planId);
            if (!temps.None()) {
                var ids = temps.Select(x => x.MaintenanceProjectId).Distinct().ToList();
                list = await _maintenanceClient.GetMaintenanceProjectListAsync(p => ids.Contains(p.Id));
                await _handler.FillMaintenanceProjectExjosn(list);
            }
            return Success(list);
        }

        /// <summary>
        /// 保存保养项目
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMaintenanceProject(MaintenanceProjectEntity entity) {
            var res = await _maintenanceClient.SaveMaintenanceProjectAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除保养项目
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMaintenanceProject(List<long> ids) {
            var res = await _maintenanceClient.DeleteMaintenanceProjectAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新保养项目
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateMaintenanceProjectStatus(CommomParameter parameter) {
            var res = await _maintenanceClient.UpdateMaintenanceProjectStatusAsync(parameter.Ids, parameter.Status);
            return Success(res);
        }

    }
}
