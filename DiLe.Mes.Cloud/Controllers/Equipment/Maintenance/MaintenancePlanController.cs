﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Common.Equipment.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Equipment.Entity.Maintenance;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Equipment.Relation.Maintenance;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Equipment.Maintenance {
    /// <summary>
    /// 保养计划
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.EquipmentManage)]
    public class MaintenancePlanController : ApiBaseController {
        private readonly EquipmentMaintenanceClient _maintenanceClient;
        private readonly EquipmentManageClient _equipmentManageClient;
        private readonly SystemClient _systemClient;
        private readonly EquipmentExjosnHandler _handler;

        /// <summary>
        /// 构造函数
        /// </summary>
        public MaintenancePlanController(EquipmentMaintenanceClient maintenanceClient,
                                         EquipmentExjosnHandler handler,
                                         EquipmentManageClient equipmentManageClient,
                                         SystemClient systemClient) {
            _maintenanceClient = maintenanceClient;
            _handler = handler;
            _equipmentManageClient = equipmentManageClient;
            _systemClient = systemClient;
        }
        /// <summary>
        /// 获取保养计划列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMaintenancePlanList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MaintenancePlanEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _maintenanceClient.GetMaintenancePlanListAsync(whereExpression.ToExpression());
                await _handler.FillMaintenancePlanExjosn(res);
                return Success(res);
            } else {
                var res = await _maintenanceClient.GetMaintenancePlanPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetMaintenancePlanDto(res.Record);
                var data = res.Adapt<PaginationModel<MaintenancePlanDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取保养计划
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMaintenancePlan(long id) {
            var dto = new MaintenancePlanDetailDto<MaintenancePlanEntity, MaintenancePlan2ProjectEntity, MaintenanceProjectEntity>();
            dto.Model = await _maintenanceClient.GetMaintenancePlanAsync(id);
            if (dto.Model != null) {
                var temps = await _maintenanceClient.GetMaintenancePlan2ProjectListByPlanIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.MaintenanceProjectId).Distinct().ToList();
                    dto.DataList = await _maintenanceClient.GetMaintenanceProjectListAsync(p => ids.Contains(p.Id));
                    await _handler.FillMaintenanceProjectExjosn(dto.DataList);
                }
                dto.EquipmentModel = await _equipmentManageClient.GetEquipmentInfoAsync(dto.Model.EquipmentId ?? 0);
                if (dto.EquipmentModel != null) {
                    await _handler.FillEquipmentInfoExjosn(new List<EquipmentInfoEntity> { dto.EquipmentModel });
                }
                //规则
                dto.ExecuteRuleModel = await _systemClient.GetExecuteRuleInfoAsync(dto.Model.ExecuteRuleId);
                await _handler.FillMaintenancePlanExjosn(new List<MaintenancePlanEntity> { dto.Model });
            }
            return Success(dto);
        }
        /// <summary>
        /// 保存保养计划
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMaintenancePlan(SaveParameter<MaintenancePlanEntity, MaintenancePlan2ProjectEntity> parameter) {
            var resId = await _maintenanceClient.SaveMaintenancePlanAsync(parameter.Model);
            if (resId > 0) {
                if (!parameter.AddRelations.None()) {
                    await _maintenanceClient.InsertMaintenancePlan2ProjectAsync(resId, parameter.AddRelations);
                }
                if (!parameter.EidtRelations.None()) {
                    await _maintenanceClient.UpdateMaintenancePlan2ProjectAsync(parameter.EidtRelations);
                }
                if (!parameter.DeleteRelations.None()) {
                    await _maintenanceClient.DeleteMaintenancePlan2ProjectAsync(parameter.DeleteRelations);
                }
            }
            return resId > 0 ? Success() : Fail();

        }
        /// <summary>
        /// 删除保养计划
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMaintenancePlan(List<long> ids) {
            var res = await _maintenanceClient.DeleteMaintenancePlanAsync(ids);
            return res ? Success() : Fail();
        }


    }
}
