﻿using MapleLeaf.Core.Excel;

namespace DiLe.Mes.Cloud.Controllers {
    /// <summary>
    /// 
    /// </summary>
    public class CommonHelper {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ExportColumn> GetAOIHeaderRows() {
            var dic = new List<ExportColumn>() {
              new() { Field ="EquipmentCode", Title= "设备编号" },
              new() { Field = "EquipmentName", Title=  "设备名称" },
              new() { Field = "Specification", Title=  "规格型号" },
              new() { Field ="EquipmentType", Title=  "设备类型" },
              new() { Field = "FactoryName", Title=  "所在工厂" },
              new() { Field = "EquipmentPosition",  Title= "存储位置" },
              new() { Field ="CheckNGNo",  Title= "检测NG数" },
              new() { Field = "CheckPendTimes", Title=  "检测待定数" },
              new() { Field ="CheckTotalTimes", Title=  "检测累计总数" },
              new() { Field ="CheckTimes", Title=  "检测总数(Wh)" }
            };
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ExportColumn> GetTrimHeaderRows() {
            var dic = new List<ExportColumn> {
                new() { Field = "EquipmentCode", Title = "设备编号" },
                new() { Field = "EquipmentName", Title = "设备名称" },
                new() { Field = "Specification", Title=  "规格型号" },
                new() { Field = "EquipmentType", Title = "设备类型" },
                new() { Field = "FactoryName", Title=  "所在工厂" },
                new() { Field = "EquipmentPosition",  Title= "存储位置" },
                new() { Field = "TrimmingPressure", Title = "切箍边压力(%)" },
                new() { Field = "TrimingTimes", Title = "切边A次数" },
                new() { Field = "TrimingTotalTimes", Title = "切箍边累计次数" },
            };
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ExportColumn> GetFormingHeaderRows() {
            var dic = new List<ExportColumn> {
                new() { Field ="EquipmentCode",Title= "设备编号" },
                new() { Field ="EquipmentName",Title= "设备名称" },
                new() { Field ="Specification",Title= "规格型号" },
                new() { Field ="EquipmentType",Title= "设备类型" },
                new() { Field ="FactoryName",Title= "所在工厂" },
                new() { Field ="EquipmentPosition",Title= "存储位置" },
                new() { Field ="EeTotal",Title= "电能累计(Wh)" },
                new() { Field ="FormingTotalTimes",Title= "成型累计次数" },
                new() { Field ="FormingSuctionTime",Title= "成型吸浆时间(S)" },
                new() { Field ="FormingDehydrationTime",Title= "成型脱水时间" },
                new() { Field ="LeftHPTotalTimes",Title= "左侧热压累计次数" },
                new() { Field ="LeftHotPressure",Title= "左侧热压压力(%)" },
                new() { Field ="LeftHPTime",Title= "左侧热压时间(S)" },
                new() { Field ="LeftUpperTempSet",Title= "左侧热压上模温度设定(℃)" },
                new() { Field ="LeftDownTempSet",Title= "左侧热压下模温度设定(℃)" },
                new() { Field ="LeftUpperTemp1",Title= "左侧热压上模温度(℃)" },
                new() { Field ="LeftUpperTemp2",Title= "左侧热压上模温度(℃)" },
                new() { Field ="LeftUpperTemp3",Title= "左侧热压上模温度(℃)" },
                new() { Field ="LeftUpperTemp4",Title= "左侧热压上模温度(℃)" },
                new() { Field ="LeftDownTemp1",Title= "左侧热压下模温度(℃)" },
                new() { Field ="LeftDownTemp2",Title= "左侧热压下模温度(℃)" },
                new() { Field ="LeftDownTemp3",Title= "左侧热压下模温度(℃)" },
                new() { Field ="LeftDownTemp4",Title= "左侧热压下模温度(℃)" },
                new() { Field ="LeftBakeTime",Title= "左侧烘烤时间(S)" },
                new() { Field ="LeftPressurizeTime",Title= "左侧加压时间(S)" },
                new() { Field ="RightHPToatalTimes",Title= "右侧热压累计次数" },
                new() { Field ="RightHotPressure",Title= "右侧热压压力(%)" },
                new() { Field ="RightHPTime",Title= "右侧热压时间(S)" },
                new() { Field ="RightUpperTempSet",Title= "右侧热压上模温度设定(℃)" },
                new() { Field ="RightDownTempSet",Title= "右侧热压下模温度设定(℃)" },
                new() { Field ="RightUpperTemp1",Title= "右侧热压上模温度(℃)" },
                new() { Field ="RightUpperTemp2",Title= "右侧热压上模温度(℃)" },
                new() { Field ="RightUpperTemp3",Title= "右侧热压上模温度(℃)" },
                new() { Field ="RightUpperTemp4",Title= "右侧热压上模温度(℃)" },
                new() { Field ="RightDownTemp1",Title= "右侧热压下模温度(℃)" },
                new() { Field ="RightDownTemp2",Title= "右侧热压下模温度(℃)" },
                new() { Field ="RightDownTemp3",Title= "右侧热压下模温度(℃)" },
                new() { Field ="RightDownTemp4",Title= "右侧热压下模温度(℃)" },
                new() { Field ="RightBrakeTime",Title= "右侧烘烤时间(S)" },
                new() { Field ="RightPressurizeTime",Title= "右侧加压时间(S)" }
            };
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ExportColumn> GetOtherHeaderRows() {
            var dic = new List<ExportColumn> {
                new() { Field = "EquipmentCode", Title = "设备编号" },
                new() { Field = "EquipmentName", Title = "设备名称" },
                new() { Field = "Specification", Title = "规格型号" },
                new() { Field = "EquipmentType", Title = "设备类型" },
                new() { Field = "FactoryName", Title = "所在工厂" },
                new() { Field = "EquipmentPosition", Title = "存储位置" },
                new() { Field = "EeTotal", Title = "电能累计(Wh)" },
                new() { Field = "FormingPressure", Title = "成型压力(Kg)" },
                new() { Field = "FormingTimes", Title = "成型次数" },
                new() { Field = "FormingTime", Title = "成型时间(S)" },
                new() { Field = "LeftHPTimes", Title = "左侧热压次数" },
                new() { Field = "LeftHPTotalTimes", Title = "左侧热压累计次数" },
                new() { Field = "LeftHotPressure", Title = "左侧热压压力(Kg)" },
                new() { Field = "LeftHPTime", Title = "左侧热压时间(S)" },
                new() { Field = "LeftUpperTempSet", Title = "左侧热压上模温度设定(℃)" },
                new() { Field = "LeftDownTempSet", Title = "左侧热压下模温度设定(℃)" },
                new() { Field = "LeftUpperTemp1", Title = "左侧热压上模温度(℃)" },
                new() { Field = "LeftUpperTemp2", Title = "左侧热压上模温度(℃)" },
                new() { Field = "LeftUpperTemp3", Title = "左侧热压上模温度(℃)" },
                new() { Field = "LeftUpperTemp4", Title = "左侧热压上模温度(℃)" },
                new() { Field = "LeftDownTemp1", Title = "左侧热压下模温度(℃)" },
                new() { Field = "LeftDownTemp2", Title = "左侧热压下模温度(℃)" },
                new() { Field = "LeftDownTemp3", Title = "左侧热压下模温度(℃)" },
                new() { Field = "LeftDownTemp4", Title = "左侧热压下模温度(℃)" },
                new() { Field = "LeftBakeTime", Title = "左侧烘烤时间(S)" },
                new() { Field = "LeftPressurizeTime", Title = "左侧加压时间(S)" },
                new() { Field = "RightHPTimes", Title = "右侧热压次数" },
                new() { Field = "RightHPToatalTimes", Title = "右侧热压累计次数" },
                new() { Field = "RightHotPressure", Title = "右侧热压压力(Kg)" },
                new() { Field = "RightHPTime", Title = "右侧热压时间(S)" },
                new() { Field = "RightUpperTempSet", Title = "右侧热压上模温度设定(℃)" },
                new() { Field = "RightDownTempSet", Title = "右侧热压下模温度设定(℃)" },
                new() { Field = "RightUpperTemp1", Title = "右侧热压上模温度(℃)" },
                new() { Field = "RightUpperTemp2", Title = "右侧热压上模温度(℃)" },
                new() { Field = "RightUpperTemp3", Title = "右侧热压上模温度(℃)" },
                new() { Field = "RightUpperTemp4", Title = "右侧热压上模温度(℃)" },
                new() { Field = "RightDownTemp1", Title = "右侧热压下模温度(℃)" },
                new() { Field = "RightDownTemp2", Title = "右侧热压下模温度(℃)" },
                new() { Field = "RightDownTemp3", Title = "右侧热压下模温度(℃)" },
                new() { Field = "RightDownTemp4", Title = "右侧热压下模温度(℃)" },
                new() { Field = "RightBrakeTime", Title = "右侧烘烤时间(S)" },
                new() { Field = "RightPressurizeTime", Title = "右侧加压时间(S)" },
                new() { Field = "CutPressure_1311", Title = "切边机压力(T)" },
                new() { Field = "CutCount_1311", Title = "切边次数" },
                new() { Field = "CutTotalCount_1311", Title = "切边累计次数" },
                new() { Field = "CheckNg_1311", Title = "检测NG数" },
                new() { Field = "CheckReady_1311", Title = "检测待定数" },
                new() { Field = "CheckNum_1311", Title = "检测总数" },
                new() { Field = "CheckTotalNum_1311", Title = "检测累计总数" },
                new() { Field = "StackingNum", Title = "堆叠次数" },
                new() { Field = "StackingFloorNum", Title = "堆叠层数" },
                new() { Field = "StackingTotalNum", Title = "堆叠数量累计" }
            };
            return dic;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ExportColumn> GetEMHeaderRows() {
            var dic = new List<ExportColumn> {
                new() { Field = "DeviceNo", Title = "设备名称" },
                new() { Field = "DeviceBrand", Title = "设备类型" },
                new() { Field = "EcTotal", Title = "累计电能(kWh)" }
            };
            return dic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<ExportColumn> GetFMHeaderRows() {
            var dic = new List<ExportColumn> {
               new() { Field = "DeviceNo", Title = "设备名称" },
               new() { Field = "DeviceBrand", Title = "设备类型" },
               new() { Field = "FlowTotal", Title = "累计流量(m³)" }
            };
            return dic;
        }
    }
}
