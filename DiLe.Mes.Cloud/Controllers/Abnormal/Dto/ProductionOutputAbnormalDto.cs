﻿using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;

namespace DiLe.Mes.Cloud.Controllers.Abnormal.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class ProductionOutputAbnormalDto {
        /// <summary>
        /// 
        /// </summary>
        public ProductionOutputAbnormalEntity Model { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public EquipmentInfoEntity EquipmentInfoModel { get; set; }
    }


}
