﻿using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.Mould.Entity.Info;

namespace DiLe.Mes.Cloud.Controllers.Abnormal.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class MouldAbnormalDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldAbnormalEntity Model { get; set; }
        /// <summary>
        /// 模具
        /// </summary>
        public MouldInfoEntity MouldInfoModel { get; set; }
    }
}
