﻿using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;

namespace DiLe.Mes.Cloud.Controllers.Abnormal.Dto {
    /// <summary>
    /// 设备异常
    /// </summary>
    public class EquipmentAbnormalDto {
        /// <summary>
        /// 设备异常
        /// </summary>
        public AlarmInfoStatisticsEntity Model { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public EquipmentInfoEntity EquipmentInfoModel { get; set; }

    }
    /// <summary>
    /// 设备异常
    /// </summary>
    public class AbnormalPointPositionDto {
        /// <summary>
        /// 
        /// </summary>
        public string FileId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<AbnormalInfo> AbnormalInfos { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class AbnormalInfo : AlarmInfoStatisticsEntity {
        /// <summary>
        /// 
        /// </summary>
        public string Position { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FileId { get; set; }
        /// <summary>
        /// 维修记录id
        /// </summary>
        public long RepairRecordId { get; set; }

        /// <summary>
        /// 设备Id
        /// </summary>
        public long? EquipmentId { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string? EquipmentSpecification { get; set; }
        /// <summary>
        /// 所在工厂名称
        /// </summary>
        public string? EquipmentFactoryName{ get; set; }
        /// <summary>
        /// 设备类型名称
        /// </summary>
        public string? EquipmentTypeName { get; set; }
        /// <summary>
        /// 设备位置
        /// </summary>
        public string? EquipmentPositionName { get; set; }
    }
}
