﻿using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Process.Entity;

namespace DiLe.Mes.Cloud.Controllers.Abnormal.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class ProductionQualityAbnormalDto {
        /// <summary>
        /// 
        /// </summary>
        public ProductionQualityAbnormalEntity Model { get; set; }
        /// <summary>
        /// 设备
        /// </summary>
        public EquipmentInfoEntity EquipmentModel { get; set; }
        /// <summary>
        /// 工序
        /// </summary>
        public ProcessEntity ProcessModel { get; set; }
    }


}
