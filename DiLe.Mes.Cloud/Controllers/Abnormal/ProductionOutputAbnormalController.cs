using DiLe.Mes.Application.Common;
using DiLe.Mes.Cloud.Controllers.Abnormal.Dto;
using DiLe.Mes.Model.Common.Abnormal.Entity;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Abnormal {
    /// <summary>
    /// 产出量异常
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Abnormal)]
    public class ProductionOutputAbnormalController : ApiBaseController {
        private readonly AbnormalClient _commonAbnormalClient;
        private readonly AbnormalExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProductionOutputAbnormalController(AbnormalClient commonAbnormal, AbnormalExjosnHandler handler) {
            _commonAbnormalClient = commonAbnormal;
            _handler = handler;
        }
        /// <summary>
        /// 获取产出量异常列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetProductionOutputAbnormalList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<ProductionOutputAbnormalEntity>();
            //if (!parameter.Keyword.IsNullOrEmpty()) {
            //    whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            //}
            if (parameter.Pagination == null) {
                var res = await _commonAbnormalClient.GetProductionOutputAbnormalListAsync(whereExpression.ToExpression());
                var list = await _handler.GetProductionOutputAbnormalDto(res);
                return Success(list);
            } else {
                var res = await _commonAbnormalClient.GetProductionOutputAbnormalPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetProductionOutputAbnormalDto(res.Record);
                var data = res.Adapt<PaginationModel<ProductionOutputAbnormalDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取产出量异常
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetProductionOutputAbnormal(long id) {
            var res = await _commonAbnormalClient.GetProductionOutputAbnormalInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存产出量异常
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveProductionOutputAbnormal(ProductionOutputAbnormalEntity entity) {
            var resId = await _commonAbnormalClient.SaveProductionOutputAbnormalAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除产出量异常
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteProductionOutputAbnormal(List<long> ids) {
            var res = await _commonAbnormalClient.DeleteProductionOutputAbnormalAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
