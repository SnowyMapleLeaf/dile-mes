﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Cloud.Controllers.Abnormal.Dto;
using DiLe.Mes.Cloud.Controllers.Mould;
using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Common.Abnormal.Entity;
using DiLe.Mes.Model.Common.Equipment.Entity.Manage;
using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Process.Entity;

namespace DiLe.Mes.Cloud.Controllers.Abnormal
{
    /// <summary>
    /// 异常
    /// </summary>
    public class AbnormalExjosnHandler {
        private readonly EquipmentManageClient _equipmentClient;
        private readonly EquipmentExjosnHandler _equipmentExjosnHandler;
        private readonly SystemClient _systemClient;
        private readonly MouldInfoClient _mouldInfoClient;
        private readonly MouldExjosnHandler _mouldExjosnHandler;
        /// <summary>
        /// 
        /// </summary>
        public AbnormalExjosnHandler(EquipmentManageClient equipmentManageClient,
                                     EquipmentExjosnHandler equipmentExjosnHandler,
                                     SystemClient systemClient,
                                     MouldInfoClient mouldInfoClient,
                                     MouldExjosnHandler mouldExjosnHandler) {

            _equipmentClient = equipmentManageClient;
            _equipmentExjosnHandler = equipmentExjosnHandler;
            _systemClient = systemClient;
            _mouldInfoClient = mouldInfoClient;
            _mouldExjosnHandler = mouldExjosnHandler;
        }
        /// <summary>
        /// 获取产品品质异常视图模型列表
        /// </summary>
        /// <param name="dataList"></param>
        /// <returns></returns>
        public async Task<List<ProductionQualityAbnormalDto>> GetProductionQualityAbnormalDto(List<ProductionQualityAbnormalEntity> dataList) {
            var list = new List<ProductionQualityAbnormalDto>();
            if (!dataList.Any()) {
                return list;
            }
            //设备
            var equipmentIds = dataList.Select(p => p.EquipmentId).Distinct().ToList();
            var equipmentList = await _equipmentClient.GetEquipmentInfoListAsync(p => equipmentIds.Contains(p.Id));
            await _equipmentExjosnHandler.FillEquipmentInfoExjosn(equipmentList);
            //制程-工序
            var processIds = dataList.Select(p => p.ProcessId).Distinct().ToList();
            var processList = await _systemClient.GetProcessListAsync(p => processIds.Contains(p.Id));
            foreach (var item in dataList) {
                var dto = new ProductionQualityAbnormalDto();
                dto.Model = item;
                dto.EquipmentModel = equipmentList.FirstOrDefault(p => p.Id == item.EquipmentId) ?? new EquipmentInfoEntity();
                dto.ProcessModel = processList.FirstOrDefault(p => p.Id == item.ProcessId) ?? new ProcessEntity();
                list.Add(dto);
            }
            return list;
        }

        /// <summary>
        /// 获取模具异常视图模型列表
        /// </summary>
        /// <param name="dataList"></param>
        /// <returns></returns>
        public async Task<List<MouldAbnormalDto>> GetMouldAbnormalDto(List<MouldAbnormalEntity> dataList) {
            var list = new List<MouldAbnormalDto>();
            if (!dataList.Any()) {
                return list;
            }
            //模具
            var mouldInfoIds = dataList.Select(p => p.MouldInfoId).Distinct().ToList();
            var mouldInfoList = await _mouldInfoClient.GetMouldInfoListAsync(p => mouldInfoIds.Contains(p.Id));
            await _mouldExjosnHandler.FillMouldInfoExjosn(mouldInfoList);

            foreach (var item in dataList) {
                var dto = new MouldAbnormalDto();
                dto.Model = item;
                dto.MouldInfoModel = mouldInfoList.FirstOrDefault(p => p.Id == item.MouldInfoId) ?? new MouldInfoEntity();
                list.Add(dto);
            }
            return list;
        }
        /// <summary>
        /// 获取设备异常视图模型列表
        /// </summary>
        /// <param name="dataList"></param>
        /// <param name="abnormals"></param>
        /// <returns></returns>
        public async Task<List<EquipmentAbnormalDto>> GetEquipmentAbnormalDto(List<AlarmInfoStatisticsEntity> dataList, List<AbnormalAlarmRuleEntity> abnormals) {
            var list = new List<EquipmentAbnormalDto>();
            if (!dataList.Any()) {
                return list;
            }
            //设备
            var equipmentCodes = dataList.Select(p => p.EquipmentCode).Distinct().ToList();
            var equipmentList = await _equipmentClient.GetEquipmentInfoListAsync(p => equipmentCodes.Contains(p.Code));
            await _equipmentExjosnHandler.FillEquipmentInfoExjosn(equipmentList);

            foreach (var item in dataList) {
                var eq = equipmentList.FirstOrDefault(p => p.Code == item.EquipmentCode) ?? new EquipmentInfoEntity();

                var dto = new EquipmentAbnormalDto {
                    Model = item,
                    EquipmentInfoModel = eq
                };
                list.Add(dto);

            }
            return list;
        }

        /// <summary>
        /// 获取产出异常视图模型列表
        /// </summary>
        /// <param name="dataList"></param>
        /// <returns></returns>
        public async Task<List<ProductionOutputAbnormalDto>> GetProductionOutputAbnormalDto(List<ProductionOutputAbnormalEntity> dataList) {
            var list = new List<ProductionOutputAbnormalDto>();
            if (!dataList.Any()) {
                return list;
            }
            //设备
            var equipmentIds = dataList.Select(p => p.EquipmentInfoId).Distinct().ToList();
            var equipmentList = await _equipmentClient.GetEquipmentInfoListAsync(p => equipmentIds.Contains(p.Id));
            await _equipmentExjosnHandler.FillEquipmentInfoExjosn(equipmentList);

            foreach (var item in dataList) {
                var dto = new ProductionOutputAbnormalDto();
                dto.Model = item;
                dto.EquipmentInfoModel = equipmentList.FirstOrDefault(p => p.Id == item.EquipmentInfoId) ?? new EquipmentInfoEntity();
                list.Add(dto);
            }
            return list;
        }

    }
}
