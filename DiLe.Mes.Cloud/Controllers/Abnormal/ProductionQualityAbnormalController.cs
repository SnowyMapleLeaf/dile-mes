using DiLe.Mes.Application.Common;
using DiLe.Mes.Cloud.Controllers.Abnormal.Dto;
using DiLe.Mes.Model.Common.Abnormal.Entity;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Abnormal {
    /// <summary>
    /// 产品品质异常
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Abnormal)]
    public class ProductionQualityAbnormalController : ApiBaseController {
        private readonly AbnormalClient _commonAbnormalClient;
        private readonly AbnormalExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProductionQualityAbnormalController(AbnormalClient commonAbnormal, AbnormalExjosnHandler handler) {
            _commonAbnormalClient = commonAbnormal;
            _handler = handler;
        }
        /// <summary>
        /// 获取产品品质异常列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetProductionQualityAbnormalList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<ProductionQualityAbnormalEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _commonAbnormalClient.GetProductionQualityAbnormalListAsync(whereExpression.ToExpression());
                var list = await _handler.GetProductionQualityAbnormalDto(res);
                return Success(list);
            } else {
                var res = await _commonAbnormalClient.GetProductionQualityAbnormalPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetProductionQualityAbnormalDto(res.Record);
                var data = res.Adapt<PaginationModel<ProductionQualityAbnormalDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取产品品质异常
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetProductionQualityAbnormal(long id) {
            var res = await _commonAbnormalClient.GetProductionQualityAbnormalInfoAsync(id);
            var dto = new ProductionQualityAbnormalDto();
            if (res != null) {
                var dataList = await _handler.GetProductionQualityAbnormalDto(new List<ProductionQualityAbnormalEntity> { res });
                dto = dataList?.FirstOrDefault();
            }
            return Success(dto);
        }
        /// <summary>
        /// 保存产品品质异常
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveProductionQualityAbnormal(ProductionQualityAbnormalEntity entity) {
            var resId = await _commonAbnormalClient.SaveProductionQualityAbnormalAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除产品品质异常
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteProductionQualityAbnormal(List<long> ids) {
            var res = await _commonAbnormalClient.DeleteProductionQualityAbnormalAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
