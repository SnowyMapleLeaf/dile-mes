using DiLe.Mes.Application.Common;
using DiLe.Mes.Cloud.Controllers.Abnormal.Dto;
using DiLe.Mes.Model.Common.Abnormal.Entity;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Abnormal {
    /// <summary>
    /// 模具异常
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Abnormal)]
    public class MouldAbnormalController : ApiBaseController {
        private readonly AbnormalClient _commonAbnormalClient;
        private readonly AbnormalExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldAbnormalController(AbnormalClient commonAbnormal, AbnormalExjosnHandler handler) {
            _commonAbnormalClient = commonAbnormal;
            _handler = handler;
        }
        /// <summary>
        /// 获取模具异常列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldAbnormalList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldAbnormalEntity>();
            //if (!parameter.Keyword.IsNullOrEmpty()) {
            //    whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
            //    whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            //}
            if (parameter.Pagination == null) {
                var res = await _commonAbnormalClient.GetMouldAbnormalListAsync(whereExpression.ToExpression());
                var list = await _handler.GetMouldAbnormalDto(res);
                return Success(list);
            } else {
                var res = await _commonAbnormalClient.GetMouldAbnormalPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetMouldAbnormalDto(res.Record);
                var data = res.Adapt<PaginationModel<MouldAbnormalDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取模具异常
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldAbnormal(long id) {
            var res = await _commonAbnormalClient.GetMouldAbnormalInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存模具异常
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldAbnormal(MouldAbnormalEntity entity) {
            var resId = await _commonAbnormalClient.SaveMouldAbnormalAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除模具异常
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldAbnormal(List<long> ids) {
            var res = await _commonAbnormalClient.DeleteMouldAbnormalAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
