using DiLe.Mes.Application.Cloud.Statistics;
using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Equipment;
using DiLe.Mes.Cloud.Controllers.Abnormal.Dto;
using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Common.Abnormal.Entity;
using Mapster;
using Microsoft.AspNetCore.Authorization;

namespace DiLe.Mes.Cloud.Controllers.Abnormal {
    /// <summary>
    /// 设备异常
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Abnormal)]
    public class EquipmentAbnormalController : ApiBaseController {
        private readonly StatisticsClient _statistics;
        private readonly AbnormalExjosnHandler _handler;
        private readonly EquipmentManageClient _manageClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        public EquipmentAbnormalController(StatisticsClient statisticsClient, AbnormalExjosnHandler handler, EquipmentManageClient manageClient) {
            _statistics = statisticsClient;
            _handler = handler;
            _manageClient = manageClient;
        }
        /// <summary>
        /// 获取设备异常列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetEquipmentAbnormalList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<AlarmInfoStatisticsEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.And(p => p.EquipmentCode.Contains(parameter.Keyword));
            }

            //设备报警信息
            var aaInfoList = await SqlSugarHelper.MasterDb.Queryable<AbnormalAlarmRuleEntity>().ToListAsync();

            if (parameter.Pagination == null) {
                var res = await _statistics.GetAlarmInfoStatisticsListAsync(whereExpression.ToExpression());
                var list = _handler.GetEquipmentAbnormalDto(res, aaInfoList);
                return Success(list);
            } else {
                var res = await _statistics.GetAlarmInfoStatisticsPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetEquipmentAbnormalDto(res.Record, aaInfoList);
                var data = res.Adapt<PaginationModel<EquipmentAbnormalDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取设备异常
        /// </summary>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public async Task<ApiResult<AbnormalPointPositionDto>> GetAbnormalInfoByEquipmentCode(string equipmentCode) {
            var dto = new AbnormalPointPositionDto() { AbnormalInfos = [] };
            var eqinfo = await _manageClient.GetEquipmentInfoAsync(x => x.Code == equipmentCode);
            //类型
            var type = await _manageClient.GetEquipmentTypeAsync(eqinfo.TypeId);
            dto.FileId = type.FileId ?? "";
            //设备报警信息
            var ruleList = await _manageClient.GetAbnormalAlarmRuleListAsync(p => p.EquipmentTypeId == eqinfo.TypeId);
            var whereExpression = Expressionable.Create<AlarmInfoStatisticsEntity>();
            whereExpression.And(x => x.EquipmentCode == equipmentCode);
            whereExpression.And(x => x.EndAbnormalTime == null);

            var reslist = await _statistics.GetAlarmInfoStatisticsListAsync(whereExpression.ToExpression());
            //Random rand = new Random();

            foreach (var item in reslist) {
                var im = item.Adapt<AbnormalInfo>();

                var rule = ruleList?.FirstOrDefault(x => x.AlarmName == item.AbnormalInfo);
                if (rule == null) {
                    continue;
                }
                im.Position = rule?.Position ?? "";
                if (im.Position.IsNullOrEmpty()) {
                    continue;
                }
                im.AbnormalLevel = rule?.AlarmLevel ?? 0;
                dto.AbnormalInfos.Add(im);
            }
            return Success(dto);
        }
    }
}
