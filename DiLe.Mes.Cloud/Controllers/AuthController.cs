﻿using Microsoft.AspNetCore.Authorization;

namespace DiLe.Mes.Cloud.Controllers {
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class AuthController : ApiBaseController {

        private readonly AuthClient _authClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        public AuthController(AuthClient authClient) {
            _authClient = authClient;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ApiResult> Login(LoginInput input) {
            var res = await _authClient.Login(input);
            return Success(res);
        }
        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public ApiResult LogOut(string account) {
            _authClient.LogOut(account);
            return Success();
        }
    }
}
