using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Common.Mould.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common;
using DiLe.Mes.Model.Common.Mould.Entity.SparePart;
using DiLe.Mes.Model.Common.Mould.Relation.SparePart;

namespace DiLe.Mes.Cloud.Controllers.Mould.SparePart
{
    /// <summary>
    /// 备件出库
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldSparePartStockOutController : ApiBaseController {
        private readonly MouldSparePartClient _mouldSparePartClient;
        private readonly MouldExjosnHandler _handler;
        private readonly SystemClient _systemClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldSparePartStockOutController(MouldSparePartClient mouldSparePart, MouldExjosnHandler handler, SystemClient systemClient) {
            _mouldSparePartClient = mouldSparePart;
            _handler = handler;
            _systemClient = systemClient;
        }
        /// <summary>
        /// 获取备件出库列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldSparePartStockOutList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldSparePartStockOutEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldSparePartClient.GetMouldSparePartStockOutListAsync(whereExpression.ToExpression());
                await _handler.FillMouldSparePartStockOutExjosn(res);
                return Success(res);
            } else {
                var res = await _mouldSparePartClient.GetMouldSparePartStockOutPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillMouldSparePartStockOutExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取备件出库
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldSparePartStockOut(long id) {
            var dto = new MouldSparePartStockOutDetailDto();
            var res = await _mouldSparePartClient.GetMouldSparePartStockOutInfoAsync(id);
            dto.Model = res;
            if (res.Id > 0) {
                var temps = await _mouldSparePartClient.GetMouldStockOut2SparePartListByStockOutIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.SparePartId).Distinct().ToList();
                    var dataList = await _mouldSparePartClient.GetMouldSparePartLedgerListAsync(p => ids.Contains(p.Id));
                    dto.DataList = await _handler.FillMouldSparePartLedgerExjosn(dataList);
                }
            }
            await _handler.FillMouldSparePartStockOutExjosn(new List<MouldSparePartStockOutEntity> { dto.Model });
            return Success(dto);
        }
        /// <summary>
        /// 保存备件出库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldSparePartStockOut(SaveParameter<MouldSparePartStockOutEntity, MouldStockOut2SparePartEntity> parameter) {
            var res = await _mouldSparePartClient.SaveMouldSparePartStockOutAsync(parameter.Model);
            if (res.Id > 0) {
                if (!parameter.AddRelations.None()) {
                    await _mouldSparePartClient.InsertMouldStockOut2SparePartAsync(res.Id, parameter.AddRelations);
                    var maps = parameter.AddRelations.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in maps) {
                        await MinusSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                }
                if (!parameter.EidtRelations.None()) {
                    await _mouldSparePartClient.UpdateMouldStockOut2SparePartAsync(parameter.EidtRelations);
                    var ids = parameter.EidtRelations.ConvertAll(x => x.Id);
                    var oldRels = await _mouldSparePartClient.GetMouldStockOut2SparePartListAsync(p => ids.Contains(p.Id));
                    var oldmaps = oldRels.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in oldmaps) {
                        await MinusSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                    var maps = parameter.EidtRelations.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in maps) {
                        await AddSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                }
                if (!parameter.DeleteRelations.None()) {
                    await _mouldSparePartClient.DeleteMouldStockOut2SparePartAsync(parameter.DeleteRelations);
                    var maps = parameter.AddRelations.GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                    foreach (var item in maps) {
                        await AddSparePartInventory(res.WarehouseId, item.Key, item.Item2);
                    }
                }
            }
            return res.Id > 0 ? Success() : Fail();

        }
        /// <summary>
        /// 删除备件出库 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldSparePartStockOut(List<long> ids) {
            var rels = await _mouldSparePartClient.GetMouldStockOut2SparePartListAsync(p => ids.Contains(p.StockOutId));
            var dataList = await _mouldSparePartClient.GetMouldSparePartStockOutListAsync(p => ids.Contains(p.Id));
            foreach (var item in dataList) {
                var maps = rels.Where(p => p.StockOutId == item.Id).GroupBy(p => p.SparePartId).Select(t => (t.Key, t.Sum(x => x.StockOutNumber))).ToList();
                foreach (var map in maps) {
                    await AddSparePartInventory(item.WarehouseId, map.Key, map.Item2);
                }
            }
            var res = await _mouldSparePartClient.DeleteMouldSparePartStockOutAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="sparePartId"></param>
        /// <param name="quantity"></param>
        private async Task AddSparePartInventory(long warehouseId, long sparePartId, int quantity) {
            var inventory = await _systemClient.GetSparePartInventoryAsync(p => p.SparePartId == sparePartId && p.WarehouseId == warehouseId);
            if (inventory == null) {
                inventory = new SparePartInventoryEntity() {
                    InventoryQuantity = quantity,
                    SparePartId = sparePartId,
                    WarehouseId = warehouseId
                };
                await _systemClient.InsertSparePartInventoryAsync(inventory);
            } else {
                inventory.InventoryQuantity += quantity;
                await _systemClient.UpdateSparePartInventoryAsync(new List<SparePartInventoryEntity> { inventory });
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="sparePartId"></param>
        /// <param name="quantity"></param>
        private async Task MinusSparePartInventory(long warehouseId, long sparePartId, int quantity) {
            var inventory = await _systemClient.GetSparePartInventoryAsync(p => p.SparePartId == sparePartId && p.WarehouseId == warehouseId);
            if (inventory != null) {
                inventory.InventoryQuantity -= quantity;
                await _systemClient.UpdateSparePartInventoryAsync(new List<SparePartInventoryEntity> { inventory });
            }
        }
    }
}
