using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Common.Mould.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Cloud.Controllers.Mould;
using DiLe.Mes.Model.Common;
using DiLe.Mes.Model.Common.Mould.Entity.SparePart;

namespace DiLe.Mes.Cloud.Controllers.Mould.SparePart
{
    /// <summary>
    /// 备件入库
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldSparePartStockInController : ApiBaseController {
        private readonly MouldSparePartClient _mouldSparePartClient;
        private readonly MouldExjosnHandler _handler;
        private readonly SystemClient _systemClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldSparePartStockInController(MouldSparePartClient mouldSparePart, MouldExjosnHandler handler, SystemClient systemClient) {
            _mouldSparePartClient = mouldSparePart;
            _handler = handler;
            _systemClient = systemClient;
        }
        /// <summary>
        /// 获取备件入库列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldSparePartStockInList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldSparePartStockInEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldSparePartClient.GetMouldSparePartStockInListAsync(whereExpression.ToExpression());
                await _handler.FillMouldSparePartStockInExjosn(res);
                return Success(res);
            } else {
                var res = await _mouldSparePartClient.GetMouldSparePartStockInPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillMouldSparePartStockInExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取备件入库
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldSparePartStockIn(long id) {
            var dto = new MouldSparePartStockInDto();
            var res = await _mouldSparePartClient.GetMouldSparePartStockInInfoAsync(id);
            if (res.Id > 0) {
                dto.Model = res;
                var sparePartLedgerModel = await _mouldSparePartClient.GetMouldSparePartLedgerInfoAsync(dto.Model.SparePartId);
                if (sparePartLedgerModel != null) {
                    dto.MouldSparePartLedgerModel = await _handler.FillMouldSparePartLedgerExjosn(sparePartLedgerModel);
                }
            }
            await _handler.FillMouldSparePartStockInExjosn(new List<MouldSparePartStockInEntity> { res });
            return Success(dto);
        }
        /// <summary>
        /// 保存备件入库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldSparePartStockIn(MouldSparePartStockInEntity entity) {
            if (entity.Id > 0) {
                var oldData = await _mouldSparePartClient.GetMouldSparePartStockInInfoAsync(entity.Id);
                if (oldData.WarehouseId == entity.WarehouseId) {
                    if (oldData.StockInNumber > entity.StockInNumber) {
                        await MinusSparePartInventory(oldData.WarehouseId, oldData.SparePartId, oldData.StockInNumber - entity.StockInNumber);
                    } else {
                        await AddSparePartInventory(entity.WarehouseId, entity.SparePartId, entity.StockInNumber - oldData.StockInNumber);
                    }
                } else {
                    await MinusSparePartInventory(oldData.WarehouseId, oldData.SparePartId, oldData.StockInNumber);
                    await AddSparePartInventory(entity.WarehouseId, entity.SparePartId, entity.StockInNumber);
                }
            } else {
                await AddSparePartInventory(entity.WarehouseId, entity.SparePartId, entity.StockInNumber);
            }
            var resId = await _mouldSparePartClient.SaveMouldSparePartStockInAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除备件入库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldSparePartStockIn(List<long> ids) {
            var list = await _mouldSparePartClient.GetMouldSparePartStockInListAsync(p => ids.Contains(p.Id));
            await MinusSparePartInventory(list);
            var res = await _mouldSparePartClient.DeleteMouldSparePartStockInAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="sparePartId"></param>
        /// <param name="quantity"></param>
        private async Task AddSparePartInventory(long warehouseId, long sparePartId, int quantity) {
            var inventory = await _systemClient.GetSparePartInventoryAsync(p => p.SparePartId == sparePartId && p.WarehouseId == warehouseId);
            if (inventory == null) {
                inventory = new SparePartInventoryEntity() {
                    InventoryQuantity = quantity,
                    SparePartId = sparePartId,
                    WarehouseId = warehouseId
                };
                await _systemClient.InsertSparePartInventoryAsync(inventory);
            } else {
                inventory.InventoryQuantity += quantity;
                await _systemClient.UpdateSparePartInventoryAsync(new List<SparePartInventoryEntity> { inventory });
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="sparePartId"></param>
        /// <param name="quantity"></param>
        private async Task MinusSparePartInventory(long warehouseId, long sparePartId, int quantity) {
            var inventory = await _systemClient.GetSparePartInventoryAsync(p => p.SparePartId == sparePartId && p.WarehouseId == warehouseId);
            if (inventory != null) {
                inventory.InventoryQuantity -= quantity;
                await _systemClient.UpdateSparePartInventoryAsync(new List<SparePartInventoryEntity> { inventory });
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        private async Task MinusSparePartInventory(List<MouldSparePartStockInEntity> list) {
            if (list.None()) {
                return;
            }
            var sparePartIds = list.Select(x => x.SparePartId).Distinct().ToList();
            var warehouseIds = list.Select(x => x.WarehouseId).Distinct().ToList();
            var inventoryList = await _systemClient.GetSparePartInventoryListAsync(p => sparePartIds.Contains(p.SparePartId) || warehouseIds.Contains(p.WarehouseId));
            var dataList = new List<SparePartInventoryEntity>();
            foreach (var item in list) {
                var inventory = inventoryList.FirstOrDefault(p => p.SparePartId == item.SparePartId && p.WarehouseId == item.WarehouseId);
                if (inventory == null) {
                    continue;
                }
                inventory.InventoryQuantity -= item.StockInNumber;
                dataList.Add(inventory);
            }
            await _systemClient.UpdateSparePartInventoryAsync(dataList);
        }
    }
}
