using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Common.Mould.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.SparePart;
using DiLe.Mes.Model.Common.Mould.Relation.SparePart;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Mould.SparePart {
    /// <summary>
    /// 备件台账
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldSparePartLedgerController : ApiBaseController {
        private readonly MouldSparePartClient _mouldSparePartClient;
        private readonly MouldInfoClient _mouldInfoClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldSparePartLedgerController(MouldSparePartClient mouldSparePart, MouldExjosnHandler handler, MouldInfoClient mouldInfoClient) {
            _mouldSparePartClient = mouldSparePart;
            _handler = handler;
            _mouldInfoClient =
            _mouldInfoClient = mouldInfoClient;
        }
        /// <summary>
        /// 获取备件台账列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldSparePartLedgerList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldSparePartLedgerEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldSparePartClient.GetMouldSparePartLedgerListAsync(whereExpression.ToExpression());
                var list = await _handler.FillMouldSparePartLedgerExjosn(res);
                return Success(list);
            } else {
                var res = await _mouldSparePartClient.GetMouldSparePartLedgerPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.FillMouldSparePartLedgerExjosn(res.Record);
                var data = res.Adapt<PaginationModel<MouldSparePartLedgerModel>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取备件台账
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldSparePartLedger(long id) {
            var dto = new MouldSparePartLedgerDto();
            var res = await _mouldSparePartClient.GetMouldSparePartLedgerInfoAsync(id);
            if (res.Id > 0) {
                var temps = await _mouldSparePartClient.GetMouldSparePart2MouldInfoListByLedgerIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.MouldInfoId).Distinct().ToList();
                    dto.DataList = await _mouldInfoClient.GetMouldInfoListAsync(x => ids.Contains(x.Id));
                    await _handler.FillMouldInfoExjosn(dto.DataList);
                }
            }
            dto.Model = await _handler.FillMouldSparePartLedgerExjosn(res);
            return Success(dto);
        }
        /// <summary>
        /// 保存备件台账
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldSparePartLedger(SaveParameter<MouldSparePartLedgerEntity, MouldSparePart2MouldInfoEntity> parameter) {
            var resId = await _mouldSparePartClient.SaveMouldSparePartLedgerAsync(parameter.Model);
            if (resId > 0) {
                if (!parameter.AddRelations.None()) {
                    await _mouldSparePartClient.InsertMouldSparePart2MouldInfoAsync(resId, parameter.AddRelations);
                }
                if (!parameter.EidtRelations.None()) {
                    await _mouldSparePartClient.UpdateMouldSparePart2MouldInfoAsync(parameter.EidtRelations);
                }
                if (!parameter.DeleteRelations.None()) {
                    await _mouldSparePartClient.DeleteMouldSparePart2MouldInfoAsync(parameter.DeleteRelations);
                }
            }
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除备件台账
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldSparePartLedger(List<long> ids) {
            var res = await _mouldSparePartClient.DeleteMouldSparePartLedgerAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
