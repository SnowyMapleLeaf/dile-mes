using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Cloud.Controllers.Mould;
using DiLe.Mes.Model.Common.Mould.Entity.SparePart;

namespace DiLe.Mes.Cloud.Controllers.Mould.SparePart
{
    /// <summary>
    /// 备件类型
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldSparePartTypeController : ApiBaseController {
        private readonly MouldSparePartClient _mouldSparePartClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldSparePartTypeController(MouldSparePartClient mouldSparePart, MouldExjosnHandler handler) {
            _mouldSparePartClient = mouldSparePart;
            _handler = handler;
        }
        /// <summary>
        /// 获取备件类型列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldSparePartTypeList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldSparePartTypeEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldSparePartClient.GetMouldSparePartTypeListAsync(whereExpression.ToExpression());
                await _handler.FillMouldSparePartTypeExjosn(res);
                return Success(res);
            } else {
                var res = await _mouldSparePartClient.GetMouldSparePartTypePageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillMouldSparePartTypeExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取备件类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldSparePartType(long id) {
            var res = await _mouldSparePartClient.GetMouldSparePartTypeInfoAsync(id);
            await _handler.FillMouldSparePartTypeExjosn(new List<MouldSparePartTypeEntity> { res });
            return Success(res);
        }
        /// <summary>
        /// 保存备件类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldSparePartType(MouldSparePartTypeEntity entity) {
            var resId = await _mouldSparePartClient.SaveMouldSparePartTypeAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除备件类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldSparePartType(List<long> ids) {
            var res = await _mouldSparePartClient.DeleteMouldSparePartTypeAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新备件类型状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateMouldSparePartTypeStatus(CommomParameter parameter) {
            var res = await _mouldSparePartClient.UpdateMouldSparePartTypeStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }
    }
}
