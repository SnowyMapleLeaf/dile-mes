using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;

namespace DiLe.Mes.Cloud.Controllers.Mould.Maintenance
{
    /// <summary>
    /// 保养项目
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldMaintenanceProjectController : ApiBaseController {
        private readonly MouldMaintenanceClient _mouldMaintenanceClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldMaintenanceProjectController(MouldMaintenanceClient mouldMaintenance, MouldExjosnHandler handler) {
            _mouldMaintenanceClient = mouldMaintenance;
            _handler = handler;
        }
        /// <summary>
        /// 获取保养项目列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldMaintenanceProjectList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldMaintenanceProjectEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldMaintenanceClient.GetMouldMaintenanceProjectListAsync(whereExpression.ToExpression());
                await _handler.FillMouldMaintenanceProjectExjosn(res);
                return Success(res);
            } else {
                var res = await _mouldMaintenanceClient.GetMouldMaintenanceProjectPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillMouldMaintenanceProjectExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取保养项目
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldMaintenanceProject(long id) {
            var res = await _mouldMaintenanceClient.GetMouldMaintenanceProjectInfoAsync(id);
            await _handler.FillMouldMaintenanceProjectExjosn(new List<MouldMaintenanceProjectEntity> { res });
            return Success(res);
        }
        /// <summary>
        /// 保存保养项目
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldMaintenanceProject(MouldMaintenanceProjectEntity entity) {
            var resId = await _mouldMaintenanceClient.SaveMouldMaintenanceProjectAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除保养项目
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldMaintenanceProject(List<long> ids) {
            var res = await _mouldMaintenanceClient.DeleteMouldMaintenanceProjectAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
