using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Common.Mould.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Model.Common.Mould.Relation.Maintenance;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Mould.Maintenance
{
    /// <summary>
    /// 保养计划
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldMaintenancePlanController : ApiBaseController {
        private readonly MouldMaintenanceClient _mouldMaintenanceClient;
        private readonly MouldInfoClient _mouldInfoClient;
        private readonly MouldExjosnHandler _handler;
        private readonly SystemClient _systemClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldMaintenancePlanController(MouldMaintenanceClient mouldMaintenance,
                                              MouldExjosnHandler handler,
                                              MouldInfoClient mouldInfoClient,
                                              SystemClient systemClient) {
            _mouldMaintenanceClient = mouldMaintenance;
            _handler = handler;
            _mouldInfoClient = mouldInfoClient;
            _systemClient = systemClient;
        }
        /// <summary>
        /// 获取保养计划列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldMaintenancePlanList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldMaintenancePlanEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldMaintenanceClient.GetMouldMaintenancePlanListAsync(whereExpression.ToExpression());
                await _handler.FillMouldMaintenancePlanExjosn(res);
                return Success(res);
            } else {
                var res = await _mouldMaintenanceClient.GetMouldMaintenancePlanPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetMouldMaintenancePlanDto(res.Record);
                var data = res.Adapt<PaginationModel<MouldMaintenancePlanDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取保养计划
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldMaintenancePlan(long id) {
            var dto = new MouldMaintenancePlanDto<MouldMaintenancePlanEntity, MouldMaintenancePlan2ProjectEntity, MouldMaintenanceProjectEntity>();
            dto.Model = await _mouldMaintenanceClient.GetMouldMaintenancePlanInfoAsync(id);
            if (dto.Model != null) {
                var temps = await _mouldMaintenanceClient.GetMouldMaintenancePlan2ProjectListByPlanIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.MaintenanceProjectId).Distinct().ToList();
                    dto.DataList = await _mouldMaintenanceClient.GetMouldMaintenanceProjectListAsync(p => ids.Contains(p.Id));
                    await _handler.FillMouldMaintenanceProjectExjosn(dto.DataList);
                }
                dto.MouldInfoModel = await _mouldInfoClient.GetMouldInfoAsync(dto.Model.MouldId);
                if (dto.MouldInfoModel != null) {
                    await _handler.FillMouldInfoExjosn(new List<MouldInfoEntity> { dto.MouldInfoModel });
                }
                //规则
                dto.ExecuteRuleModel = await _systemClient.GetExecuteRuleInfoAsync(dto.Model.ExecuteRuleId);
                await _handler.FillMouldMaintenancePlanExjosn(new List<MouldMaintenancePlanEntity> { dto.Model });
            }
            return Success(dto);
        }
        /// <summary>
        /// 保存保养计划
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldMaintenancePlan(SaveParameter<MouldMaintenancePlanEntity, MouldMaintenancePlan2ProjectEntity> parameter) {
            var resId = await _mouldMaintenanceClient.SaveMouldMaintenancePlanAsync(parameter.Model);
            if (resId > 0) {
                if (!parameter.AddRelations.None()) {
                    await _mouldMaintenanceClient.InsertMouldMaintenancePlan2ProjectAsync(resId, parameter.AddRelations);
                }
                if (!parameter.EidtRelations.None()) {
                    await _mouldMaintenanceClient.UpdateMouldMaintenancePlan2ProjectAsync(parameter.EidtRelations);
                }
                if (!parameter.DeleteRelations.None()) {
                    await _mouldMaintenanceClient.DeleteMouldMaintenancePlan2ProjectAsync(parameter.DeleteRelations);
                }
            }
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除保养计划
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldMaintenancePlan(List<long> ids) {
            var res = await _mouldMaintenanceClient.DeleteMouldMaintenancePlanAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
