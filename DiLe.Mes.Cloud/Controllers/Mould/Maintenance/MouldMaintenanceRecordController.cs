using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Common.Mould.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using Mapster;

namespace DiLe.Mes.Cloud.Controllers.Mould.Maintenance
{
    /// <summary>
    /// 保养记录
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldMaintenanceRecordController : ApiBaseController {
        private readonly MouldMaintenanceClient _mouldMaintenanceClient;
        private readonly MouldInfoClient _mouldInfoClient;
        private readonly MouldSparePartClient _mouldSparePartClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldMaintenanceRecordController(MouldMaintenanceClient mouldMaintenance,
                                                MouldExjosnHandler handler,
                                                MouldSparePartClient mouldSparePartClient,
                                                MouldInfoClient mouldInfoClient) {
            _mouldMaintenanceClient = mouldMaintenance;
            _handler = handler;
            _mouldSparePartClient = mouldSparePartClient;
            _mouldInfoClient = mouldInfoClient;
        }
        /// <summary>
        /// 获取保养记录列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldMaintenanceRecordList(MouldMaintenanceRecordQueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldMaintenanceRecordEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.MouldId > 0) {
                whereExpression.And(x => x.MouldId == parameter.MouldId);
            }
            if (parameter.Pagination == null) {
                var res = await _mouldMaintenanceClient.GetMouldMaintenanceRecordListAsync(whereExpression.ToExpression());
                var list = await _handler.GetMouldMaintenanceRecordDto(res);
                return Success(list);
            } else {
                var res = await _mouldMaintenanceClient.GetMouldMaintenanceRecordPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetMouldMaintenanceRecordDto(res.Record);
                var data = res.Adapt<PaginationModel<MouldMaintenanceRecordDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取保养记录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldMaintenanceRecord(long id) {
            var dto = new MouldMaintenanceRecordDetailDto();
            var res = await _mouldMaintenanceClient.GetMouldMaintenanceRecordInfoAsync(id);
            if (res != null && res.Id > 0) {
                await _handler.FillMouldMaintenanceRecordExjosn(new List<MouldMaintenanceRecordEntity> { res });
                dto.Model = res;
                var projecttemps = await _mouldMaintenanceClient.GetMouldMaintenanceRecord2ProjectListByPlanIdAsync(id);
                if (!projecttemps.None()) {
                    dto.ProjectRelations = projecttemps;
                    var ids = projecttemps.Select(x => x.MaintenanceProjectId).Distinct().ToList();
                    dto.ProjectDataList = await _mouldMaintenanceClient.GetMouldMaintenanceProjectListAsync(x => ids.Contains(x.Id));
                    await _handler.FillMouldMaintenanceProjectExjosn(dto.ProjectDataList);
                }
                var spareparttemps = await _mouldMaintenanceClient.GetMouldMaintenanceRecord2SparePartListByPlanIdAsync(id);
                if (!spareparttemps.None()) {
                    dto.SparePartRelations = spareparttemps;
                    var ids = spareparttemps.Select(x => x.SparePartId).Distinct().ToList();
                    var sparePartDataList = await _mouldSparePartClient.GetMouldSparePartLedgerListAsync(x => ids.Contains(x.Id));
                    dto.SparePartDataList = await _handler.FillMouldSparePartLedgerExjosn(sparePartDataList);
                }

                dto.PlanModel = await _mouldMaintenanceClient.GetMouldMaintenancePlanInfoAsync(res.MaintenancePlanId);
                await _handler.FillMouldMaintenancePlanExjosn(new List<MouldMaintenancePlanEntity> { dto.PlanModel });
                dto.MouldModel = await _mouldInfoClient.GetMouldInfoAsync(res.MouldId);
                await _handler.FillMouldInfoExjosn(new List<MouldInfoEntity> { dto.MouldModel });
            }
            return Success(dto);
        }
        /// <summary>
        /// 保存保养记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldMaintenanceRecord(MouldMaintenanceRecordSaveParameter parameter) {
            var resId = await _mouldMaintenanceClient.SaveMouldMaintenanceRecordAsync(parameter.Model);
            if (resId > 0) {
                if (!parameter.AddProjectRelations.None()) {
                    await _mouldMaintenanceClient.InsertMouldMaintenanceRecord2ProjectAsync(resId, parameter.AddProjectRelations);
                }
                if (!parameter.EidtProjectRelations.None()) {
                    await _mouldMaintenanceClient.UpdateMouldMaintenanceRecord2ProjectAsync(parameter.EidtProjectRelations);
                }
                if (!parameter.DeleteProjectRelations.None()) {
                    await _mouldMaintenanceClient.DeleteMouldMaintenanceRecord2ProjectAsync(parameter.DeleteProjectRelations);
                }

                if (!parameter.AddSparePartRelations.None()) {
                    await _mouldMaintenanceClient.InsertMouldMaintenanceRecord2SparePartAsync(resId, parameter.AddSparePartRelations);
                }
                if (!parameter.EidtSparePartRelations.None()) {
                    await _mouldMaintenanceClient.UpdateMouldMaintenanceRecord2SparePartAsync(parameter.EidtSparePartRelations);
                }
                if (!parameter.DeleteSparePartRelations.None()) {
                    await _mouldMaintenanceClient.DeleteMouldMaintenanceRecord2SparePartAsync(parameter.DeleteSparePartRelations);
                }
            }
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除保养记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldMaintenanceRecord(List<long> ids) {
            var res = await _mouldMaintenanceClient.DeleteMouldMaintenanceRecordAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
