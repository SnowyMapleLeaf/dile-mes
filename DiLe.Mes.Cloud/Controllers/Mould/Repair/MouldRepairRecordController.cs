using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Common.Mould.ViewModel;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.Info;
using DiLe.Mes.Model.Common.Mould.Entity.Repair;
using DiLe.Mes.Model.Common.Mould.Relation.Repair;

namespace DiLe.Mes.Cloud.Controllers.Mould.Repair {
    /// <summary>
    /// 维修记录
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldRepairRecordController : ApiBaseController {
        private readonly MouldRepairClient _mouldRepairClient;
        private readonly MouldExjosnHandler _handler;
        private readonly MouldInfoClient _mouldInfoClient;
        private readonly MouldSparePartClient _mouldSparePartClient;

        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldRepairRecordController(MouldRepairClient mouldRepair, MouldExjosnHandler handler, MouldInfoClient mouldInfoClient, MouldSparePartClient mouldSparePartClient) {
            _mouldRepairClient = mouldRepair;
            _handler = handler;
            _mouldInfoClient = mouldInfoClient;
            _mouldSparePartClient = mouldSparePartClient;
        }
        /// <summary>
        /// 获取维修记录列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldRepairRecordList(MouldRepairRecordQueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldRepairRecordEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.MouldId > 0) {
                whereExpression.And(x => x.MouldId == parameter.MouldId);
            }
            if (parameter.Pagination == null) {
                var res = await _mouldRepairClient.GetMouldRepairRecordListAsync(whereExpression.ToExpression());
                var list = await _handler.GetMouldRepairRecordDto(res);
                return Success(list);
            } else {
                var res = await _mouldRepairClient.GetMouldRepairRecordPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                var list = await _handler.GetMouldRepairRecordDto(res.Record);
                var data = res.Adapt<PaginationModel<MouldRepairRecordDto>>();
                data.Record = list;
                return Success(data);
            }
        }
        /// <summary>
        /// 获取维修记录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldRepairRecord(long id) {
            var dto = new MouldRepairRecordDetailDto<MouldRepairRecordEntity, MouldRepairRecord2SparePartEntity, MouldSparePartLedgerModel>();
            dto.Model = await _mouldRepairClient.GetMouldRepairRecordInfoAsync(id);
            await _handler.FillMouldRepairRecordExjosn(new List<MouldRepairRecordEntity> { dto.Model });
            if (dto.Model != null) {
                var temps = await _mouldRepairClient.GetMouldRepairRecord2SparePartListByRecordIdAsync(id);
                if (!temps.None()) {
                    dto.Relations = temps;
                    var ids = temps.Select(x => x.SparePartId).Distinct().ToList();
                    var dataList = await _mouldSparePartClient.GetMouldSparePartLedgerListAsync(p => ids.Contains(p.Id));
                    dto.DataList = await _handler.FillMouldSparePartLedgerExjosn(dataList);
                }
                dto.MouldModel = await _mouldInfoClient.GetMouldInfoAsync(dto.Model.MouldId);
                if (dto.MouldModel != null) {
                    await _handler.FillMouldInfoExjosn(new List<MouldInfoEntity> { dto.MouldModel });
                }
            }
            return Success(dto);
        }
        /// <summary>
        /// 保存维修记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldRepairRecord(SaveParameter<MouldRepairRecordEntity, MouldRepairRecord2SparePartEntity> parameter) {
            var resId = await _mouldRepairClient.SaveMouldRepairRecordAsync(parameter.Model);
            if (resId > 0) {
                if (!parameter.AddRelations.None()) {
                    await _mouldRepairClient.InsertMouldRepairRecord2SparePartAsync(resId, parameter.AddRelations);
                }
                if (!parameter.EidtRelations.None()) {
                    await _mouldRepairClient.UpdateMouldRepairRecord2SparePartAsync(parameter.EidtRelations);
                }
                if (!parameter.DeleteRelations.None()) {
                    await _mouldRepairClient.DeleteMouldRepairRecord2SparePartAsync(parameter.DeleteRelations);
                }
            }
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除维修记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldRepairRecord(List<long> ids) {
            var res = await _mouldRepairClient.DeleteMouldRepairRecordAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
