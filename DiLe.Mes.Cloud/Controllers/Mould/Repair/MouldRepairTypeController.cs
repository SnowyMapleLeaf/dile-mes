using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.Repair;

namespace DiLe.Mes.Cloud.Controllers.Mould.Repair
{
    /// <summary>
    /// 故障类型
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldRepairTypeController : ApiBaseController {
        private readonly MouldRepairClient _mouldRepairClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldRepairTypeController(MouldRepairClient mouldRepair, MouldExjosnHandler handler) {
            _mouldRepairClient = mouldRepair;
            _handler = handler;
        }
        /// <summary>
        /// 获取故障类型列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldRepairTypeList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldRepairTypeEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldRepairClient.GetMouldRepairTypeListAsync(whereExpression.ToExpression());
                await _handler.FillMouldRepairTypeExjosn(res);
                return Success(res);
            } else {
                var res = await _mouldRepairClient.GetMouldRepairTypePageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillMouldRepairTypeExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取故障类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldRepairType(long id) {
            var res = await _mouldRepairClient.GetMouldRepairTypeInfoAsync(id);
            await _handler.FillMouldRepairTypeExjosn(new List<MouldRepairTypeEntity> { res });
            return Success(res);
        }
        /// <summary>
        /// 保存故障类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldRepairType(MouldRepairTypeEntity entity) {
            var resId = await _mouldRepairClient.SaveMouldRepairTypeAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除故障类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldRepairType(List<long> ids) {
            var res = await _mouldRepairClient.DeleteMouldRepairTypeAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新故障类型状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateMouldRepairTypeStatus(CommomParameter parameter) {
            var res = await _mouldRepairClient.UpdateMouldRepairTypeStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }
    }
}
