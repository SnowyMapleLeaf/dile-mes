using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.Info;

namespace DiLe.Mes.Cloud.Controllers.Mould.Info
{
    /// <summary>
    /// 模具状态
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldStatusController : ApiBaseController {
        private readonly MouldInfoClient _mouldInfoClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldStatusController(MouldInfoClient mouldInfo, MouldExjosnHandler handler) {
            _mouldInfoClient = mouldInfo;
            _handler = handler;
        }
        /// <summary>
        /// 获取模具状态列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldStatusList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldStatusEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldInfoClient.GetMouldStatusListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _mouldInfoClient.GetMouldStatusPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取模具状态
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldStatus(long id) {
            var res = await _mouldInfoClient.GetMouldStatusInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存模具状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldStatus(MouldStatusEntity entity) {
            var resId = await _mouldInfoClient.SaveMouldStatusAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除模具状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldStatus(List<long> ids) {
            var res = await _mouldInfoClient.DeleteMouldStatusAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新模具状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateMouldStatusStatus(CommomParameter parameter) {
            var res = await _mouldInfoClient.UpdateMouldStatusStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }
    }
}
