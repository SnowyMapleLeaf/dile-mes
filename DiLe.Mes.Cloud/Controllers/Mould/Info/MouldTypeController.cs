using DiLe.Mes.Application.Common.Mould;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Model.Common.Mould.Entity.Info;

namespace DiLe.Mes.Cloud.Controllers.Mould.Info
{
    /// <summary>
    /// 模具类型
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.MouldManage)]
    public class MouldTypeController : ApiBaseController {
        private readonly MouldInfoClient _mouldInfoClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MouldTypeController(MouldInfoClient mouldInfo, MouldExjosnHandler handler) {
            _mouldInfoClient = mouldInfo;
            _handler = handler;
        }
        /// <summary>
        /// 获取模具类型列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMouldTypeList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MouldTypeEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _mouldInfoClient.GetMouldTypeListAsync(whereExpression.ToExpression());
                await _handler.FillMouldTypeExjosn(res);
                return Success(res);
            } else {
                var res = await _mouldInfoClient.GetMouldTypePageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillMouldTypeExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取模具类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMouldType(long id) {
            var res = await _mouldInfoClient.GetMouldTypeInfoAsync(id);
            if (res != null) {
                await _handler.FillMouldTypeExjosn(new List<MouldTypeEntity> { res });
            }
            return Success(res);
        }
        /// <summary>
        /// 保存模具类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMouldType(MouldTypeEntity entity) {
            var resId = await _mouldInfoClient.SaveMouldTypeAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除模具类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMouldType(List<long> ids) {
            var res = await _mouldInfoClient.DeleteMouldTypeAsync(ids);
            return res ? Success() : Fail();
        }

        /// <summary>
        /// 更新模具类型状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateMouldTypeStatus(CommomParameter parameter) {
            var res = await _mouldInfoClient.UpdateMouldTypeStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }
    }
}
