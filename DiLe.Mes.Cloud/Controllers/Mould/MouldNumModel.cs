﻿using DiLe.Mes.Model.Common.Mould.Entity.Info;

namespace DiLe.Mes.Cloud.Controllers.Mould {
    /// <summary>
    /// 
    /// </summary>
    public class MouldInfoDto {
        /// <summary>
        /// 
        /// </summary>
        public MouldInfoEntity Model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PaginationModel<MouldNumModel> MouldNumData { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldNumModel {
        /// <summary>
        /// 
        /// </summary>
        public string WorkOrderCode { get; set; }
        /// <summary>
        /// 使用日期
        /// </summary>
        public DateTime UseDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// 累计模次
        /// </summary>
        public int TotalNum { get; set; }
        /// <summary>
        /// 剩余模次
        /// </summary>
        public int SurplusNum { get; set; }
        /// <summary>
        /// 操作员
        /// </summary>
        public string Operator { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        public string OperationDate { get; set; }

    }
}
