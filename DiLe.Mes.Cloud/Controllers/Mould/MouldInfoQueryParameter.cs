﻿using DiLe.Mes.Model.Common.Mould.Entity.Maintenance;
using DiLe.Mes.Model.Common.Mould.Relation.Maintenance;

namespace DiLe.Mes.Cloud.Controllers.Mould {

    /// <summary>
    /// 
    /// </summary>
    public class MouldInfoQueryParameter : QueryParameter {
        /// <summary>
        /// 位置
        /// </summary>
        public long? PositionId { get; set; } = 0;
        /// <summary>
        /// 状态
        /// </summary>
        public long? StatusId { get; set; } = 0;
        /// <summary>
        /// 类型
        /// </summary>
        public long? TypeId { get; set; } = 0;

    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldNumberParameter {
        /// <summary>
        /// 
        /// </summary>

        public PaginationModel Pagination { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public long MouldId { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class MouldRepairRecordQueryParameter : QueryParameter {
        /// <summary>
        /// 
        /// </summary>
        public long MouldId { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MouldMaintenanceRecordQueryParameter : QueryParameter {
        /// <summary>
        /// 
        /// </summary>
        public long MouldId { get; set; }
    }

    /// <summary>
    /// 模具保养
    /// </summary>
    public class MouldMaintenanceRecordSaveParameter {
        /// <summary>
        ///  保养记录
        /// </summary>
        public MouldMaintenanceRecordEntity Model { get; set; }
        /// <summary>
        /// 保养记录和保养项目
        /// </summary>
        public List<MouldMaintenanceRecord2ProjectEntity> AddProjectRelations { get; set; } = new List<MouldMaintenanceRecord2ProjectEntity>();
        /// <summary>
        /// 保养记录和保养项目
        /// </summary>
        public List<MouldMaintenanceRecord2ProjectEntity> EidtProjectRelations { get; set; } = new List<MouldMaintenanceRecord2ProjectEntity>();
        /// <summary>
        ///保养记录和保养项目
        /// </summary>
        public List<long> DeleteProjectRelations { get; set; } = new List<long>();

        /// <summary>
        /// 保养记录和保养计划
        /// </summary>
        public List<MouldMaintenanceRecord2SparePartEntity> AddSparePartRelations { get; set; } = new List<MouldMaintenanceRecord2SparePartEntity>();
        /// <summary>
        /// 保养记录和保养计划
        /// </summary>
        public List<MouldMaintenanceRecord2SparePartEntity> EidtSparePartRelations { get; set; } = new List<MouldMaintenanceRecord2SparePartEntity>();
        /// <summary>
        /// 保养记录和保养计划
        /// </summary>
        public List<long> DeleteSparePartRelations { get; set; } = [];
    }

}
