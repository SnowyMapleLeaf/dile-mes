﻿using DiLe.Mes.Model.Common.Product.Entity;
using DiLe.Mes.Model.Common.Product.Relation;

namespace DiLe.Mes.Cloud.Controllers.SystemManage {
    /// <summary>
    /// 
    /// </summary>
    public class ProductInfoSaveParameter {
        /// <summary>
        ///  产品信息
        /// </summary>
        public ProductInfoEntity Model { get; set; }
        /// <summary>
        /// 产品信息和制程属性
        /// </summary>
        public List<ProductInfo2ProcessAttributeEntity> AddProcessAttributeRelations { get; set; } = new List<ProductInfo2ProcessAttributeEntity>();
        /// <summary>
        /// 产品信息和制程属性
        /// </summary>
        public List<ProductInfo2ProcessAttributeEntity> EidtProcessAttributeRelations { get; set; } = new List<ProductInfo2ProcessAttributeEntity>();
        /// <summary>
        ///产品信息和制程属性
        /// </summary>
        public List<long> DeleteProcessAttributeRelations { get; set; } = new List<long>();

        /// <summary>
        /// 产品信息和属性
        /// </summary>
        public List<ProductInfo2AttributeEntity> AddProductAttributeRelations { get; set; } = new List<ProductInfo2AttributeEntity>();
        /// <summary>
        /// 产品信息和属性
        /// </summary>
        public List<ProductInfo2AttributeEntity> EidtProductAttributeRelations { get; set; } = new List<ProductInfo2AttributeEntity>();
        /// <summary>
        /// 产品信息和属性
        /// </summary>
        public List<long> DeleteProductAttributeRelations { get; set; } = new List<long>();
    }
}
