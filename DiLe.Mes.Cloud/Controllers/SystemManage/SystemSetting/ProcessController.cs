﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.Process.Entity;
using SqlSugar;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 工序
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class ProcessController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemClient"></param>
        /// <param name="organizationClient"></param>
        public ProcessController(SystemClient systemClient, OrganizationClient organizationClient) {
            _systemClient = systemClient;
            _organizationClient = organizationClient;
        }


        /// <summary>
        /// 获取工序列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetProcessList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<ProcessEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetProcessListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _systemClient.GetProcessPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取工序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetProcessInfo(long id) {
            var res = await _systemClient.GetProcessInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存工序
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveProcess(ProcessEntity entity) {
            var res = await _systemClient.SaveProcessAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除工序
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteProcess(List<long> ids) {
            var res = await _systemClient.DeleteProcessAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新工序状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateProcessStatus(CommomParameter parameter) {
            var res = await _systemClient.UpdateProcessStatusAsync(parameter.Ids, parameter.Status);
            return Success(res);
        }
    }
}
