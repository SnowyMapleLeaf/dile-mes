﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.System.Entity;
using Microsoft.AspNetCore.Mvc;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 计量单位
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class MeasureUnitController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="systemClient"></param>
        public MeasureUnitController(SystemClient systemClient) {
            _systemClient = systemClient;
        }

        /// <summary>
        /// 获取计量单位列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMeasureUnitList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MeasureUnitEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetMeasureUnitListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _systemClient.GetMeasureUnitPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取计量单位信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMeasureUnitInfo(long id) {
            var res = await _systemClient.GetMeasureUnitInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存计量单位信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMeasureUnit(MeasureUnitEntity entity) {
            var res = await _systemClient.SaveMeasureUnitAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除计量单位信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMeasureUnit(List<long> ids) {
            var res = await _systemClient.DeleteMeasureUnitAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
