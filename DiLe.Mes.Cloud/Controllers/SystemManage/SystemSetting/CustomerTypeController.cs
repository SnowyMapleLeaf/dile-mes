﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 客户类型
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class CustomerTypeController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemClient"></param>
        /// <param name="organizationClient"></param>
        public CustomerTypeController(SystemClient systemClient, OrganizationClient organizationClient) {
            _systemClient = systemClient;
            _organizationClient = organizationClient;
        }
        /// <summary>
        /// 获取客户类型列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetCustomerTypeList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<CustomerTypeEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetCustomerTypeListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _systemClient.GetCustomerTypePageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取客户类型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetCustomerTypeInfo(long id) {
            var res = await _systemClient.GetCustomerTypeInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存客户类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveCustomerType(CustomerTypeEntity entity) {
            var res = await _systemClient.SaveCustomerTypeAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除客户类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteCustomerType(List<long> ids) {
            var res = await _systemClient.DeleteCustomerTypeAsync(ids);
            return res ? Success() : Fail();
        }

    }
}
