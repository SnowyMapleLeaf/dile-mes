﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.System.Entity;
using Microsoft.AspNetCore.Mvc;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 仓库
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class WarehouseController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="systemClient"></param>
        /// <param name="organizationClient"></param>
        public WarehouseController(SystemClient systemClient, OrganizationClient organizationClient) {
            _systemClient = systemClient;
            _organizationClient = organizationClient;
        }
        /// <summary>
        /// 获取仓库信息列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetWarehouseList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<WarehouseEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword!));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword!));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetWarehouseListAsync(whereExpression.ToExpression());
                await FillWarehouseExjosn(res);
                return Success(res);
            } else {
                var res = await _systemClient.GetWarehousePageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await FillWarehouseExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取仓库信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetWarehouseInfo(long id) {
            var res = await _systemClient.GetWarehouseInfoAsync(id);
            if (res != null) {
                //部门
                var department = await _organizationClient.GetOrganizationAsync(res?.DepartmentId);
                res!.ExtJson.Add("DepartmentName", department?.Name);
                //负责人
                var user = await _organizationClient.GetUserByIdAsync(res.Manager);
                res.ExtJson.Add("ManagerName", user?.Name);
                //上级仓库
                var parent = await _systemClient.GetWarehouseInfoAsync(res.ParentId);
                res.ExtJson.Add("ParentName", parent?.Name);
            }

            return Success(res);
        }
        /// <summary>
        /// 保存仓库信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveWarehouse(WarehouseEntity entity) {
            var res = await _systemClient.SaveWarehouseAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除仓库信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteWarehouse(List<long> ids) {
            var res = await _systemClient.DeleteWarehouseAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新仓库状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateWarehouseStatus(CommomParameter parameter) {
            var res = await _systemClient.UpdateWarehouseStatusAsync(parameter.Ids, parameter.Status);
            return Success(res);
        }



        #region 其他方法
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataList"></param>
        private async Task FillWarehouseExjosn(List<WarehouseEntity> dataList) {
            if (!dataList.Any()) {
                return;
            }
            //部门
            var departmentIds = dataList.Where(x => x.DepartmentId != null).Select(p => p.DepartmentId!.Value).Distinct().ToList() ?? new List<long>();
            var departmentList = await _organizationClient.GetOrganizationListAsync(departmentIds);
            //负责人
            List<long> userIds = dataList.Select(p => p.Manager).Distinct().ToList() ?? new List<long>();
            var userList = await _organizationClient.GetUserListByIdAsync(userIds);

            foreach (var item in dataList) {
                var department = departmentList?.FirstOrDefault(p => p.Id == item.DepartmentId);
                item.ExtJson.Add("DepartmentName", department?.Name);
                var manager = userList?.FirstOrDefault(p => p.Id == item.Manager);
                item.ExtJson.Add("ManagerName", manager?.Name);
            }
        }
        #endregion

    }
}
