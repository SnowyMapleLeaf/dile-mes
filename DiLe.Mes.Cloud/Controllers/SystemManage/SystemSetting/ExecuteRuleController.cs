﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 执行规则
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class ExecuteRuleController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="systemClient"></param>
        public ExecuteRuleController(SystemClient systemClient) {
            _systemClient = systemClient;
        }

        /// <summary>
        /// 获取执行规则信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetExecuteRuleList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<ExecuteRuleEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.And(p => p.Name.Contains(parameter.Keyword!));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetExecuteRuleListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _systemClient.GetExecuteRuleListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取执行规则信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetExecuteRuleInfo(long id) {
            var res = await _systemClient.GetExecuteRuleInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存执行规则信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveExecuteRule(ExecuteRuleEntity entity) {
            var res = await _systemClient.SaveExecuteRuleAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除执行规则信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteExecuteRule(List<long> ids) {
            var res = await _systemClient.DeleteExecuteRuleAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新执行规则状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateExecuteRuleStatus(CommomParameter parameter) {
            var res = await _systemClient.UpdateExecuteRuleStatusAsync(parameter.Ids, parameter.Status);
            return Success(res);
        }

    }
}
