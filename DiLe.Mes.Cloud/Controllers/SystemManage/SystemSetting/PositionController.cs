﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 存放位置
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class PositionController : ApiBaseController {

        private readonly SystemClient _systemClient;
        private readonly SystemExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public PositionController(SystemClient systemClient, SystemExjosnHandler handler) {
            _systemClient = systemClient;
            _handler = handler;
        }
        /// <summary>
        /// 获取存放位置
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetPositionPageList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<PositionEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.And(p => p.Position.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetPositionListAsync(whereExpression.ToExpression());
                await _handler.FillPositionExjosn(res);
                return Success(res);
            } else {
                var res = await _systemClient.GetPositionPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillPositionExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取存放位置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetPosition(long id) {
            var res = await _systemClient.GetPositionAsync(id);
            await _handler.FillPositionExjosn(new List<PositionEntity> { res });
            return Success(res);
        }
        /// <summary>
        /// 保存存放位置
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SavePosition(PositionEntity entity) {
            var res = await _systemClient.SavePositionAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除存放位置
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeletePosition(List<long> ids) {
            var res = await _systemClient.DeletePositionAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新存放位置状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdatePositionStatus(CommomParameter parameter) {
            var res = await _systemClient.UpdatePositionStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }


    }
}
