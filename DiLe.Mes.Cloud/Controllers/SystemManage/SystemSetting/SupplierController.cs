﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.System.Entity;
using SqlSugar;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 供应商
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class SupplierController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemClient"></param>
        /// <param name="organizationClient"></param>
        public SupplierController(SystemClient systemClient, OrganizationClient organizationClient) {
            _systemClient = systemClient;
            _organizationClient = organizationClient;
        }
        /// <summary>
        /// 获取供应商列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetSupplierList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<SupplierEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword!));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword!));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetSupplierListAsync(whereExpression.ToExpression());
                await FillSupplierExjosn(res);
                return Success(res);
            } else {
                var res = await _systemClient.GetSupplierPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await FillSupplierExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取供应商
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetSupplierInfo(long id) {
            var res = await _systemClient.GetSupplierInfoAsync(id);
            await FillSupplierExjosn(new List<SupplierEntity> { res });
            return Success(res);
        }
        /// <summary>
        /// 保存供应商
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveSupplier(SupplierEntity entity) {
            var res = await _systemClient.SaveSupplierAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除供应商
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteSupplier(List<long> ids) {
            var res = await _systemClient.DeleteSupplierAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新供应商状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateSupplierStatus(CommomParameter parameter) {
            var res = await _systemClient.UpdateSupplierStatusAsync(parameter.Ids, parameter.Status);
            return Success(res);
        }
        #region 其他方法
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataList"></param>
        private async Task FillSupplierExjosn(List<SupplierEntity> dataList) {
            if (!dataList.Any()) {
                return;
            }
            //联系人
            List<long> userIds = dataList.Where(x => x.Contact != null).Select(p => p.Contact!.Value).Distinct().ToList() ?? new List<long>();
            var userList = await _organizationClient.GetUserListByIdAsync(userIds);

            foreach (var item in dataList) {
                var contact = userList?.FirstOrDefault(p => p.Id == item.Contact);
                item.ExtJson.Add("ContactName", contact?.Name);
            }
        }
        #endregion
    }
}
