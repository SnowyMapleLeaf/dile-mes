﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.System.Entity;


namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 客户
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class CustomerController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemClient"></param>
        /// <param name="organizationClient"></param>
        public CustomerController(SystemClient systemClient, OrganizationClient organizationClient) {
            _systemClient = systemClient;
            _organizationClient = organizationClient;
        }


        /// <summary>
        /// 获取客户列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetCustomerList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<CustomerEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword!));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword!));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetCustomerListAsync(whereExpression.ToExpression());
                await FillCustomerExjosn(res);
                return Success(res);
            } else {
                var res = await _systemClient.GetCustomerPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await FillCustomerExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取客户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetCustomerInfo(long id) {
            var res = await _systemClient.GetCustomerInfoAsync(id);
            await FillCustomerExjosn(new List<CustomerEntity> { res });
            return Success(res);
        }
        /// <summary>
        /// 保存客户
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveCustomer(CustomerEntity entity) {
            var res = await _systemClient.SaveCustomerAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除客户
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteCustomer(List<long> ids) {
            var res = await _systemClient.DeleteCustomerAsync(ids);
            return res ? Success() : Fail();
        }
        #region 其他方法
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataList"></param>
        private async Task FillCustomerExjosn(List<CustomerEntity> dataList) {
            if (!dataList.Any()) {
                return;
            }
            //类型
            List<long> typeIds = dataList.Where(x => x.TypeId != null).Select(p => p.TypeId!.Value).Distinct().ToList() ?? new List<long>();
            var typeList = await _systemClient.GetCustomerTypeListAsync(p => typeIds.Contains(p.Id));
            //区域
            List<long> regionIds = dataList.Where(x => x.RegionId != null).Select(p => p.RegionId!.Value).Distinct().ToList() ?? new List<long>();
            var regionList = await _systemClient.GetRegionListAsync(p => regionIds.Contains(p.Id));
            //联系人
            List<long> userIds = dataList.Where(x => x.Contact != null).Select(p => p.Contact!.Value).Distinct().ToList() ?? new List<long>();
            var userList = await _organizationClient.GetUserListByIdAsync(userIds);

            foreach (var item in dataList) {
                var type = typeList?.FirstOrDefault(p => p.Id == item.TypeId);
                item.ExtJson.Add("TypeName", type?.Name);
                var region = regionList?.FirstOrDefault(p => p.Id == item.RegionId);
                item.ExtJson.Add("RegionName", region?.Name);

                var contact = userList?.FirstOrDefault(p => p.Id == item.Contact);
                item.ExtJson.Add("ContactName", contact?.Name);
            }
        }
        #endregion
    }
}
