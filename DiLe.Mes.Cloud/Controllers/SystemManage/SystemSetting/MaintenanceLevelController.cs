﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 保养级别
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class MaintenanceLevelController : ApiBaseController {

        private readonly SystemClient _systemClient;
        private readonly SystemExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MaintenanceLevelController(SystemClient systemClient, SystemExjosnHandler handler) {
            _systemClient = systemClient;
            _handler = handler;
        }
        /// <summary>
        /// 获取保养级别
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetMaintenanceLevelPageList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<MaintenanceLevelEntity>();

            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetMaintenanceLevelListAsync(whereExpression.ToExpression());

                return Success(res);
            } else {
                var res = await _systemClient.GetMaintenanceLevelPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取保养级别
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetMaintenanceLevel(long id) {
            var res = await _systemClient.GetMaintenanceLevelAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存保养级别
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveMaintenanceLevel(MaintenanceLevelEntity entity) {
            var res = await _systemClient.SaveMaintenanceLevelAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除保养级别
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteMaintenanceLevel(List<long> ids) {
            var res = await _systemClient.DeleteMaintenanceLevelAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新保养级别状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateMaintenanceLevelStatus(CommomParameter parameter) {
            var res = await _systemClient.UpdateMaintenanceLevelStatusAsync(parameter.Ids, parameter.Status);
            return res ? Success() : Fail();
        }


    }
}
