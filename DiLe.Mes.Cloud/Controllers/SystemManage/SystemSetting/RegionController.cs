﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.System.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.SystemSetting {
    /// <summary>
    /// 区域
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class RegionController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly SystemClient _systemClient;
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemClient"></param>
        /// <param name="organizationClient"></param>
        public RegionController(SystemClient systemClient, OrganizationClient organizationClient) {
            _systemClient = systemClient;
            _organizationClient = organizationClient;
        }
        /// <summary>
        /// 获取区域列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetRegionList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<RegionEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword!));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword!));
            }
            if (parameter.Pagination == null) {
                var res = await _systemClient.GetRegionListAsync(whereExpression.ToExpression());
                await FillRegionExjosn(res);
                return Success(res);
            } else {
                var res = await _systemClient.GetRegionPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await FillRegionExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取区域
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetRegionInfo(long id) {
            var res = await _systemClient.GetRegionInfoAsync(id);
            await FillRegionExjosn(new List<RegionEntity> { res });
            if (res.ParentId > 0) {
                var parent = await _systemClient.GetRegionInfoAsync(res.ParentId);
                res.ExtJson.Add("ParentName", parent?.Name);
            }
            return Success(res);
        }
        /// <summary>
        /// 保存区域
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveRegion(RegionEntity entity) {
            var res = await _systemClient.SaveRegionAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除区域
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteRegion(List<long> ids) {
            var res = await _systemClient.DeleteRegionAsync(ids);
            return res ? Success() : Fail();
        }
        #region 其他方法
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataList"></param>
        private async Task FillRegionExjosn(List<RegionEntity> dataList) {
            if (!dataList.Any()) {
                return;
            }
            //联系人
            List<long> userIds = dataList.Where(x => x.Manager != null).Select(p => p.Manager!.Value).Distinct().ToList() ?? new List<long>();
            var userList = await _organizationClient.GetUserListByIdAsync(userIds);

            foreach (var item in dataList) {
                var manager = userList?.FirstOrDefault(p => p.Id == item.Manager);
                item.ExtJson.Add("ManagerName", manager?.Name);
            }
        }
        #endregion
    }
}
