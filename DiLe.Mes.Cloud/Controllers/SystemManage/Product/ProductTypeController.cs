using DiLe.Mes.Application.Common;
using DiLe.Mes.Application.Handler;
using DiLe.Mes.Cloud.Controllers.Mould;
using DiLe.Mes.Model.Common.Product.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.Product {
    /// <summary>
    /// 产品类型
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class ProductTypeController : ApiBaseController {
        private readonly ProductClient _commonProductClient;
        private readonly MouldExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProductTypeController(ProductClient commonProduct, MouldExjosnHandler handler) {
            _commonProductClient = commonProduct;
            _handler = handler;
        }
        /// <summary>
        /// 获取产品类型列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetProductTypeList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<ProductTypeEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _commonProductClient.GetProductTypeListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _commonProductClient.GetProductTypePageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取产品类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetProductType(long id) {
            var res = await _commonProductClient.GetProductTypeInfoAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存产品类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveProductType(ProductTypeEntity entity) {
            var resId = await _commonProductClient.SaveProductTypeAsync(entity);
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除产品类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteProductType(List<long> ids) {
            var res = await _commonProductClient.DeleteProductTypeAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
