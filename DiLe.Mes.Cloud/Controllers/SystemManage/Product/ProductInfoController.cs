using DiLe.Mes.Application.Common;
using DiLe.Mes.Cloud.Controllers.SystemManage;
using DiLe.Mes.Model.Common.Product.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.Product {
    /// <summary>
    /// 产品信息
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.System)]
    public class ProductInfoController : ApiBaseController {
        private readonly ProductClient _commonProductClient;
        private readonly SystemExjosnHandler _handler;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ProductInfoController(ProductClient commonProduct, SystemExjosnHandler handler) {
            _commonProductClient = commonProduct;
            _handler = handler;
        }
        /// <summary>
        /// 获取产品信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetProductInfoList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<ProductInfoEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _commonProductClient.GetProductInfoListAsync(whereExpression.ToExpression());
                await _handler.FillProductInfoExjosn(res);
                return Success(res);
            } else {
                var res = await _commonProductClient.GetProductInfoPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await _handler.FillProductInfoExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取产品信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetProductInfo(long id) {

            var dto = new ProductInfoDetailDto();
            var res = await _commonProductClient.GetProductInfoInfoAsync(id);
            if (res != null && res.Id > 0) {
                await _handler.FillProductInfoExjosn(new List<ProductInfoEntity> { res });
                dto.Model = res;
                var processRelList = await _commonProductClient.GetProductInfo2ProcessAttributeListByInfoIdAsync(id);
                if (!processRelList.None()) {
                    dto.ProcessAttributeRelations = processRelList;
                }
                var productRelList = await _commonProductClient.GetProductInfo2AttributeListByInfoIdAsync(id);
                if (!productRelList.None()) {
                    dto.ProductAttributeRelations = productRelList;
                }
                dto.CategoryModel = await _commonProductClient.GetProductTypeInfoAsync(res.CategoryId);
            }
            return Success(dto);
        }
        /// <summary>
        /// 保存产品信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveProductInfo(ProductInfoSaveParameter parameter) {
            var resId = await _commonProductClient.SaveProductInfoAsync(parameter.Model);
            if (resId > 0) {
                if (!parameter.AddProductAttributeRelations.None()) {
                    await _commonProductClient.InsertProductInfo2AttributeAsync(resId, parameter.AddProductAttributeRelations);
                }
                if (!parameter.EidtProductAttributeRelations.None()) {
                    await _commonProductClient.UpdateProductInfo2AttributeAsync(parameter.EidtProductAttributeRelations);
                }
                if (!parameter.DeleteProductAttributeRelations.None()) {
                    await _commonProductClient.DeleteProductInfo2AttributeAsync(parameter.DeleteProductAttributeRelations);
                }

                if (!parameter.AddProcessAttributeRelations.None()) {
                    await _commonProductClient.InsertProductInfo2ProcessAttributeAsync(resId, parameter.AddProcessAttributeRelations);
                }
                if (!parameter.EidtProcessAttributeRelations.None()) {
                    await _commonProductClient.UpdateProductInfo2ProcessAttributeAsync(parameter.EidtProcessAttributeRelations);
                }
                if (!parameter.DeleteProcessAttributeRelations.None()) {
                    await _commonProductClient.DeleteProductInfo2ProcessAttributeAsync(parameter.DeleteProcessAttributeRelations);
                }
            }
            return resId > 0 ? Success() : Fail();
        }
        /// <summary>
        /// 删除产品信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteProductInfo(List<long> ids) {
            var res = await _commonProductClient.DeleteProductInfoAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
