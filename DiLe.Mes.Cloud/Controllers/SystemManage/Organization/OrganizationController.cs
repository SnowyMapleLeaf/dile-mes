﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Cloud.Controllers.SystemManage.Organization.Parameters;
using DiLe.Mes.Model.Common.Organization.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.Organization {
    /// <summary>
    /// 组织
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Organization)]
    public class OrganizationController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationClient"></param>
        public OrganizationController(OrganizationClient organizationClient) {
            _organizationClient = organizationClient;
        }
        /// <summary>
        /// 获取组织信息列表
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetOrganizationList(OrganizationQueryParameter parameter) {
            var whereExpression = Expressionable.Create<OrganizationEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.And(p => p.Name.Contains(parameter.Keyword));
            }
            if (!parameter.Type.IsNullOrEmpty()) {
                whereExpression.And(p => p.Type == parameter.Type);
            }
            if (parameter.Pagination == null) {
                var res = await _organizationClient.GetOrganizationListAsync(whereExpression.ToExpression());
                await FillOrganizationExjosn(res);
                return Success(res);
            } else {
                var res = await _organizationClient.GetOrganizationPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                await FillOrganizationExjosn(res.Record);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取组织信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetOrganizationInfo(long id) {
            var res = await _organizationClient.GetOrganizationAsync(id);
            var user = await _organizationClient.GetUserByIdAsync(res.Manager);
            res.ExtJson.Add("ManagerName", user?.Name);
            var parent = await _organizationClient.GetOrganizationAsync(res.ParentId);
            res.ExtJson.Add("ParentName", parent?.Name);
            return Success(res);
        }
        /// <summary>
        /// 保存组织信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveOrganization(OrganizationEntity entity) {
            var res = await _organizationClient.SaveOrganizationAsync(entity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除组织信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteOrganization(List<long> ids) {
            var res = await _organizationClient.DeleteOrganizationAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新组织状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateOrganizationStatus(CommomParameter parameter) {
            var res = await _organizationClient.UpdateOrganizationStatusAsync(parameter.Ids, parameter.Status);
            return Success(res);
        }




        #region 其他方法
        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizations"></param>
        private async Task FillOrganizationExjosn(List<OrganizationEntity> organizations) {
            if (organizations.Count == 0) {
                return;
            }
            var orgIds = organizations.ConvertAll(p => p.Id);
            var userArr = await _organizationClient.GetUserListByOrgIdAsync(orgIds);

            //部门经理
            var userIds = organizations.Select(p => p.Manager).Distinct().ToList();
            var userList = await _organizationClient.GetUserListByIdAsync(userIds);

            foreach (var item in organizations) {
                var count = userArr?.Count(p => p.DepartmentId == item.Id);
                item.ExtJson.Add("UserCount", count);
                var temp = userList?.FirstOrDefault(p => p.Id == item.Manager);
                item.ExtJson.Add("ManagerName", temp?.Name);
            }
        }
        #endregion
    }
}
