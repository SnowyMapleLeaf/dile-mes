﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.Organization.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.Organization {
    /// <summary>
    /// 权限
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Organization)]
    public class PermissionController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="organization"></param>
        public PermissionController(OrganizationClient organization) {
            _organizationClient = organization;
        }
        /// <summary>
        /// 获取权限信息
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetPermissionByRoleId(long roleId) {
            var res = await _organizationClient.GetPermissionByRoleIdAsync(roleId);
            return Success(res);
        }
        /// <summary>
        /// 保存权限信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SavePermission(PermissionEntity entity) {
            await _organizationClient.SavePermissionAsync(entity);
            return Success();
        }
        /// <summary>
        /// 删除权限信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeletePermission(List<long> ids) {
            var res = await _organizationClient.DeletePermissionAsync(ids);
            return res ? Success() : Fail();
        }
    }
}
