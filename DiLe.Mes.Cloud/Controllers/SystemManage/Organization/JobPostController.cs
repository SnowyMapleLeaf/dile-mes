﻿using DiLe.Mes.Application.Common;
using DiLe.Mes.Model.Common.Organization.Entity;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.Organization {
    /// <summary>
    /// 工作岗位
    /// </summary>
    [ApiExplorerSettings(GroupName = ApiCloudGroupConst.Organization)]
    public class JobPostController : ApiBaseController {
        /// <summary>
        /// 
        /// </summary>
        private readonly OrganizationClient _organizationClient;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationClient"></param>
        public JobPostController(OrganizationClient organizationClient) {
            _organizationClient = organizationClient;
        }


        /// <summary>
        /// 获取岗位信息列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> GetJobPostList(QueryParameter parameter) {
            var whereExpression = Expressionable.Create<JobPostEntity>();
            if (!parameter.Keyword.IsNullOrEmpty()) {
                whereExpression.Or(p => p.Name.Contains(parameter.Keyword));
                whereExpression.Or(p => p.Code.Contains(parameter.Keyword));
            }
            if (parameter.Pagination == null) {
                var res = await _organizationClient.GetJobPostListAsync(whereExpression.ToExpression());
                return Success(res);
            } else {
                var res = await _organizationClient.GetJobPostPageListAsync(whereExpression.ToExpression(), parameter.Pagination);
                return Success(res);
            }
        }
        /// <summary>
        /// 获取岗位信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetJobPostInfo(long id) {
            var res = await _organizationClient.GetJobPostAsync(id);
            return Success(res);
        }
        /// <summary>
        /// 保存岗位信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SaveJobPost(JobPostEntity jobPostEntity) {
            var res = await _organizationClient.SaveJobPostAsync(jobPostEntity);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 删除岗位信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> DeleteJobPost(List<long> ids) {
            var res = await _organizationClient.DeleteJobPostAsync(ids);
            return res ? Success() : Fail();
        }
        /// <summary>
        /// 更新岗位状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> UpdateJobPostStatus(CommomParameter parameter) {
            var res = await _organizationClient.UpdateJobPostStatusAsync(parameter.Ids, parameter.Status);
            return Success(res);
        }

    }
}
