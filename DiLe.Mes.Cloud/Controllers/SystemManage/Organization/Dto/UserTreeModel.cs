﻿namespace DiLe.Mes.Cloud.Controllers.SystemManage.Organization.Dto {
    /// <summary>
    /// 
    /// </summary>
    public class UserTreeModel {
        /// <summary>
        /// 主键
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// 类型 1 部门 2 人员
        /// </summary>
        public int? Type { get; set; } = 1;
        /// <summary>
        /// 部门名称
        /// </summary>
        public string OrgName { get; set; }
        /// <summary>
        /// 子集
        /// </summary>
        public List<UserTreeModel> Children { get; set; } = new List<UserTreeModel>();
    }
}
