﻿using DiLe.Mes.Model.Common.Organization.Entity;
using DiLe.Mes.Model.Common.Organization.Relation;

namespace DiLe.Mes.Cloud.Controllers.SystemManage.Organization.Parameters {
    /// <summary>
    /// 
    /// </summary>
    public class RoleParameter {
        /// <summary>
        /// 
        /// </summary>
        public RoleEntity? Model { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<UserRoleEntity> Relations { get; set; } = new List<UserRoleEntity>();
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrganizationQueryParameter : QueryParameter {
        /// <summary>
        /// 
        /// </summary>
        public string? Type { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserQueryParameter : QueryParameter {
        /// <summary>
        /// 
        /// </summary>
        public long? OrgId { get; set; } = 0;
    }
}
