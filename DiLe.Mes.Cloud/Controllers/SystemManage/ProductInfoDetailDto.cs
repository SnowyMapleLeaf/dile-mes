﻿using DiLe.Mes.Model.Common.Process.Entity;
using DiLe.Mes.Model.Common.Product.Entity;
using DiLe.Mes.Model.Common.Product.Relation;

namespace DiLe.Mes.Cloud.Controllers.SystemManage {

    /// <summary>
    /// 
    /// </summary>
    public class ProductInfoDetailDto {
        /// <summary>
        /// 
        /// </summary>
        public ProductInfoEntity Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ProductInfo2ProcessAttributeEntity> ProcessAttributeRelations { get; set; } = new List<ProductInfo2ProcessAttributeEntity>();
        /// <summary>
        /// 
        /// </summary>
        public List<ProductInfo2AttributeEntity> ProductAttributeRelations { get; set; } = new List<ProductInfo2AttributeEntity>();
        /// <summary>
        /// 类型
        /// </summary>
        public ProductTypeEntity CategoryModel { get; set; }
    }
}
