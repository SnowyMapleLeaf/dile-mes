﻿using KnifeUI.Swagger.Net;
using MapleLeaf.Core.Swagger;

namespace DiLe.Mes.Cloud.Middleware {
    /// <summary>
    /// 
    /// </summary>
    public static class SwaggerMiddleware {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        public static void UseSwaggerMilddleware(this IApplicationBuilder app) {
            if (app == null)
                throw new ArgumentNullException(nameof(app));

            app.UseSwagger();
            //app.UseSwaggerUI(c => {
            //    //根据版本名称倒序 遍历展示
            //    SwaggerExtensions.GetCloudSwaggerApiInfos().OrderBy(e => e.Order).ToList().ForEach(version => {
            //        c.SwaggerEndpoint($"/swagger/{version.UrlPrefix}/swagger.json", $"{version.Name}");
            //    });

            //});


            app.UseKnife4UI(c => {
                c.RoutePrefix = ""; // serve the UI at root
                //c.SwaggerEndpoint("/v1/api-docs", "V1 Docs");
                SwaggerExtensions.GetCloudSwaggerApiInfos().OrderBy(e => e.Order).ToList().ForEach(version => {
                    c.SwaggerEndpoint($"/swagger/{version.UrlPrefix}/swagger.json", $"{version.Name}");
                });
            });

        }
    }
}
