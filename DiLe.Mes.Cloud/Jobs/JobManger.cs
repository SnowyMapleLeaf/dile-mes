﻿using DiLe.Mes.Cloud.Expansion;
using EasyNetQ;
using MapleLeaf.Core.EventBus;

namespace DiLe.Mes.Cloud.Jobs {
    /// <summary>
    /// 
    /// </summary>
    public class JobManger {
        private readonly static IBus _ibus = AppHelper.GetService<IBus>();
        /// <summary>
        /// 接收数据
        /// </summary>
        public static void StartDataReceiveJob() {
            _ibus.SendReceive.Receive<MessageModel>("data_push_statistics", DataReceiveStatistics.SaveData);
            _ibus.SendReceive.Receive<MessageModel>("data_push", DataReceive.SaveData);
            _ibus.SendReceive.Receive<MessageModel>("data_push_alarm", DataReceiveAlarm.SaveData);
        }
    }
}
