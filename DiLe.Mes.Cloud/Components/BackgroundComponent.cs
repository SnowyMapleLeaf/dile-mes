﻿using DiLe.Mes.Cloud.Jobs;

namespace DiLe.Mes.Cloud.Components {
    /// <summary>
    /// 
    /// </summary>
    public static class BackgroundComponent {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void AddBackgroundComponent(this WebApplicationBuilder builder) {
            builder.Host.ConfigureServices((hostContext, services) => {
                services.AddHostedService<MyBackgroundService>();
            });
        }

    }
    /// <summary>
    /// 
    /// </summary>
    public class MyBackgroundService : BackgroundService {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stoppingToken"></param>
        /// <returns></returns>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            Console.WriteLine("程序启动完成！");
            //接收数据
            JobManger.StartDataReceiveJob();
            // 等待程序关闭的信号
            await Task.Delay(Timeout.Infinite, stoppingToken);
        }
    }
}
