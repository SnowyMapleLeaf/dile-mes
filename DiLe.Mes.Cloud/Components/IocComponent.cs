﻿using DiLe.Mes.Service.Interface;

namespace DiLe.Mes.Cloud.Components {
    /// <summary>
    /// 
    /// </summary>
    public static class IocComponent {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void AddIocComponent(this WebApplicationBuilder builder) {

            //builder.Services.Scan(scan => scan
            //    // 扫描特定类型所在的程序集，这里是 ITransient所在的程序集
            //    .FromAssemblyOf<IScoped>()
            //    // .AddClasses 在上面获取到的程序集中扫描所有公开、非抽象类型
            //    // 之后可以通过委托进行类型筛选，例如下面只扫描实现 ITransientService 的类型
            //    .AddClasses(classes => classes.AssignableTo<IScoped>())
            //    // 将上面的类型作为它实现的所有接口进行注册
            //    // 如果类型实现了 N 个接口，那么就会有三个独立的注册
            //    .AsImplementedInterfaces()
            //    // 最后指定注册的生存期，如瞬时，作用域，还是单例
            //    .WithScopedLifetime()
            //);
            builder.Services.Scan(scan => scan
               // 扫描特定类型所在的程序集，这里是 IApplicationClient 所在的程序集
               .FromAssemblyOf<IApplicationClient>()
               .AddClasses(classes => classes.AssignableTo<IApplicationClient>())
               .AsSelf()
               .WithScopedLifetime()
           );
            builder.Services.Scan(scan => scan
                // 扫描特定类型所在的程序集，这里是 IApplicationClient 所在的程序集
                .FromAssemblyOf<IScoped>()
                .AddClasses(classes => classes.AssignableTo<IScoped>())
                .AsSelf()
                .WithScopedLifetime()
            );

        }
    }
}
