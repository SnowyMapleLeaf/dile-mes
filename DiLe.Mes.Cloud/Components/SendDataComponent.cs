﻿using DiLe.Mes.Model.Common.Organization.Entity;
using MapleLeaf.Core.AppSetting;

namespace DiLe.Mes.Cloud.Components {
    /// <summary>
    /// 
    /// </summary>
    public static class SendDataComponent {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void AddSendDataComponent(this WebApplicationBuilder builder) {
            AppSettingModel appinfo = AppHelper.AppSettingInfo;

            if (!appinfo.SeedData.ToBoolean()) {
                return;
            }
            Assembly assembly = typeof(UserEntity).Assembly;
            Type[] allTypes = assembly.GetTypes();

            Task.Run(() => {
                if (appinfo.SystemSign == "cloud") {
                    Type[] cloudtypes = allTypes.Where(it => it.FullName!.StartsWith("DiLe.Mes.Model.Cloud") && it.FullName!.EndsWith("Entity")).ToArray();
                    SqlSugarHelper.CloudDb.CodeFirst.SetStringDefaultLength(2000).InitTables(cloudtypes);
                }
            });
            Task.Run(() => {
                Type[] commiontypes = allTypes.Where(it => it.FullName!.Contains("DiLe.Mes.Model.Common")).ToArray();
                SqlSugarHelper.MasterDb.CodeFirst.SetStringDefaultLength(2000).InitTables(commiontypes);
            });
        }
    }
}

