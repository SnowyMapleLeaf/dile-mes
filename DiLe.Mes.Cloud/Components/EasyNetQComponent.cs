﻿using EasyNetQ;

namespace DiLe.Mes.Cloud.Components {
    /// <summary>
    /// 
    /// </summary>
    public static class EasyNetQComponent {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddEasyNetQ(this IServiceCollection services) {
            string hostName = AppHelper.GetConfig("RabbitMQ", "HostName");
            string userName = AppHelper.GetConfig("RabbitMQ", "UserName");
            string password = AppHelper.GetConfig("RabbitMQ", "Password");
            string connectionString = $"host={hostName};virtualHost=/;username={userName};password={password};timeout=60";
            var bus = RabbitHutch.CreateBus(connectionString);
            services.AddSingleton(bus);
            return services;
        }
    }
}
