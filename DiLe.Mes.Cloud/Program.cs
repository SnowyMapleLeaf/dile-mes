using DiLe.Mes.Application.Handler;
using DiLe.Mes.Cloud.Components;
using DiLe.Mes.Cloud.Controllers.Abnormal;
using DiLe.Mes.Cloud.Controllers.SystemManage;
using DiLe.Mes.Cloud.Jobs;
using DiLe.Mes.Cloud.Middleware;
using Newtonsoft.Json.Serialization;


var builder = WebApplication.CreateBuilder(args);
//添加项目静态信息
AppHelper.Initialize(builder.Configuration, builder.Environment);
//依赖注入
builder.AddIocComponent();
//数据库
builder.AddSqlSugarSetup();

builder.Services.AddScoped<EquipmentExjosnHandler>();
builder.Services.AddScoped<MouldExjosnHandler>();
builder.Services.AddScoped<SystemExjosnHandler>();
builder.Services.AddScoped<AbnormalExjosnHandler>();
//JWT
builder.AddAuthComponent();
//跨域
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
builder.Services.AddCors(options => {
    options.AddPolicy(MyAllowSpecificOrigins, builder => {
        builder.AllowAnyMethod().SetIsOriginAllowed(_ => true).AllowAnyHeader().AllowCredentials();
    });
});
builder.Services.AddEasyNetQ();
//系统启动完成后执行
builder.AddBackgroundComponent();

//获取IP
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

builder.Services.AddControllers(options => {
    options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true;
    options.AllowEmptyInputInBodyModelBinding = true;
}).AddNewtonsoftJson(opt => {
    opt.SerializerSettings.NullValueHandling = NullValueHandling.Include;
    //大驼峰
    //opt.SerializerSettings.ContractResolver = new DefaultContractResolver();
    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//添加Swagger
builder.AddSwaggerComponent();

//注册成服务
builder.Host.UseWindowsService();


var app = builder.Build();

AppHelper.SetServiceProvider(app.Services);
//种子数据
builder.AddSendDataComponent();



app.UseMiddleware<RequestLoggingMiddleware>();

app.UseCors(MyAllowSpecificOrigins);//启用跨域问题
// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment()) {
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
// 封装Swagger展示
app.UseSwaggerMilddleware();

//异常处理
app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();

//认证中间件
app.UseAuthentication();
//授权中间件
app.UseAuthorization();

app.MapControllers();

app.Run();
