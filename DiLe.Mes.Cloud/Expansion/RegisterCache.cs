﻿using DiLe.Mes.Application.Common;
using MapleLeaf.Core.Cache.Redis;

namespace DiLe.Mes.Cloud.Expansion {
    /// <summary>
    /// 
    /// </summary>
    public static class RegisterCache {
        /// <summary>
        /// 添加缓存数据
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static async Task AddCache(this WebApplicationBuilder builder) {
            IServiceProvider ServiceProvider = builder.Services.BuildServiceProvider();
            var client = ServiceProvider.GetRequiredService<OrganizationClient>();
            var userList = await client.GetUserListAsync();
            var userRedisClient = new RedisClient(RedisConst.UserStore);
            var dic = new Dictionary<string, object>();
            foreach (var item in userList) {
                dic.Add($"{item.Id}", item);
            }
            userRedisClient.SetMultiple(dic);
        }
    }
}
