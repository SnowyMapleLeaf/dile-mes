﻿using DiLe.Mes.Application.Cloud.Statistics;
using DiLe.Mes.Model.Cloud.Dashboard.Statistics;
using DiLe.Mes.Model.Cloud.Statistics;
using DiLe.Mes.Service.Cloud.Statistics;
using DiLe.Mes.Service.Common.Equipment.Info;
using MapleLeaf.Core.EventBus;
using MapleLeaf.Core.Serilog;

namespace DiLe.Mes.Cloud.Expansion {
    /// <summary>
    /// 
    /// </summary>
    public class DataReceiveStatistics {

        private readonly static StatisticsClient _statisticsClient = AppHelper.GetService<StatisticsClient>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static void SaveData(MessageModel model) {
            if (model.SendMsg.IsNullOrEmpty()) {
                return;
            }
            switch (model.MsgSign) {
                case "EquipmentStatistics":
                    _ = ReveiveEquipmentStatistics(model);
                    break;
                case "EquipmentOutputStatistics":
                    _ = ReveiveEquipmentOutputStatistics(model);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 设备统计信息
        /// </summary>
        /// <returns></returns>
        private static bool ReveiveEquipmentStatistics(MessageModel model) {
            string id = Guid.NewGuid().ToString();
            try {
                SerilogLogHelper.WriteSystemLog($"设备统计信息开始接收=>{id}：{model.AppId}-{model.SendMsg}");

                var models = JsonConvert.DeserializeObject<List<EquipmentStatisticsEntity>>(model.SendMsg!);
                if (models == null && models.None()) {
                    return false;
                }
                var day = models.First().Date;
                models!.ForEach(x => {
                    x.FactoryId = model.AppId;
                });
                var res = _statisticsClient.DeletEquipmentStatisticsList(p => p.Date == day);
                //插入
                res = _statisticsClient.InsertEquipmentStatistics(models) > 0;
                SerilogLogHelper.WriteSystemLog($"设备统计信息接收完成=>{id}");
                return res;
            } catch (Exception ex) {
                var str = $"设备统计信息接收失败=>{id}：{ex.Message}";
                SerilogLogHelper.WriteSystemErrorLog(str);
                return false;
            }
        }
        /// <summary>
        /// 设备产出统计
        /// </summary>
        /// <returns></returns>
        private static bool ReveiveEquipmentOutputStatistics(MessageModel model) {
            string id = Guid.NewGuid().ToString();
            try {
                SerilogLogHelper.WriteSystemLog($"设备产出统计信息开始接收=>{id} 工厂：{model.AppId}：{model.SendMsg}");
                var day = DateTime.Now.Date;
                if (DateTime.Now.Hour < 8) {
                    day = DateTime.Now.AddDays(-1).Date;
                }
                var models = JsonConvert.DeserializeObject<List<EquipmentOutputStatisticsEntity>>(model.SendMsg);
                if (models == null && models.None()) {
                    return false;
                }
                models!.ForEach(x => {
                    x.MouldCavityNum = 0;
                    x.FactoryId = model.AppId;
                });
                var res = _statisticsClient.DeletEquipmentOutputStatisticsList(p => p.Date == day);
                //插入
                res = _statisticsClient.InsertEquipmentOutputStatistics(models) > 0;
                SerilogLogHelper.WriteSystemLog($"设备产出统计信息接收完成=>{id}");
                return res;
            } catch (Exception ex) {
                var str = $"设备产出统计信息接收失败=>{id}：{ex.Message}";
                SerilogLogHelper.WriteSystemErrorLog(str);
                return false;
            }
        }
    }
}
