﻿using MapleLeaf.DataBase.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Local.Entity {
    /// <summary>
    /// PLC点位数据传输（流量计）
    /// </summary>
    [SugarTable("bus_fmpointinfo", "PLC点位数据传输（流量计）")]
    public class FMPointInfoEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 设备标识符
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识符")]
        public string DeviceBrand { set; get; }
        /// <summary>
        /// 报警信息
        /// </summary>
        [SugarColumn(ColumnDescription = "报警信息")]
        public string AlarmInfo { set; get; }
        /// <summary>
        /// 流量累计
        /// </summary>
        [SugarColumn(ColumnDescription = "流量累计")]
        public string FlowTotal { set; get; }
        /// <summary>
        /// 是否推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否推送")]
        public bool? IsPushed { set; get; } = false;
    }
}
