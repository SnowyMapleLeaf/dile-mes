﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Local.Entity {
    /// <summary>
    /// PLC点位信息-成型机
    /// </summary>
    [SugarTable("bus_pointinfo", "PLC点位信息")]
    public class PointInfoEntity : PointBaseInfo {
        /// <summary>
        /// 成型压力
        /// </summary>
        [SugarColumn(ColumnDescription = "成型压力")]
        public string FormingPressure { set; get; }
        /// <summary>
        /// 成型次数
        /// </summary>
        [SugarColumn(ColumnDescription = "成型次数")]
        public string FormingTimes { set; get; }
        /// <summary>
        /// 成型时间(吸浆时间+脱水时间)
        /// </summary>
        [SugarColumn(ColumnDescription = "成型时间(吸浆时间+脱水时间)")]
        public string? FormingTime { set; get; }
        /// <summary>
        /// 成型吸浆时间
        /// </summary>
        public string? FormingSuctionTime { set; get; }
        /// <summary>
        /// 成型脱水时间
        /// </summary>
        public string? FormingDehydrationTime { set; get; }
        /// <summary>
        /// 成型累计次数
        /// </summary>
        [SugarColumn(ColumnDescription = "成型累计次数")]
        public string FormingTotalTimes { set; get; }
        /// <summary>
        /// 左侧热压上模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压上模温度")]
        public string LeftUpperTemp { set; get; }
        /// <summary>
        /// 左侧热压上模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压上模温度设定")]
        public string LeftUpperTempSet { set; get; }
        /// <summary>
        /// 左侧热压下模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压下模温度设定")]
        public string LeftDownTempSet { set; get; }
        /// <summary>
        /// 左侧热压下模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压下模温度")]
        public string LeftDownTemp { set; get; }
        /// <summary>
        /// 左侧热压压力
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压压力")]
        public string LeftHotPressure { set; get; }
        /// <summary>
        /// 左侧热压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压时间")]
        public string LeftHPTime { set; get; }
        /// <summary>
        /// 左侧烘烤时间
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧烘烤时间")]
        public string LeftBakeTime { set; get; }
        /// <summary>
        /// 左侧加压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧加压时间")]
        public string LeftPressurizeTime { set; get; }
        /// <summary>
        /// 左侧热压次数
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压次数")]
        public string LeftHPTimes { set; get; }
        /// <summary>
        /// 左侧热压累计次数
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压累计次数")]
        public string LeftHPTotalTimes { set; get; }
        /// <summary>
        /// 右侧热压上模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压上模温度")]
        public string RightUpperTemp { set; get; }
        /// <summary>
        /// 右侧热压上模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压上模温度设定")]
        public string RightUpperTempSet { set; get; }
        /// <summary>
        /// 右侧热压下模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压下模温度设定")]
        public string RightDownTempSet { set; get; }
        /// <summary>
        /// 右侧热压下模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压下模温度")]
        public string RightDownTemp { set; get; }
        /// <summary>
        /// 右侧热压压力
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压压力")]
        public string RightHotPressure { set; get; }
        /// <summary>
        /// 右侧热压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压时间")]
        public string RightHPTime { set; get; }
        /// <summary>
        /// 右侧烘烤时间
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧烘烤时间")]
        public string RightBrakeTime { set; get; }
        /// <summary>
        /// 右侧加压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧加压时间")]
        public string RightPressurizeTime { set; get; }
        /// <summary>
        /// 右侧热压次数
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压次数")]
        public string RightHPTimes { set; get; }
        /// <summary>
        /// 右侧热压累计次数
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压累计次数")]
        public string RightHPToatalTimes { set; get; }
        /// <summary>
        /// 电能累计
        /// </summary>
        [SugarColumn(ColumnDescription = "电能累计")]
        public string EeTotal { set; get; }
        /// <summary>
        /// 是否推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否推送")]
        public bool? IsPushed { set; get; } = false;

    }
}
