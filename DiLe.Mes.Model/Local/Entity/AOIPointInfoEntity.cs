﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Local.Entity {
    /// <summary>
    /// PLC点位数据传输（AOI CCD）
    /// </summary>
    [SugarTable("bus_aoipointinfo", "PLC点位数据传输（AOI CCD）")]
    public class AOIPointInfoEntity : PointBaseInfo {
        /// <summary>
        /// 检测NG数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测NG数")]
        public string CheckNGNo { set; get; }
        /// <summary>
        /// 检测待定数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测待定数")]
        public string CheckPendTimes { set; get; }
        /// <summary>
        /// 检测累计总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测累计总数")]
        public string CheckTotalTimes { set; get; }
        /// <summary>
        /// 检测总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测总数")]
        public string CheckTimes { set; get; }
        /// <summary>
        /// 是否推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否推送")]
        public bool? IsPushed { set; get; } = false;
    }
}
