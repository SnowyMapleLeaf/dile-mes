﻿using MapleLeaf.Core.Entity;
using MapleLeaf.DataBase.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Local.Entity {
    /// <summary>
    /// PLC点位数据传输（电能表）
    /// </summary>
    [SugarTable("bus_empointinfo", "PLC点位数据传输（电能表）")]
    public class EMPointInfoEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 设备标识符
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识符")]
        public string DeviceBrand { set; get; }
        /// <summary>
        /// 报警信息
        /// </summary>
        [SugarColumn(ColumnDescription = "报警信息")]
        public string AlarmInfo { set; get; }
        /// <summary>
        /// 能耗累计
        /// </summary>
        [SugarColumn(ColumnDescription = "能耗累计")]
        public string EcTotal { set; get; }
        /// <summary>
        /// 是否推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否推送")]
        public bool? IsPushed { set; get; } = false;
    }
}
