﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Local.Abnormal.Entity {
    /// <summary>
    /// 报警信息
    /// </summary>
    [SugarTable("bus_alarminfo", "报警信息")]
    public class AlarmInfoEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string? DeviceNo { set; get; }
        /// <summary>
        /// 报警日期
        /// </summary>
        [SugarColumn(ColumnDescription = "报警日期")]
        public string AlarmDate { set; get; }
        /// <summary>
        /// 报警信息
        /// </summary>
        [SugarColumn(ColumnDescription = "报警信息")]
        public string? AlarmData { set; get; }
        /// <summary>
        /// 报警开始时间
        /// </summary>
        [SugarColumn(ColumnDescription = "报警开始时间")]
        public DateTime? StartAlarmTime { set; get; }
        /// <summary>
        /// 报警结束时间
        /// </summary>
        [SugarColumn(ColumnDescription = "报警结束时间")]
        public DateTime? EndAlarmTime { set; get; }

        /// <summary>
        /// 是否已经推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否已经推送")]
        public bool IsPushed { set; get; } = false;
        /// <summary>
        /// 是否已经解除
        /// </summary>
        [SugarColumn(ColumnDescription = "是否已经解除")]
        public bool IsAlarmRelease { set; get; } = false;
        
    }
}
