﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Local.Abnormal.Entity {
    /// <summary>
    /// 报警临时信息
    /// </summary>
    [SugarTable("bus_alarmtempinfo", "报警临时信息")]
    public class AlarmTempInfoEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string? DeviceNo { set; get; }
        /// <summary>
        /// 报警信息
        /// </summary>
        [SugarColumn(ColumnDescription = "报警信息")]
        public string? AlarmData { set; get; }
    }
}
