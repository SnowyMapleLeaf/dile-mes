﻿using MapleLeaf.DataBase.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Local.WorkOrder.Entity {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("wo_workorderinfo", "生产工单")]
    public class WorkOrderEntity : AbstractBaseEntity {
        /// <summary>
        /// 生产工单号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 订单客户
        /// </summary>
        public string Customer { get; set; }
        /// <summary>
        /// 产品
        /// </summary>
        public long ProductId { get; set; }
        /// <summary>
        /// 交货日期
        /// </summary>
        public DateTime DeliveryDate { get; set; }
        /// <summary>
        /// 计划完成时间
        /// </summary>
        public DateTime PlanEndDate { get; set; }
        /// <summary>
        /// 计划开始时间
        /// </summary>
        public DateTime PlanStartDate { get; set; }
        /// <summary>
        /// 工单优先级
        /// </summary>
        public string Priority { get; set; }
        /// <summary>
        /// 生产订单编号
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 订单数量
        /// </summary>
        public int OrderQuantity { get; set; }
        /// <summary>
        /// 工单数量
        /// </summary>
        public int Quantity { get; set; }
    }
}
