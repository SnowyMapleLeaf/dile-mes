﻿namespace DiLe.Mes.Model.Cloud.Statistics {
    /// <summary>
    /// ESG数据-电能表统计
    /// </summary>
    [SugarTable("statistics_empoint", "ESG数据-电能表统计")]
    public class EMPointStatisticsEntity : AbstractBaseEntity {
        /// <summary>
        /// 采集日期
        /// </summary>
        [SugarColumn(ColumnDescription = "采集日期")]
        public string Date { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        [SugarColumn(ColumnDescription = "设备名称")]
        public string Name { set; get; }
        /// <summary>
        /// 设备类型
        /// </summary>
        [SugarColumn(ColumnDescription = "设备类型")]
        public string Type { set; get; }
        /// <summary>
        /// 累计电能(KWH)
        /// </summary>
        [SugarColumn(ColumnDescription = "累计电能(KWH)")]
        public string ElectricEnergy { set; get; }
        /// <summary>
        /// 工厂ID
        /// </summary>
        [SugarColumn(ColumnDescription = "工厂ID")]
        public long? FactoryId { get; set; } = 0;
        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(ColumnDescription = "排序")]
        public int OrderNumber { get; set; } = 0;
    }
}
