﻿namespace DiLe.Mes.Model.Cloud.Statistics {
    /// <summary>
    /// AOI CCD PLC点位数据统计
    /// </summary>
    [SugarTable("statistics_aoipoint", "AOI CCD PLC点位数据统计")]
    public class AOIPointStatisticsEntity : AbstractBaseEntity {
        /// <summary>
        /// 采集时间
        /// </summary>
        [SugarColumn(ColumnDescription = "采集时间")]
        public string AcquisitionTime { set; get; }
        /// <summary>
        /// 设备标识
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 工厂ID
        /// </summary>
        [SugarColumn(ColumnDescription = "工厂ID")]
        public long? FactoryId { get; set; } = 0;




        /// <summary>
        /// 检测NG数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测NG数")]
        public string CheckNGNo { set; get; }
        /// <summary>
        /// 检测待定数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测待定数")]
        public string CheckPendTimes { set; get; }
        /// <summary>
        /// 检测累计总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测累计总数")]
        public string CheckTotalTimes { set; get; }
        /// <summary>
        /// 检测总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测总数")]
        public string CheckTimes { set; get; }
    }
}
