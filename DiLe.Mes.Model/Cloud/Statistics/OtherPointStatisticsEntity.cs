﻿namespace DiLe.Mes.Model.Cloud.Statistics {
    /// <summary>
    /// 1311 PLC点位信息统计信息
    /// </summary>
    [SugarTable("statistics_otherpoint", "1311 PLC点位信息统计信息")]
    public class OtherPointStatisticsEntity : AbstractBaseEntity {
        /// <summary>
        /// 采集时间
        /// </summary>
        [SugarColumn(ColumnDescription = "采集时间")]
        public string AcquisitionTime { set; get; }
        /// <summary>
        /// 设备标识
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 工厂ID
        /// </summary>
        [SugarColumn(ColumnDescription = "工厂ID")]
        public long? FactoryId { get; set; } = 0;
        /// <summary>
        /// 成型压力
        /// </summary>
        [SugarColumn(ColumnDescription = "成型压力")]
        public string FormingPressure { set; get; }
        /// <summary>
        /// 成型次数
        /// </summary>
        [SugarColumn(ColumnDescription = "成型次数")]
        public string FormingTimes { set; get; }
        /// <summary>
        /// 成型时间(吸浆时间+脱水时间)
        /// </summary>
        [SugarColumn(ColumnDescription = "成型时间(吸浆时间+脱水时间)")]
        public string FormingTime { set; get; }
        /// <summary>
        /// 成型累计次数
        /// </summary>
        [SugarColumn(ColumnDescription = "成型累计次数")]
        public string FormingTotalTimes { set; get; }
        /// <summary>
        /// 左侧热压上模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压上模温度")]
        public string LeftUpperTemp { set; get; }
        /// <summary>
        /// 左侧热压上模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压上模温度设定")]
        public string LeftUpperTempSet { set; get; }
        /// <summary>
        /// 左侧热压下模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压下模温度设定")]
        public string LeftDownTempSet { set; get; }
        /// <summary>
        /// 左侧热压下模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压下模温度")]
        public string LeftDownTemp { set; get; }
        /// <summary>
        /// 左侧热压压力
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压压力")]
        public string LeftHotPressure { set; get; }
        /// <summary>
        /// 左侧热压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压时间")]
        public string LeftHPTime { set; get; }
        /// <summary>
        /// 左侧烘烤时间
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧烘烤时间")]
        public string LeftBakeTime { set; get; }
        /// <summary>
        /// 左侧加压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧加压时间")]
        public string LeftPressurizeTime { set; get; }
        /// <summary>
        /// 左侧热压次数
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压次数")]
        public string LeftHPTimes { set; get; }
        /// <summary>
        /// 左侧热压累计次数
        /// </summary>
        [SugarColumn(ColumnDescription = "左侧热压累计次数")]
        public string LeftHPTotalTimes { set; get; }
        /// <summary>
        /// 右侧热压上模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压上模温度")]
        public string RightUpperTemp { set; get; }
        /// <summary>
        /// 右侧热压上模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压上模温度设定")]
        public string RightUpperTempSet { set; get; }
        /// <summary>
        /// 右侧热压下模温度设定
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压下模温度设定")]
        public string RightDownTempSet { set; get; }
        /// <summary>
        /// 右侧热压下模温度
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压下模温度")]
        public string RightDownTemp { set; get; }
        /// <summary>
        /// 右侧热压压力
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压压力")]
        public string RightHotPressure { set; get; }
        /// <summary>
        /// 右侧热压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压时间")]
        public string RightHPTime { set; get; }
        /// <summary>
        /// 右侧烘烤时间
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧烘烤时间")]
        public string RightBrakeTime { set; get; }
        /// <summary>
        /// 右侧加压时间
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧加压时间")]
        public string RightPressurizeTime { set; get; }
        /// <summary>
        /// 右侧热压次数
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压次数")]
        public string RightHPTimes { set; get; }
        /// <summary>
        /// 右侧热压累计次数
        /// </summary>
        [SugarColumn(ColumnDescription = "右侧热压累计次数")]
        public string RightHPToatalTimes { set; get; }
        /// <summary>
        /// 电能累计
        /// </summary>
        [SugarColumn(ColumnDescription = "电能累计")]
        public string EeTotal { set; get; }
        /// <summary>
        /// 切边机压力
        /// </summary>
        [SugarColumn(ColumnDescription = "切边机压力")]
        public string CutPressure_1311 { set; get; }
        /// <summary>
        /// 切边次数
        /// </summary>
        [SugarColumn(ColumnDescription = "切边次数")]
        public string CutCount_1311 { set; get; }
        /// <summary>
        /// 切边累计次数
        /// </summary>
        [SugarColumn(ColumnDescription = "切边累计次数")]
        public string CutTotalCount_1311 { set; get; }
        /// <summary>
        /// 检测NG数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测NG数")]
        public string CheckNg_1311 { set; get; }
        /// <summary>
        /// 检测待定数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测待定数")]
        public string CheckReady_1311 { set; get; }
        /// <summary>
        /// 检测总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测总数")]
        public string CheckNum_1311 { set; get; }
        /// <summary>
        /// 检测累计总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测累计总数")]
        public string CheckTotalNum_1311 { set; get; }
        /// <summary>
        /// 堆叠次数
        /// </summary>
        [SugarColumn(ColumnDescription = "堆叠次数")]
        public string StackingNum { set; get; }
        /// <summary>
        /// 堆叠层数
        /// </summary>
        [SugarColumn(ColumnDescription = "堆叠层数")]
        public string StackingFloorNum { set; get; }
        /// <summary>
        /// 堆叠数量累计
        /// </summary>
        [SugarColumn(ColumnDescription = "堆叠数量累计")]
        public string StackingTotalNum { set; get; }

    }
}
