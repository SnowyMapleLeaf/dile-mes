﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Statistics {
    /// <summary>
    /// 统计设备信息
    /// </summary>
    [SugarTable("statistics_equipmentinfo", "统计设备信息")]
    public class EquipmentStatisticsEntity : DataEntityBase {
        /// <summary>
        /// 日期
        /// </summary>
        [SugarColumn(ColumnDescription = "日期")]
        public DateTime? Date { set; get; }
        /// <summary>
        /// 班次 日班 ：Day shift 夜班： Night shift
        /// </summary>
        [SugarColumn(ColumnDescription = "班次")]
        public string? Classes { set; get; }
        /// <summary>
        /// 设备标识
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 开机时间
        /// </summary>
        [SugarColumn(ColumnDescription = "开机时间")]
        public DateTime? EquipmentStartTime { set; get; }
        /// <summary>
        /// 关机时间
        /// </summary>
        [SugarColumn(ColumnDescription = "关机时间")]
        public DateTime? EquipmentEndTime { set; get; }
        /// <summary>
        /// 实际工时
        /// </summary>
        [SugarColumn(ColumnDescription = "实际工时")]
        public decimal? ActualWorkingHour { get; set; }
    }
}
