﻿namespace DiLe.Mes.Model.Cloud.Statistics {
    /// <summary>
    /// 切边机PLC点位数据统计
    /// </summary>
    [SugarTable("statistics_trimpoint", "切边机PLC点位数据统计")]
    public class TrimPointStatisticsEntity : AbstractBaseEntity {
        /// <summary>
        /// 采集时间
        /// </summary>
        [SugarColumn(ColumnDescription = "采集时间")]
        public string AcquisitionTime { set; get; }
        /// <summary>
        /// 设备标识
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 工厂ID
        /// </summary>
        [SugarColumn(ColumnDescription = "工厂ID")]
        public long? FactoryId { get; set; } = 0;
        /// <summary>
        /// 切边压力
        /// </summary>
        [SugarColumn(ColumnDescription = "切边压力")]
        public string? TrimmingPressure { set; get; }
        /// <summary>
        /// 切边次数
        /// </summary>
        [SugarColumn(ColumnDescription = "切边次数")]
        public string? TrimingTimes { set; get; }
        /// <summary>
        /// 设备启用后切边模总工作次数
        /// </summary>
        [SugarColumn(ColumnDescription = "设备启用后切边模总工作次数")]
        public string? TrimingTotalTimes { set; get; }
    }
}
