﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Cloud.Statistics.Model {
    /// <summary>
    /// 
    /// </summary>
    public class AlarmReportModel {
        /// <summary>
        /// 
        /// </summary>
        public string Abnormaldate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EquipmentCode { get; set; }
    }
    
}
