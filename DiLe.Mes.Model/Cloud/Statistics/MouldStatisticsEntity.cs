﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Statistics {
    /// <summary>
    /// 统计模具信息
    /// </summary>
    [SugarTable("statistics_mouldinfo", "统计模具信息")]
    public class MouldStatisticsEntity : AbstractBaseEntity {
        /// <summary>
        /// 模具主键标识
        /// </summary>
        [SugarColumn(ColumnDescription = "模具主键标识")]
        public long MouldId { set; get; }
        /// <summary>
        /// 累计模次
        /// </summary>
        [SugarColumn(ColumnDescription = "累计模次")]
        public int MouldNumber{ set; get; } = 0;
    }
}
