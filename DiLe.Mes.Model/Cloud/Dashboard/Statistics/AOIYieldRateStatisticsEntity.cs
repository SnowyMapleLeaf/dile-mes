﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Dashboard.Statistics {
    /// <summary>
    /// AOI设备良率
    /// </summary>
    [SugarTable("statistics_aoiyieldrate", "统计-AOI设备良率")]
    public class AOIYieldRateStatisticsEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        [SugarColumn(ColumnDescription = "设备名称")]
        public string EquipmentName { set; get; }
        /// <summary>
        /// 订单编号
        /// </summary>
        [SugarColumn(ColumnDescription = "订单编号")]
        public string? OrderCode { set; get; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [SugarColumn(ColumnDescription = "产品名称")]
        public string? ProductName { set; get; }
        /// <summary>
        /// 检验日期
        /// </summary>
        [SugarColumn(ColumnDescription = "检验日期")]
        public DateTime? CheckDate { set; get; }
        /// <summary>
        /// 起始NG总数
        /// </summary>
        [SugarColumn(ColumnDescription = "起始NG总数")]
        public int StartCheckNGNo { set; get; }
        /// <summary>
        /// 结算NG总数
        /// </summary>
        [SugarColumn(ColumnDescription = "起始NG总数")]
        public int? EndCheckNGNo { set; get; }

        /// <summary>
        /// 起始检验总数
        /// </summary>
        [SugarColumn(ColumnDescription = "起始检验总数")]
        public int StartCheckTimes { set; get; }
        /// <summary>
        /// 结算检验总数
        /// </summary>
        [SugarColumn(ColumnDescription = "结算检验总数")]
        public int? EndCheckTimes { set; get; }
    }

}
