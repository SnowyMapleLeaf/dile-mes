﻿namespace DiLe.Mes.Model.Cloud.Dashboard.Statistics {
    /// <summary>
    /// 报警信息
    /// </summary>
    [SugarTable("statistics_alarminfo", "报警信息")]
    public class AlarmInfoStatisticsEntity : DataEntityBase {
        /// <summary>
        /// 报警日期
        /// </summary>
        [SugarColumn(ColumnDescription = "报警日期")]
        public string AbnormalDate { set; get; }
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        [SugarColumn(ColumnDescription = "设备名称")]
        public string EquipmentName { set; get; }
        /// <summary>
        /// 报警开始时间
        /// </summary>
        [SugarColumn(ColumnDescription = "报警开始时间")]
        public DateTime StartAbnormalTime { set; get; }
        /// <summary>
        /// 报警时间
        /// </summary>
        [SugarColumn(ColumnDescription = "报警结束时间")]
        public DateTime? EndAbnormalTime { set; get; }
        /// <summary>
        /// 报警信息
        /// </summary>
        [SugarColumn(ColumnDescription = "报警信息")]
        public string? AbnormalInfo { set; get; }
        /// <summary>
        /// 报警内容
        /// </summary>
        [SugarColumn(ColumnDescription = "报警内容")]
        public string? AbnormalContent { set; get; }
        /// <summary>
        /// 异常类别
        /// </summary>
        [SugarColumn(ColumnDescription = "异常类别")]
        public string? AbnormalCategory { get; set; }
        /// <summary>
        /// 异常级别
        /// </summary>
        [SugarColumn(ColumnDescription = "异常级别")]
        public int AbnormalLevel { get; set; } = 0;
        /// <summary>
        /// 提醒内容
        /// </summary>
        [SugarColumn(ColumnDescription = "提醒内容")]
        public string? RemindInfo { get; set; }
    }
}
