﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Dashboard.Statistics {
    /// <summary>
    /// 设备产出
    /// </summary>
    [SugarTable("statistics_equipmentoutput", "统计-设备产出")]
    public class EquipmentOutputStatisticsEntity : DataEntityBase {
        /// <summary>
        /// 日期
        /// </summary>
        [SugarColumn(ColumnDescription = "日期")]
        public DateTime? Date { set; get; }
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { set; get; }
        /// <summary>
        /// 设备名称
        /// </summary>
        [SugarColumn(ColumnDescription = "设备名称")]
        public string EquipmentName { set; get; }
        /// <summary>
        /// 订单编号
        /// </summary>
        [SugarColumn(ColumnDescription = "订单编号")]
        public string? OrderCode { set; get; }
        /// <summary>
        /// 起始数量
        /// </summary>
        [SugarColumn(ColumnDescription = "起始数量")]
        public int? StartNumber { set; get; }
        /// <summary>
        /// 结算数量
        /// </summary>
        [SugarColumn(ColumnDescription = "结算数量")]
        public int? EndNumber { set; get; }
        /// <summary>
        /// 起算模次
        /// </summary>
        [SugarColumn(ColumnDescription = "起算模次")]
        public int? StartMouldNumber { set; get; }
        /// <summary>
        /// 结算模次
        /// </summary>
        [SugarColumn(ColumnDescription = "结算模次")]
        public int? EndMouldNumber { set; get; }

        /// <summary>
        /// 模具编号
        /// </summary>
        [SugarColumn(ColumnDescription = "模具编号")]
        public string? MouldCode { set; get; }
        /// <summary>
        /// 模具穴数
        /// </summary>
        [SugarColumn(ColumnDescription = "模具穴数")]
        public int MouldCavityNum { get; set; } = 0;

        /// <summary>
        /// 起算电能
        /// </summary>
        [SugarColumn(ColumnDescription = "起算电能")]
        public int? StartEnergy { set; get; }
        /// <summary>
        /// 结算电能
        /// </summary>
        [SugarColumn(ColumnDescription = "结算电能")]
        public int? EndEnergy { set; get; }

        /// <summary>
        /// 起始检测NG数
        /// </summary>
        [SugarColumn(ColumnDescription = "起始检测NG数")]
        public int? StartCheckNG { set; get; }
        /// <summary>
        /// 结算检测NG数
        /// </summary>
        [SugarColumn(ColumnDescription = "结算检测NG数")]
        public int? EndCheckNG { set; get; }

    }

}
