﻿using MapleLeaf.DataBase.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Dashboard.Entity {

    /// <summary>
    /// 包装訂單Vs出貨數量
    /// </summary>
    [SugarTable("dashboard")]
    public class DashboardEntity : AbstractBaseEntity {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 月份
        /// </summary>
        [SugarColumn(ColumnDescription = "月份")]
        public string Month { get; set; }
        /// <summary>
        /// 订单目标数量
        /// </summary>
        [SugarColumn(ColumnDescription = "订单目标数量")]
        public int OraderTargetQuantity { get; set; }
        /// <summary>
        /// 实际出货数量
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public int ActualQuantity { get; set; }
        /// <summary>
        /// 累计差异数量
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public int? CumulativeDiffQuantity { get; set; } = 0;


        /// <summary>
        /// 累计订单数量
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public int CumulativeOrderQuantity { get; set; }
        /// <summary>
        /// 累计出货数量
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public int CumulativeShipmentQuantity { get; set; }

        /// <summary>
        /// 目標值
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public double TargetQuantity { get; set; }
        /// <summary>
        /// 達成率
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public double CompletionRate { get; set; }

        /// <summary>
        /// 直通率目标
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public double ThroughRateTarget { get; set; }
        /// <summary>
        /// 实绩
        /// </summary>
        [SugarColumn(ColumnDescription = "实际出货数量")]
        public double ActualPerformance { get; set; }

        /// <summary>
        /// 工厂名称
        /// </summary>
        [SugarColumn(ColumnDescription = "工厂名称")]
        public string FactoryName { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [SugarColumn(ColumnDescription = "产品名称")]
        public string? ProductName { get; set; }



        /// <summary>
        /// AOI订单出货目标数量
        /// </summary>
        [SugarColumn(ColumnDescription = "订单出货目标数量")]
        public int? OraderTargetQuantityA { get; set; }
        /// <summary>
        /// 堆疊套袋機 订单出货目标数量
        /// </summary>
        [SugarColumn(ColumnDescription = "订单出货目标数量")]
        public int? OraderTargetQuantityB { get; set; }
    }
}
