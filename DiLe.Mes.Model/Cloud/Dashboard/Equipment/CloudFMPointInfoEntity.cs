﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Dashboard.Equipment {
    /// <summary>
    /// PLC点位数据传输（流量计）
    /// </summary>
    [SugarTable("dashboard_fmpointinfo", "PLC点位数据传输（流量计）")]
    public class CloudFMPointInfoEntity : EquipmentDashboard {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 设备标识符
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识符")]
        public string DeviceBrand { set; get; }
        /// <summary>
        /// 报警信息
        /// </summary>
        [SugarColumn(ColumnDescription = "报警信息")]
        public string AlarmInfo { set; get; }
        /// <summary>
        /// 能耗累计
        /// </summary>
        [SugarColumn(ColumnDescription = "能耗累计")]
        public string FlowTotal { set; get; }
    }
}
