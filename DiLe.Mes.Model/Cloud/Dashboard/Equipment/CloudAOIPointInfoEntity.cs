﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Dashboard.Equipment {
    /// <summary>
    /// PLC点位数据传输（AOI CCD）
    /// </summary>
    [SugarTable("dashboard_aoipointinfo", "PLC点位数据传输（AOI CCD）")]
    public class CloudAOIPointInfoEntity : EquipmentDashboard {
        /// <summary>
        /// 设备标识
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string? DeviceNo { set; get; }
        /// <summary>
        /// 设备标识符
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识符")]
        public string DeviceBrand { set; get; }
        /// <summary>
        /// 机台编号
        /// </summary>
        [SugarColumn(ColumnDescription = "机台编号")]
        public string MachineNo { set; get; }
        /// <summary>
        /// 检测NG数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测NG数")]
        public string CheckNGNo { set; get; }
        /// <summary>
        /// 检测待定数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测待定数")]
        public string CheckPendTimes { set; get; }
        /// <summary>
        /// 检测累计总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测累计总数")]
        public string CheckTotalTimes { set; get; }
        /// <summary>
        /// 检测总数
        /// </summary>
        [SugarColumn(ColumnDescription = "检测总数")]
        public string CheckTimes { set; get; }
    }
}
