﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Cloud.Dashboard.Equipment {
    /// <summary>
    /// PLC点位数据传输（切边机）
    /// </summary>
    [SugarTable("dashboard_trimpointinfo", "PLC点位数据传输（切边机）")]
    public class CloudTrimPointInfoEntity : PointBaseInfo {
        /// <summary>
        /// 切边压力
        /// </summary>
        [SugarColumn(ColumnDescription = "切边压力")]
        public string? TrimmingPressure { set; get; }
        /// <summary>
        /// 切边次数
        /// </summary>
        [SugarColumn(ColumnDescription = "切边次数")]
        public string? TrimingTimes { set; get; }
        /// <summary>
        /// 设备启用后切边模总工作次数
        /// </summary>
        [SugarColumn(ColumnDescription = "设备启用后切边模总工作次数")]
        public string? TrimingTotalTimes { set; get; }
    }
}
