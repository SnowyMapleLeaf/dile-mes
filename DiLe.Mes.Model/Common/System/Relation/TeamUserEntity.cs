﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Relation {
    /// <summary>
    /// 班组成员信息
    /// </summary>
    [SugarTable("sys_temp_user", "班组成员信息")]
    public class TeamUserEntity : AbstractBaseEntity {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnDescription = "用户主键")]
        public long UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnDescription = "班组主键")]
        public long TeamId { get; set; }
    }
}