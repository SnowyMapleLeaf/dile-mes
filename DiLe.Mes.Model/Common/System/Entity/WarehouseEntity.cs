﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("sys_warehouse", "仓库管理")]
    public class WarehouseEntity : DataEntityBase {
        /// <summary>
        /// 代号
        /// </summary>
        [SugarColumn(ColumnDescription = "代号")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        [SugarColumn(ColumnDescription = "部门")]
        public long? DepartmentId { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [SugarColumn(ColumnDescription = "负责人")]
        public long Manager { get; set; } = 0;
        /// <summary>
        /// 联系方式
        /// </summary>
        [SugarColumn(ColumnDescription = "联系方式")]
        public string? Contact { get; set; }
        /// <summary>
        /// 上级
        /// </summary>
        [SugarColumn(ColumnDescription = "上级")]
        public long? ParentId { get; set; } = 0;
    }
}