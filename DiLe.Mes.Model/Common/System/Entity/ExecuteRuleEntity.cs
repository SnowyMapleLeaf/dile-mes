﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 执行规则
    /// </summary>
    [SugarTable("sys_executerule", "执行规则")]
    public class ExecuteRuleEntity : DataEntityBase {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 规则表达式
        /// </summary>
        [SugarColumn(ColumnDescription = "规则表达式")]
        public string RuleExpression { get; set; }
        /// <summary>
        /// 规则描述
        /// </summary>
        [SugarColumn(ColumnDescription = "规则描述")]
        public string RuleDescription { get; set; }

    }
}