﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 计量单位
    /// </summary>
    [SugarTable("sys_measureunit", "计量单位")]
    public class MeasureUnitEntity : AbstractBaseEntity {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 简称
        /// </summary>
        [SugarColumn(ColumnDescription = "简称")]
        public string ShortName { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 类型 
        /// </summary>
        [SugarColumn(ColumnDescription = "类型")]
        public string Type { get; set; }
        /// <summary>
        /// 换算单位
        /// </summary>
        [SugarColumn(ColumnDescription = "换算单位")]
        public string? ConversionUnit { get; set; }
        /// <summary>
        /// 换算比例
        /// </summary>
        [SugarColumn(ColumnDescription = "换算比例")]
        public string? ConversionProportion { get; set; }
    }
}