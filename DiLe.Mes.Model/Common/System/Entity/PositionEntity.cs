﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 存放位置
    /// </summary>
    [SugarTable("sys_position", "存放位置")]
    public class PositionEntity : DataEntityBase {
        /// <summary>
        /// 存放位置
        /// </summary>
        [SugarColumn(ColumnDescription = "存放位置")]
        public string Position { get; set; }
        /// <summary>
        /// 所属工厂
        /// </summary>
        [SugarColumn(ColumnDescription = "所属工厂")]
        public long? Factory { get; set; }
        /// <summary>
        /// 所属车间
        /// </summary>
        [SugarColumn(ColumnDescription = "所属车间")]
        public string Workshop { get; set; }
        /// <summary>
        /// 所属客户
        /// </summary>
        [SugarColumn(ColumnDescription = "所属客户")]
        public long? Customer { get; set; } = 0;
    }
}