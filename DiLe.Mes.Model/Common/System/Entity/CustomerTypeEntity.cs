﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 客户类型
    /// </summary>
    [SugarTable("sys_customertype", "客户类型")]
    public class CustomerTypeEntity : AbstractBaseEntity {
        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 类型说明
        /// </summary>
        [SugarColumn(ColumnDescription = "类型说明")]
        public string? TypeDesc { get; set; }
    }
}