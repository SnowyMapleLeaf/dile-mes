﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("sys_team", "班组管理")]
    public class TeamEntity : DataEntityBase {
        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [SugarColumn(ColumnDescription = "类型")]
        public string TeamType { get; set; }
        /// <summary>
        /// 成员
        /// </summary>
        [SugarColumn(ColumnDescription = "成员")]
        public string? Members { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [SugarColumn(ColumnDescription = "负责人")]
        public long Manager { get; set; } = 0;
    }
}