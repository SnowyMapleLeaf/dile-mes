﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 供应商
    /// </summary>
    [SugarTable("sys_supplier", "供应商")]
    public class SupplierEntity : DataEntityBase {
        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 简称
        /// </summary>
        [SugarColumn(ColumnDescription = "简称")]
        public string? ShortName { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [SugarColumn(ColumnDescription = "类型")]
        public string TypeName { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        [SugarColumn(ColumnDescription = "联系人")]
        public long? Contact { get; set; } = 0;
        /// <summary>
        /// 联系电话
        /// </summary>
        [SugarColumn(ColumnDescription = "联系电话")]
        public string? Telephone { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [SugarColumn(ColumnDescription = "邮箱")]
        public string? EmailAddress { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [SugarColumn(ColumnDescription = "地址")]
        public string? Address { get; set; }
    }
}