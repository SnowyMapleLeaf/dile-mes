﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.System.Entity {
    /// <summary>
    /// 区域
    /// </summary>
    [SugarTable("sys_region", "区域")]
    public class RegionEntity : AbstractBaseEntity {
        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [SugarColumn(ColumnDescription = "负责人")]
        public long? Manager { get; set; } = 0;
        /// <summary>
        /// 联系电话
        /// </summary>
        [SugarColumn(ColumnDescription = "联系电话")]
        public string? Telephone { get; set; }
        /// <summary>
        /// 父级
        /// </summary>
        [SugarColumn(ColumnDescription = "父级")]
        public long? ParentId { get; set; } = 0;
    }
}