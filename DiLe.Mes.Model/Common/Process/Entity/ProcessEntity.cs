﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Process.Entity
{
    /// <summary>
    /// 工序
    /// </summary>
    [SugarTable("sys_process", "工序")]
    public class ProcessEntity : DataEntityBase {
        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [SugarColumn(ColumnDescription = "类型")]
        public string TypeName { get; set; }

        /// <summary>
        /// 待工时间
        /// </summary>
        [SugarColumn(ColumnDescription = "待工时间")]
        public DateTime? WaitingTime { get; set; }
        /// <summary>
        /// 标准工时
        /// </summary>
        [SugarColumn(ColumnDescription = "标准工时")]
        public string? StandardWorkingHour { get; set; }
        /// <summary>
        /// 报工方式
        /// </summary>
        [SugarColumn(ColumnDescription = "报工方式")]
        public string? WorkReportingMode { get; set; }
        /// <summary>
        /// 检验标准
        /// </summary>
        [SugarColumn(ColumnDescription = "检验标准")]
        public string? InspectionStandard { get; set; }
    }
}