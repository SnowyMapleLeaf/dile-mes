﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.Mould.Relation.SparePart
{
    /// <summary>
    /// 备件关联模具
    /// </summary>
    [SugarTable("mould_sparepart2mouldinfo", "备件关联模具")]
    public class MouldSparePart2MouldInfoEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 备件id
        /// </summary>
        public long SparePartId { get; set; } = 0;
        /// <summary>
        /// 模具id
        /// </summary>
        public long MouldInfoId { get; set; } = 0;
        /// <summary>
        /// 模具部位
        /// </summary>
        public string? MouldPart { get; set; }
    }
}
