﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Relation.Maintenance
{
    /// <summary>
    /// 保养记录和备件
    /// </summary>
    [SugarTable("mould_maintenancerecord2sparepart", "保养记录和备件")]
    public class MouldMaintenanceRecord2SparePartEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 保养记录
        /// </summary>
        [SugarColumn(ColumnDescription = "保养记录")]
        public long MaintenanceRecordId { get; set; } = 0;
        /// <summary>
        /// 备件
        /// </summary>
        [SugarColumn(ColumnDescription = "备件")]
        public long SparePartId { get; set; } = 0;
        /// <summary>
        /// 领用数量
        /// </summary>
        [SugarColumn(ColumnDescription = "领用数量")]
        public int? DeliveryNumber { get; set; } = 0;
    }
}