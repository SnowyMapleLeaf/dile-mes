﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Relation.Repair
{
    /// <summary>
    /// 维修记录和备件
    /// </summary>
    [SugarTable("mould_repairrecord2sparepart", "维修记录和备件")]
    public class MouldRepairRecord2SparePartEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 维修记录
        /// </summary>
        public long RepairRecordId { get; set; }
        /// <summary>
        /// 备件
        /// </summary>
        public long SparePartId { get; set; }
        /// <summary>
        /// 领用数量
        /// </summary>
        public int RecipientNumber { get; set; }
    }

}
