﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.Maintenance
{
    /// <summary>
    /// 保养项目
    /// </summary>
    [SugarTable("mould_maintenanceproject", "保养项目")]
    public class MouldMaintenanceProjectEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 保养类型
        /// </summary>
        public long MaintenanceType { get; set; } = 0;
        /// <summary>
        /// 保养部位
        /// </summary>
        public string MaintenancePart { get; set; }
        /// <summary>
        /// 保养要求
        /// </summary>
        public string MaintenanceDemand { get; set; }
        /// <summary>
        /// 保养级别
        /// </summary>
        public long MaintenanceLevel { get; set; } = 0;
    }
}