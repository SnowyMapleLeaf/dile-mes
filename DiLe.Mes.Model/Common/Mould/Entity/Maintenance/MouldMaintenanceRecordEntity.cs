﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.Maintenance
{
    /// <summary>
    /// 保养记录
    /// </summary>
    [SugarTable("mould_maintenancerecord", "保养记录")]
    public class MouldMaintenanceRecordEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 模具主键
        /// </summary>
        public long MouldId { get; set; } = 0;
        /// <summary>
        /// 保养任务单号
        /// </summary>
        [SugarColumn(ColumnDescription = "保养任务单号")]
        public string Code { set; get; }
        /// <summary>
        /// 保养任务主题
        /// </summary>
        [SugarColumn(ColumnDescription = "保养任务主题")]
        public string Name { set; get; }
        /// <summary>
        /// 紧急程度
        /// </summary>
        [SugarColumn(ColumnDescription = "紧急程度")]
        public string? UrgencyLevel { set; get; }
        /// <summary>
        /// 是否停机
        /// </summary>
        [SugarColumn(ColumnDescription = "是否停机")]
        public bool IsStop { set; get; } = false;
        /// <summary>
        /// 计划主键
        /// </summary>
        public long MaintenancePlanId { get; set; } = 0;


        #region 更换备件
        /// <summary>
        /// 是否更换备件
        /// </summary>
        [SugarColumn(ColumnDescription = "是否更换备件")]
        public bool IsReplace { set; get; } = false;
        /// <summary>
        /// 领用单号
        /// </summary>
        [SugarColumn(ColumnDescription = "领用单号")]
        public string? DeliveryNoteCode { set; get; }
        /// <summary>
        /// 领用主题
        /// </summary>
        [SugarColumn(ColumnDescription = "领用主题")]
        public string? DeliveryName { set; get; }
        /// <summary>
        /// 仓库
        /// </summary>
        [SugarColumn(ColumnDescription = "仓库")]
        public long WarehouseId { get; set; } = 0;
        /// <summary>
        /// 领用原因
        /// </summary>
        [SugarColumn(ColumnDescription = "领用原因")]
        public string? DeliveryReason { set; get; }
        /// <summary>
        /// 领用时间
        /// </summary>
        [SugarColumn(ColumnDescription = "领用时间")]
        public DateTime? DeliveryDate { set; get; }
        #endregion
    }
}