﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.SparePart
{
    /// <summary>
    /// 备件类型
    /// </summary>
    [SugarTable("mould_spareparttype", "备件类型")]
    public class MouldSparePartTypeEntity : DataEntityBase {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 上级
        /// </summary>
        [SugarColumn(ColumnDescription = "上级")]
        public long? ParentId { get; set; } = 0;
    }
}
