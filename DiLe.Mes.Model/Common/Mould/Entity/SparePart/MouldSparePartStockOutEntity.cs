﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.SparePart
{
    /// <summary>
    /// 备件出库
    /// </summary>
    [SugarTable("mould_sparepartstockout", "出库")]
    public class MouldSparePartStockOutEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 出库主题
        /// </summary>
        [SugarColumn(ColumnDescription = "出库主题")]
        public string Name { get; set; }
        /// <summary>
        /// 出库编号
        /// </summary>
        [SugarColumn(ColumnDescription = "出库编号")]
        public string Code { get; set; }
        /// <summary>
        /// 出库类型
        /// </summary>
        [SugarColumn(ColumnDescription = "出库类型")]
        public string Type { get; set; }
        /// <summary>
        /// 出库日期
        /// </summary>
        [SugarColumn(ColumnDescription = "规格")]
        public DateTime? StockOutDateTime { get; set; }
        /// <summary>
        /// 出库申请人
        /// </summary>
        [SugarColumn(ColumnDescription = "申请人")]
        public long Applicant { get; set; } = 0;
        /// <summary>
        /// 仓库
        /// </summary>
        [SugarColumn(ColumnDescription = "仓库")]
        public long WarehouseId { get; set; } = 0;
        /// <summary>
        /// 出库人员
        /// </summary>
        [SugarColumn(ColumnDescription = "出库人员")]
        public long StockOutUserId { get; set; } = 0;
    }
}
