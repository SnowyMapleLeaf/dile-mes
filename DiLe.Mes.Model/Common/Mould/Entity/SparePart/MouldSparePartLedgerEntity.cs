﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.Mould.Entity.SparePart
{
    /// <summary>
    /// 备件台账
    /// </summary>
    [SugarTable("mould_sparepartledger", "备件台账")]
    public class MouldSparePartLedgerEntity : AbstractBaseEntity
    {
        #region 备件信息
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        [SugarColumn(ColumnDescription = "规格")]
        public string Specification { get; set; }

        /// <summary>
        /// 备件类型
        /// </summary>
        [SugarColumn(ColumnDescription = "备件类型")]
        public long TypeId { get; set; } = 0;

        /// <summary>
        /// 备件单位(计量单位)
        /// </summary>
        [SugarColumn(ColumnDescription = "备件单位")]
        public long Unit { get; set; } = 0;

        /// <summary>
        /// 生产厂商
        /// </summary>
        [SugarColumn(ColumnDescription = "生产厂商")]
        public string Manufacturer { get; set; }
        #endregion
        #region 库存信息
        /// <summary>
        /// 库存上限
        /// </summary>
        [SugarColumn(ColumnDescription = "库存上限")]
        public int InventoryUpperLimit { get; set; }
        /// <summary>
        /// 库存下限
        /// </summary>
        [SugarColumn(ColumnDescription = "库存下限")]
        public int InventoryLowerLimit { get; set; }
        /// <summary>
        /// 更换周期
        /// </summary>
        [SugarColumn(ColumnDescription = "更换周期")]
        public int ReplacementCycle { get; set; }
        /// <summary>
        /// 更换周期单位
        /// </summary>
        [SugarColumn(ColumnDescription = "更换周期单位")]
        public string ReplacementCycleUnit { get; set; }
       
        #endregion
    }
}
