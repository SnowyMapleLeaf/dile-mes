﻿using MapleLeaf.Core.Entity;
using MapleLeaf.Core.Extension;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.Repair {
    /// <summary>
    /// 维修记录
    /// </summary>
    [SugarTable("mould_repairrecord", "维修记录")]
    public class MouldRepairRecordEntity : AbstractBaseEntity {
        #region 模具
        /// <summary>
        /// 模具主键
        /// </summary>
        public long MouldId { get; set; } = 0;
        #endregion

        #region 故障信息
        /// <summary>
        /// 故障时间
        /// </summary>
        public DateTime? FaultDate { get; set; }
        /// <summary>
        /// 紧急程度
        /// </summary>
        public string DegreeUrgency { get; set; }
        /// <summary>
        /// 故障类型
        /// </summary>
        public long? FaultType { get; set; }
        /// <summary>
        /// 是否停机
        /// </summary>
        public bool IsShutdown { get; set; }
        /// <summary>
        /// 故障描述
        /// </summary>
        public string FaultDesc { get; set; }
        #endregion
        #region 维修信息
        /// <summary>
        /// 工单主题
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 工单单号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 维修等级
        /// </summary>
        public string Level { get; set; }
        /// <summary>
        /// 维修班组
        /// </summary>
        public long? TeamId { get; set; }
        /// <summary>
        /// 维修负责人
        /// </summary>
        public long? Manager { get; set; }
        /// <summary>
        /// 维修开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 维修结束时间
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 维修总时长
        /// </summary>
        [SugarColumn(ColumnDescription = "维修总时长")]
        public double TotalDuration { get; set; } = 0;

        /// <summary>
        /// 其他维修人员
        /// </summary>
        public string? Personnels { get; set; }
        /// <summary>
        /// 其他维修人员
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public List<long> PersonnelList {
            get {
                var list = new List<long>();
                if (!Personnels.IsNullOrEmpty()) {
                    var arr = Personnels!.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var item in arr) {
                        list.Add(item.ToLong());
                    }
                }
                return list;
            }
        }
        /// <summary>
        /// 工单状态
        /// </summary>
        public string? WorkOrderStatus { get; set; }
        #endregion
        #region 更换备件
        /// <summary>
        /// 是否更换备件
        /// </summary>
        public bool IsReplace { get; set; }
        /// <summary>
        /// 领用单号
        /// </summary>
        public string RecipientCode { get; set; }
        /// <summary>
        /// 领用主题
        /// </summary>
        public string? RecipientName { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long? WarehouseId { get; set; }
        /// <summary>
        /// 领用原因
        /// </summary>
        public string? RecipientReason { get; set; }
        /// <summary>
        /// 领用时间
        /// </summary>
        public DateTime? RecipientDate { get; set; }
        #endregion
    }

}
