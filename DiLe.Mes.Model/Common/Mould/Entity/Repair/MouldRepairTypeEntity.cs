﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.Mould.Entity.Repair {
    /// <summary>
    /// 故障类型
    /// </summary>
    [SugarTable("mould_repairtype", "故障类型")]
    public class MouldRepairTypeEntity : DataEntityBase {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long? ParentId { get; set; } = 0;
    }

}
