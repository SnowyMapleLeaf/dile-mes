﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.Info {
    /// <summary>
    /// 模具类型
    /// </summary>
    [SugarTable("mould_type", "模具类型")]
    public class MouldTypeEntity : DataEntityBase {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 上级
        /// </summary>
        [SugarColumn(ColumnDescription = "上级")]
        public long? ParentId { get; set; } = 0;
    }
}
