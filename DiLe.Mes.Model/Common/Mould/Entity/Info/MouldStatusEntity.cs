﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.Info
{
    /// <summary>
    /// 模具状态
    /// </summary>
    [SugarTable("mould_status", "模具状态")]
    public class MouldStatusEntity : DataEntityBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 状态说明
        /// </summary>
        [SugarColumn(ColumnDescription = "状态说明")]
        public string? Description { get; set; }
    }
}
