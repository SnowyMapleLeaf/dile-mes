﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Mould.Entity.Info {
    /// <summary>
    /// 模具档案
    /// </summary>
    [SugarTable("mould_info", "模具档案")]
    public class MouldInfoEntity : AbstractBaseEntity {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }
        /// <summary>
        /// 模组编码
        /// </summary>
        [SugarColumn(ColumnDescription = "模组编码")]
        public string MouldGroupCode { get; set; }
        /// <summary>
        /// 模具寿命
        /// </summary>
        [SugarColumn(ColumnDescription = "模具寿命")]
        public int MouldLife { get; set; } = 0;
        /// <summary>
        /// 模具类型
        /// </summary>
        [SugarColumn(ColumnDescription = "模具类型")]
        public long TypeId { get; set; } = 0;

        /// <summary>
        /// 当前状态
        /// </summary>
        [SugarColumn(ColumnDescription = "当前状态")]
        public long StatusId { get; set; } = 0;
        /// <summary>
        /// 规格
        /// </summary>
        [SugarColumn(ColumnDescription = "规格")]
        public string? Specification { get; set; }
        /// <summary>
        /// 所在工厂
        /// </summary>
        [SugarColumn(ColumnDescription = "所在工厂")]
        public long? FactoryId { get; set; } = 0;
        /// <summary>
        /// 存放位置
        /// </summary>
        [SugarColumn(ColumnDescription = "存放位置")]
        public long PositionId { get; set; } = 0;
        /// <summary>
        /// 接收日期
        /// </summary>
        [SugarColumn(ColumnDescription = "接收日期")]
        public DateTime? ReceivedDate { get; set; }
        /// <summary>
        /// 模具穴数
        /// </summary>
        [SugarColumn(ColumnDescription = "模具穴数")]
        public int CavityNum { get; set; } = 0;
        /// <summary>
        /// 出厂日期
        /// </summary>
        [SugarColumn(ColumnDescription = "出厂日期")]
        public DateTime? ProductionDate { get; set; }
        /// <summary>
        /// 生产厂商
        /// </summary>
        [SugarColumn(ColumnDescription = "生产厂商")]
        public string? Manufacturer { get; set; }
        /// <summary>
        /// 限定次数
        /// </summary>
        [SugarColumn(ColumnDescription = "限定次数")]
        public int LimitedNum { get; set; }=0;
        /// <summary>
        /// 适用设备类型
        /// </summary>
        [SugarColumn(ColumnDescription = "适用设备类型")]
        public long EquipmentTypeId { get; set; } = 0;
        /// <summary>
        /// 模具负责人
        /// </summary>
        [SugarColumn(ColumnDescription = "模具负责人")]
        public long Manager { get; set; } = 0;
        /// <summary>
        /// 联系方式
        /// </summary>
        [SugarColumn(ColumnDescription = "联系方式")]
        public string? Contact { get; set; }
        /// <summary>
        /// 操作员
        /// </summary>
        public long Operator { get; set; } = 0;
        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime OperatingTime { get; set; } = DateTime.Now;
    }
}
