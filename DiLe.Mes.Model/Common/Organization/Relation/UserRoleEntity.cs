﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Organization.Relation {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("sys_user_role", "用户角色信息")]
    public class UserRoleEntity : AbstractBaseEntity {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnDescription = "用户主键")]
        public long UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnDescription = "角色主键")]
        public long RoleId { get; set; }
    }
}