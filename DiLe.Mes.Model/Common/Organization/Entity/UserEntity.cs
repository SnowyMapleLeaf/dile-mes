﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Organization.Entity {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("sys_user", "用户信息")]
    public class UserEntity : DataEntityBase {
        /// <summary>
        /// 账号
        /// </summary>
        [SugarColumn(ColumnDescription = "账号")]
        public string Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [SugarColumn(ColumnDescription = "密码")]
        public string? Password { get; set; } = "888888";
        /// <summary>
        /// 用户名
        /// </summary>
        [SugarColumn(ColumnDescription = "用户名")]
        public string Name { get; set; }
        /// <summary>
        /// 工号
        /// </summary>
        [SugarColumn(ColumnDescription = "工号")]
        public string Code { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [SugarColumn(ColumnDescription = "性别")]
        public string? Sex { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        [SugarColumn(ColumnDescription = "部门")]
        public long DepartmentId { get; set; } = 0;
        /// <summary>
        /// 工作岗位
        /// </summary>
        [SugarColumn(ColumnDescription = "工作岗位")]
        public long JobPostId { get; set; } = 0;
        /// <summary>
        /// 联系电话
        /// </summary>
        [SugarColumn(ColumnDescription = "联系电话")]
        public string? Telephone { get; set; }
        /// <summary>
        /// 邮箱地址
        /// </summary>
        [SugarColumn(ColumnDescription = "邮箱地址")]
        public string? EmailAddress { get; set; }

    }
}