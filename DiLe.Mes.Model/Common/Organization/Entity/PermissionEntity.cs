﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Organization.Entity {
    /// <summary>
    /// 权限管理
    /// </summary>
    [SugarTable("sys_permission", "权限管理")]
    public class PermissionEntity : AbstractBaseEntity {
        /// <summary>
        /// 角色主键
        /// </summary>
        [SugarColumn(ColumnDescription = "角色主键")]
        public long? RoleId { get; set; } = 0;
        /// <summary>
        /// 权限内容
        /// </summary>
        [SugarColumn(ColumnDescription = "权限内容", ColumnDataType = "text")]
        public string PowerSetJson { get; set; }
    }
}