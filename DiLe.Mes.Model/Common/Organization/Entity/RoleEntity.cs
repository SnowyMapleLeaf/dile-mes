﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Organization.Entity {
    /// <summary>
    /// 
    /// </summary>
    [SugarTable("sys_role", "用户角色")]
    public class RoleEntity : DataEntityBase {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
    }
}