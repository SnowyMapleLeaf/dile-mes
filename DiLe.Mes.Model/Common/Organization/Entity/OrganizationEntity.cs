﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Organization.Entity {
    /// <summary>
    /// 组织信息
    /// </summary>
    [SugarTable("sys_organization", "组织信息")]
    public class OrganizationEntity : DataEntityBase {
        /// <summary>
        /// 部门名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 上级部门
        /// </summary>
        [SugarColumn(ColumnDescription = "上级部门")]
        public long ParentId { get; set; } = 0;
        /// <summary>
        /// 部门主管
        /// </summary>
        [SugarColumn(ColumnDescription = "部门主管")]
        public long Manager { get; set; } = 0;
        /// <summary>
        /// 组织类型:集团-Group 部门-Department 工厂-Factory
        /// </summary>
        [SugarColumn(ColumnDescription = "组织类型:集团-Group 部门-Department 工厂-Factory")]
        public string Type { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(ColumnDescription = "排序")]
        public int? OrderNumber { get; set; } = 0;
    }
}