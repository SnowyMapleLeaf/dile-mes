﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Organization.Entity {
    /// <summary>
    /// 工作岗位信息
    /// </summary>
    [SugarTable("sys_jobpost", "工作岗位信息")]
    public class JobPostEntity : DataEntityBase {
        /// <summary>
        /// 岗位名称
        /// </summary>
        [SugarColumn(ColumnDescription = "岗位名称")]
        public string Name { get; set; }
        /// <summary>
        /// 岗位编码
        /// </summary>
        [SugarColumn(ColumnDescription = "岗位编码")]
        public string Code { get; set; }
    }
}