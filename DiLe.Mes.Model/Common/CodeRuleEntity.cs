﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common {
    /// <summary>
    /// 编码规则
    /// </summary>
    [SugarTable("sys_coderule", "编码规则")]
    public class CodeRuleEntity : AbstractBaseEntity {
        /// <summary>
        /// 前缀
        /// </summary>
        [SugarColumn(ColumnDescription = "前缀")]
        public string Prefix { get; set; }
        /// <summary>
        /// 流水码
        /// </summary>
        [SugarColumn(ColumnDescription = "流水码")]
        public int SerialCode { get; set; }
        /// <summary>
        /// 位数
        /// </summary>
        [SugarColumn(ColumnDescription = "位数")]
        public int DigitCapacity { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [SugarColumn(ColumnDescription = "描述")]
        public string? Description { get; set; }
    }
}