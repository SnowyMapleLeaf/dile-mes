﻿using MapleLeaf.Core.Entity;
using MapleLeaf.Core.Extension;
using Newtonsoft.Json;
using Serilog;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Entity.Maintenance {
    /// <summary>
    /// 保养计划
    /// </summary>
    [SugarTable("bus_maintenanceplan", "保养计划")]
    public class MaintenancePlanEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备主键
        /// </summary>
        public long? EquipmentId { get; set; } = 0;
        /// <summary>
        /// 设备类型主键
        /// </summary>
        public long? EquipmentTypeId { get; set; } = 0;
        /// <summary>
        /// 保养类型 0：设备保养  1：设备类型保养
        /// </summary>
        [SugarColumn(ColumnDescription = "保养类型")]
        public int MaintenanceType { get; set; } = 0;

        /// <summary>
        /// 计划编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 计划名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 计划类型
        /// </summary>
        public string PlanType { get; set; }
        /// <summary>
        /// 执行规则主键
        /// </summary>
        public long ExecuteRuleId { get; set; } = 0;

        /// <summary>
        /// 保养班组
        /// </summary>
        public long TeamId { get; set; } = 0;
        /// <summary>
        /// 保养负责人
        /// </summary>
        public long Manager { get; set; } = 0;
        /// <summary>
        /// 其他人员
        /// </summary>
        public string OtherUser { get; set; }

        /// <summary>
        /// 其他人员
        /// </summary>
        [SugarColumn(IsIgnore = true), JsonIgnore]
        public List<long> OtherUserIds {
            get {
                var list = new List<long>();
                if (!OtherUser.IsNullOrEmpty()) {
                    var arr = OtherUser.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var item in arr) {
                        list.Add(item.ToLong());
                    }
                }
                return list;
            }
        }

    }
}