﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Entity.Maintenance {
    /// <summary>
    /// 保养记录
    /// </summary>
    [SugarTable("bus_maintenancerecord", "保养记录")]
    public class MaintenanceRecordEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备主键
        /// </summary>
        public long EquipmentId { get; set; } = 0;
        /// <summary>
        /// 保养任务单号
        /// </summary>
        [SugarColumn(ColumnDescription = "保养任务单号")]
        public string? Code { set; get; }
        /// <summary>
        /// 保养任务主题
        /// </summary>
        [SugarColumn(ColumnDescription = "保养任务主题")]
        public string Name { set; get; }
        /// <summary>
        /// 紧急程度
        /// </summary>
        [SugarColumn(ColumnDescription = "紧急程度")]
        public string? UrgencyLevel { set; get; }
        /// <summary>
        /// 是否停机
        /// </summary>
        [SugarColumn(ColumnDescription = "是否停机")]
        public bool IsStop { set; get; } = false;
        /// <summary>
        /// 保养计划主键
        /// </summary>
        public long MaintenancePlanId { get; set; } = 0;


        #region 更换备件
        /// <summary>
        /// 是否更换备件
        /// </summary>
        [SugarColumn(ColumnDescription = "是否更换备件")]
        public bool IsReplace { set; get; } = false;
        /// <summary>
        /// 
        /// </summary>
        public long SparePartStockOutId { set; get; }
        #endregion
    }
}