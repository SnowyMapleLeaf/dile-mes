﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.Equipment.Entity.SparePart {
    /// <summary>
    /// 备件入库
    /// </summary>
    [SugarTable("bus_sparepartstockin", "入库")]
    public class SparePartStockInEntity : AbstractBaseEntity {
        /// <summary>
        /// 入库主题
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        ///  入库编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 入库类型
        /// </summary>
        [SugarColumn(ColumnDescription = "入库类型")]
        public string? TypeName { get; set; }
        /// <summary>
        ///  备件
        /// </summary>
        public long SparePartId { get; set; }
        /// <summary>
        /// 入库数量
        /// </summary>
        public int StockInNumber { get; set; }
        /// <summary>
        /// 位置编码
        /// </summary>
        public string? PositionCode { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public long WarehouseId { get; set; } = 0;


    }
}
