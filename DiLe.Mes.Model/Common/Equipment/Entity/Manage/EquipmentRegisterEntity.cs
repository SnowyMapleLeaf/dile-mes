﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Entity.Manage {
    /// <summary>
    /// 设备注册
    /// </summary>
    [SugarTable("bus_equipmentregister", "设备注册")]
    public class EquipmentRegisterEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备
        /// </summary>
        [SugarColumn(ColumnDescription = "设备")]
        public long EquipmentInfoId { get; set; } = 0;
        /// <summary>
        /// 注册编码
        /// </summary>
        [SugarColumn(ColumnDescription = "注册编码")]
        public string? Code { get; set; }
        /// <summary>
        /// PLC地址
        /// </summary>
        [SugarColumn(ColumnDescription = "PLC地址")]
        public string PLCAddress { get; set; }
        /// <summary>
        /// 工控机地址
        /// </summary>
        [SugarColumn(ColumnDescription = "工控机地址")]
        public string IPCAddress { get; set; }
        /// <summary>
        /// 备用工控机地址
        /// </summary>
        [SugarColumn(ColumnDescription = "备用工控机地址")]
        public string SpareIPCAddress { get; set; }

        /// <summary>
        /// 设备标识
        /// </summary>
        [SugarColumn(ColumnDescription = "设备标识")]
        public string EquipmentSign { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public bool Status { get; set; } = true;


    }
}