﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Entity.Manage {
    /// <summary>
    /// 设备类型
    /// </summary>
    [SugarTable("bus_equipmenttype", "设备类型")]
    public class EquipmentTypeEntity : DataEntityBase {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string? Code { get; set; }
        /// <summary>
        /// 上级
        /// </summary>
        [SugarColumn(ColumnDescription = "上级")]
        public long? ParentId { get; set; } = 0;
        /// <summary>
        /// 标准工时
        /// </summary>
        [SugarColumn(ColumnDescription = "标准工时")]
        public decimal? StandardWorkingHour { get; set; } = 0;
        /// <summary>
        /// 图片
        /// </summary>
        [SugarColumn(ColumnDescription = "图片")]
        public string? FileId { get; set; }
    }
}