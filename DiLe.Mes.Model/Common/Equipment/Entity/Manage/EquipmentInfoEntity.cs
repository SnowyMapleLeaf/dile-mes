﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Entity.Manage {
    /// <summary>
    /// 设备档案
    /// </summary>
    [SugarTable("bus_equipmentinfo", "设备档案")]
    public class EquipmentInfoEntity : DataEntityBase {
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [SugarColumn(ColumnDescription = "编码")]
        public string Code { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        [SugarColumn(ColumnDescription = "规格")]
        public string Specification { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        [SugarColumn(ColumnDescription = "设备类型")]
        public long TypeId { get; set; } = 0;
        /// <summary>
        /// 存储位置
        /// </summary>
        [SugarColumn(ColumnDescription = "存储位置")]
        public long? PositionId { get; set; } = 0;

        /// <summary>
        /// 所属制程
        /// </summary>
        [SugarColumn(ColumnDescription = "所属制程")]
        public long? ProcessId { get; set; }
        /// <summary>
        /// 所属客户
        /// </summary>
        [SugarColumn(ColumnDescription = "所属客户")]
        public long? CustomerId { get; set; } = 0;
        #region 使用情况
        /// <summary>
        /// 当前状态
        /// </summary>
        [SugarColumn(ColumnDescription = "当前状态")]
        public long? CurrentStatus { get; set; } = 0;
        /// <summary>
        /// 使用部门
        /// </summary>
        [SugarColumn(ColumnDescription = "使用部门")]
        public long? DepartmentId { get; set; } = 0;
        /// <summary>
        /// 负责人
        /// </summary>
        [SugarColumn(ColumnDescription = "负责人")]
        public long? Manager { get; set; } = 0;
        /// <summary>
        /// 联系方式
        /// </summary>
        [SugarColumn(ColumnDescription = "联系方式")]
        public string Contact { get; set; }
        /// <summary>
        /// 启用时间
        /// </summary>
        public DateTime? ActivationTime { get; set; }
        /// <summary>
        /// 总功率
        /// </summary>
        public string TotalRower { get; set; }
        #endregion
        //#region  购置信息
        ///// <summary>
        ///// 采购订单
        ///// </summary>
        //public long? PurchaseOrderId { get; set; } = 0;
        ///// <summary>
        ///// 供应商
        ///// </summary>
        //public long? Supplier { get; set; } = 0;
        ///// <summary>
        ///// 采购部门
        ///// </summary>
        //public long? PurchaseDepartmentId { get; set; } = 0;
        ///// <summary>
        ///// 采购人员
        ///// </summary>
        //public long? PurchaseUserId { get; set; } = 0;
        ///// <summary>
        ///// 采购日期
        ///// </summary>
        //public DateTime? PurchaseDate { get; set; }
        ///// <summary>
        ///// 生产厂商
        ///// </summary>
        //public string? Manufacturer { get; set; }
        ///// <summary>
        ///// 使用寿命
        ///// </summary>
        //public int ServiceLife { get; set; } = 0;
        ///// <summary>
        ///// 购买价格
        ///// </summary>
        //public double Price { get; set; } = 0;


        ///// <summary>
        ///// 出厂日期
        ///// </summary>
        //public DateTime? ProductionDate { get; set; }

        //#endregion

        /// <summary>
        /// 日班标准工时
        /// </summary>
        [SugarColumn(ColumnDescription = "日班标准工时")]
        public decimal? DayStandardHour { get; set; }
        /// <summary>
        /// 夜班标准工时
        /// </summary>
        [SugarColumn(ColumnDescription = "夜班标准工时")]
        public decimal? NightStandardHour { get; set; }

  
    }
}