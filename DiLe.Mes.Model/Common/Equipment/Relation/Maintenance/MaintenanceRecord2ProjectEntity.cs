﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Relation.Maintenance
{
    /// <summary>
    /// 保养记录和保养项目
    /// </summary>
    [SugarTable("bus_maintenancerecord2project", "保养记录和保养项目")]
    public class MaintenanceRecord2ProjectEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 保养记录
        /// </summary>
        [SugarColumn(ColumnDescription = "保养记录")]
        public long MaintenanceRecordId { get; set; } = 0;
        /// <summary>
        /// 保养项目
        /// </summary>
        [SugarColumn(ColumnDescription = "保养项目")]
        public long MaintenanceProjectId { get; set; } = 0;
    }
}