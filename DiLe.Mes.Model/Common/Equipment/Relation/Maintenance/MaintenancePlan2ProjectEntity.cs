﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Relation.Maintenance
{
    /// <summary>
    /// 保养计划和保养项目
    /// </summary>
    [SugarTable("bus_maintenanceplan2project", "保养计划和保养项目")]
    public class MaintenancePlan2ProjectEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 保养计划
        /// </summary>
        [SugarColumn(ColumnDescription = "保养计划")]
        public long MaintenancePlanId { get; set; } = 0;
        /// <summary>
        /// 保养项目
        /// </summary>
        [SugarColumn(ColumnDescription = "保养项目")]
        public long MaintenanceProjectId { get; set; } = 0;
    }
}