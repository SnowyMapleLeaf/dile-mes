﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.Equipment.Relation.SparePart
{
    /// <summary>
    /// 备件关联设备
    /// </summary>
    [SugarTable("bus_sparepart_equipment", "备件关联设备")]
    public class SparePartEquipmentEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 备件id
        /// </summary>
        public long SparePartId { get; set; } = 0;
        /// <summary>
        /// 设备id
        /// </summary>
        public long EquipmentId { get; set; } = 0;
        /// <summary>
        /// 设备部位
        /// </summary>
        public string? EquipmentPart { get; set; }
    }
}
