﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Relation.SparePart
{
    /// <summary>
    /// 出库关联备件
    /// </summary>
    [SugarTable("bus_stockout_sparepart", "出库关联备件")]
    public class StockOutSparePartEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 备件id
        /// </summary>
        public long SparePartId { get; set; } = 0;
        /// <summary>
        /// 出库id
        /// </summary>
        public long StockOutId { get; set; } = 0;
        /// <summary>
        /// 出库数量
        /// </summary>
        public int StockOutNumber { get; set; }
    }
}
