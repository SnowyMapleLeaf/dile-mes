﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Equipment.Relation.Repair
{
    /// <summary>
    /// 维修记录和备件
    /// </summary>
    [SugarTable("bus_repairrecord2sparepart", "维修记录和备件")]
    public class RepairRecord2SparePartEntity : AbstractBaseEntity
    {
        /// <summary>
        /// 维修记录
        /// </summary>
        public long RepairRecordId { get; set; }
        /// <summary>
        /// 备件
        /// </summary>
        public long SparePartId { get; set; }
        /// <summary>
        /// 领用数量
        /// </summary>
        public int RecipientNumber { get; set; }
    }

}
