﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.WorkOrder.Entity {
    /// <summary>
    /// 订单信息
    /// </summary>
    [SugarTable("wo_orderinfo", "订单信息")]
    public class OrderEntity : AbstractBaseEntity {
        /// <summary>
        /// 订单编号
        /// </summary>
        [SugarColumn(ColumnDescription = "订单编号")]
        public string OrderCode { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [SugarColumn(ColumnDescription = "产品名称")]
        public string ProductName { get; set; }
        /// <summary>
        /// 订单数量
        /// </summary>
        [SugarColumn(ColumnDescription = "订单数量")]
        public int OrderQuantity { get; set; }
        /// <summary>
        /// 工单数量
        /// </summary>
        [SugarColumn(ColumnDescription = "工单数量")]
        public int Quantity { get; set; }
    }
}
