﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.WorkOrder.Entity {
    /// <summary>
    /// 模具模次
    /// </summary>
    [SugarTable("wo_mouldnumber", "模具模次")]
    public class MouldNumberEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int MoldNumber { get; set; } = 0;
    }
}
