﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.WorkOrder.Entity {
    /// <summary>
    /// 模具更换
    /// </summary>
    [SugarTable("wo_mouldchange", "模具更换")]
    public class MouldChangeEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 工单编号
        /// </summary>
        [SugarColumn(ColumnDescription = "工单编号")]
        public string WorkOrderCode { get; set; }
        /// <summary>
        /// 原模具编号
        /// </summary>
        [SugarColumn(ColumnDescription = "原模具编号")]
        public string SourceMouldCode { get; set; }
        /// <summary>
        /// 更换后模具编号
        /// </summary>
        [SugarColumn(ColumnDescription = "更换后模具编号")]
        public string TargetMouldCode { get; set; }
        /// <summary>
        /// 模具模次
        /// </summary>
        [SugarColumn(ColumnDescription = "模具模次")]
        public int? MouldNumber { get; set; } = 0;
    }
}
