﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.WorkOrder.Entity {
    /// <summary>
    /// 报工记录
    /// </summary>
    [SugarTable("wo_reportworkrecord", "报工记录")]
    public class ReportWorkRecordEntity : DataEntityBase {
        /// <summary>
        /// 开工时间
        /// </summary>
        [SugarColumn(ColumnDescription = "开工时间")]
        public DateTime? ReportWorkTime { set; get; }
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 报工人员
        /// </summary>
        [SugarColumn(ColumnDescription = "报工人员")]
        public string ReportWorkUser { get; set; }
        /// <summary>
        /// 报工数量
        /// </summary>
        [SugarColumn(ColumnDescription = "报工数量")]
        public int ReportWorkQuantity { get; set; }
        /// <summary>
        /// 模具模次
        /// </summary>
        [SugarColumn(ColumnDescription = "模具模次")]
        public int MouldNumber { get; set; } = 0;
        /// <summary>
        /// 是否推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否推送")]
        public bool? IsPushed { set; get; } = false;
    }
}
