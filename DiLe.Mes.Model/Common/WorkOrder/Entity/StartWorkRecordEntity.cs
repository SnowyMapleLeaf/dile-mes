﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.WorkOrder.Entity {
    /// <summary>
    /// 开工记录
    /// </summary>
    [SugarTable("wo_startworkrecord", "开工记录")]
    public class StartWorkRecordEntity : DataEntityBase {
        /// <summary>
        /// 开工时间
        /// </summary>
        [SugarColumn(ColumnDescription = "开工时间")]
        public DateTime? StartWorkTime { set; get; }
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string EquipmentCode { get; set; }
        /// <summary>
        /// 工单编号
        /// </summary>
        [SugarColumn(ColumnDescription = "工单编号")]
        public string WorkOrderCode { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        [SugarColumn(ColumnDescription = "订单编号")]
        public string OrderCode { get; set; }
        /// <summary>
        /// 工单数量
        /// </summary>
        [SugarColumn(ColumnDescription = "工单数量")]
        public int WorkOrderQuantity { get; set; } = 0;
        /// <summary>
        /// 产品名称
        /// </summary>
        [SugarColumn(ColumnDescription = "产品名称")]
        public string ProductName { get; set; }

        /// <summary>
        /// 模具模次
        /// </summary>
        [SugarColumn(ColumnDescription = "模具模次")]
        public int MouldNumber { get; set; } = 0;
        /// <summary>
        /// 是否推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否推送")]
        public bool? IsPushed { set; get; } = false;

    }
}
