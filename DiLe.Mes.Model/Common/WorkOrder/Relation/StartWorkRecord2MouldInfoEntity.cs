﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.WorkOrder.Relation {
    /// <summary>
    /// 开工记录和模具
    /// </summary>
    [SugarTable("wo_startworkrecord2mouldinfo", "开工记录和模具")]
    public class StartWorkRecord2MouldInfoEntity : DataEntityBase {
        /// <summary>
        /// 模具主键
        /// </summary>
        [SugarColumn(ColumnDescription = "模具主键")]
        public long MouldId { get; set; }

        /// <summary>
        /// 开工记录主键
        /// </summary>
        [SugarColumn(ColumnDescription = "开工记录主键")]
        public long StartWorkRecordId { get; set; }
        /// <summary>
        /// 是否推送
        /// </summary>
        [SugarColumn(ColumnDescription = "是否推送")]
        public bool? IsPushed { set; get; } = false;

    }
}
