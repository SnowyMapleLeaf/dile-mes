﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.APP {
    /// <summary>
    /// 设备状态
    /// </summary>
    [SugarTable("app_equipmentstatusinfo", "设备状态")]
    public class EquipmentStatusInfoEntity : AbstractBaseEntity {
        /// <summary>
        /// 离线时间
        /// </summary>
        [SugarColumn(ColumnDescription = "离线时间")]
        public DateTime OfflineDateTime { set; get; }
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string DeviceNo { set; get; }

    }
}
