﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.APP {
    /// <summary>
    /// 设备运行时间
    /// </summary>
    [SugarTable("app_equipmentoperationinfo", "设备运行时间")]
    public class EquipmentOperationEntity : DataEntityBase {
        /// <summary>
        /// 运行日期
        /// </summary>
        [SugarColumn(ColumnDescription = "运行日期")]
        public DateTime OperationDateTime { set; get; }
        /// <summary>
        /// 设备编号
        /// </summary>
        [SugarColumn(ColumnDescription = "设备编号")]
        public string DeviceNo { set; get; }
        /// <summary>
        /// 设备启动时间
        /// </summary>
        [SugarColumn(ColumnDescription = "设备启动时间")]
        public DateTime StartTime { set; get; }
        /// <summary>
        /// 设备停止时间
        /// </summary>
        [SugarColumn(ColumnDescription = "设备停止时间")]
        public DateTime? EndTime { set; get; }
        /// <summary>
        /// 设备急停时间
        /// </summary>
        [SugarColumn(ColumnDescription = "设备急停时间")]
        public DateTime EStopTime { set; get; }
        /// <summary>
        /// 设备暂停时间
        /// </summary>
        [SugarColumn(ColumnDescription = "设备暂停时间")]
        public DateTime PauseTime { set; get; }

        /// <summary>
        /// 设备关闭
        /// </summary>
        [SugarColumn(ColumnDescription = "设备关闭")]
        public bool IsFinish { set; get; } = false;
    }
}
