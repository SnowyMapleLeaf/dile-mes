﻿using MapleLeaf.Core.Entity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common {
    /// <summary>
    /// 备件库存表
    /// </summary>
    [SugarTable("sparepartinventory", "备件库存表")]
    public class SparePartInventoryEntity : AbstractBaseEntity {
        /// <summary>
        /// 备件
        /// </summary>
        [SugarColumn(ColumnDescription = "备件")]
        public long SparePartId { get; set; } = 0;
        /// <summary>
        /// 仓库
        /// </summary>
        [SugarColumn(ColumnDescription = "仓库")]
        public long WarehouseId { get; set; } = 0;
        /// <summary>
        /// 库存数量
        /// </summary>
        [SugarColumn(ColumnDescription = "库存数量")]
        public int InventoryQuantity { get; set; }
    }
}
