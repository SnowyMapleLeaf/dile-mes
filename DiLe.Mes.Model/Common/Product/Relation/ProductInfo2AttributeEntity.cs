﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Product.Relation {
    /// <summary>
    /// 产品信息和产品属性
    /// </summary>
    [SugarTable("sys_productinfo2attribute", "产品信息和产品属性")]
    public class ProductInfo2AttributeEntity : AbstractBaseEntity {
        /// <summary>
        /// 产品信息
        /// </summary>
        [SugarColumn(ColumnDescription = "产品信息")]
        public long InfoId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 标准值
        /// </summary>
        [SugarColumn(ColumnDescription = "标准值")]
        public string StandardValue { get; set; }
        /// <summary>
        /// 上限
        /// </summary>
        [SugarColumn(ColumnDescription = "上限")]
        public string UpperLimit { get; set; }
        /// <summary>
        /// 下限
        /// </summary>
        [SugarColumn(ColumnDescription = "下限")]
        public string LowerLimit { get; set; }
    }
}