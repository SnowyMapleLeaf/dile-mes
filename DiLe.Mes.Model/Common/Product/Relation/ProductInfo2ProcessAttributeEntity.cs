﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Product.Relation {
    /// <summary>
    /// 产品信息和制程属性
    /// </summary>
    [SugarTable("sys_productinfo2processattribute", "产品信息和制程属性")]
    public class ProductInfo2ProcessAttributeEntity : AbstractBaseEntity {
        /// <summary>
        /// 产品信息
        /// </summary>
        [SugarColumn(ColumnDescription = "产品信息")]
        public long InfoId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        /// <summary>
        /// 属性名称
        /// </summary>
        [SugarColumn(ColumnDescription = "名称")]
        public string AttributeName { get; set; }
        /// <summary>
        /// 标准值
        /// </summary>
        [SugarColumn(ColumnDescription = "标准值")]
        public string StandardValue { get; set; }
    }
}