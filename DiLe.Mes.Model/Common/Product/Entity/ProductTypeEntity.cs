﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Product.Entity {
    /// <summary>
    /// 产品类别
    /// </summary>
    [SugarTable("sys_producttype", "产品类别")]
    public class ProductTypeEntity : AbstractBaseEntity {
        /// <summary>
        /// 产品类别编码
        /// </summary>
        [SugarColumn(ColumnDescription = "产品类别编码")]
        public string Code { get; set; }
        /// <summary>
        /// 产品类别名称
        /// </summary>
        [SugarColumn(ColumnDescription = "产品类别名称")]
        public string Name { get; set; }
        /// <summary>
        /// 产品类型说明
        /// </summary>
        [SugarColumn(ColumnDescription = "产品类型说明")]
        public string TypeDescription { get; set; }
    }
}