﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Product.Entity {
    /// <summary>
    /// 产品管理
    /// </summary>
    [SugarTable("sys_productinfo", "产品管理")]
    public class ProductInfoEntity : AbstractBaseEntity {
        /// <summary>
        /// 产品编码
        /// </summary>
        [SugarColumn(ColumnDescription = "产品编码")]
        public string Code { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [SugarColumn(ColumnDescription = "产品名称")]
        public string Name { get; set; }
        /// <summary>
        /// 产品类别
        /// </summary>
        [SugarColumn(ColumnDescription = "产品类别")]
        public long CategoryId { get; set; } = 0;
        /// <summary>
        /// 产品类型
        /// </summary>
        [SugarColumn(ColumnDescription = "产品类型")]
        public string TypeName { get; set; }
        /// <summary>
        /// 产品单位
        /// </summary>
        [SugarColumn(ColumnDescription = "产品单位")]
        public long UnitId { get; set; } = 0;
        /// <summary>
        /// 产品型号
        /// </summary>
        [SugarColumn(ColumnDescription = "产品型号")]
        public string Model { get; set; }
    }
}