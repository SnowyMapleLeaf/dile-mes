﻿using MapleLeaf.Core.Entity;
using Serilog;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiLe.Mes.Model.Common.Abnormal.Entity {
    /// <summary>
    /// 异常报警规则
    /// </summary>
    [SugarTable("abn_abnormalalarmrule", "异常报警规则")]
    public class AbnormalAlarmRuleEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备类型
        /// </summary>
        public long EquipmentTypeId { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string AlarmDescription { get; set; }
        /// <summary>
        /// 报警级别
        /// </summary>
        public int AlarmLevel { get; set; }
        /// <summary>
        /// 报警名称
        /// </summary>
        public string AlarmName { get; set; }
        /// <summary>
        /// 位置
        /// </summary>
        public string? Position { get; set; }

    }
}
