﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Abnormal.Entity {
    /// <summary>
    /// 产品品质异常
    /// </summary>
    [SugarTable("abn_productionqualityabnormal", "产品品质异常")]
    public class ProductionQualityAbnormalEntity : AbstractBaseEntity {
        /// <summary>
        /// 产品编号
        /// </summary>
        [SugarColumn(ColumnDescription = "产品编号")]
        public string Code { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [SugarColumn(ColumnDescription = "产品名称")]
        public string Name { get; set; }
        /// <summary>
        /// 制程
        /// </summary>
        [SugarColumn(ColumnDescription = "制程")]
        public long ProcessId { get; set; } = 0;
        /// <summary>
        /// 工单
        /// </summary>
        [SugarColumn(ColumnDescription = "工单")]
        public long WorkOrderId { get; set; } = 0;
        /// <summary>
        /// 设备
        /// </summary>
        [SugarColumn(ColumnDescription = "设备")]
        public long EquipmentId { get; set; } = 0;

        /// <summary>
        /// 异常日期
        /// </summary>
        [SugarColumn(ColumnDescription = "异常日期")]
        public string AbnormalDate { get; set; }
        /// <summary>
        /// 异常时间
        /// </summary>
        [SugarColumn(ColumnDescription = "异常时间")]
        public string AbnormalTime { get; set; }
        /// <summary>
        /// 异常内容
        /// </summary>
        [SugarColumn(ColumnDescription = "异常内容")]
        public string AbnormalInfo { get; set; }
        /// <summary>
        /// 产品良率
        /// </summary>
        [SugarColumn(ColumnDescription = "产品良率")]
        public string ProductionYield { get; set; }

        /// <summary>
        /// 操作员
        /// </summary>
        [SugarColumn(ColumnDescription = "操作员")]
        public string Operator { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        [SugarColumn(ColumnDescription = "操作时间")]
        public string OperatingTime { get; set; }
        /// <summary>
        /// 操作日期
        /// </summary>
        [SugarColumn(ColumnDescription = "操作日期")]
        public string OperatingDate { get; set; }
    }
}
