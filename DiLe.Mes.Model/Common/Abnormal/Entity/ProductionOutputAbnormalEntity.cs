﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Abnormal.Entity {
    /// <summary>
    /// 产出量异常
    /// </summary>
    [SugarTable("abn_productionoutputabnormal", "产出量异常")]
    public class ProductionOutputAbnormalEntity : AbstractBaseEntity {
        /// <summary>
        /// 设备
        /// </summary>
        [SugarColumn(ColumnDescription = "设备")]
        public long EquipmentInfoId { get; set; } = 0;
        /// <summary>
        /// 异常日期
        /// </summary>
        [SugarColumn(ColumnDescription = "异常日期")]
        public string AbnormalDate { get; set; }
        /// <summary>
        /// 异常时间
        /// </summary>
        [SugarColumn(ColumnDescription = "异常时间")]
        public string AbnormalTime { get; set; }
        /// <summary>
        /// 标准产出量
        /// </summary>
        [SugarColumn(ColumnDescription = "标准产出量")]
        public string StandardProductionOutput { get; set; }
        /// <summary>
        /// 实际产出量
        /// </summary>
        [SugarColumn(ColumnDescription = "实际产出量")]
        public string ActualProductionOutput { get; set; }
        /// <summary>
        /// 异常级别
        /// </summary>
        [SugarColumn(ColumnDescription = "异常级别")]
        public string AbnormalLevel { get; set; }
        /// <summary>
        /// 提醒内容
        /// </summary>
        [SugarColumn(ColumnDescription = "提醒内容")]
        public string RemindInfo { get; set; }
    }
}
