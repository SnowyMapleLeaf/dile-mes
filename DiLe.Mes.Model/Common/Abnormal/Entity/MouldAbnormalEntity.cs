﻿using MapleLeaf.Core.Entity;
using SqlSugar;

namespace DiLe.Mes.Model.Common.Abnormal.Entity {
    /// <summary>
    /// 模具异常
    /// </summary>
    [SugarTable("abn_mouldabnormal", "模具异常")]
    public class MouldAbnormalEntity : AbstractBaseEntity {
        /// <summary>
        /// 模具
        /// </summary>
        [SugarColumn(ColumnDescription = "模具")]
        public long MouldInfoId { get; set; } =0;
      
        /// <summary>
        /// 异常日期
        /// </summary>
        [SugarColumn(ColumnDescription = "异常日期")]
        public string AbnormalDate { get; set; }
        /// <summary>
        /// 异常时间
        /// </summary>
        [SugarColumn(ColumnDescription = "异常时间")]
        public string AbnormalTime { get; set; }
        /// <summary>
        /// 异常类别
        /// </summary>
        [SugarColumn(ColumnDescription = "异常类别")]
        public string AbnormalCategory { get; set; }
        /// <summary>
        /// 异常内容
        /// </summary>
        [SugarColumn(ColumnDescription = "异常内容")]
        public string AbnormalInfo { get; set; }
        /// <summary>
        /// 异常级别
        /// </summary>
        [SugarColumn(ColumnDescription = "异常级别")]
        public string AbnormalLevel { get; set; }
        /// <summary>
        /// 提醒内容
        /// </summary>
        [SugarColumn(ColumnDescription = "提醒内容")]
        public string RemindInfo { get; set; }
    }
}
